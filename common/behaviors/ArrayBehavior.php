<?php

/**
 * Created by PhpStorm.
 * User: User
 * Date: 010 10.08.17
 * Time: 16:46
 */

namespace common\behaviors;

use yii\base\Behavior;
use yii\db\ActiveRecord;

class ArrayBehavior extends Behavior
{
    /** @var array  */
    public $attributes = [];

    /**
     * @return array
     */
    public function events()
    {
        return [
            ActiveRecord::EVENT_BEFORE_INSERT   => 'convertToJson',
            ActiveRecord::EVENT_BEFORE_UPDATE   => 'convertToJson',
            ActiveRecord::EVENT_AFTER_FIND      => 'convertToArray',
            ActiveRecord::EVENT_AFTER_UPDATE    => 'convertToArray',
            ActiveRecord::EVENT_AFTER_INSERT    => 'convertToArray',
            ActiveRecord::EVENT_INIT            => 'convertToArray',
        ];
    }

    /**
     * Convert to Json
     * @param $event
     */
    public function convertToJson($event)
    {
        if ($this->attributes && is_array($this->attributes)) {
            foreach ($this->attributes as $attribute) {
                if (isset($this->owner->{$attribute}) && $this->owner->{$attribute} &&
                    (is_array($this->owner->{$attribute}) || is_object($this->owner->{$attribute}))) {
                    $this->owner->{$attribute} = json_encode($this->owner->{$attribute});
                }
            }
        }
    }

    /**
     * Convert to array
     * @param $event
     */
    public function convertToArray($event)
    {
        if ($this->attributes && is_array($this->attributes)) {
            foreach ($this->attributes as $attribute) {
                if (isset($this->owner->{$attribute}) && $this->owner->{$attribute} && is_string($this->owner->{$attribute})) {
                    $this->owner->{$attribute} = json_decode($this->owner->{$attribute});
                }
            }
        }
    }
}