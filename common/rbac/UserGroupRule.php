<?php
/**
 * Created by PhpStorm.
 * User: Vlad
 * Date: 06.04.2017
 * Time: 18:08
 */

namespace common\rbac;

use Yii;
use yii\rbac\Rule;

/**
 * Checks if user group matches
 */
class UserGroupRule extends Rule
{
    public $name = 'userGroup';

    public function execute($user, $item, $params)
    {
        if (!Yii::$app->user->isGuest) {
            $priority = Yii::$app->user->identity->role->priority;
            $is_admin = Yii::$app->user->identity->role->is_admin;

            if ($is_admin) {
                switch ($item->name) {
                    case 'admin':
                        return $priority == 1;
                        break;
                    default:
                        return false;
                        break;
                }
            } else {
                switch ($item->name) {
                    case 'agent':
                        return $priority == 1;
                        break;
                    default:
                        return false;
                        break;
                }
            }
        }

        return false;
    }
}
