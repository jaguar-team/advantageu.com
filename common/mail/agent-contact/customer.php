Hi <?= $agent_contact_model->full_name; ?>,
<br>
Welcome to HomeLight! You're on your way to finding the perfect real estate agent. We analyze over 18 million transactions to find the right agent for you. Find out more about how it works.
<br>
We saw that you reached out to Dsfds Fsdf. We have relayed your message and Dsfds should be reaching out to you soon. If the agent is unavailable, a HomeLight concierge will reach out to you to see if we can help.
<br>
<br>
Still have questions?
<br>
Call us at 888-998-1909 or reply to this email, and we’ll be happy to help. We are open Monday to Saturday 6 a.m.-6 p.m. and Sunday 7:30 a.m.-5 p.m., all Pacific Time.
<br>
Thanks and we look forward to helping you!
<br>
The HomeLight Team