<?php
return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
    	//other ext.
		'googlePlaces' => [
			'class' => '\dosamigos\google\places\Places',
			'key' => 'AIzaSyDbgI-qDa-U_JWtQbuqj7kOdBK4vYBCJQ8',
			'format' => 'json' // or 'xml'
		],
		'googlePlacesSearch' => [
			'class' => '\dosamigos\google\places\Search',
			'key' => 'AIzaSyDbgI-qDa-U_JWtQbuqj7kOdBK4vYBCJQ8',
			'format' => 'json' // or 'xml'
		],
	    'blog' => [
		    'class' => '\monitorbacklinks\yii2wp\Wordpress',
		    'endpoint' => 'https://resources.advantageu.com/xmlrpc.php',
		    'username' => 'AdvantageU',
		    'password' => '!FwTs1P(cVR!%Q9s@MvDIlEb'
	    ],
		'jsUrlManager' => [
			'class' => \dmirogin\js\urlmanager\JsUrlManager::class,
		],
	    //default
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
            'defaultRoles' => ['admin', 'agent'],
        ],
	    //custom
        'uploadPhoto' => [
            'class' => 'common\components\UploadPhoto',
        ],
        'setting' => [
            'class' => 'common\models\Setting',
        ],
        'userIdentity' => [
	        'class' => 'common\models\User',
        ],
	    'userProfile' => [
		    'class' => 'common\models\UserProfile',
	    ],
	    'userRole' => [
		    'class' => 'common\models\UserRole',
	    ],
        'loginForm' => [
	        'class' => 'common\models\LoginForm',
        ],
	    'googleAuth' => [
		    'class' => 'common\components\GoogleAuth',
	    ],
        'resultMatches' => [
            'class' => 'frontend\components\ResultMatches',
        ],
    ],
];
