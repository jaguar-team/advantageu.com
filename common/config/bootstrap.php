<?php

Yii::setAlias('@common', dirname(__DIR__));
Yii::setAlias('@frontend', dirname(dirname(__DIR__)) . '/frontend');
Yii::setAlias('@backend', dirname(dirname(__DIR__)) . '/backend');
Yii::setAlias('@console', dirname(dirname(__DIR__)) . '/console');

/** dir img */
Yii::setAlias('@dir_img', dirname(dirname(__DIR__)). '/img');
Yii::setAlias('@dir_img_upload', dirname(dirname(__DIR__)). '/img/upload');
Yii::setAlias('@dir_img_user', dirname(dirname(__DIR__)). '/img/user');
Yii::setAlias('@dir_user_avatar', dirname(dirname(__DIR__)). '/img/user/avatar');

/** uri img */
Yii::setAlias('@uri_img', '/img');
Yii::setAlias('@uri_img_upload', '/img/upload');
Yii::setAlias('@uri_img_user', '/img/user');
Yii::setAlias('@uri_user_avatar', '/img/user/avatar');
