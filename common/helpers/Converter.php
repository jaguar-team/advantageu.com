<?php

/**
 * Created by PhpStorm.
 * User: User
 * Date: 031 31.08.17
 * Time: 18:28
 */

namespace common\helpers;

class Converter
{
    /**
     * Convert string zip codes to array or human string
     * Example: Convert user BIO zip codes fields to zip codes
     * @param $zip_codes
     * @param bool $human_string
     * @return bool|string
     */
    public static function stringToZipCodesArray($zip_codes, $human_string = false)
    {
        if (is_string($zip_codes)) {
            $zip_codes = explode("\n", $zip_codes);
            $zip_codes = implode(',', $zip_codes);
            $zip_codes = explode(',', $zip_codes);
            $zip_codes = array_map('trim', $zip_codes);

            if ($zip_codes) {
                foreach ($zip_codes as $number_zip => $zip_code) {
                    if (!preg_match('/^[0-9]{5}$/i', $zip_code)) {
                        unset($zip_codes[$number_zip]);
                    }
                }
            } else {
                return false;
            }

            return $human_string ? implode(', ', $zip_codes) : $human_string;
        }
    }
}