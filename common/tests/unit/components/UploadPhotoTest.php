<?php
/**
 * Created by PhpStorm.
 * User: Vlad
 * Date: 18.04.2017
 * Time: 10:26
 */

namespace common\tests\unit\components;

use Yii;
use common\models\UserProfile;
use common\components\UploadPhoto;

class UploadPhotoTest extends \Codeception\Test\Unit
{
    /**
     * @var \common\tests\UnitTester
     */
    protected $tester;


    public function _before()
    {
    }

    public function testUploadPhoto()
    {
        $user_profile = \Yii::createObject([
            'class' => UserProfile::className(),
            'photo' => [
                'name'      => 'user_avatar.png',
                'type'      => 'jpg',
                'size'      => '4543',
                'tmp_name'  => 'user_avatar.png',
            ],
        ]);

        $component = \Yii::createObject([
            'class'             => UploadPhoto::className(),
            '_object'           => $user_profile,
            'property_name'     => 'photo',
        ]);

        $component->userAvatar();
    }
}