<?php

namespace common\tests\unit\models;

use Yii;
use common\models\LoginForm;
use common\fixtures\UserFixture as UserFixture;
use common\fixtures\UserRoleFixture as UserRoleFixture;

/**
 * Login form test
 */
class LoginFormTest extends \Codeception\Test\Unit
{
    /**
     * @var \common\tests\UnitTester
     */
    protected $tester;


    public function _before()
    {
        $this->tester->haveFixtures([
            'user_role' => [
                'class' => UserRoleFixture::className(),
                'dataFile' => codecept_data_dir() . 'user_role.php'
            ],
            'user' => [
                'class' => UserFixture::className(),
                'dataFile' => codecept_data_dir() . 'user.php'
            ],
        ]);
    }

    public function testLoginNoUser()
    {
        $model = new LoginForm([
            'email' => 'not_existing_email',
            'password' => 'not_existing_password',
        ]);

        expect('model should not login user', $model->login())->false();
        expect('user should not be logged in', Yii::$app->user->isGuest)->true();
    }

    public function testLoginWrongPassword()
    {
        $model = new LoginForm([
            'email' => 'admin@admin.com',
            'password' => 'wrong_password',
        ]);

        expect('model should not login user', $model->login())->false();
        expect('error message should be set', $model->errors)->hasKey('password');
        expect('user should not be logged in', Yii::$app->user->isGuest)->true();
    }

    public function testLoginCorrect()
    {
        $model = new LoginForm([
            'email' => 'no_admin@admin.com',
            'password' => 'password_0',
        ]);

        expect('model should login user', $model->login())->true();
        expect('model shouldn\'t login user to admin panel', $model->login(true))->false();
        expect('error message should not be set', $model->errors)->hasntKey('password');
        expect('user should be logged in', Yii::$app->user->isGuest)->false();
    }

    public function testLoginAdminCorrect()
    {
        $model = new LoginForm([
            'email' => 'admin@admin.com',
            'password' => 'password_0',
        ]);

        expect('model should login user to admin panel', $model->login(true))->true();
        expect('model shouldn\'t login user', $model->login())->false();
        expect('error message should not be set', $model->errors)->hasntKey('password');
        expect('user should be logged in', Yii::$app->user->isGuest)->false();
    }

    public function testLoginWhenActive()
    {
        $model = new LoginForm([
            'email' => 'no_admin@admin.com',
            'password' => 'password_0',
        ]);

        expect('model should login user', $model->login())->true();
        expect('model shouldn\'t login user to admin panel', $model->login(true))->false();
    }

    public function testLoginWhenNoActive()
    {
        $model = new LoginForm([
            'email' => 'no_admin_no_active@admin.com',
            'password' => 'password_0',
        ]);

        expect('model should no login user', $model->login())->false();
        expect('model shouldn\'t login user to admin panel', $model->login(true))->false();
    }

    public function testLoginAdminWhenActive()
    {
        $model = new LoginForm([
            'email' => 'admin@admin.com',
            'password' => 'password_0',
        ]);

        expect('model should login user to admin panel', $model->login(true))->true();
        expect('model shouldn\'t login user', $model->login())->false();
    }

    public function testLoginAdminWhenNoActive()
    {
        $model = new LoginForm([
            'email' => 'admin_no_active@admin.com',
            'password' => 'password_0',
        ]);

        expect('model shouldn\'t login user to admin panel', $model->login(true))->false();
        expect('model shouldn\'t login user', $model->login())->false();
    }
}
