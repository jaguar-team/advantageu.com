<?php
/**
 * Created by PhpStorm.
 * User: Vlad
 * Date: 18.04.2017
 * Time: 15:49
 */

namespace common\tests\unit\models;

use Yii;
use common\models\Setting;
use common\fixtures\SettingFixture;

class SettingTest extends \Codeception\Test\Unit
{
    /**
     * @var \common\tests\UnitTester
     */
    protected $tester;

    protected $_setting;

    public function _before()
    {
        $this->tester->haveFixtures([
            'user_role' => [
                'class' => SettingFixture::className(),
                'dataFile' => codecept_data_dir() . 'setting.php'
            ],
        ]);
    }

    public function testCorrectInsert()
    {
        $this->_setting = \Yii::createObject([
            'class' => Setting::className(),
            'name'  => 'not_isset_name',
            'value' => 'not_isset_value',
        ]);

        expect('should be true', $this->_setting->save())->true();
    }

    public function testCorrectUpdate()
    {
        $this->_setting = \Yii::createObject([
            'class' => Setting::className(),
            'name'  => 'setting_test_1',
            'value' => 'new value',
        ]);

        expect('should be 1', $this->_setting->save())->equals(1);
        expect('should be new value', $this->_setting->value)->equals('new value');
    }

    public function testGetValueByName()
    {
        $this->_setting = \Yii::createObject(['class' => Setting::className()]);

        expect('Should be setting_test_value_1', $this->_setting->getValueByName('setting_test_1'))->equals('setting_test_value_1');
        expect('Should be false', $this->_setting->getValueByName('not_isset'))->false();
    }
}