<?php
/**
 * Created by PhpStorm.
 * User: Vlad
 * Date: 14.04.2017
 * Time: 16:44
 */

namespace common\tests\unit\models;

use Yii;
use yii\helpers\Url;
use common\fixtures\UserFixture as UserFixture;
use common\fixtures\UserRoleFixture as UserRoleFixture;
use common\models\User;
use common\models\UserProfile;

class UserProfileTest extends \Codeception\Test\Unit
{
    /**
     * @var \common\tests\UnitTester
     */
    protected $tester;

    protected $_user_profile;

    public function _before()
    {
        $this->tester->haveFixtures([
            'user_role' => [
                'class' => UserRoleFixture::className(),
                'dataFile' => codecept_data_dir() . 'user_role.php'
            ],
            'user' => [
                'class' => UserFixture::className(),
                'dataFile' => codecept_data_dir() . 'user.php'
            ]
        ]);

        $this->_user_profile = new UserProfile();
    }

    public function testCorrectAddProfile()
    {
        $this->_user_profile->setAttributes([
            'id_user'           => 1,
            'company'           => 'Test company',
            'birth_year'        => '1995-02-02',
            'license_issued'    => '2017-03-03',
            'website_url'       => 'site.com',
            'blog_url'          => 'http://blog.com',
        ]);

        expect('Should be true', $this->_user_profile->save())->true();
        expect('Should be empty', $this->_user_profile->errors)->isEmpty();
        expect('Should be 1', preg_match('/^http:\/\/.*$/', $this->_user_profile->blog_url))->equals(1);
        expect('Should be 1', preg_match('/^http:\/\/.*$/', $this->_user_profile->website_url))->equals(1);
    }

    public function testGetPhotoUrl()
    {
        $this->_user_profile->photo = 'user_profile_photo.png';

        expect('Should be equals', $this->_user_profile->getPhotoUrl())->equals(Url::to('@uri_user_avatar/user_profile_photo.png', true));
    }

    public function testGetDefaultPhotoUrl()
    {
        $profile = $this->_user_profile;
        expect('Should be equals', $this->_user_profile->getPhotoUrl())->equals(Url::to('@uri_img/'.$profile::DEFAULT_PHOTO, true));
    }

    public function testGetFullName()
    {
        $this->_user_profile->first_name = 'First';
        $this->_user_profile->last_name = 'Last';
        expect('Should be equals', $this->_user_profile->getFullName())->equals('First Last');

        $this->_user_profile->first_name = 'First';
        $this->_user_profile->last_name = 'Last';
        expect('Should be equals', $this->_user_profile->getFullName(true))->equals('First L.');

        $this->_user_profile->first_name = 'First';
        $this->_user_profile->last_name = '';
        expect('Should be equals', $this->_user_profile->getFullName())->equals('First');

        $this->_user_profile->first_name = '';
        $this->_user_profile->last_name = 'Last';
        expect('Should be equals', $this->_user_profile->getFullName())->equals('Last');
    }
}