<?php
/**
 * Created by PhpStorm.
 * User: Vlad
 * Date: 11.04.2017
 * Time: 11:26
 */

namespace common\tests\unit\models;


use Yii;
use common\fixtures\UserFixture as UserFixture;
use common\fixtures\UserRoleFixture as UserRoleFixture;
use common\models\User;

/**
 * User test
 */
class UserTest extends \Codeception\Test\Unit
{
    /**
     * @var \common\tests\UnitTester
     */
    protected $tester;

    protected $_user;


    public function _before()
    {
        $this->tester->haveFixtures([
            'user_role' => [
                'class' => UserRoleFixture::className(),
                'dataFile' => codecept_data_dir() . 'user_role.php'
            ],
            'user' => [
                'class' => UserFixture::className(),
                'dataFile' => codecept_data_dir() . 'user.php'
            ]
        ]);
    }
    
    public function testCorrectRegisterUser()
    {
        $this->_user = new User([
            'id_role'   => 1,
            'email'     => 'add_user@add.com',
            'password'  => 'qwerty',
        ]);

        //expect('User should be added', $this->_user->register())->true();
        //expect('Profile should be added', $this->_user->profile)->notNull();
    }

    public function testGetUserByExistsEmail()
    {
        $this->_user = User::findOne(['email' => 'admin@admin.com']);

        expect('user shouldn\'t be null', $this->_user)->notNull();
        expect('model should be User object', $this->_user)->isInstanceOf(User::class);
    }

    public function testGetUserByNotExistsEmail()
    {
        $this->_user = User::findOne(['email' => 'not_exists_email@not_exists.com']);

        expect('user should be null', $this->_user)->null();
        expect('model shouldn\'t be User object', $this->_user)->isNotInstanceOf(User::class);
    }

    public function testUserIsActive()
    {
        $this->_user = User::findOne(['email' => 'admin@admin.com']);

        expect('user should be active', $this->_user->isActive())->true();
    }

    public function testUserNoActive()
    {
        $this->_user = User::findOne(['email' => 'admin_no_active@admin.com']);

        expect('user should be no active', $this->_user->isActive())->false();
    }

    public function testUserIsAdmin()
    {
        $this->_user = User::findOne(['email' => 'admin@admin.com']);

        expect('user should be admin', $this->_user->isAdmin())->true();
    }

    public function testUserIsNoAdmin()
    {
        $this->_user = User::findOne(['email' => 'no_admin@admin.com']);

        expect('user should be admin', $this->_user->isAdmin())->false();
    }
}