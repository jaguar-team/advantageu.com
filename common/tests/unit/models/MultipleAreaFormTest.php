<?php
/**
 * Created by PhpStorm.
 * User: Vlad
 * Date: 24.04.2017
 * Time: 14:25
 */

namespace common\tests\unit\models;

use Yii;
use common\models\MultipleAreaForm;

class MultipleAreaFormTest extends \Codeception\Test\Unit
{
    public $_area_form;

    public function testCorrectValidate()
    {
        $this->_area_form = \Yii::createObject([
            'class'     => MultipleAreaForm::className(),
            'areas'     => '[
            {"state" : "State 1", "short_name" : "UA", "city" : "City 1", "region" : "Region 1"},
            {"state" : "State 2", "short_name" : "UA", "city" : "City 2", "region" : "Region 2"},
            {"state" : "State 3", "short_name" : "UA", "city" : "City 3", "region" : "Region 3"},
            {"state" : "State 4", "short_name" : "UA", "city" : "City 4", "region" : "Region 4"}
            ]',
        ]);

        expect('Validate should be true', $this->_area_form->validate())->true();
        expect('Areas property should not be empty', is_array($this->_area_form->areas))->notEmpty();
        expect('Areas property should be array', is_array($this->_area_form->areas))->true();
        expect('Count areas should be 4', count($this->_area_form->areas))->equals(4);
    }

    public function testWrongValidate()
    {
        $this->_area_form = \Yii::createObject([
            'class'     => MultipleAreaForm::className(),
            'areas'     => '[
                {"state" : "", "short_name" : "1234567891011", "region" : "Region"}
            ]'
        ]);

        expect('Validate should be false', $this->_area_form->validate())->false();
        expect('Error message should not be empty',$this->_area_form->errors)->notEmpty();
        expect('State error message should be exists', $this->_area_form->errors)->hasKey('state');
        expect('Short name error message should be exists', $this->_area_form->errors)->hasKey('short_name');
        expect('City error message should be exists', $this->_area_form->errors)->hasKey('city');
    }
}