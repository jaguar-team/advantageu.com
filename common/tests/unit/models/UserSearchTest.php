<?php
/**
 * Created by PhpStorm.
 * User: Vlad
 * Date: 19.04.2017
 * Time: 11:22
 */

namespace common\tests\unit\models;

use common\models\UserProfile;
use Yii;
use common\models\UserSearch;
use common\fixtures\UserFixture as UserFixture;
use common\fixtures\UserProfileFixture as UserProfileFixture;
use common\fixtures\UserRoleFixture as UserRoleFixture;

class UserSearchTest extends \Codeception\Test\Unit
{
    /**
     * @var \common\tests\UnitTester
     */
    protected $tester;

    public function _before()
    {
        $this->tester->haveFixtures([
            'user_role' => [
                'class' => UserRoleFixture::className(),
                'dataFile' => codecept_data_dir() . 'user_role.php'
            ],
            'user' => [
                'class' => UserFixture::className(),
                'dataFile' => codecept_data_dir() . 'user.php'
            ],
            'user_profile' => [
                'class' => UserProfileFixture::className(),
                'dataFile' => codecept_data_dir() . 'user_profile.php'
            ],
        ]);
    }
    
    public function testSearch()
    {
        $user_search = \Yii::createObject(['class' => UserSearch::className()]);

        $data_provider_admin = $user_search->search([], true);
        $data_provider_agent = $user_search->search([]);
        
        expect('Should be 2', $data_provider_admin->getTotalCount())->equals(2);
        expect('Should be 2', $data_provider_agent->getTotalCount())->equals(2);
    }

    public function testSearchWithEmail()
    {
        $user_search = \Yii::createObject(['class' => UserSearch::className()]);

        $params = [
            //admin
            1 => ['UserSearch' => ['email' => 'admin@admin.com']],
            2 => ['UserSearch' => ['email' => 'admin']],
            3 => ['UserSearch' => ['email' => 'not_isset_email@gmail.com']],
            // agent
            4 => ['UserSearch' => ['email' => 'no_admin@admin.com']],
            5 => ['UserSearch' => ['email' => 'no_admin']],
            6 => ['UserSearch' => ['email' => 'not_isset_agent_email@gmail.com']],
        ];

        expect('Should be 1', $user_search->search($params[1], true)->getTotalCount())->equals(1);
        expect('Should be 2', $user_search->search($params[2], true)->getTotalCount())->equals(2);
        expect('Should be 0', $user_search->search($params[3], true)->getTotalCount())->equals(0);

        expect('Should be 1', $user_search->search($params[4])->getTotalCount())->equals(1);
        expect('Should be 2', $user_search->search($params[5])->getTotalCount())->equals(2);
        expect('Should be 0', $user_search->search($params[6])->getTotalCount())->equals(0);
    }

    public function testSearchWithRole()
    {
        $user_search = \Yii::createObject(['class' => UserSearch::className()]);

        $params = [
            //admin
            1 => ['UserSearch' => ['id_role' => 1]],
            2 => ['UserSearch' => ['id_role' => 111]],
            // agent
            3 => ['UserSearch' => ['id_role' => 2]],
            4 => ['UserSearch' => ['id_role' => 111]],
        ];

        expect('Should be 2', $user_search->search($params[1], true)->getTotalCount())->equals(2);
        expect('Should be 2', $user_search->search($params[2], true)->getTotalCount())->equals(2);


        expect('Should be 2', $user_search->search($params[3])->getTotalCount())->equals(2);
        expect('Should be 2', $user_search->search($params[4])->getTotalCount())->equals(2);
    }
}