<?php
/**
 * Created by PhpStorm.
 * User: Vlad
 * Date: 18.04.2017
 * Time: 15:54
 */

return [
    [
        'id'            => 1,
        'name'          => 'setting_test_1',
        'value'         => 'setting_test_value_1',
        'created_at'    => '1402312317',
        'updated_at'    => '1402312317',
    ],
    [
        'id'            => 2,
        'name'          => 'setting_test_2',
        'value'         => 'setting_test_value_2',
        'created_at'    => '1402312318',
        'updated_at'    => '1402312318',
    ],
];