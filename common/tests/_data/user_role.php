<?php

return [
    [
        'id'        => '1',
        'priority'  => '1',
        'is_admin'  => '1',
        'name'      => 'Administrator',
    ],
    [
        'id'        => '2',
        'priority'  => '1',
        'is_admin'  => '0',
        'name'      => 'Real Estate Agent',
    ],
];
