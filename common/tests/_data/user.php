<?php

return [
    [
        'id' => 1,
        'id_role' => '1',
        'email' => 'admin@admin.com',
        'auth_key' => 'HP187Mvq7Mmm3CTU80dLkGmni_FUH_lR',
        //password_0
        'password_hash' => '$2y$13$EjaPFBnZOQsHdGuHI.xvhuDp1fHpo8hKRSk6yshqa9c5EG8s3C3lO',
        'password_reset_token' => 'ExzkCOaYc1L8IOBs4wdTGGbgNiG3Wz1I_1402312317',
        'status' => 1,
        'created_at' => '1402312317',
        'updated_at' => '1402312317',
    ],
    [
        'id' => 2,
        'id_role' => '2',
        'email' => 'no_admin@admin.com',
        'auth_key' => 'HP187Mvq7Mmm3CTU80dLkGmni_FUH_lJ',
        //password_0
        'password_hash' => '$2y$13$EjaPFBnZOQsHdGuHI.xvhuDp1fHpo8hKRSk6yshqa9c5EG8s3C3lO',
        'password_reset_token' => 'ExzkCOaYc1L8IOBs4wdTGGbgNiG3Wz1I_1402312315',
        'status' => 1,
        'created_at' => '1402312317',
        'updated_at' => '1402312317',
    ],
    [
        'id' => 3,
        'id_role' => '1',
        'email' => 'admin_no_active@admin.com',
        'auth_key' => 'HP187Mvq7Mmm3CTU80dLkGmni_FUH_lK',
        //password_0
        'password_hash' => '$2y$13$EjaPFBnZOQsHdGuHI.xvhuDp1fHpo8hKRSk6yshqa9c5EG8s3C3lO',
        'password_reset_token' => 'ExzkCOaYc1L8IOBs4wdTGGbgNiG3Wz1I_1402312318',
        'status' => 0,
        'created_at' => '1402312317',
        'updated_at' => '1402312317',
    ],
    [
        'id' => 4,
        'id_role' => '2',
        'email' => 'no_admin_no_active@admin.com',
        'auth_key' => 'HP187Mvq7Mmm3CTU80dLkGmni_FUH_lB',
        //password_0
        'password_hash' => '$2y$13$EjaPFBnZOQsHdGuHI.xvhuDp1fHpo8hKRSk6yshqa9c5EG8s3C3lO',
        'password_reset_token' => 'ExzkCOaYc1L8IOBs4wdTGGbgNiG3Wz1I_1402312310',
        'status' => 0,
        'created_at' => '1402312317',
        'updated_at' => '1402312317',
    ],
];
