<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 027 27.04.17
 * Time: 15:39
 */

namespace common\fixtures;

use yii\test\ActiveFixture;

class AreaFixture extends ActiveFixture
{
    public $modelClass = 'common\models\Area';
}