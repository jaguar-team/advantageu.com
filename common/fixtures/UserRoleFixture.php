<?php
/**
 * Created by PhpStorm.
 * User: Vlad
 * Date: 11.04.2017
 * Time: 11:54
 */

namespace common\fixtures;

use yii\test\ActiveFixture;

class UserRoleFixture extends ActiveFixture
{
    public $modelClass = 'common\models\UserRole';
}