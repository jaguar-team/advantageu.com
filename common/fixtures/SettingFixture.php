<?php
/**
 * Created by PhpStorm.
 * User: Vlad
 * Date: 18.04.2017
 * Time: 15:57
 */

namespace common\fixtures;

use yii\test\ActiveFixture;

class SettingFixture extends ActiveFixture
{
    public $modelClass = 'common\models\Setting';
}