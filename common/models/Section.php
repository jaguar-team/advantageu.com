<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "{{%section}}".
 *
 * @property int $id
 * @property int $id_role
 * @property string $name
 * @property string $color
 * @property integer $position
 * @property string $created_at
 * @property string $updated_at
 *
 * @property UserRole $role
 * @property SectionValue $sectionValue
 */
class Section extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%section}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // id role rules
            ['id_role', 'required'],
            ['id_role', 'integer'],
            [['id_role'], 'exist', 'skipOnError' => true, 'targetClass' => UserRole::className(), 'targetAttribute' => ['id_role' => 'id']],

            // name rules
            ['name', 'required'],
            ['name', 'string', 'max' => 255],

            // position rules
            ['position', 'integer'],

            // color rules
            ['color', 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_role' => 'Id Role',
            'name' => 'Name',
            'color' => 'Color',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRole()
    {
        return $this->hasOne(UserRole::className(), ['id' => 'id_role']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSectionValues()
    {
        return $this->hasMany(SectionValue::className(), ['id_section' => 'id']);
    }

    /**
     * @param $id_role
     * @return array|\yii\db\ActiveRecord[]
     */
    public function getByIdRole($id_role)
    {
        return static::find()->where(['id_role' => $id_role])->indexBy('id')->asArray()->all();
    }

    /**
     * @return array
     */
    public function getDropdown()
    {
        return static::find()->select(['name'])->indexBy('id')->column();
    }

    /**
     * @param $id_role
     * @return array
     */
    public function getDropdownByUserRole($id_role)
    {
        return static::find()->where(['id_role' => $id_role])->select(['name', 'color'])->indexBy('id')->column();
    }
}
