<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%property_location}}".
 *
 * @property int $id
 * @property int $id_property
 * @property string $city
 * @property string $state
 * @property string $zipcode
 * @property string $address_1
 * @property string $address_2
 * @property string $photo
 *
 * @property Property $property
 */
class PropertyLocation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%property_location}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            //id property rules
            ['id_property', 'integer'],
            ['id_property', 'exist', 'skipOnError' => true, 'targetClass' => Property::className(), 'targetAttribute' => ['id_property' => 'id']],

            //city rules
            ['city', 'required'],
            ['city', 'string', 'max' => 255],

            //state rules
            ['state', 'required'],
            ['state', 'string', 'max' => 255],

            //address 1 rules
            ['address_1', 'required'],
            ['address_1', 'string', 'max' => 255],

            //address 2 rules
            ['address_2', 'string', 'max' => 255],

            //zipcode rules
            ['zipcode', 'string', 'max' => 255],

            //photo rules
            ['photo', 'string', 'max' => 255],

            //place id rules
            ['place_id', 'string', 'max' => 255],

            //lng rules
            ['lng', 'double'],

            //lat rules
            ['lat', 'double'],

            //short name
            ['short_name', 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'            => 'ID',
            'id_property'   => 'Id Property',
            'city'          => 'City',
            'state'         => 'State',
            'zipcode'       => 'Zipcode',
            'address_1'     => 'Address 1',
            'address_2'     => 'Address 2',
            'photo'         => 'Photo',
            'place_id'      => 'Place id',
            'lng'           => 'Longitude',
            'lat'           => 'Latitude',
            'short_name'    => 'Short name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProperty()
    {
        return $this->hasOne(Property::className(), ['id' => 'id_property']);
    }
}
