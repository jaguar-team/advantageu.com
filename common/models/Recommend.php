<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "recommend".
 *
 * @property int $id
 * @property int $id_user_profile
 * @property int $id_role
 * @property string $first_name
 * @property string $last_name
 * @property string $email
 * @property string $company
 * @property string $phone_number
 * @property string $body
 * @property string $created_at
 * @property string $updated_at
 *
 * @property UserRole $role
 * @property UserProfile $userProfile
 */
class Recommend extends \yii\db\ActiveRecord
{
    CONST SCENARIO_SKIP_ID_PROFILE = 'skip_id_profile';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'recommend';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            //id user profile rules
            ['id_user_profile', 'required', 'except' => self::SCENARIO_SKIP_ID_PROFILE],
            ['id_user_profile', 'integer'],
            ['id_user_profile', 'exist', 'skipOnError' => true, 'targetClass' => UserProfile::className(), 'targetAttribute' => ['id_user_profile' => 'id']],

            //id role
            ['id_role', 'required'],
            ['id_role', 'integer'],
            ['id_role', 'exist', 'skipOnError' => true, 'targetClass' => UserRole::className(), 'targetAttribute' => ['id_role' => 'id']],

            //first name rules
            ['first_name', 'required'],
            ['first_name', 'trim'],
            ['first_name', 'string', 'min' => 2, 'max' => 255],

            //last name rules
            ['last_name', 'required'],
            ['last_name', 'trim'],
            ['last_name', 'string', 'min' => 2, 'max' => 255],

            //email rules
            ['email', 'required'],
            ['email', 'trim'],
            ['email', 'email'],

            //phone number rules
            ['phone_number', 'string', 'max' => 255],
            ['phone_number', 'match', 'pattern' => '/^\([0-9]{3}\)[0-9]{3}-[0-9]{4}$/i'],

            //company number rules
            ['company', 'string', 'max' => 255],

            //body rules
            ['body', 'required'],
            ['body', 'string'],

            //created at rules
            ['created_at', 'integer'],

            //updated at rules
            ['updated_at', 'integer'],
        ];
    }

    /**
     * @return array
     */
    public function scenarios()
    {
        return ArrayHelper::merge([
            self::SCENARIO_SKIP_ID_PROFILE => ['id_role', 'first_name', 'last_name', 'email', 'phone_number', 'company', 'body'],
        ], parent::scenarios());
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_user_profile' => 'Id User Profile',
            'id_role' => 'Id Role',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'email' => 'Email',
            'company' => 'Company',
            'phone_number' => 'Phone Number',
            'body' => 'Body',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRole()
    {
        return $this->hasOne(UserRole::className(), ['id' => 'id_role']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserProfile()
    {
        return $this->hasOne(UserProfile::className(), ['id' => 'id_user_profile']);
    }
}
