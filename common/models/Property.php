<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%property}}".
 *
 * @property int $id
 * @property int $id_transaction
 * @property int $id_type
 * @property int $id_status
 * @property int $foreclosue
 * @property int $reo
 * @property int $short_sale
 * @property int $beds
 * @property int $baths
 * @property int $square_feet
 *
 * @property PropertyStatus $status
 * @property Transaction $transaction
 * @property PropertyType $type
 * @property PropertyLocation[] $propertyLocations
 */
class Property extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%property}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            //id transaction rules
            ['id_transaction', 'integer'],
            ['id_transaction', 'exist', 'skipOnError' => true, 'targetClass' => Transaction::className(), 'targetAttribute' => ['id_transaction' => 'id']],
            ['id_transaction', 'unique'],

            //id type rules
            ['id_type', 'required'],
            ['id_type', 'integer'],
            ['id_type', 'exist', 'skipOnError' => true, 'targetClass' => PropertyType::className(), 'targetAttribute' => ['id_type' => 'id']],

            //beds rules
            ['beds', 'integer'],

            //baths rules
            ['baths', 'integer'],

            //square_feet rules
            ['square_feet', 'integer'],

            //garage rules
            ['garage', 'integer'],

            //title rules
            ['title', 'string', 'min' => 1, 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_transaction' => 'Id Transaction',
            'id_type' => 'Property Type',
            'id_status' => 'Id Status',
            'id_property_owner_type' => 'Id Property Owner Type',
            'id_transaction_type' => 'Id Transaction Type',
            'beds' => 'Beds',
            'baths' => 'Baths',
            'square_feet' => 'Square Feet',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(PropertyStatus::className(), ['id' => 'id_status']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransaction()
    {
        return $this->hasOne(Transaction::className(), ['id' => 'id_transaction']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransactionType()
    {
        return $this->hasOne(TransactionType::className(), ['id' => 'id_transaction_type']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType()
    {
        return $this->hasOne(PropertyType::className(), ['id' => 'id_type']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLocation()
    {
        return $this->hasOne(PropertyLocation::className(), ['id_property' => 'id']);
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        if ($insert) {
            $related_records = $this->getRelatedRecords();

            /** Transaction client */
            if (isset($related_records['location'])) {
                $this->link('location', $related_records['location']);
            }
        }
        parent::afterSave($insert, $changedAttributes);
    }
}
