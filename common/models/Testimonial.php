<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%testimonial}}".
 *
 * @property int $id
 * @property int $id_user_profile
 * @property string $name
 * @property string $body
 * @property string $created_at
 * @property string $updated_at
 *
 * @property UserProfile $userProfile
 */
class Testimonial extends \yii\db\ActiveRecord
{
    CONST SCENARIO_SKIP_ID_PROFILE = 'skip_id_profile';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%testimonial}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            //id user profile rules
            ['id_user_profile', 'required', 'except' => self::SCENARIO_SKIP_ID_PROFILE],
            ['id_user_profile', 'integer'],
            ['id_user_profile', 'exist', 'skipOnError' => true, 'targetClass' => UserProfile::className(), 'targetAttribute' => ['id_user_profile' => 'id']],

            //body rules
            ['body', 'required'],
            ['body', 'string'],

            //name rules
            ['name', 'required'],
            ['name', 'trim'],
            ['name', 'string', 'max' => 255],

            //created at rules
            ['created_at', 'integer'],

            //updated at rules
            ['updated_at', 'integer'],
        ];
    }

    /**
     * @return array
     */
    public function scenarios()
    {
        return ArrayHelper::merge([
            self::SCENARIO_SKIP_ID_PROFILE => [],
        ], parent::scenarios());
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_user_profile' => 'Id User Profile',
            'name' => 'Name',
            'body' => 'Body',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserProfile()
    {
        return $this->hasOne(UserProfile::className(), ['id' => 'id_user_profile']);
    }
}
