<?php
/**
 * Created by PhpStorm.
 * User: Vlad
 * Date: 12.04.2017
 * Time: 19:11
 */

namespace common\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\User;
use common\models\UserRole;
use common\models\query\UserQuery;

/**
 * UserSearch represents the model behind the search form about User.
 */
class UserSearch extends Model
{
    /** @var string */
    public $full_name;

    /** @var integer */
    public $id_role;

    /** @var  string */
    public $zip_codes;

    /** @var string */
    public $email;

    /** @var integer */
    public $transaction_quantity;

    /** @var integer */
    public $created_at;

    /** @var integer */
    public $updated_at;

    /** @var  ActiveDataProvider */
    public $data_provider;

    /** @var array|string */
    public $role = [];

    /**
     * UserSearch constructor.
     * @param bool $role, available params: admin, buyer, seller, agent
     * @param array $config
     */
    public function __construct($role = false, array $config = [])
    {
        $this->role = $role;
        parent::__construct($config);
    }

    /** @inheritdoc */
    public function rules()
    {
        return [
            //name rules
            ['full_name', 'string'],

            //created at rules
            ['created_at', 'string'],

            //updated at rules
            ['updated_at', 'string'],

            //email rules
            ['email', 'safe'],

            //zip_codes rules
            ['zip_codes', 'safe'],

            ['transaction_quantity', 'safe'],

            //id role rules
            ['id_role', 'integer'],
            ['id_role', 'exist', 'targetClass' => UserRole::className(), 'targetAttribute' => ['id_role' => 'id']],
        ];
    }

    /** @inheritdoc */
    public function attributeLabels()
    {
        return [
            'id_role'       => 'Role',
            'email'         => 'Email',
            'created_at'    => 'Registration time',
        ];
    }

    /**
     * @return UserQuery
     */
    public function buildQuery()
    {
        $query = User::find()
            ->alias('u')
            ->select([
                'u.*',
                'transaction_quantity'  => 'COUNT(t.id)',
            ])
            ->joinWith('role ur')
            ->joinWith('transactions t')
            ->joinWith('profile up')
            ->where('u.id != "'.\Yii::$app->user->identity->id.'"')
            ->andWhere($this->buildRoleCondition())
            ->groupBy('u.id');

        if ($this->load(\Yii::$app->request->queryParams) && $this->validate()) {
            //Zip Codes
            $query->andFilterWhere(['LIKE', 'CONCAT(up.zip_codes_daily, \' \', up.zip_codes_high_turnover, \' \', up.zip_codes_just)', $this->zip_codes]);

            $query->andHaving(['>=', 'COUNT(t.id)', $this->transaction_quantity]);
            $query->andFilterWhere(['ur.id' => $this->id_role]);
            $query->andFilterWhere(['LIKE', 'u.email', $this->email]);
            $query->andFilterWhere(['>=', 'u.created_at', strtotime($this->created_at)]);
            $query->andFilterWhere(['>=', 'u.updated_at', strtotime($this->updated_at)]);
            $query->andFilterWhere(['LIKE', 'CONCAT(up.first_name, \' \', up.last_name)', $this->full_name]);
        }

        return $query;
    }

    /**
     * @return array
     */
    public function buildRoleCondition()
    {
        $conditions = [];

        if (is_array($this->role)) {
            foreach ($this->role as $name) {
                $conditions['ur.is_'.$name] = User::STATUS_ACTIVE;
            }
        }

        return $conditions;
    }

    /**
     * @return array
     */
    public function getSort()
    {
        return [
            'defaultOrder' => [
                'created_at' => 'DESC',
            ],
            'attributes' => [
                'created_at',
                'updated_at',
                'zip_codes',
                'email',
                'id_role',
                'full_name' => [
                    'label'     => 'Name',
                    'asc'       => ['up.first_name' => SORT_ASC, 'up.last_name' => SORT_ASC],
                    'desc'      => ['up.first_name' => SORT_DESC, 'up.last_name' => SORT_DESC],
                    'default'   => SORT_ASC
                ],
                'transaction_quantity' => [
                    'label' => 'Transaction quantity',
                ],
            ],
        ];
    }

    /**
     * @return ActiveDataProvider
     */
    public function search()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => $this->buildQuery(),
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => $this->getSort(),
        ]);

        return $dataProvider;
    }
}