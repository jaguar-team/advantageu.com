<?php

/**
 * Created by PhpStorm.
 * User: User
 * Date: 027 27.07.17
 * Time: 16:33
 */

namespace common\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Transaction;
use common\models\PropertyType;


class PropertySearch extends Model
{
    /** @var string */
    public $name;

    /** @var integer */
    public $type;

    /** @var string */
    public $listing_date;

    /** @var double */
    public $listing_price;

    /** @var double */
    public $selling_date;

    /** @var double */
    public $selling_price;

    /** @var string */
    public $client_full_name;

    /** @var string */
    public $agent_full_name;

    /** @var string */
    public $created_at;

    /** @var string */
    public $updated_at;

    /** @var string */
    public $location;

    /** @var  ActiveDataProvider */
    public $data_provider;

    public function rules()
    {
        return [
            //name
            ['name', 'string'],

            //agent name rules
            ['agent_full_name', 'string'],

            //client name rules
            ['client_full_name', 'string'],

            //location rules
            ['location', 'string'],

            //type rules
            ['type', 'integer'],
            ['type', 'exist', 'skipOnError' => true, 'targetClass' => PropertyType::className(), 'targetAttribute' => ['type' => 'id']],

            //location rules
            ['selling_price', 'number'],

            //location rules
            ['listing_price', 'number'],

            //updated at rules
            ['selling_date', 'safe'],

            //created at rules
            ['listing_date', 'safe'],

            //updated at rules
            ['created_at', 'safe'],

            //created at rules
            ['created_at', 'safe'],
        ];
    }

    /**
     *
     */
    public function buildQuery()
    {
        $query = Transaction::find()
            ->alias('t')
            ->joinWith(['agent ta' => function($q_agent){
                $q_agent->joinWith('profile ap');
            }])
            ->joinWith(['agent tc' => function($q_client){
                $q_client->joinWith('profile cp');
            }])
            ->joinWith('transactionInfo ti')
            ->joinWith(['property p' => function($q_property) {
                $q_property->joinWith('location pl');
                $q_property->joinWith('type pt');
            }]);

        if ($this->load(\Yii::$app->request->queryParams) && $this->validate()) {
            //transaction
            $query->andFilterWhere(['>=', 't.created_at', strtotime($this->created_at)]);
            $query->andFilterWhere(['>=', 't.updated_at', strtotime($this->updated_at)]);
            //transaction info
            $query->andFilterWhere(['>=', 'ti.selling_date', strtotime($this->selling_date)]);
            $query->andFilterWhere(['>=', 'ti.listing_date', strtotime($this->listing_date)]);
            $query->andFilterWhere(['>=', 'ti.listing_price', $this->listing_price]);
            $query->andFilterWhere(['>=', 'ti.selling_price', $this->selling_price]);
            //transaction client
            $query->andFilterWhere(['LIKE', 'CONCAT(cp.first_name, \' \', cp.last_name)', $this->client_full_name]);
            //transaction agent
            $query->andFilterWhere(['LIKE', 'CONCAT(ap.first_name, \' \', ap.last_name)', $this->agent_full_name]);
            //property
            $query->andFilterWhere(['pt.id' => $this->type]);
            $query->andFilterWhere(['LIKE', 'pt.title', $this->name]);
            //property location
            $query->andFilterWhere(['LIKE', 'CONCAT(pl.city, \' \', pl.short_name, \' \', pl.address_1, \' \', pl.zipcode)', $this->location]);
        }

        return $query;
    }

    /**
     * @return array
     */
    public function getSort()
    {
        return [
            'defaultOrder' => [
                'created_at' => 'DESC',
            ],
            'attributes' => [
                'status',
                'featured',
                'created_at',
                'updated_at',
                'agent_full_name' => [
                    'label'     => 'Agent name',
                    'asc'       => ['ta.first_name' => SORT_ASC, 'ta.last_name' => SORT_ASC],
                    'desc'      => ['ta.first_name' => SORT_DESC, 'ta.last_name' => SORT_DESC],
                    'default'   => SORT_ASC
                ],
                'client_full_name' => [
                    'label'     => 'Client name',
                    'asc'       => ['tc.first_name' => SORT_ASC, 'tc.last_name' => SORT_ASC],
                    'desc'      => ['tc.first_name' => SORT_DESC, 'tc.last_name' => SORT_DESC],
                    'default'   => SORT_ASC
                ],
                'location' => [
                    'label'     => 'Location',
                    'asc'       => ['pl.city' => SORT_ASC, 'pl.pl.short_name' => SORT_ASC, 'pl.zipcode' => SORT_ASC],
                    'desc'      => ['pl.city' => SORT_DESC, 'pl.pl.short_name' => SORT_DESC, 'pl.zipcode' => SORT_DESC],
                    'default'   => SORT_ASC
                ],
                'type' => [
                    'label'     => 'Type',
                    'asc'       => ['pt.name' => SORT_ASC],
                    'desc'      => ['pt.name' => SORT_DESC],
                    'default'   => SORT_ASC
                ],
                'listing_date' => [
                    'label'     => 'Listed date',
                    'asc'       => ['ti.listing_date' => SORT_ASC],
                    'desc'      => ['ti.listing_date' => SORT_DESC],
                    'default'   => SORT_ASC
                ],
                'selling_date' => [
                    'label'     => 'Sold date',
                    'asc'       => ['ti.selling_date' => SORT_ASC],
                    'desc'      => ['ti.selling_date' => SORT_DESC],
                    'default'   => SORT_ASC
                ],
            ],
        ];
    }

    /**
     * @return ActiveDataProvider
     */
    public function search()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => $this->buildQuery(),
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => $this->getSort(),
        ]);

        return $dataProvider;
    }
}