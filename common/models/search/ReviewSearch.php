<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 002 02.08.17
 * Time: 14:35
 */

namespace common\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Review;

class ReviewSearch extends Model
{
    /** @var string */
    public $title;

    /** @var string */
    public $owner_info;

    /** @var string */
    public $sender_info;

    /** @var double */
    public $star;

    /** @var string */
    public $created_at;

    /** @var string */
    public $updated_at;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            //title rules
            ['title', 'string'],

            //owner full name rules
            ['owner_info', 'string'],

            //sender full name rules
            ['sender_info', 'string'],

            //star rules
            ['star', 'number'],

            //updated at rules
            ['created_at', 'safe'],

            //created at rules
            ['created_at', 'safe'],
        ];
    }

    public function buildQuery()
    {
        $query = Review::find()
            ->alias('r')
            ->joinWith(['user u' => function($q_user){
                $q_user->joinWith('profile up');
            }]);

        if ($this->load(\Yii::$app->request->queryParams) && $this->validate()) {
            $query->andFilterWhere(['LIKE', 'r.title', $this->title]);
            $query->andFilterWhere(['LIKE', 'CONCAT(r.first_name, \' \', r.last_name, \' \', r.email)', $this->sender_info]);
            $query->andFilterWhere(['LIKE', 'CONCAT(up.first_name, \' \', up.last_name, \' \', u.email)', $this->owner_info]);
            $query->andFilterWhere(['>=', 'r.star', $this->star]);
            $query->andFilterWhere(['>=', 'r.created_at', strtotime($this->created_at)]);
            $query->andFilterWhere(['>=', 'r.updated_at', strtotime($this->updated_at)]);
        }

        return $query;
    }

    /**
     * @return array
     */
    public function getSort()
    {
        return [
            'defaultOrder' => [
                'created_at' => 'DESC',
            ],
            'attributes' => [
                'status',
                'created_at',
                'updated_at',
                'sender_info' => [
                    'label'     => 'Sender info',
                    'asc'       => ['r.first_name' => SORT_ASC, 'r.last_name' => SORT_ASC],
                    'desc'      => ['r.first_name' => SORT_DESC, 'r.last_name' => SORT_DESC],
                    'default'   => SORT_ASC
                ],
                'owner_info' => [
                    'label'     => 'Owner info',
                    'asc'       => ['up.first_name' => SORT_ASC, 'up.last_name' => SORT_ASC],
                    'desc'      => ['up.first_name' => SORT_DESC, 'up.last_name' => SORT_DESC],
                    'default'   => SORT_ASC
                ],
                'star' => [
                    'label'     => 'Rating',
                    'asc'       => ['r.star' => SORT_ASC],
                    'desc'      => ['r.star' => SORT_DESC],
                    'default'   => SORT_ASC
                ],
                'title' => [
                    'label'     => 'Title',
                    'asc'       => ['r.title' => SORT_ASC],
                    'desc'      => ['r.title' => SORT_DESC],
                    'default'   => SORT_ASC
                ],
            ],
        ];
    }

    /**
     * @return ActiveDataProvider
     */
    public function search()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => $this->buildQuery(),
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => $this->getSort(),
        ]);

        return $dataProvider;
    }
}