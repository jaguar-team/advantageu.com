<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%area}}".
 *
 * @property int $id
 * @property int $id_user_profile
 * @property string $state
 * @property string $short_name
 * @property string $region
 * @property string $city
 * @property string $county
 * @property string $address_1
 * @property string $place_id
 * @property integer $zip_code
 *
 * @property UserProfile $userProfile
 */
class Area extends \yii\db\ActiveRecord
{
    CONST SCENARIO_SKIP_ID_PROFILE = 'skip_id_profile';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%area}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [

            //user profile id rules
            ['id_user_profile', 'required', 'on' => ['default']],
            ['id_user_profile', 'integer'],
            ['id_user_profile', 'exist', 'skipOnError' => true, 'targetClass' => UserProfile::className(), 'targetAttribute' => ['id_user_profile' => 'id']],

            //state rules
            ['state', 'string', 'max' => 255],

            //start_name rules
            ['short_name', 'string', 'max' => 10],

            //region rules
            ['region', 'string', 'max' => 255],

            //city rules
            ['city', 'string', 'max' => 255],

            //county rules
            ['county', 'string', 'max' => 255],

            //address 1 rules
            ['address_1', 'string', 'max' => 255],

            //place id rules
            ['place_id', 'string', 'max' => 255],

            //zip code rules
            ['zip_code', 'required'],
            ['zip_code', 'number'],
            ['zip_code', 'match', 'pattern' => '/^[0-9]{5}$/i'],
            ['zip_code', 'unique', 'targetClass' => Area::className(), 'message' => 'This zip code is already added.', 'filter' => function($query) {
                return $query->where([
                    'id_user_profile'   => $this->getAttribute('id_user_profile'),
                    'zip_code'          => $this->getAttribute('zip_code')
                ]);
            }],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_user_profile' => 'Id User Profile',
            'state' => 'State',
            'short_name' => 'Short Name',
            'city' => 'City',
            'region' => 'Region',
        ];
    }

    /**
     * @return array
     */
    public function scenarios()
    {
        return ArrayHelper::merge([
            self::SCENARIO_SKIP_ID_PROFILE => ['region', 'state', 'short_name', 'city', 'zip_code', 'address_1', 'county', 'place_id'],
        ], parent::scenarios());
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserProfile()
    {
        return $this->hasOne(UserProfile::className(), ['id' => 'id_user_profile']);
    }
}