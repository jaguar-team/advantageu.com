<?php
namespace common\models;

use Codeception\PHPUnit\Constraint\Page;
use Yii;
use yii\base\Model;
use common\models\User;

/**
 * Login form
 */
class LoginForm extends Model
{
    public $email;
    public $password;
    public $rememberMe = true;

    private $_user;

    /**
     * LoginForm constructor.
     * Init User model
     * @param array $config
     */
    public function init()
    {
        parent::init();

        $this->_user = new User();
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['email', 'password'], 'required'],
            ['email', 'email'],
            // rememberMe must be a boolean value
            ['rememberMe', 'boolean'],
            // password is validated by validatePassword()
            ['password', 'validatePassword'],
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $this->_user = User::find()->byEmail($this->email);
            if (!$this->_user || !$this->_user->validatePassword($this->password)) {
                $this->addError($attribute, 'Incorrect email or password.');
            }
        }
    }

    /**
     * Logs in a user using the provided username and password.
     *
     * @return bool whether the user is logged in successfully
     */
    public function login($admin = false)
    {
        if ($this->validate()) {

            $this->_user = User::find()->byEmail($this->email);

            /** if connect to admin panel */
            if ($admin) {
                if (!$this->_user->isAdmin()) {
                    return false;
                }
            } else {
                if ($this->_user->isAdmin()) {
	                $this->addError('password', 'Incorrect email or password.');
                    return false;
                }
            }

	        /** check user activation */
	        if (!$this->_user->isActive()) {
		        $this->addError('password', 'Your account is not activate.');
		        return false;
	        }

            return Yii::$app->user->login($this->_user, $this->rememberMe ? 3600 * 24 * 30 : 0);
        } else {
            return false;
        }
    }
}
