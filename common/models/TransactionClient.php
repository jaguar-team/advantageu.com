<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%transaction_client}}".
 *
 * @property int $id
 * @property int $id_transaction
 * @property string $first_name
 * @property string $last_name
 * @property string $email
 *
 * @property Transaction $transaction
 */
class TransactionClient extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%transaction_client}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            //id transaction rules
            //['id_transaction', 'required'],
            ['id_transaction', 'integer'],
            ['id_transaction', 'unique'],
            ['id_transaction', 'exist', 'skipOnError' => true, 'targetClass' => Transaction::className(), 'targetAttribute' => ['id_transaction' => 'id']],

            //first name rules
            ['first_name', 'required'],
            ['first_name', 'string', 'max' => 255],

            //last name rules
            ['last_name', 'required'],
            ['last_name', 'string', 'max' => 255],

            //email rules
            ['email', 'required'],
            ['email', 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_transaction' => 'Id Transaction',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'email' => 'Email',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransaction()
    {
        return $this->hasOne(Transaction::className(), ['id' => 'id_transaction']);
    }
}
