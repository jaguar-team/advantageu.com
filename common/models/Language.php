<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%language}}".
 *
 * @property int $id
 * @property int $id_user_profile
 * @property string $name
 *
 * @property UserProfile $userProfile
 */
class Language extends \yii\db\ActiveRecord
{
    CONST SCENARIO_SKIP_ID_PROFILE = 'skip_id_profile';
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%language}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            //id profile rules
            ['id_user_profile', 'required', 'except' => self::SCENARIO_SKIP_ID_PROFILE],
            ['id_user_profile', 'integer'],
            ['id_user_profile', 'exist', 'skipOnError' => true, 'targetClass' => UserProfile::className(), 'targetAttribute' => ['id_user_profile' => 'id']],

            //name rules
            ['name', 'required'],
            ['name', 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     * @return array
     */
    public function scenarios()
    {
        return ArrayHelper::merge([
            self::SCENARIO_SKIP_ID_PROFILE => ['name'],
        ], parent::scenarios());
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_user_profile' => 'Id User Profile',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserProfile()
    {
        return $this->hasOne(UserProfile::className(), ['id' => 'id_user_profile']);
    }
}
