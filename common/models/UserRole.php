<?php

namespace common\models;

use Yii;
use common\models\query\UserRoleQuery;

/**
 * This is the model class for table "{{%user_role}}".
 *
 * @property int $id
 * @property int $status
 * @property boolean $is_admin
 * @property string $name
 *
 * @property Advantage[] $advantages
 * @property KnowFrom[] $knowFroms
 * @property Recommend[] $recommends
 * @property Section[] $sections
 * @property User[] $users
 */
class UserRole extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user_role}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
        	// priority rules
            ['priority', 'integer'],

	        // name rules
	        ['name', 'required'],
	        ['name', 'name'],
	        ['name', 'string', 'max' => 255],

	        // is admin rules
	        ['is_admin', 'required'],
	        ['is_admin', 'boolean'],
	        ['is_admin', 'default', 'value' => 0],

	        // is agent rules
	        ['is_agent', 'required'],
	        ['is_agent', 'boolean'],
	        ['is_agent', 'default', 'value' => 0],

	        // is seller rules
	        ['is_seller', 'required'],
	        ['is_seller', 'boolean'],
	        ['is_seller', 'default', 'value' => 0],

	        // is buyer rules
	        ['is_buyer', 'required'],
	        ['is_buyer', 'boolean'],
	        ['is_buyer', 'default', 'value' => 0],

	        // default rules
	        ['default', 'required'],
	        ['default', 'boolean'],
	        ['default', 'default', 'value' => 0],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'            => 'ID',
            'priority'      => 'Priority',
            'name'          => 'Name',
            'is_admin'      => 'Is admin',
            'is_agent'      => 'Is agent',
            'is_seller'     => 'Is seller',
            'is_buyer'      => 'Is buyer',
            'default'       => 'Default',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdvantages()
    {
        return $this->hasMany(Advantage::className(), ['id_role' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKnowFroms()
    {
        return $this->hasMany(KnowFrom::className(), ['id_role' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRecommends()
    {
        return $this->hasMany(Recommend::className(), ['id_role' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSections()
    {
        return $this->hasMany(Section::className(), ['id_role' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['id_role' => 'id']);
    }

	/**
	 * @inheritdoc
	 * @return UserRoleQuery the active query used by this AR class.
	 */
	public static function find()
	{
		return new UserRoleQuery(get_called_class());
	}

	public function getName()
    {
        if ($this->is_admin) {
            return 'admin';
        } elseif ($this->is_agent) {
            return 'agent';
        } elseif ($this->is_seller) {
            return 'seller';
        } elseif ($this->is_buyer) {
            return 'buyer';
        } else {
            return null;
        }
    }
}
