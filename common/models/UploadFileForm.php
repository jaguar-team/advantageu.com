<?php
/**
 * Created by PhpStorm.
 * User: Vlad
 * Date: 20.04.2017
 * Time: 15:58
 */

namespace common\models;

use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Inflector;
use common\components\UploadPhoto;

class UploadFileForm extends Model
{
    CONST SCENARIO_IMG = 'img';
    CONST SCENARIO_USER_AVATAR = 'user_avatar';

    public $_profile;

    public $user_avatar;

    public $img;

    public $crop;

    /**
     * UploadAvatarForm constructor.
     * @param UserProfile $profile
     * @param array $config
     */
    public function __construct(UserProfile $profile, array $config = [])
    {
        $this->_profile  = $profile;
        $this->_profile->scenario = UserProfile::SCENARIO_UPLOAD_AVATAR;

        parent::__construct($config);
    }

    /**
     * @inheritdoc
     * @return array
     */
    public function rules()
    {
        return [
            //photo rules
            ['user_avatar', 'image', 'extensions' => ['jpg', 'jpeg', 'png'], 'maxFiles' => 1],
            ['img', 'image', 'extensions' => ['jpg', 'jpeg', 'png'], 'maxFiles' => 1],
            //crop rules
            ['crop', 'safe'],
            //['crop', 'validateCrop'],
        ];
    }

    /**
     * @inheritdoc
     * @return array
     */
    public function scenarios()
    {
        return ArrayHelper::merge([
            self::SCENARIO_IMG              => ['user_avatar', 'crop'],
            self::SCENARIO_USER_AVATAR      => ['img', 'crop'],
        ], parent::scenarios());
    }

    public function validateCrop($attribute, $params)
    {
        if (isset($attribute['width'])) {
            if (!is_int($attribute['width'])) {
                $this->addError($attribute, 'Crop width must be integer.');
            }
        }

        if (isset($attribute['height'])) {
            if (!is_int($attribute['height'])) {
                $this->addError($attribute, 'Crop height must be integer.');
            }
        }

        if (isset($attribute['start']['x'])) {
            if (!is_int($attribute['start']['x'])) {
                $this->addError($attribute, 'Crop height must be integer.');
            }
        }

        if (isset($attribute['start']['y'])) {
            if (!is_int($attribute['start']['y'])) {
                $this->addError($attribute, 'Crop height must be integer.');
            }
        }
    }

    /**
     * @return string|null
     */
    public function upload()
    {
        $upload_photo = \Yii::createObject([
            'class'         => UploadPhoto::className(),
            '_object'       => $this,
            'property_name' => $this->scenario,
            'crop'          => $this->crop,
        ]);
        
        $upload_method = Inflector::variablize($this->scenario);

        if (method_exists($upload_photo, $upload_method)) {
            if ($upload_photo->{$upload_method}()->upload()) {
                return $upload_photo->file_name;
            }
        }

        return NULL;
    }
}