<?php

/**
 * Created by PhpStorm.
 * User: User
 * Date: 027 27.06.17
 * Time: 12:51
 */

namespace common\models\relation;

use common\models\Transaction;
use common\models\User;
use yii\base\Object;

class UserTransactionRelation extends Object
{
    public $user;

    public $relation;

    /**
     * UserTransactionRelation constructor.
     * @param User $user
     * @param array $config
     */
    public function __construct(User $user, array $config = [])
    {
        $this->user = $user;
        $this->relation = $user->getTransactions()->alias('t');

        parent::__construct($config);
    }

    /**
     * @return $this
     */
    public function active()
    {
        $this->relation = $this->relation->andWhere(['t.status' => Transaction::STATUS_ACTIVE]);

        return $this;
    }

    /**
     * @return $this
     */
    public function notActive()
    {
        $this->relation = $this->relation->andWhere(['t.status' => Transaction::STATUS_NOT_ACTIVE]);

        return $this;
    }

    /**
     * @return $this
     */
    public function sell()
    {
        $this->relation= $this->relation = $this->relation
            ->joinWith(['client uc' => function($q_client){
                $q_client->joinWith('role ur');
            }])
            ->andWhere(['ur.is_seller' => User::STATUS_ACTIVE]);

        return $this;
    }

    /**
     * @return $this
     */
    public function buy()
    {
        $this->relation= $this->relation = $this->relation
            ->joinWith(['client uc' => function($q_client){
                $q_client->joinWith('role ur');
            }])
            ->andWhere(['ur.is_buyer' => User::STATUS_ACTIVE]);

        return $this;
    }

    /**
     * @return int|string
     */
    public function totalNumber()
    {
        return $this->relation->count();
    }

    /**
     * @return mixed
     */
    public function totalSellingPrice()
    {
        return $this->relation
            ->joinWith('transactionInfo ti')
            ->sum('ti.selling_price');
    }

    /**
     * @return mixed
     */
    public function bestLocation()
    {
        return $this->relation
            ->select([
                //'id'                => 't.id',
                'best_location'     => 'pl.city',
                'count'             => 'COUNT(pl.city)',
            ])
            ->joinWith(['property p' => function($e_property) {
                $e_property->joinWith('location pl');
            }], false)
            ->groupBy('pl.city')->asArray()->one();
    }

    /**
     * @return $this
     */
    public function asArray()
    {
        $this->relation = $this->relation->asArray();

        return $this;
    }

    /**
     * @return array|null|\yii\db\ActiveRecord
     */
    public function one()
    {
        return $this->relation->one();
    }

    /**
     * @return array|\yii\db\ActiveRecord[]
     */
    public function all()
    {
        return $this->relation->all();
    }
}