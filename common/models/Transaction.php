<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "{{%transaction}}".
 *
 * @property int $id
 * @property int $id_user_profile
 * @property int $status
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Property $property
 * @property UserProfile $userProfile
 * @property TransactionAgent $transactionAgent
 * @property TransactionClient $transactionClient
 * @property TransactionInfo $transactionInfo
 */
class Transaction extends \yii\db\ActiveRecord
{
    CONST STATUS_ACTIVE = 1;
    CONST STATUS_NOT_ACTIVE = 0;

    CONST FEATURED_ACTIVE = 1;
    CONST FEATURED_NOT_ACTIVE = 0;

    CONST APPROVED = 1;
    CONST NOT_APPROVED = 0;

    CONST SCENARIO_SKIP_ID_USER = 'skip_id_user';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%transaction}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            //id_agent rules
            ['id_agent', 'integer'],
            ['id_agent', 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['id_agent' => 'id']],

            //id_client rules
            ['id_client', 'integer'],
            ['id_client', 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['id_client' => 'id']],

            //status rules
            ['status', 'boolean'],
            ['status', 'default', 'value' => 0],

            //created at rules
            ['created_at', 'integer'],

            //updated at rules
            ['updated_at', 'integer'],

            //featured rules
            ['featured', 'boolean'],
            ['featured', 'default', 'value' => self::FEATURED_NOT_ACTIVE],
            ['featured', 'in', 'range' => [self::FEATURED_ACTIVE, self::FEATURED_NOT_ACTIVE]],
        ];
    }

    /**
     * @return array
     */
    public function scenarios()
    {
        return ArrayHelper::merge([], parent::scenarios());
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_user_profile' => 'Id User Profile',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @inheritdoc
     */
    public function transactions()
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_INSERT | self::OP_UPDATE,
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProperty()
    {
        return $this->hasOne(Property::className(), ['id_transaction' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgent()
    {
        return $this->hasOne(User::className(), ['id' => 'id_agent']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(User::className(), ['id' => 'id_client']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransactionAgents()
    {
        return $this->hasMany(TransactionAgent::className(), ['id_transaction' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransactionClient()
    {
        return $this->hasOne(TransactionClient::className(), ['id_transaction' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransactionInfo()
    {
        return $this->hasOne(TransactionInfo::className(), ['id_transaction' => 'id']);
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        if ($insert) {
            $related_records = $this->getRelatedRecords();

            /** Transaction agent */
            if (isset($related_records['transactionAgents'])) {
                foreach ($related_records['transactionAgents'] as $index => $related_record) {
                    $this->link('transactionAgents', $related_record);
                }
            }

            /** Transaction client */
            if (isset($related_records['transactionClient'])) {
                $this->link('transactionClient', $related_records['transactionClient']);
            }

            /** Transaction info */
            if (isset($related_records['transactionInfo'])) {
                $this->link('transactionInfo', $related_records['transactionInfo']);
            }

            /** Property */
            if (isset($related_records['property'])) {
                $this->link('property', $related_records['property']);
            }
        }
        parent::afterSave($insert, $changedAttributes);
    }
}
