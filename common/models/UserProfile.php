<?php

namespace common\models;

use yii;
use yii\db\ActiveRecord;
use yii\helpers\Url;
use yii\data\Pagination;
use yii\helpers\ArrayHelper;
use common\components\ZohoIntegration;
use common\behaviors\ArrayBehavior;
use common\components\GooglePlaceImplantation;

/**
 * This is the model class for table "{{%user_profile}}".
 * ---------------------------------------------------------------------------------------------------------------------
 * MAIN INFO
 * ---------------------------------------------------------------------------------------------------------------------
 * @property int $id
 * @property int $id_user
 * @property string $company
 * @property string $first_name
 * @property string $last_name
 * @property string $biography
 * @property string $phone_number
 * @property string $photo
 * @property string $website_url
 * @property string $education
 * @property string $license_number
 * @property integer $experience
 * @property integer $sms_send
 * @property integer $email_send
 * ---------------------------------------------------------------------------------------------------------------------
 * AGENT BIO
 * @property integer $timezone
 * @property integer $account_number
 * @property boolean $google_calendar
 * @property string $team_name
 * @property string $team_info
 * @property string $admin_first_name
 * @property string $admin_last_name
 * @property string $admin_email
 * @property string $admin_phone_number
 * @property string $admin_mobile_carrier
 * @property string $listing_agent_first_name
 * @property string $listing_agent_last_name
 * @property string $listing_agent_phone_number
 * @property string $listing_mobile_carrier
 * @property string $phonetic_pronunciation
 * ---------------------------------------------------------------------------------------------------------------------
 * @property Area[] $areas
 * @property Language[] $languages
 * @property User $user
 */
class UserProfile extends ActiveRecord
{
    CONST DIR_PHOTO = 'img/user';
    CONST DEFAULT_PHOTO = 'default-admin-avatar.png';

    CONST SCENARIO_UPLOAD_AVATAR = 'uploadAvatar';
    CONST SCENARIO_ZOHO_CONSOLE = 'zohoConsole';
    CONST SCENARIO_CREATE_ACCOUNT = 'createAccount';

    public $timezone_list = [
        1 => 'Eastern Standard Time',
        2 => 'Central Standard Time',
        3 => 'Mountain Standard Time',
        4 => 'Pacific Standard Time',
    ];

    public $carrier_list = [
        1 => 'Verizon',
        2 => 'Sprint',
        3 => 'AT&T',
        4 => 'T-Mobile',
    ];

    public $package_level_list = [
        1 => 'Other Account',
        2 => 'IntelliList Pro',
        3 => 'Black Diamond',
        4 => 'Gold Exclusive',
    ];

    public $property_type_list = [
        1 => 'Land',
        2 => 'Multifamily',
        3 => 'Mobile',
        4 => 'Modular',
        5 => 'Commercial',
        6 => 'None to omit',
    ];

    public $designation_list = [
        1   => 'Accredited Buyer Representative',
        2   => 'Certified Residential Specialist',
        3   => 'Certified New Homes Sale Specialist',
        4   => 'Short Sale and Foreclosure Expert',
        5   => 'Certified International Property Specialist',
        6   => 'Accredited Seller Representative',
        7   => 'Certified Home Stager',
        8   => 'Short Sale and Foreclosure Expert',
        9   => 'Certified Luxury Home Specialist',
        10  => 'Short Sale and Foreclosure Expert',
        11  => 'Certified Real Estate Divorce Specialist',
        12  => 'Other (Please Specify)',
    ];

    public $statements_list = [
        1   => 'Easy Exit Listing - I’m so confident that you will be satisfied with my service that you can cancel the contract at any time with no penalties if I’m not doing my job',
        2   => '30 Day Guarantee - If you want to try selling FSBO, no problem, if you find your own buyer in the next 30 days before I do, then you owe me nothing.',
        3   => 'Communication Guarantee - You will get a live call/email from me every week letting you know what actions I’ve taken and what activity there has been on your property.',
        4   => 'If I can’t sell your property within the 1st 60 days, I’ll reduce my seller’s commission and add it to the buyer’s agent commission.',
        5   => 'If I can’t sell your property within 90 days, I will pay you $500.',
        6   => 'Flexible commission options - we are willing to offer alternatives to traditional commission',
        7   => 'Free professional Photography - One of the number 1# ways to get buyers to consider a property is the quality of the images used',
    ];

    public $advantages_list = [
        1   => 'Professional Staging: Offer a complementary staging consultation',
        2   => 'Professional home photography: We send a photographer to your home to take a number of photos that showcase your home.',
        3   => 'Postcards and advertising: We will send out custom postcards to potential buyers.',
        4   => 'Virtual tour: We will create a virtual tour of your home and link it to on a variety of sites.',
        5   => 'Create unique single property website for each home.',
        6   => 'Social media marketing campaign: We will post your listing on our social media feeds.',
        7   => 'Professional ad writing: Your listing is written by a professional marketing copywriter.',
        8   => 'Property brochures: We will create color brochures for prospective buyers highlighting your property.',
        9   => 'Target buyer lead generation websites: Your home is listed on lead generation websites geared to attract buyers.',
        10  => 'Agent-to-agent networking: We make sure all the top performing agents in the area know about your listing.',
        11  => 'Open house: If you desire, we will host an open house to showcase your home.',
        12  => 'We provide assistance with coordination of any repairs/upgrades needed to get the home ready to list',
        13  => 'Other (Please Specify)',
    ];

    protected $pagination;

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' => ArrayBehavior::className(),
                'attributes' => ['advantages', 'statements', 'special_designations', 'property_avoid',],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user_profile}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            //id user rules
            ['id_user', 'integer'],
            ['id_user', 'unique'],
            ['id_user', 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['id_user' => 'id']],

            //company rules
            ['company', 'string', 'max' => 255],

            //first name rules
            ['first_name', 'trim'],
            ['first_name', 'string', 'max' => 255, 'min' => 1],
            ['first_name', 'match', 'pattern' => '/^[A-z]{1,255}$/i', 'message' => 'Allowable characters: A-z'],

            //last name rules
            ['last_name', 'trim'],
            ['last_name', 'string', 'max' => 255, 'min' => 1],
            ['last_name', 'match', 'pattern' => '/^[A-z]{1,255}$/i', 'message' => 'Allowable characters: A-z'],

            //last name rules
            ['phone_number', 'string', 'max' => 255],
            ['phone_number', 'match', 'pattern' => '/^\([0-9]{3}\)[0-9]{3}-[0-9]{4}$/i'],

            //education rules
            ['education', 'string', 'max' => 255],

            //biography rules
            ['biography', 'string'],

            //license number
            ['license_number', 'string'],

            //photo rules
            ['photo', 'string', 'max' => 255],

            //website url rules
            ['website_url', 'string', 'max' => 255],
            ['website_url', 'url','defaultScheme' => 'http'],

            //sms send rules
            ['sms_send', 'boolean'],
            ['sms_send', 'default', 'value' => 0],

            //email send rules
            ['email_send', 'boolean'],
            ['email_send', 'default', 'value' => 0],

            #HARD FIELDS FOR ZOHOH SURVEY
            ['google_calendar', 'boolean'],
            ['google_calendar', 'default', 'value' => 0],
            //[['listing_mobile_carrier', 'admin_mobile_carrier'], 'exist', 'targetAttribute' => array_keys($this->designation_list)],
            //['timezone', 'exist', 'targetAttribute' => array_keys($this->timezone_list)],
            //['package_level', 'exist', 'targetAttribute' => array_keys($this->package_level_list)],
            [[
                'phonetic_pronunciation', 'team_name', 'listing_agent_phone_number','listing_agent_mobile_number',
                'listing_agent_first_name', 'listing_agent_last_name', 'admin_first_name',
                'admin_last_name', 'admin_email', 'admin_phone_number', 'facebook_login', 'facebook_password',
                'twitter_login', 'twitter_password', 'youtube_login', 'youtube_password', 'mls_name', 'mls_website',
                'mls_username', 'mls_password', 'redx_username', 'redx_password'
            ], 'string', 'min' => 1, 'max' => 255],
            [[
                'team_info',  'uvps', 'schedule_question_1',
                'schedule_question_2', 'schedule_question_3', 'schedule_question_4', 'zip_codes_daily', 'zip_codes_just',
                'zip_codes_high_turnover', 'property_list',
            ], 'string'],
            [[
                'account_number', 'experience', 'timezone', 'listing_mobile_carrier', 'admin_mobile_carrier', 
                'average_days_on_market', 'number_home_listed', 'number_home_sold', 'package_level', 'listing_sale_price_ratio',
                'total_sale_price', 'average_home_sale_price',
            ], 'number'],
            [['advantages', 'special_designations', 'statements', 'property_avoid'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_user' => 'Id User',
            'company' => 'Company',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'biography' => 'Biography',
            'phone_number' => 'Phone Number',
            'photo' => 'Photo',
            'license_number' => 'License number',
            'website_url' => 'Website Url',
            'education' => 'Education',
        ];
    }

    public function zohoAttributes()
    {
        return [
            'fields' => [
                'First Name'				=> ['getAttribute' => function() { return $this->first_name; }, 'setAttribute' => function($first_name) { return $this->first_name = $first_name; }],
                'Last Name'				    => ['getAttribute' => function() { return $this->last_name; }, 'setAttribute' => function($last_name) { return $this->last_name = $last_name; }],
                'Company'				    => ['getAttribute' => function() { return $this->company; }, 'setAttribute' => function($company) { return $this->company = $company; }],
                'Phone'				        => ['getAttribute' => function() { return $this->phone_number; }, 'setAttribute' => function($phone_number) { return $this->phone_number = $phone_number; }],
                'Website'				    => ['getAttribute' => function() { return $this->website_url; }, 'setAttribute' => function($website_url) { return $this->website_url = $website_url; }],
                'SMS Approved'			    => ['getAttribute' => function() { return $this->sms_send; }, 'setAttribute' => function($sms_send) { return $this->sms_send = $sms_send; }],
                //Agent BIO
                'Email Opt Out'				=> ['getAttribute' => function() { return $this->email_send; }, 'setAttribute' => function($email_send) { return $this->email_send = $email_send; }],
                'Confirm your Timezone'	    => ['getAttribute' => function() { return $this->timezone; }, 'setAttribute' => function($timezone) {
                    if ($key = array_search($timezone, $this->timezone_list)) {
                        return $this->timezone = $key;
                    }
                    return NULL;
                }],
                'Did you share your calendar'       => ['getAttribute' => function() { return $this->google_calendar; }, 'setAttribute' => function($google_calendar) {
                    return $google_calendar == 'Yes' ? $this->google_calendar = 1 : $this->google_calendar = 0;
                }],
                'Office Phone'                              => ['getAttribute' => function() { return $this->phone_number; }, 'setAttribute' => function($company) { return $this->phone_number = $company; }],
                'Company Name'				                => ['getAttribute' => function() { return $this->company; }, 'setAttribute' => function($company) { return $this->company = $company; }],
                'Account Number'                            => ['getAttribute' => function() { return $this->account_number; }, 'setAttribute' => function($account_number) { return $this->account_number = $account_number; }],
                'Team Name'                                 => ['getAttribute' => function() { return $this->team_name; }, 'setAttribute' => function($team_name) { return $this->team_name = $team_name; }],
                'Firstname'				                    => ['getAttribute' => function() { return $this->first_name; }, 'setAttribute' => function($first_name) { return $this->first_name = $first_name; }],
                'Lastname'				                    => ['getAttribute' => function() { return $this->last_name; }, 'setAttribute' => function($last_name) { return $this->last_name = $last_name; }],
                'How Many Years of Real Estate Experience'  => ['getAttribute' => function() { return $this->experience; }, 'setAttribute' => function($experience) { return $this->experience = $experience; }],
                'What is the phonetic pronunciation of your name'  => ['getAttribute' => function() { return $this->phonetic_pronunciation; }, 'setAttribute' => function($phonetic_pronunciation) { return $this->phonetic_pronunciation = $phonetic_pronunciation; }],
                'Company Website'				            => ['getAttribute' => function() { return $this->website_url; }, 'setAttribute' => function($website_url) { return $this->website_url = $website_url; }],
                'Provide name, email and phone number for anyone on' => ['getAttribute' => function() { return $this->team_info; }, 'setAttribute' => function($team_info) { return $this->team_info = $team_info; }],
                'Administrator\'s Firstname' => ['getAttribute' => function() { return $this->admin_first_name; }, 'setAttribute' => function($admin_first_name) { return $this->admin_first_name = $admin_first_name; }],
                'Administrator Lastname' => ['getAttribute' => function() { return $this->admin_last_name; }, 'setAttribute' => function($admin_last_name) { return $this->admin_last_name = $admin_last_name; }],
                'Administrator Email' => ['getAttribute' => function() { return $this->admin_email; }, 'setAttribute' => function($admin_email) { return $this->admin_email = $admin_email; }],
                'Administrator Phone' => ['getAttribute' => function() { return $this->admin_phone_number; }, 'setAttribute' => function($admin_phone_number) { return $this->admin_phone_number = $admin_phone_number; }],
                'Select Mobile Carrier - Admin' => ['getAttribute' => function() { return $this->admin_mobile_carrier ? $this->carrier_list[$this->admin_mobile_carrier] : NULL; }, 'setAttribute' => function($admin_mobile_carrier) {
                    if ($key = array_search($admin_mobile_carrier, $this->carrier_list)) {
                        return $this->admin_mobile_carrier = $key;
                    }
                    return $this->admin_mobile_carrier = NULL;
                }],
                'Listing Agent First Name' => ['getAttribute' => function() { return $this->listing_agent_first_name; }, 'setAttribute' => function($listing_agent_first_name) { return $this->listing_agent_first_name = $listing_agent_first_name; }],
                'Listing Agent Last Name' => ['getAttribute' => function() { return $this->listing_agent_last_name; }, 'setAttribute' => function($listing_agent_last_name) { return $this->listing_agent_last_name = $listing_agent_last_name; }],
                'Listing Agent Cell Phone Number' => ['getAttribute' => function() { return $this->listing_agent_phone_number; }, 'setAttribute' => function($listing_agent_phone_number) { return $this->listing_agent_phone_number = $listing_agent_phone_number; }],
                'Select Mobile Carrier' => ['getAttribute' => function() { return $this->listing_mobile_carrier ? $this->carrier_list[$this->listing_mobile_carrier] : NULL; }, 'setAttribute' => function($listing_mobile_carrier) {
                    if ($key = array_search($listing_mobile_carrier, $this->carrier_list)) {
                        return $this->listing_mobile_carrier = $key;
                    }
                    return $this->listing_mobile_carrier = NULL;
                }],
                'Number of homes Listed' => ['getAttribute' => function() { return $this->number_home_listed; }, 'setAttribute' => function($number_home_listed) { return $this->number_home_listed = $number_home_listed; }],
                'What\'s your average days on market' => ['getAttribute' => function() { return $this->average_days_on_market; }, 'setAttribute' => function($average_days_on_market) { return $this->average_days_on_market = $average_days_on_market; }],
                'Average Home Sale Price' => ['getAttribute' => function() { return $this->average_home_sale_price; }, 'setAttribute' => function($average_home_sale_price) { return $this->average_home_sale_price = $average_home_sale_price; }],
                'Number of homes sold' => ['getAttribute' => function() { return $this->number_home_sold; }, 'setAttribute' => function($number_home_sold) { return $this->number_home_sold = $number_home_sold; }],
                'Listing to Sales Price Ratio' => ['getAttribute' => function() { return $this->listing_sale_price_ratio; }, 'setAttribute' => function($listing_sale_price_ratio) { return $this->listing_sale_price_ratio = $listing_sale_price_ratio; }],
                'Total Sales Volume' => ['getAttribute' => function() { return $this->total_sale_price; }, 'setAttribute' => function($total_sale_price) { return $this->total_sale_price = $total_sale_price; }],
                'UVP Statements' => ['getAttribute' => function() { return $this->uvps; }, 'setAttribute' => function($uvps) { return $this->uvps = $uvps; }],
                'Package Level' => ['getAttribute' => function() { return $this->package_level ? $this->package_level_list[$this->package_level] : NULL; }, 'setAttribute' => function($package_level) {
                    if ($key = array_search($package_level, $this->package_level_list)) {
                        return $this->package_level = $key;
                    }
                    return $this->package_level = NULL;
                }],
                'Select 3-5 advantages of your marketing plan' => ['getAttribute' => function() {
                    return $this->advantages ? implode(',', $this->advantages) : NULL;
                }, 'setAttribute' => function($advantages) {
                    $advantages = explode(',', $advantages);
                    $advantages_matched = [];
                    foreach ($advantages as $advantage) {
                        if ($matched = array_search($advantage, $this->advantages_list)) {
                            $advantages_matched[] = $matched;
                        }
                    }
                    return $this->advantages = $advantages_matched ? $advantages_matched : NULL;
                }],
                'Facebook Password' => ['getAttribute' => function() { return $this->facebook_password; }, 'setAttribute' => function($facebook_password) { return $this->facebook_password = $facebook_password; }],
                'Facebook Email / Mobile Number' => ['getAttribute' => function() { return $this->facebook_login; }, 'setAttribute' => function($facebook_login) { return $this->facebook_login = $facebook_login; }],
                'Twitter Password' => ['getAttribute' => function() { return $this->twitter_password; }, 'setAttribute' => function($twitter_password) { return $this->twitter_password = $twitter_password; }],
                'Twitter Username' => ['getAttribute' => function() { return $this->twitter_login; }, 'setAttribute' => function($twitter_login) { return $this->twitter_login = $twitter_login; }],
                'YouTube Password' => ['getAttribute' => function() { return $this->youtube_password; }, 'setAttribute' => function($youtube_password) { return $this->youtube_password = $youtube_password; }],
                'Youtube Email' => ['getAttribute' => function() { return $this->youtube_login; }, 'setAttribute' => function($youtube_login) { return $this->youtube_login = $youtube_login; }],
                'REDX Username' => ['getAttribute' => function() { return $this->redx_username; }, 'setAttribute' => function($redx_username) { return $this->redx_username = $redx_username; }],
                'REDX Password' => ['getAttribute' => function() { return $this->redx_password; }, 'setAttribute' => function($redx_password) { return $this->redx_password = $redx_password; }],
                'What\'s your work week availability?' => ['getAttribute' => function() { return $this->schedule_question_1; }, 'setAttribute' => function($schedule_question_1) { return $this->schedule_question_1 = $schedule_question_1; }],
                //'What\'s your availability?' => ['getAttribute' => function() { return $this->schedule_question_2; }, 'setAttribute' => function($schedule_question_2) { return $this->schedule_question_2 = $schedule_question_2; }],
                'What\'s your weekend availability?' => ['getAttribute' => function() { return $this->schedule_question_2; }, 'setAttribute' => function($schedule_question_2) { return $this->schedule_question_2 = $schedule_question_2; }],
                'Any vacation days planned? (Give specific dates)' => ['getAttribute' => function() { return $this->schedule_question_4; }, 'setAttribute' => function($schedule_question_4) { return $this->schedule_question_4 = $schedule_question_4; }],
                'Do you need 24 hours before an appointment?' => ['getAttribute' => function() { return $this->schedule_question_3; }, 'setAttribute' => function($schedule_question_3) { return $this->schedule_question_3 = $schedule_question_3; }],
                'Zipcodes Just Listed Just Sold' => ['getAttribute' => function() { return $this->zip_codes_just; }, 'setAttribute' => function($zip_codes_just) { return $this->zip_codes_just = $zip_codes_just; }],
                'Zipcodes 15 miles radius' => ['getAttribute' => function() { return $this->zip_codes_daily; }, 'setAttribute' => function($zip_codes_daily) { return $this->zip_codes_daily = $zip_codes_daily; }],
                '3 Zipcodes for High Turnover areas' => ['getAttribute' => function() { return $this->zip_codes_high_turnover; }, 'setAttribute' => function($zip_codes_high_turnover) { return $this->zip_codes_high_turnover = $zip_codes_high_turnover; }],
                'What property types should we avoid?' => ['getAttribute' => function() { return $this->property_avoid ? implode(',', $this->property_avoid) : NULL; }, 'setAttribute' => function($properties_avoid) {
                    $properties = explode(',', $properties_avoid);
                    $properties_matched = [];
                    foreach ($properties as $property) {
                        if ($matched = array_search($property, $this->property_type_list)) {
                            $properties_matched[] = $matched;
                        }
                    }
                    return $this->property_avoid = $properties_matched ? $properties_matched : NULL;
                }],
            ],
        ];
    }

    /**
     * @inheritdoc
     * @return array
     */
    public function scenarios()
    {
        return ArrayHelper::merge([
            self::SCENARIO_UPLOAD_AVATAR    => ['photo'],
            self::SCENARIO_ZOHO_CONSOLE     => [],
            self::SCENARIO_CREATE_ACCOUNT   => ['first_name', 'last_name', 'phone_number', 'company', 'website_url',
            'education', 'license_number', 'email_send', 'sms_send', 'biography'],
        ], parent::scenarios());
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAreas()
    {
        return $this->hasMany(Area::className(), ['id_user_profile' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguages()
    {
        return $this->hasMany(Language::className(), ['id_user_profile' => 'id']);
    }

    public function getAgentContacts($pagination_size = 20, $status = 'read', $as_array = false, $count = false)
    {
        $query = $this->hasMany(AgentContact::className(), ['id_user_profile' => 'id']);

        switch ($status) {
            case 'read':
                $query = $query->where(['status' => AgentContact::STATUS_READ]);
                break;
            case 'unread':
                $query = $query->where(['status' => AgentContact::STATUS_UNREAD]);
                break;
            default:
                break;
        }

        if (!$count && $this->setPagination($query, $pagination_size, 'agent-contact-page') > 0) {
            $query = $query->offset($this->pagination->offset)->limit($this->pagination->limit);
        }

        if ($count) {
            return $query->count();
        }

        if ($as_array) {
            return $query->orderBy('status ASC, updated_at ASC')->asArray()->all();
        }

        return $query;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'id_user']);
    }

    /**
     * @return string
     */
    public function getFullName($short = false)
    {
        if ($this->first_name || $this->last_name) {
            /** if short full name */
            if ($short && $this->last_name) {
                $this->last_name = substr($this->last_name, 0, 1).'.';
            }

            if ($this->first_name && $this->last_name) {
                return $this->first_name.' '.$this->last_name;
            } elseif ($this->first_name && !$this->last_name) {
                return $this->first_name;
            } else {
                return $this->last_name;
            }
        } else {
            return $this->user->email;
        }
    }

    /**
     * Get performance by type
     * Default type: city
     * Possible type: year, transaction_type, property_type
     * @param bool $group
     * @return array|bool|\yii\db\ActiveRecord[]
     */
    public function getPerformance($group = false, $best = false, $recursion = false)
    {
        if (!$this->id) {
            return false;
        }

        $count_transactions = self::getTransactions()->where(['status' => 1])->count();

        switch ($group) {
            case 'year' :
                $select = 't.id, 
                           COUNT(t.id) AS `count`, 
                           round((COUNT(pl.city)/'.$count_transactions.') * 100) AS `percent`,
                           YEAR(FROM_UNIXTIME(ti.selling_date)) AS `year`';
                $group_by = 'year';
                $order_by = 'year ASC';
                break;
            case 'transaction_type':
                $select = 'tt.id, 
                           tt.name as `type`, 
                           COUNT(tt.name) AS `count`,
                           round((COUNT(pl.city)/'.$count_transactions.') * 100) AS `percent`';
                $group_by = 'type';
                $order_by = 'count DESC';
                break;
            case 'property_type':
                $select = 'pt.id, 
                           pt.name as `type`, 
                           COUNT(pt.name) AS `count`,
                           round((COUNT(pl.city)/'.$count_transactions.') * 100) AS `percent`';
                $group_by = 'type';
                $order_by = 'count DESC';
                break;
            default: //city
                $select = [
                    'id'            => 't.id',
                    'city'          => 'pl.city',
                    'count'         => 'COUNT(pl.city)',
                    'percent'       => 'round((COUNT(pl.city)/'.$count_transactions.') * 100)',
                ];
                $order_by = 'count DESC';
                $group_by = 'city';
                break;
        }

        $query = self::find()
            ->select($select)
            ->alias('up')
            ->where(['up.id' => $this->id, 't.status' => 1])
            ->joinWith(['transactions t' => function($q_profile) {
                $q_profile->joinWith('transactionInfo ti');
                $q_profile->joinWith('transactionClient tc');
                $q_profile->joinWith(['property p' => function($q_property_location) {
                    $q_property_location->joinWith('propertyLocation pl');
                    $q_property_location->joinWith('transactionType tt');
                    $q_property_location->joinWith('type pt');
                }]);
            }])
            ->groupBy($group_by)
            ->orderBy($order_by);

        $query = $query->asArray();
        if ($best) {
            if (!$recursion) {
                $best_city = $this->getPerformance('city', true, true);
                $query = $query->andWhere(['pl.city' => $best_city['city']])->limit(1)->one();
                if (isset($best_city['city'])) {

                    switch ($group) {
                        case 'year' :
                            $group_value = $query['year'];
                            break;
                        case 'transaction_type':
                            $group_value = $query['type'];
                            break;
                        case 'property_type':
                            $group_value = $query['type'];
                            break;
                        default: //city
                            $group_value = '';
                            break;
                    }

                    $results = $this->getBestPerformanceByCity($best_city['city'], $group, $group_value);
                    $query['total_user'] = $results['total_user'];
                    $query['total_transaction'] = $results['total_transaction'];
                    $query['average'] = $results['average'];
                    $query['rank'] = $results['rank'];
                }
            } else {
                $query = $query->limit(1)->one();
            }
        } else {
            $query = $query->all();
        }

        return $query;
    }

    public function getBestPerformanceByCity($city, $type, $value)
    {
        if (!$this->id) {
            return false;
        }

        switch ($type) {
            case 'year':
                $and_where = ['YEAR(FROM_UNIXTIME(ti.selling_date))' => $value];
                break;
            case 'transaction_type':
                $and_where = ['tt.name' => $value];
                break;
            case 'property_type':
                $and_where = ['pt.name' => $value];
                break;
            default: //city
                $and_where = '';
                break;
        }

        $query = self::find()
            ->select([
                'id'        => 'up.id',
                'count'     => 'count(*)',
            ])
            ->alias('up')
            ->where(['t.status' => 1, 'pl.city' => $city])
            ->andWhere($and_where)
            ->joinWith(['transactions t' => function($q_profile) {
                $q_profile->joinWith('transactionInfo ti');
                $q_profile->joinWith('transactionClient tc');
                $q_profile->joinWith(['property p' => function($q_property_location) {
                    $q_property_location->joinWith('propertyLocation pl');
                    $q_property_location->joinWith('transactionType tt');
                    $q_property_location->joinWith('type pt');
                }]);
            }])
            ->groupBy('up.id')
            ->orderBy('count DESC');

        $list = $query->orderBy('count DESC')->asArray()->all();
        $total_user = $query->count();
        $total_transaction = $query->sum('count');
        $average = ($total_transaction/$total_user);
        $rank = 0;
        foreach ($list as $index => $item) {
            if ((int)$item['id'] == $this->id){
                $rank = ($index + 1);
            }
        }

        return [
            'rank'                    => $rank,
            'average'                 => $average,
            'total_user'              => $total_user,
            'total_transaction'       => $total_transaction,
            'list'                    => $list,
        ];
    }

    /**
     * Get user profile photo url
     * @return string
     */
    public function getPhotoUrl()
    {
        return $this->photo ? Url::to('@uri_user_avatar/'.$this->photo, true) : Url::to('@uri_img/'.self::DEFAULT_PHOTO, true);
    }

    public function getPagination()
    {
        return $this->pagination;
    }

    /**
     * @param $query
     * @return mixed
     */
    public function setPagination($query, $pagination_size, $page_param)
    {
        $count = $query->count();
        $this->pagination = new Pagination([
            'totalCount'        => $count,
            'defaultPageSize'   => $pagination_size,
            'pageParam'         => $page_param,
        ]);

        return $count;
    }

    public function convertBioZipCodesToAreas()
    {

    }

    public function afterSave($insert, $changedAttributes)
    {
        //var_dump($changedAttributes);
        //exit();
        if ($insert) {
            $related_records = $this->getRelatedRecords();
            /** AREAS */
            if (isset($related_records['areas'])) {
                foreach ($related_records['areas'] as $index => $area) {
                    $this->link('areas', $area);
                }
            }
        }

        // Zoho integrate
        if ($this->scenario == UserProfile::SCENARIO_CREATE_ACCOUNT) {
            $zoho = Yii::createObject(['class' => ZohoIntegration::className()]);
            //$zoho->synchronizeUserProfile($this);
            $this->user->id_zoho = $zoho->synchronizeUserInfoViaUserProfileModel($this);
            $this->user->save();
        }
        
        // Convert BIOS
        //if (isset($changedAttributes['zip_codes_daily'])) {
            //$google_place_implantation = \Yii::createObject(['class' => GooglePlaceImplantation::className()]);
            //$google_place_implantation->convertZipCodesToUserArea($this->getAttribute('id'), $this->getAttribute('zip_codes_daily'));
        //}
        //if (isset($changedAttributes['zip_codes_high_turnover'])) {
            //$google_place_implantation = \Yii::createObject(['class' => GooglePlaceImplantation::className()]);
            //$google_place_implantation->convertZipCodesToUserArea($this->getAttribute('id'), $this->getAttribute('zip_codes_high_turnover'));
        //}

        parent::afterSave($insert, $changedAttributes); // TODO: Change the autogenerated stub
    }
}