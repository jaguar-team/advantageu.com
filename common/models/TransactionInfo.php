<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\validators\CompareValidator;

/**
 * This is the model class for table "{{%transaction_info}}".
 *
 * @property int $id
 * @property int $id_transaction
 * @property string $mls
 * @property string $mls_number
 * @property string $financing_type
 *
 * @property Transaction $transaction
 */
class TransactionInfo extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%transaction_info}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            //id transaction rules
            //['id_transaction', 'required'],
            ['id_transaction', 'integer'],
            ['id_transaction', 'unique'],
            ['id_transaction', 'exist', 'skipOnError' => true, 'targetClass' => Transaction::className(), 'targetAttribute' => ['id_transaction' => 'id']],

            //mls rules
            ['mls', 'string', 'max' => 255],

            //mls rules
            ['mls_number', 'string', 'max' => 255],

            //mls rules
            ['financing_type', 'string', 'max' => 255],

            // selling_price rules
            ['selling_price', 'double'],

            //listing_price rules
            ['listing_price', 'required'],
            ['listing_price', 'double'],

            //selling_date rules
            ['selling_date', 'safe'],
            ['selling_date', 'compare', 'compareAttribute' => 'listing_date', 'operator' => '>=', 'type' => CompareValidator::TYPE_NUMBER],
            ['selling_date', 'date', 'timestampAttribute' => 'selling_date'],
            ['selling_date', 'filter', 'filter' => function($value) {
                if ($value) {
                    return Yii::$app->formatter->asTimestamp($value);
                } else {
                    return NULL;
                }
            }],

            //listing_date rules
            ['listing_date', 'required'],
            ['listing_date', 'date', 'timestampAttribute' => 'listing_date'],
            ['listing_date', 'compare', 'compareAttribute' => 'selling_date', 'operator' => '<=', 'type' => CompareValidator::TYPE_NUMBER],
            ['listing_date', 'filter', 'filter' => function($value) {
                if ($value) {
                    return Yii::$app->formatter->asTimestamp($value);
                } else {
                    return NULL;
                }
            }],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_transaction' => 'Id Transaction',
            'mls' => 'Mls',
            'mls_number' => 'Mls Number',
            'financing_type' => 'Financing Type',
            'selling_price' => 'Selling price',
            'listing_price' => 'Listing price',
            'selling_date' => 'Selling date',
            'listing_date' => 'Listing date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransaction()
    {
        return $this->hasOne(Transaction::className(), ['id' => 'id_transaction']);
    }
}
