<?php
/**
 * Created by PhpStorm.
 * User: Vlad
 * Date: 21.04.2017
 * Time: 17:05
 */

namespace common\models;

use Yii;
use yii\web\HttpException;
use yii\base\Model;
use common\models\Area;

class MultipleAreaForm extends Model
{
    /** @var \common\models\Area  */
    public $_area;

    /** @var  array */
    public $areas;

    /**
     * MultipleAreaForm constructor.
     * @param \common\models\Area $area
     * @param array $config
     */
    public function __construct(Area $area, array $config =[])
    {
        $this->_area = $area;
        parent::__construct($config);
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['areas', 'required'],
            ['areas', 'filter', 'filter' => function($areas) {
                return $this->filterAreas($areas);
            }],
        ];
    }

    /**
     * @return array
     */
    public function attributes()
    {
        return [
            'areas' => 'Areas',
        ];
    }

    /**
     * @param $areas
     * @return array
     * @throws HttpException
     * @throws \yii\base\InvalidConfigException
     */
    public function filterAreas($areas)
    {
        if (is_array($areas)) {
            throw new HttpException(500, 'Property $areas should be string ( Format JSON ).');
        }

        $areas = json_decode($areas, true);
        
        foreach ($areas as $index => $area) {

            $_area = \Yii::createObject([ 
                'class'         => Area::className(),
                'scenario'      => Area::SCENARIO_MULTIPLE_INSERT,
                'state'         => isset($area['state']) ? $area['state'] : '',
                'short_name'    => isset($area['short_name']) ? $area['short_name'] : '',
                'region'        => isset($area['region']) ? $area['region'] : '',
                'city'          => isset($area['city']) ? $area['city'] : '',
            ]);

            if ($_area->validate()) {
                $areas[$index] = $_area;
            } else {
                $this->addErrors($_area->getErrors());
                break;
            }
        }

        return $areas;
    }
}