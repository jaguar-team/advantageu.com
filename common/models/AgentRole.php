<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%agent_role}}".
 *
 * @property int $id
 * @property int $is_list
 * @property int $is_sell
 *
 * @property TransactionAgent $transactionAgent
 */
class AgentRole extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%agent_role}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['is_list', 'is_sell'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'is_list' => 'Is List',
            'is_sell' => 'Is Sell',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransactionAgent()
    {
        return $this->hasOne(TransactionAgent::className(), ['id_role' => 'id']);
    }
}
