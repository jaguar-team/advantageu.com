<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%transaction_agent}}".
 *
 * @property int $id
 * @property int $id_transaction
 * @property int $id_role
 * @property string $price
 * @property string $agent_license_number
 * @property string $agent_name
 * @property string $agent_office_name
 * @property int $date
 *
 * @property AgentRole $role
 * @property Transaction $transaction
 */
class TransactionAgent extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%transaction_agent}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            //id transaction rules
            //['id_transaction', 'required'],
            ['id_transaction', 'integer'],
            ['id_transaction', 'exist', 'skipOnError' => true, 'targetClass' => Transaction::className(), 'targetAttribute' => ['id_transaction' => 'id']],

            //id_role rules
            ['id_role', 'required'],
            ['id_role', 'integer'],
            ['id_role', 'exist', 'skipOnError' => true, 'targetClass' => UserRole::className(), 'targetAttribute' => ['id_role' => 'id']],

            //agent license_number rules
            ['agent_license_number', 'string', 'max' => 255],

            //agent name rules
            ['agent_name', 'string', 'max' => 255],

            //agent office name rules
            ['agent_office_name', 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_transaction' => 'Id Transaction',
            'id_role' => 'Id Role',
            'price' => 'Price',
            'agent_license_number' => 'Agent License Number',
            'agent_name' => 'Agent Name',
            'agent_office_name' => 'Agent Office Name',
            'date' => 'Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserRole()
    {
        return $this->hasOne(UserRole::className(), ['id' => 'id_role']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransaction()
    {
        return $this->hasOne(Transaction::className(), ['id' => 'id_transaction']);
    }
}
