<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%property_status}}".
 *
 * @property int $id
 * @property string $name
 *
 * @property Property[] $properties
 */
class PropertyStatus extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%property_status}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            //name rules
            ['name', 'required'],
            ['name', 'string', 'max' => 255],
            ['name', 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProperties()
    {
        return $this->hasMany(Property::className(), ['id_status' => 'id']);
    }
}
