<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%user_address}}".
 *
 * @property int $id
 * @property int $id_user
 * @property string $state
 * @property string $city
 * @property string $address
 * @property string $short_name
 * @property string $zip_code
 * @property string $lat
 * @property string $lng
 * @property string $place_id
 *
 * @property User $user
 */
class UserAddress extends \yii\db\ActiveRecord
{
    CONST SCENARIO_ZOHO_CONSOLE = 'zohoConsole';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user_address}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['state', 'city'], 'required'],
            [['id_user'], 'integer'],
            [['state', 'city', 'address', 'short_name', 'zip_code', 'lat', 'lng', 'place_id'], 'string', 'max' => 255],
            [['id_user'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['id_user' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_user' => 'Id User',
            'state' => 'State',
            'city' => 'City',
            'address' => 'Address',
            'short_name' => 'Short Name',
            'zip_code' => 'Zip Code',
            'lat' => 'Lat',
            'lng' => 'Lng',
            'place_id' => 'Place ID',
        ];
    }

    public function zohoAttributes()
    {
        return [
            'fields' => [
                'State'		=> ['getAttribute' => function() { return $this->state; }, 'setAttribute' => function($state) { return $this->state = $state; }],
                'City'	    => ['getAttribute' => function() { return $this->city; }, 'setAttribute' => function($city) { return $this->city = $city; }],
                'Street'	=> ['getAttribute' => function() { return $this->address; }, 'setAttribute' => function($address) { return $this->address = $address; }],
                'Address 1'	=> ['getAttribute' => function() { return $this->address; }, 'setAttribute' => function($address) { return $this->address = $address; }],
                'Zip Code'	=> ['getAttribute' => function() { return $this->zip_code; }, 'setAttribute' => function($zip_code) { return $this->zip_code = $zip_code; }],
            ],
        ];
    }

    /**
     * @inheritdoc
     * @return array
     */
    public function scenarios()
    {
        return ArrayHelper::merge([
            self::SCENARIO_ZOHO_CONSOLE     => [],
        ], parent::scenarios());
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'id_user']);
    }

    /**
     * @inheritdoc
     * @return \common\models\query\UserAddressQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\UserAddressQuery(get_called_class());
    }
}
