<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%favorite}}".
 *
 * @property int $id
 * @property int $id_user
 * @property int $id_favorite
 *
 * @property User $user
 */
class Favorite extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%favorite}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            //id user rules
            ['id_user', 'required'],
            ['id_user', 'integer'],
            ['id_user', 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['id_user' => 'id']],

            //id favorite rules
            ['id_favorite', 'required'],
            ['id_favorite', 'integer'],
            ['id_favorite', 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['id_user' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_user' => 'Id User',
            'id_favorite' => 'Id Favorite',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'id_user']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFavoriteUser()
    {
        return $this->hasOne(User::className(), ['id' => 'id_favorite']);
    }

    /**
     * @inheritdoc
     * @return \common\models\query\FavoriteQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\FavoriteQuery(get_called_class());
    }
}
