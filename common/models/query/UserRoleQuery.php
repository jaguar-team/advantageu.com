<?php

namespace common\models\query;

/**
 * This is the ActiveQuery class for [[UserRole]].
 *
 * @see UserRole
 */
class UserRoleQuery extends \yii\db\ActiveQuery
{
	/**
	 * @return $this
	 */
    public function _default()
    {
        return $this->andWhere(['default' => 1]);
    }

	/**
	 * @return $this
	 */
    public function admin($or = false)
    {
	    return $or ? $this->orWhere(['is_admin' => 1]) : $this->andWhere(['is_admin' => 1]);
    }

	/**
	 * @return $this
	 */
	public function agent($or = false)
	{
		return $or ? $this->orWhere(['is_agent' => 1]) : $this->andWhere(['is_agent' => 1]);
	}

	/**
	 * @return $this
	 */
	public function seller($or = false)
	{
		return $or ? $this->orWhere(['is_seller' => 1]) : $this->andWhere(['is_seller' => 1]);
	}

	/**
	 * @return $this
	 */
	public function buyer($or = false)
	{
		return $or ? $this->orWhere(['is_buyer' => 1]) : $this->andWhere(['is_buyer' => 1]);
	}

	/**
	 * @param null $db
	 *
	 * @return array|\yii\db\ActiveRecord[]
	 */
    public function all($db = null)
    {
	    return parent::all($db);
    }

	/**
	 * @param null $db
	 *
	 * @return array|null|\yii\db\ActiveRecord
	 */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
