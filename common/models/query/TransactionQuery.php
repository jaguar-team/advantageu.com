<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 022 22.06.17
 * Time: 18:16
 */

namespace common\models\query;

use yii\db\ActiveQuery;

class TransactionQuery extends ActiveQuery {

	/**
	 * @param null $db
	 *
	 * @return array|\yii\db\ActiveRecord[]
	 */
	public function all($db = null)
	{
		return parent::all($db);
	}

	/**
	 * @param null $db
	 *
	 * @return array|null|\yii\db\ActiveRecord
	 */
	public function one($db = null)
	{
		return parent::one($db);
	}

}