<?php
namespace common\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use yii\helpers\ArrayHelper;
use yii\data\Pagination;
use common\models\UserRole;
use common\models\UserProfile;
use common\components\Zoho;
use common\models\query\UserQuery;
use common\models\relation\UserTransactionRelation;

/**
 * User model
 *
 * @property integer $id
 * @property integer $id_role
 * @property integer $id_zoho
 * @property integer $id_bio
 * @property integer $id_google
 * @property integer $id_facebook
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $auth_key
 * @property integer $status
 * @property boolean $top
 * @property integer $certificate
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $password write-only password
 */
class User extends ActiveRecord implements IdentityInterface
{
    CONST SCENARIO_AUTO_PASSWORD = 'autoPassword';
    CONST SCENARIO_ZOHO_CONSOLE = 'zohoConsole';
    CONST SCENARIO_CREATE_ACCOUNT = 'createAccount';

    const STATUS_ACTIVE = 1;
    const STATUS_NOT_ACTIVE = 0;

    const NOT_CERTIFICATE = 0;
    const CERTIFICATE = 1;

    const PRIVATE_ACCOUNT = 1;
    const NOT_PRIVATE_ACCOUNT = 0;

    const PRIVATE_PROPERTY = 1;
    const NOT_PRIVATE_PROPERTY = 0;

    const TOP = 1;
    const NOT_TOP = 0;

    public $user_transaction_relation;

    public $transaction_quantity;

    public $transaction_quantity_by_type;

    public $average_price;

    public $average_days_on_market;

    public $pagination;

    /** public $google_id; **/

    public function __construct(array $config = [])
    {
        parent::__construct($config);
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            //id_role rules
            ['id_role', 'required'],
            ['id_role', 'integer'],
            ['id_role', 'exist', 'targetClass' => UserRole::className(), 'targetAttribute' => ['id_role' => 'id']],

            //email rules
            ['email', 'required'],
            ['email', 'string', 'max' => 255],
            ['email', 'email'],
            ['email', 'trim'],
            ['email', 'unique', 'targetClass' => User::className(), 'message' => 'This email is already exists.'],


            //status rules
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_NOT_ACTIVE]],

            //password hash
            ['password_hash', 'required', 'except' => [User::SCENARIO_AUTO_PASSWORD]],
            ['password_hash', 'string', 'min' => 6, 'max' => 255],
            ['password_hash', 'safe'],

            //auth key
            ['auth_key', 'safe'],

            //password reset token
            ['password_reset_token', 'safe'],

	        //id zoho rules
	        ['id_zoho', 'integer'],
            ['id_zoho', 'unique', 'targetClass' => User::className(), 'message' => 'This Zoho Id is already exists.'],

            //id zoho rules
            ['id_bio', 'integer'],
            ['id_bio', 'unique', 'targetClass' => User::className(), 'message' => 'This BIO Id is already exists.'],

	        //id google rules
	        ['id_google', 'integer'],

	        //id facebook rules
	        ['id_facebook', 'integer'],

            //certificate rules
            ['certificate', 'integer'],
            ['certificate', 'default', 'value' => self::NOT_CERTIFICATE],

            //certificate rules
            ['private_account', 'integer'],
            ['private_account', 'default', 'value' => self::NOT_PRIVATE_ACCOUNT],

            //certificate rules
            ['private_property', 'integer'],
            ['private_property', 'default', 'value' => self::NOT_PRIVATE_PROPERTY],

            //status rules
            ['top', 'integer'],
            ['top', 'default', 'value' => self::NOT_TOP],
        ];
    }

    /**
     * @inheritdoc
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id_role'       => 'Role',
            'email'         => 'Email',
            'password_hash' => 'Password',
        ];
    }

    public function zohoAttributes()
    {
        return [
            'primaryKey' => 'Email',
            'fields' => [
                'Email'				        => ['getAttribute' => function() { return $this->email; }, 'setAttribute' => function($email) { return $this->email = $email; }, 'column_name' => 'email'],
                'LEADID'			        => ['getAttribute' => function() { return $this->id_zoho; }, 'setAttribute' => function($id_zoho) { return $this->id_zoho = $id_zoho; }],
                'CUSTOMMODULE2_ID'	        => ['getAttribute' => function() { return $this->id_bio; }, 'setAttribute' => function($id_bio) { return $this->id_bio = $id_bio; }],
                'CUSTOMMODULE7_ID'	        => ['getAttribute' => function() { return $this->id_bio; }, 'setAttribute' => function($id_bio) { return $this->id_bio = $id_bio; }],
                'Division'			        => ['required' => true, 'getAttribute' => function() { return $this->role ? $this->role->name : NULL; }, 'setAttribute' => function($role_name = false) {
                    if ($role_name && $role_model = UserRole::find()->where(['name' => $role_name])->limit(1)->one()) {
                        return $this->id_role = $role_model->name;
                    } else {
                        return $this->id_role = UserRole::find()->agent()->_default()->select(['id'])->scalar();
                    }
                }],
            ],
        ];
    }

    /**
     * @inheritdoc
     * @return array
     */
    public function scenarios()
    {
        return ArrayHelper::merge([
            self::SCENARIO_AUTO_PASSWORD    => [],
            self::SCENARIO_ZOHO_CONSOLE     => [],
            self::SCENARIO_CREATE_ACCOUNT   => [],
        ], parent::scenarios());
    }

    /**
     * @inheritdoc
     */
    public function transactions()
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_INSERT | self::OP_UPDATE,
        ];
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return bool
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * Check user status
     * @return bool
     */
    public function isAdmin()
    {
        return $this->role->is_admin ? true : false;
    }

    /**
     * Check user activation
     * @return bool
     */
    public function isActive()
    {
        return $this->status ? true : false;
    }

    /**
     * Check is owner
     * @return bool
     */
    public function isOwner()
    {
        if (\Yii::$app->user->isGuest) {
            return false;
        } else {
            return \Yii::$app->user->id == $this->id ? true : false;
        }
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * Get User role
     * @return \yii\db\ActiveQuery
     */
    public function getRole()
    {
        return $this->hasOne(UserRole::className(), ['id' => 'id_role']);
    }

    /**
     * Get user profile
     * @return \yii\db\ActiveQuery
     */
    public function getProfile()
    {
        return $this->hasOne(UserProfile::className(), ['id_user' => 'id']);
    }

    /**
     *
     */
    public function getAreas()
    {
        return $this->hasMany(Area::className(), ['id_user_profile' => 'id'])->via('profile');
    }

    /**
     *
     */
    public function getLanguages()
    {
        return $this->hasMany(Language::className(), ['id_user_profile' => 'id'])->via('profile');
    }

    /**
     * @return ActiveQuery
     */
    public function getSectionValues()
    {
        return $this->hasMany(Area::className(), ['id_user' => 'id']);
    }

    /**
     * Variable: reviews
     * @return ActiveQuery
     */
    public function getReviews($pagination_size = 20)
    {
        $query = $this->hasMany(Review::className(), ['id_user' => 'id']);

        if ($pagination_size && $pagination_size > 0) {
            if ($this->setPagination($query, $pagination_size, 'review-page') > 0) {
                $query =  $query->offset($this->pagination->offset)->limit($this->pagination->limit);
            }
        }

        return $query;
    }

    /**
     * @return ActiveQuery
     */
    public function getFavorites()
    {
        return $this->hasMany(Favorite::className(), ['id_user' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getAddedFavorites()
    {
        return $this->hasMany(Favorite::className(), ['id_favorite' => 'id']);
    }

    /**
     * Get count reviews
     * Variable: averageRatingReviews
     * @return false|null|string
     */
    public function getAverageRatingReviews()
    {
        return $this->getReviews()
            ->select(['rating' => 'ROUND(SUM(star)/COUNT(id), 1)'])
            ->scalar();
    }

    /**
     * Get performance reviews
     * Variable: performanceReviews
     * @return ActiveQuery
     */
    public function getPerformanceReviews()
    {
        $count = $this->getReviews()->count();

        return $this->getReviews()->select([
                'count'      => 'COUNT(star)',
                'rating'     => 'ROUND(star)',
                'percent'    => 'ROUND((COUNT(star) /'.$count.') * 100)',
            ])
            ->groupBy('rating')->indexBy('rating')->asArray();
    }

    /**
     * Variable: addresses
     * @return null|\yii\db\ActiveQuery
     */
    public function getAddresses()
    {
        return $this->hasMany(UserAddress::className(), ['id_user' => 'id']);
    }

    /**
     * Variable: transactions
     * @return null|\yii\db\ActiveQuery
     */
    public function getTransactions($pagination_size = 20)
    {
        if ($this->role) {
            if ($this->role->is_agent) {
                $query = $this->hasMany(Transaction::className(), ['id_agent' => 'id']);
            } else {
                $query = $this->hasMany(Transaction::className(), ['id_client' => 'id']);
            }
        } else {
            $query = $this->hasMany(Transaction::className(), ['id_agent' => 'id']);
        }

        if ($pagination_size && $pagination_size > 0) {
            if ($this->setPagination($query, $pagination_size, 'transaction-page') > 0) {
                $query =  $query->offset($this->pagination->offset)->limit($this->pagination->limit);
            }
        }

        return $query;
    }

    /**
     * @param $query
     * @return mixed
     */
    public function setPagination($query, $pagination_size, $page_param)
    {
        $count = $query->count();
        $this->pagination = new Pagination([
            'totalCount'        => $count,
            'defaultPageSize'   => $pagination_size,
            'pageParam'         => $page_param,
        ]);

        return $count;
    }

    /**
     * @return array|null|ActiveRecord
     */
    public function getBestYear()
    {
        if ($this->isNewRecord) {
            return null;
        }

        return $this->getTransactions()
            ->select([
                'year' => 'YEAR(FROM_UNIXTIME(ti.selling_date))',
            ])
            ->groupBy('year')
            ->limit(1)
            ->asArray()->one();
    }


    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

	/**
	 * @inheritdoc
	 * @return UserQuery the active query used by this AR class.
	 */
	public static function find()
	{
		return new UserQuery(get_called_class());
	}

    /**
     * @return object
     */
	public function getTransactionRelation()
    {
        if (!$this->user_transaction_relation) {
            $this->user_transaction_relation = \Yii::createObject(['class' => UserTransactionRelation::className()], [$this]);
        }

        return $this->user_transaction_relation;
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if ($insert) {
            $this->setPassword($this->password_hash);
            $this->generateAuthKey();
            $this->generatePasswordResetToken();
        }

        return parent::beforeSave($insert);
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        if ($insert) {
            if ($related_records = $this->getRelatedRecords()) {

                /** PROFILE */
                if (isset($related_records['profile'])) {
                    $this->link('profile', $related_records['profile']);
                    //$this->link('profile', $related_records['profile']);
                }

                /** ADDRESSES */
                if (isset($related_records['addresses'])) {
                    foreach ($related_records['addresses'] as $index => $related_record) {
                        $this->link('addresses', $related_record);
                    }
                }
            }
            //if not exist user profile
            if (!$this->profile) {
                $user_profile_model = \Yii::createObject(['class' => UserProfile::className()]);
                $this->link('profile', $user_profile_model);
            }
        }

        parent::afterSave($insert, $changedAttributes);
    }
}