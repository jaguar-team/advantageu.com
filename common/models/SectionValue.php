<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%section_value}}".
 *
 * @property int $id
 * @property int $id_user
 * @property int $id_section
 * @property string $value
 *
 * @property Section $section
 * @property User $user
 */
class SectionValue extends \yii\db\ActiveRecord
{
    CONST SCENARIO_SKIP_ID_USER = 'skip_id_user';
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%section_value}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            //id user rules
            ['id_user', 'required', 'except' => ['except' => self::SCENARIO_SKIP_ID_USER]],
            ['id_user', 'integer'],
            ['id_user', 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['id_user' => 'id']],

            //id section rules
            ['id_section', 'required'],
            ['id_section', 'integer'],
            ['id_section', 'exist', 'skipOnError' => true, 'targetClass' => Section::className(), 'targetAttribute' => ['id_section' => 'id']],

            //value rules
            ['value', 'required'],
            ['value', 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_user' => 'Id User',
            'id_section' => 'Id Section',
            'value' => 'Value',
        ];
    }

    /**
     * @return array
     */
    public function scenarios()
    {
        return ArrayHelper::merge([
            self::SCENARIO_SKIP_ID_USER => ['id_user', 'id_section', 'value', 'city'],
        ], parent::scenarios());
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSection()
    {
        return $this->hasOne(Section::className(), ['id' => 'id_section']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'id_user']);
    }
}
