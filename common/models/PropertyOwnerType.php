<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%property_owner_type}}".
 *
 * @property int $id
 * @property string $name
 *
 * @property Property[] $properties
 */
class PropertyOwnerType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%property_owner_type}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProperties()
    {
        return $this->hasMany(Property::className(), ['id_property_owner_type' => 'id']);
    }
}
