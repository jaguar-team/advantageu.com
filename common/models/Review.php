<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "review".
 *
 * @property int $id
 * @property int $id_user
 * @property int $advantage
 * @property double $star
 * @property string $body
 * @property string $first_name
 * @property string $last_name
 * @property string $know_from
 * @property string $email
 * @property int $status
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Advantage $advantage0
 * @property UserProfile $userProfile
 */
class Review extends \yii\db\ActiveRecord
{
    CONST SCENARIO_SKIP_ID_USER = 'skip_id_user';

    CONST STATUS_ACTIVE = 1;
    CONST STATUS_NOT_ACTIVE = 0;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'review';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            //id user rules
            ['id_user', 'required', 'except' => self::SCENARIO_SKIP_ID_USER],
            ['id_user', 'integer'],
            ['id_user', 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['id_user' => 'id']],

            //title rules
            ['title', 'required'],
            ['title', 'string', 'min' => 1, 'max' => 255],

            //star rules
            ['star', 'required'],
            ['star', 'number'],

            //body rules
            ['body', 'required'],
            ['body', 'string'],

            //first name rules
            ['first_name', 'required'],
            ['first_name', 'trim'],
            ['first_name', 'string', 'min' => 2, 'max' => 255],

            //last name rules
            ['last_name', 'required'],
            ['last_name', 'trim'],
            ['last_name', 'string', 'min' => 2, 'max' => 255],

            //email rules
            ['email', 'required'],
            ['email', 'trim'],
            ['email', 'email'],

            //status rules
            ['status', 'boolean'],
            ['status', 'default', 'value' => 1],

            //created at rules
            ['created_at', 'integer'],

            //updated at rules
            ['updated_at', 'integer'],
        ];
    }

    /**
     * @return array
     */
    public function scenarios()
    {
        return ArrayHelper::merge([
            self::SCENARIO_SKIP_ID_USER => ['title', 'star', 'body', 'first_name', 'last_name', 'email', 'status'],
        ], parent::scenarios());
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_user' => 'Id User',
            'star' => 'Star',
            'body' => 'Body',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'email' => 'Email',
            'title' => 'Title',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdvantage()
    {
        return $this->hasOne(Advantage::className(), ['id' => 'advantage']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'id_user']);
    }
}
