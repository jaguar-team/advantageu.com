<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%customer_bid}}".
 *
 * @property int $id
 * @property int $id_customer
 * @property string $location
 * @property int $lead_type
 * @property int $property_type
 * @property int $price
 * @property int $period
 * @property int $other_action
 *
 * @property Customer $customer
 */
class CustomerBid extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%customer_bid}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_customer', 'location', 'lead_type', 'property_type', 'price', 'period', 'other_action'], 'required'],
            [['id_customer', 'lead_type', 'property_type', 'price', 'period', 'other_action'], 'integer'],
            [['location'], 'string', 'max' => 255],
            [['id_customer'], 'exist', 'skipOnError' => true, 'targetClass' => Customer::className(), 'targetAttribute' => ['id_customer' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_customer' => 'Id Customer',
            'location' => 'Location',
            'lead_type' => 'Lead Type',
            'property_type' => 'Property Type',
            'price' => 'Price',
            'period' => 'Period',
            'other_action' => 'Other Action',
        ];
    }

    /**
     * Get lead types names
     * @return array
     */
    public function leadTypeLabels()
    {
        return [
            1   => ['name' => 'Buy', 'desc' => 'Save an average of $23,000 per home purchase.', 'favicon' => 'key-modern'],
            2   => ['name' => 'Sell', 'desc' => 'Our agents sell homes an average of 23 days faster.', 'favicon' => 'tag'],
        ];
    }

    public function propertyTypeLabels()
    {
        return [
            1   => ['name' => 'Family Home', 'favicon' => 'home'],
            2   => ['name' => 'Condo', 'favicon' => 'building'],
            3   => ['name' => 'Townhouse', 'favicon' => 'townhouse'],
            4   => ['name' => 'Mobile Home', 'favicon' => 'mobilehome'],
            5   => ['name' => 'Vacant Lot', 'favicon' => 'square-o'],
            6   => ['name' => 'Commercial', 'favicon' => 'office'],
        ];
    }

    public function priceLabels()
    {
        return [
            1   => ['name' => '$70k or less'],
            2   => ['name' => '$70k - $100k'],
            3   => ['name' => '$100k - $165k'],
            4   => ['name' => '$165k - $320k'],
            5   => ['name' => '$320k - $750k'],
            6   => ['name' => '$750k or more'],
        ];
    }

    public function periodLabels()
    {
        return [
            1   => ['name' => 'ASAP'],
            2   => ['name' => '1-3 months'],
            3   => ['name' => '3-6 months'],
            4   => ['name' => '6-12 months'],
            5   => ['name' => '12+ months'],
            6   => ['name' => 'Not Sure'],
        ];
    }

    public function otherActionLabels()
    {
        return [
            0 => ['name' => 'No'],
            1 => ['name' => 'Yes'],
        ];
        /*return [
            1 => ['name' => 'Only Selling'],
            2 => ['name' => 'Only Buying'],
            3 => ['name' => 'Selling and Buying'],
            4 => ['name' => 'Need Consultation'],
        ];*/
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(Customer::className(), ['id' => 'id_customer']);
    }
}
