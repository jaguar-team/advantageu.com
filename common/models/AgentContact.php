<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%agent_contact}}".
 *
 * @property int $id
 * @property int $id_user_profile
 * @property string $full_name
 * @property string $email
 * @property string $phone
 * @property string $message
 * @property string $created_at
 * @property string $updated_at
 *
 * @property UserProfile $userProfile
 */
class AgentContact extends \yii\db\ActiveRecord
{
    CONST SCENARIO_SKIP_ID_PROFILE = 'skip_id_profile';
    CONST STATUS_UNREAD = 0;
    CONST STATUS_READ = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%agent_contact}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     * @return array
     */
    public function scenarios()
    {
        return ArrayHelper::merge([
            self::SCENARIO_SKIP_ID_PROFILE => ['id_user_profile', 'full_name', 'email', 'phone', 'message'],
        ], parent::scenarios());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            //id profile rules
            ['id_user_profile', 'required', 'except' => self::SCENARIO_SKIP_ID_PROFILE],
            ['id_user_profile', 'integer'],
            ['id_user_profile', 'exist', 'skipOnError' => true, 'targetClass' => UserProfile::className(), 'targetAttribute' => ['id_user_profile' => 'id']],

            //full name rules
            ['full_name', 'required'],
            ['full_name', 'trim'],
            ['full_name', 'string', 'min' => 2, 'max' => 255],
            ['full_name', 'match', 'pattern' => '/^[A-z]{1,100}\s[A-z]{1,100}$/i'],

            //email rules
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'trim'],

            //phone rules
            ['phone', 'required'],
            ['phone', 'string', 'max' => 255],
            ['phone', 'match', 'pattern' => '/^\([0-9]{3}\)[0-9]{3}-[0-9]{4}$/i'],

            //status rules
            ['status', 'default', 'value' => self::STATUS_UNREAD],
            ['status', 'in', 'range' => [self::STATUS_UNREAD, self::STATUS_READ]],

            ['message', 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'                => 'ID',
            'id_user_profile'   => 'Id User Profile',
            'full_name'         => 'Full Name',
            'email'             => 'Email',
            'phone'             => 'Phone',
            'message'           => 'Message',
            'created_at'        => 'Created At',
            'updated_at'        => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserProfile()
    {
        return $this->hasOne(UserProfile::className(), ['id' => 'id_user_profile']);
    }

    /**
     * Send email to customer
     * @return bool
     */
    public function sendCustomerEmail()
    {
        return \Yii::$app->mailer->compose('agent-contact/customer', [
            'agent_contact_model' => $this,
        ])
            ->setFrom('customer@'.\Yii::$app->getRequest()->serverName)
            ->setTo($this->email)
            ->setSubject('Your '.\Yii::$app->setting->getValueByName('site_name').' Real Estate Agent Inquiry')
            ->send();
    }

    /**
     * Send email to agent
     * @return bool
     */
    public function sendAgentEmail()
    {
        return \Yii::$app->mailer->compose('agent-contact/agent', [
            'agent_contact_model' => $this,
        ])
            ->setFrom('agent@'.\Yii::$app->getRequest()->serverName)
            ->setTo($this->email)
            ->setSubject('Your '.\Yii::$app->setting->getValueByName('site_name').' Real Estate Agent Inquiry')
            ->send();
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        if ($insert) {
            $this->sendCustomerEmail();
            $this->sendAgentEmail();
        }

        parent::afterSave($insert, $changedAttributes);
    }
}
