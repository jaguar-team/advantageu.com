<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "know_from".
 *
 * @property int $id
 * @property int $id_role
 * @property string $name
 * @property string $created_at
 * @property string $updated_at
 *
 * @property UserRole $role
 */
class KnowFrom extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%know_from}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_role', 'name'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['id_role', 'created_at', 'updated_at'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['id_role'], 'exist', 'skipOnError' => true, 'targetClass' => UserRole::className(), 'targetAttribute' => ['id_role' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_role' => 'Id Role',
            'name' => 'Name',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRole()
    {
        return $this->hasOne(UserRole::className(), ['id' => 'id_role']);
    }

    /**
     * @param $id_role
     * @return array|\yii\db\ActiveRecord[]
     */
    public function getByIdRole($id_role)
    {
        return static::find()->where(['id_role' => $id_role])->indexBy('id')->asArray()->all();
    }

    /**
     * @return array
     */
    public function getDropdown()
    {
        return static::find()->select(['name'])->indexBy('id')->column();
    }

    /**
     * @param $id_role
     * @return array
     */
    public function getDropdownByUserRole($id_role)
    {
        return static::find()->where(['id_role' => $id_role])->select(['name'])->indexBy('id')->column();
    }
}
