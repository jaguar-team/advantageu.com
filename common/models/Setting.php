<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "{{%setting}}".
 *
 * @property int $id
 * @property string $name
 * @property string $value
 * @property string $created_at
 * @property string $upadted_at
 */
class Setting extends ActiveRecord
{
    //public $primaryKey = 'name';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%setting}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            //name rules
            ['name', 'required'],
            ['name', 'string', 'max' => 255],
            //['name', 'unique'],

            //value rules
            ['value', 'required'],
            ['value', 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'value' => 'Value',
            'created_at' => 'Created At',
            'upadted_at' => 'Upadted At',
        ];
    }

    /**
     * @param $name
     * @return static
     */
    public static function findByName($name)
    {
        return Setting::findOne(['name' => $name]);
    }

    /**
     * @param $name
     * @return false|null|string
     */
    public function getValueByName($name)
    {
        return Setting::find()->where(['name' => $name])->select(['value'])->limit(1)->scalar();
    }

    /**
     * @param bool $runValidation
     * @param null $attributeNames
     * @return bool|int
     * @throws \Exception
     * @throws \Throwable
     */
    public function save($runValidation = true, $attributeNames = null)
    {
        if ($runValidation) {
            if (!$this->validate()) {
                return false;
            }
        }

        if ($setting = Setting::findByName($this->name)) {
            $setting->value = $this->value;
            return $setting->update();
        } else {
            return $this->insert();
        }
    }
}