<?php

namespace common\components;

use Yii;
use yii\base\Object;
use yii\helpers\Url;
use common\models\User;
use common\models\UserProfile;

class GoogleAuth extends Object
{
	const URL = 'https://accounts.google.com/o/oauth2/auth';
    const TOKEN_URL = 'https://accounts.google.com/o/oauth2/token';
    const PERSONAIL_INFO_URL = 'https://www.googleapis.com/oauth2/v1/userinfo';
    const RESPONSE_TYPE = 'code';
    const GRANT_TYPE = 'authorization_code';
    const SCOPE = 'https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/userinfo.profile';
    const CLIENT_ID = '295296516687-luj1ajo1hq72o5ma9efu5raclv94q5cn.apps.googleusercontent.com';
    const SECRET_ID = 'PQyFkqZ78rYgPzIVdCWwjRjg';

    public $access_token;

    public $id_token;

    public $user_info = [];

    public function getUrl($tag = false)
    {
        $params = [
            'redirect_uri'  => $this->getRedirectUrl(),
            'response_type' => self::RESPONSE_TYPE,
            'client_id'     => self::CLIENT_ID,
            'scope'         => self::SCOPE,
        ];

        return self::URL.'?'.urldecode(http_build_query($params));
    }

    public function getToken()
    {
        if ($code = \Yii::$app->request->get('code')) {
            $params = [
                'client_id'     => self::CLIENT_ID,
                'client_secret' => self::SECRET_ID,
                'redirect_uri'  => $this->getRedirectUrl(),
                'grant_type'    => self::GRANT_TYPE,
                'code'          => $code
            ];

            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, self::TOKEN_URL);
            curl_setopt($curl, CURLOPT_POST, 1);
            curl_setopt($curl, CURLOPT_POSTFIELDS, urldecode(http_build_query($params)));
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            $token_info = json_decode(curl_exec($curl));
            curl_close($curl);

            if (!isset($token_info->error) && isset($token_info->access_token) && isset($token_info->id_token)) {
            	$this->access_token = $token_info->access_token;
            	$this->id_token = $token_info->id_token;
            }
        }

        return $this;
    }

    public function setUserInfo()
    {
    	if ($this->access_token && $this->id_token) {
    		$params = ['access_token' => $this->access_token];

    		$this->user_info = json_decode(file_get_contents(self::PERSONAIL_INFO_URL.'?'.urldecode(http_build_query($params))));
    	}

    	return $this;
    }

    public function setUserModel()
    {
	    $user = \Yii::$app->userIdentity;
	    $user->email = isset($this->user_info->email) ? $this->user_info->email : '';
	    $user->google_id = isset($this->user_info->id) ? $this->user_info->id : '';

	    return $this;
    }

    public function setUserProfileModel()
    {
    	$user_profile = \Yii::$app->userProfile;
    	$user_profile->first_name = isset($this->user_info->given_name) ? $this->user_info->given_name : '';
	    $user_profile->last_name = isset($this->user_info->family_name) ? $this->user_info->family_name : '';

	    return $this;
    }

    public function getRedirectUrl()
    {
    	return Url::home(true);
    }

    public function isExistsCode()
    {
    	return \Yii::$app->request->get('code') ? true : false;
    }

    public function isShowPassword()
    {
    	return isset($this->user_info->id) ? true : false;
    }
}
