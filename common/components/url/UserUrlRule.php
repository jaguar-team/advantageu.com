<?php

namespace common\components\url;

use yii\web\UrlRuleInterface;
use yii\base\Object;
use common\models\User;

class UserUrlRule extends Object implements UrlRuleInterface
{
    public $routes = [
        'agent/profile',
        'agent/review',
        'agent/add-property'
    ];

    public function createUrl($manager, $route, $params)
    {
        if (!isset($params['id']) || !in_array($route, $this->routes)) {
            return false;
        }

        $user = User::findOne($params['id']);
        // init get params
        $get = [];
        $match__key_params = preg_grep("/^([A-z])+\-page$/", array_keys($params));
        foreach ($match__key_params as $key) {
            $get[$key] = $params[$key];
        }
        $get = http_build_query($get);

        if ($user && $role = $user->role->getName()) {
            // user profile
            if ($route == 'agent/profile') {
                return $role.'/'.$params['id'].($get ? '?'.$get : '');
            }
            // user reviews
            if ($route == 'agent/review') {
                return $role.'/'.$params['id'].'/reviews'.($get ? '?'.$get : '');
            }
            // user reviews
            if ($route == 'agent/add-property') {
                return $role.'/'.$params['id'].'/add-property'.($get ? '?'.$get : '');
            }
        }

        return false;
    }

    public function parseRequest($manager, $request)
    {
        $pathInfo = trim($request->getPathInfo(), '/');
        $url_data = explode('/', $pathInfo);

        if (preg_match('/^(buyer|seller|agent|admin)\/(\d)+(\/)?(.)*$/', $pathInfo, $matches)) {

            if (isset($url_data[1])) {
                $params['id'] = $url_data[1];
                $user = User::findOne($params['id']);

                if (!$user || !$user->role || ($url_data[0] != $user->role->getName())) {
                    return false;
                }

                // user profile
                if (preg_match('%^(buyer|seller|agent|admin)\/\d*$%', $pathInfo, $matches)) {
                    return ['agent/profile', $params];
                }
                // user reviews
                if (preg_match('%^(buyer|seller|agent|admin)\/\d*\/reviews$%', $pathInfo, $matches)) {
                    return ['agent/review', $params];
                }
                // user properties
                if (preg_match('%^(buyer|seller|agent|admin)/\d*\/add-property%', $pathInfo, $matches)) {
                    return ['agent/add-property', $params];
                }
            }

            return false;
        }

        return false;
    }
}