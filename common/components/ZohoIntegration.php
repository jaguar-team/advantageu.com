<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 015 15.08.17
 * Time: 19:38
 */

namespace common\components;

use yii;
use yii\helpers\ArrayHelper;
use yii\base\InvalidConfigException;
use common\models\PropertyType;
use common\models\UserProfile;

class ZohoIntegration extends Zoho
{
    /**
     * Synchronize info FROM User and UserProfile model
     * @param UserProfile $user_profile
     * @return int|null ( ID ZOHO )
     */
    public function synchronizeUserInfoViaUserProfileModel(UserProfile &$user_profile)
    {
        $accordance_data = ArrayHelper::merge($user_profile->zohoAttributes()['fields'], $user_profile->user->zohoAttributes()['fields']);

        if ($user_profile->user->role->is_seller || $user_profile->role->is_buyer) {
            $this->setModule(Zoho::MODULE_CLIENT);
        } else {
            $this->setModule(Zoho::MODULE_AGENT);
        }

        return $this->setAccordanceData($accordance_data)->saveRecord($this->getIdZoho());
    }

    /**
     * Synchronize BIO info FROM User and UserProfile model
     * @param UserProfile $user_profile
     * @return int|null ( ID BIO )
     */
    public function synchronizeUserBioInfoViaUserProfileModel(UserProfile &$user_profile)
    {
        $accordance_data = ArrayHelper::merge($user_profile->zohoAttributes()['fields'], $user_profile->user->zohoAttributes()['fields']);

        if ($user_profile->user->role->is_agent) {
            return $this
                ->setModule(Zoho::MODULE_AGENT_BIO)
                ->setAccordanceData($accordance_data)
                ->saveRecord($this->getIdZoho());
        }

        return NULL;
    }
}