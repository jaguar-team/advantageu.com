<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 024 24.08.17
 * Time: 16:30
 */

namespace common\components;

use common\models\UserProfile;
use yii\base\Object;
use common\models\Area;

class GooglePlaceImplantation extends Object
{
    CONST STATUS_SUCCESS = 'OK';

    /**
     * Convert all bios zip codes to user area
     * @return array
     */
    public function convertAllBiosZipCodesToUserArea()
    {
        $user_profiles = UserProfile::find()
            ->select(['id', 'zip_codes_daily', 'zip_codes_high_turnover'])
            ->asArray()
            ->indexBy('id')
            ->all();

        $added_zipcodes = [];

        foreach ($user_profiles as $id => $fields) {
            foreach ($fields as $field_name => $field) {
                if ($field_name != 'id') {
                    $added_zipcodes[$id] = $this->convertZipCodesToUserArea($id, $field);
                }
            }
        }

        return $added_zipcodes;
    }

    /**
     * Convert zip codes to user area
     * @param $id_user_profile
     * @param string|array $zip_codes
     * @return int
     * @throws \yii\base\InvalidConfigException
     */
    public function convertZipCodesToUserArea($id_user_profile, $zip_codes)
    {
        $zip_codes = $this->parseZohoUserZipCodes($zip_codes);
        $added_zipcodes = 0;

        if ($zip_codes) {
            foreach ($zip_codes as $number_zip => $zip_code) {
                if (preg_match('/^[0-9]{5}$/i', $zip_code)) {
                    $search_results = \Yii::$app->googlePlacesSearch
                        ->autoComplete($zip_code, 'en', ['components' => 'country:us', 'types' => '(regions)']);
                    var_dump($search_results);
                    if ($search_results->status == self::STATUS_SUCCESS) {
                        $place_id = $search_results->predictions[0]->place_id;
                        $place = \Yii::$app->googlePlaces->details($place_id);

                        if ($place->status == self::STATUS_SUCCESS) {
                            $address_components = $place->result->address_components;
                            $area = \Yii::createObject(['class' => Area::className(), 'scenario' => Area::SCENARIO_SKIP_ID_PROFILE]);
                            $area->setAttribute('place_id', $place_id);
                            $area->setAttribute('id_user_profile', $id_user_profile);
                            foreach ($address_components as $number_component => $address_component) {
                                if (in_array('administrative_area_level_1', $address_component->types)) {
                                    $area->setAttribute('state', $address_component->long_name);
                                    $area->setAttribute('short_name', $address_component->short_name);
                                }
                                if (in_array('administrative_area_level_2', $address_component->types)) {
                                    $area->setAttribute('county', $address_component->long_name);
                                }
                                if (in_array('postal_code', $address_component->types)) {
                                    $area->setAttribute('zip_code', $address_component->long_name);
                                }
                                if (in_array('locality', $address_component->types)) {
                                    $area->setAttribute('address_1', $address_component->long_name);
                                }
                            }

                            if ($area->validate()) {
                                if ($area->save()) {
                                    ++$added_zipcodes;
                                }
                            }
                        }
                    }
                }
            }
        }

        return $added_zipcodes;
    }

    /**
     * @param $zip_codes
     * @return array|bool|string
     */
    public function parseZohoUserZipCodes($zip_codes) {
        if (is_string($zip_codes) && !is_array($zip_codes)) {
            $zip_codes = explode("\n", $zip_codes);
            $zip_codes = implode(',', $zip_codes);
            $zip_codes = explode(',', $zip_codes);
            $zip_codes = array_map('trim', $zip_codes);

            return $zip_codes;
        }

        return $zip_codes;
    }
}