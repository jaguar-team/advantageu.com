<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 015 15.06.17
 * Time: 16:42
 */

namespace common\components;

use common\models\UserAddress;
use common\models\UserRole;
use yii;
use yii\base\Object;
use common\models\User;
use common\models\UserProfile;

class Zoho extends Object {

	/*
	 * Zoho ID
	 * @var big integer
	 */
	public $id_zoho;

	/**
	 * Zoho module
	 * @var
	 */
	protected $module;

	/**
	 * Request data for Zoho
	 * @var array
	 */
	protected $data = [];

	/**
	 * Zoho results
	 * @var
	 */
	protected $result;

	/**
	 * Zoho result conver via $this->getResultData()
	 * @var
	 */
	protected $convert_result;

	/*
	 * @var object User
	 */
	public $user;

	/*
	 * @var object User Role
	 */
	public $user_role;

	/*
	 * @var object User Profile
	 */
	public $user_profile;

	/*
	 * @var object User Address
	 */
	public $user_address;

	/**
	 * AGENT MODULE IN ZOHO
	 */
	CONST MODULE_AGENT = 'Leads';

	/**
	 * CLIENT MODULE IN ZOHO
	 */
	CONST MODULE_CLIENT = 'CustomModule2';

	/**
	 * AGENT BIO IN ZOHO
	 */
	CONST MODULE_AGENT_BIO = 'CustomModule7';

	/**
	 * @var string
	 */
	public $url = 'https://crm.zoho.com/crm/private';

	/**
	 * @var string
	 */
	public $auth_token = 'ff4fe4b0f1ea91fb540f26ae91f0fe85';

	/**
	 * Get records
	 * @param int $from, DEFAULT: 0
	 * @param int $to, DEFAULT: 200
	 * @param int $new_format, DEFAULT: 1
	 * @return array|bool|mixed
	 */
	public function getRecords($from = 0, $to = 200, $new_format = 1)
	{
		$params = [
			'authtoken' => $this->auth_token,
			'scope'     => 'crmapi',
			'newFormat' => $new_format,
			'fromIndex' => $from,
			'toIndex'	=> $to,
		];

		return $this->sendQuery($this->constructUrl('getRecords', 'json'), $params, true);
	}

	/**
	 * @param bool $id
	 *
	 * @return integer|null
	 */
	public function saveRecord($id = false)
	{
		$params = [
			'authtoken' => $this->auth_token,
			'scope'     => 'crmapi',
			'xmlData'   => $this->convertDataToXml(),
		];

		if ($id) { // update
			$params['id'] = $id;
			$url = $this->constructUrl('updateRecords', 'json');
		} else { // insert
			$url = $this->constructUrl('insertRecords', 'json');
		}

		$convert_data = $this->sendQuery($url, $params, true);

		return isset($convert_data['Id']) ? $convert_data['Id'] : NULL;
	}

	/**
	 * Build and send query
	 * @param $url
	 * @param bool $params
	 * @param bool $convert_data
	 * @return array|bool|mixed
	 */
	public function sendQuery($url, $params = false, $convert_data = false)
	{
		$params = http_build_query($params);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_TIMEOUT, 30);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

		if ($params) {
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
		}

		$this->result = curl_exec($ch);
		curl_close($ch);

		return $convert_data ? $this->getResultData() : $this->result;
	}

	/**
	 * @param string $format
	 *
	 * @return bool|string
	 */
	public function constructUrl($method, $format = 'xml')
	{
		if (!$this->getModule()) {
			return false;
		}

		return $this->url.'/'.$format.'/'.$this->getModule().'/'.$method;
	}

	/**
	 * Convert array to XML format for Zoho
	 * @return bool|mixed
	 */
	public function convertDataToXml()
	{
		if (!$this->data || !$this->module) {
			return false;
		}

		$xml = new \SimpleXMLElement('<'.$this->module.'/>', false, false);

		$number_row = 1;

		foreach ($this->data as $data) {
			// add row
			$row = $xml->addChild('row');
			$row->addAttribute('no', $number_row);

			//add data to row
			foreach ($data as $key => $value) {
				$fl = $row->addChild('FL', $value);
				$fl->addAttribute('val', $key);
			}

			$number_row++;
		}

		return $xml->asXML();
	}

	/**
	 * @return array|bool
	 */
	public function getResultData()
	{
		if (!$this->result) {
			return false;
		}

		$result = json_decode($this->result);

		// AFTER INSERT OR UPDATE RECORD
		if (isset($result->response->result->recorddetail->FL)) {
			$fl = $result->response->result->recorddetail->FL;
			$convert_data = [];

			foreach ($fl as $index => $data) {
				$convert_data[$data->val] = $data->content;
			}

			return $this->convert_result = $convert_data;
		}

		// GET RECORDS
		if (isset($result->response->result->{$this->module}->row)) {

			$rows = $result->response->result->{$this->module}->row;
			$convert_data = [];

			foreach ($rows as $index_row => $row) {
				if (isset($row->FL)) {
					foreach ($row->FL as $index_value => $content) {
						$convert_data[$index_row][$content->val] = $content->content;
					}
				}
			}

			return $this->convert_result = $convert_data;
		}

		return false;
	}

	/**
	 * @return bool
	 */
	public function save()
	{
		if (!$this->user) {
			return false;
		}

		if ($this->user->validate()) {
			//populate user profile
			if ($this->user_profile->validate()) {

				if ($this->user_profile->isNewRecord) {
					$this->user->populateRelation('profile', $this->user_profile);
				} else {
					$this->user_profile->save();
				}
			}

			//populate user address
			if ($this->user_address->validate()) {
				$user_addresses[] = $this->user_address;
				$this->user->populateRelation('addresses', $user_addresses);
			}

			return $this->user->save();
		}

		return false;
	}

	/**
	 * Populate objects with current Zoho response.
	 * ```php
	 *
	 * $response = $this->getRecords();
	 * $this->populateObjectsFromZohoResponse($response[0])->save();
	 *
	 * ```
	 * @param array $zoho_response. This is Zoho response, it should be processed via $this->getResultData() method.
	 * @return $this
	 */
	public function populateObjectsFromZohoResponse(array $zoho_response)
	{
		if ($this->module && $this->convert_result) {

			$this->user = \Yii::createObject(['class' => User::className(), 'scenario' => User::SCENARIO_AUTO_PASSWORD]);
			$this->user_profile = \Yii::createObject(['class' => UserProfile::className(), 'scenario' => UserProfile::SCENARIO_ZOHO_CONSOLE]);
			$this->user_address = \Yii::createObject(['class' => UserAddress::className()]);

			$this->populateObject($this->user, $zoho_response);

			if ($this->user->isNewRecord) {
				$this->populateObject($this->user_profile, $zoho_response);
			} else {
				$this->user_profile = $this->user->profile;
				$this->user_profile->scenario = UserProfile::SCENARIO_ZOHO_CONSOLE;
				$this->populateObject($this->user_profile, $zoho_response);
			}
			$this->populateObject($this->user_address, $zoho_response);
		}

		return $this;
	}

	/**
	 * @param $object
	 * @param $zoho_response
	 * @return $this
	 */
	public function populateObject(&$object, $zoho_response)
	{
		if (is_array($zoho_response) && is_object($object)) {

			$primary_key = isset($object->zohoAttributes()['primaryKey']) ? $object->zohoAttributes()['primaryKey'] : false;

			$fields = $object->zohoAttributes()['fields'];

			if ($primary_key && isset($zoho_response[$primary_key]) && $exists_object = $object->find()
				->where([$fields[$primary_key]['column_name'] => $zoho_response[$primary_key]])
				->limit(1)
				->one()) {
				$object = $exists_object;
				$object->scenario = 'zohoConsole';
			}

			foreach ($fields as $zoho_name => $actions) {
				if (isset($zoho_response[$zoho_name])) {
					$fields[$zoho_name]['setAttribute']($zoho_response[$zoho_name]);
				} elseif (isset($fields[$zoho_name]['required'])) {
					$fields[$zoho_name]['setAttribute']();
				}
			}
		}

		return $this;
	}

	/**
	 * @param $zoho_attributes
	 * @return $this
	 */
	public function setAccordanceData($zoho_attributes)
	{
		$data = [];
		
		if (is_array($zoho_attributes)) {
			foreach ($zoho_attributes as $zoho_name => $actions) {
				$data[0][$zoho_name] = $actions['getAttribute']();
			}
			$this->setData($data);
		}

		return $this;
	}

	/**
	 * @param User $user
	 * @return $this
	 */
	public function setUser(User $user)
	{
		if ($user instanceof User) {
			$this->user = $user;
			$this->user_role = $user->role;
			$this->user_profile = $user->profile;
		}

		return $this;
	}

	/**
	 * @param UserProfile $user_profile
	 * @return $this
	 */
	public function setUserProfile(UserProfile $user_profile)
	{
		if ($user_profile instanceof UserProfile) {
			$this->user_profile = $user_profile;
			$this->user = $this->user_profile->user;
			$this->user_role = $this->user->role;
		}

		return $this;
	}

	/**
	 * Get Zoho id
	 * @return null|integer
	 */
	public function getIdZoho()
	{
		if (!$this->id_zoho) {
			if ($this->user) {
				$this->id_zoho = $this->user->id_zoho ? $this->user->id_zoho : NULL;
			}
		}

		return $this->id_zoho;
	}

	/**
	 * @param $data
	 *
	 * @return $this
	 */
	public function setData($data)
	{
		$this->data = $data;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getData()
	{
		return $this->data;
	}

	/**
	 * @return mixed
	 */
	public function getModule()
	{
		return $this->module;
	}

	/**
	 * @param $module
	 *
	 * @return $this
	 */
	public function setModule($module) {
		$this->module = $module;

		return $this;
	}
}