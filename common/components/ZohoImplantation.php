<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 015 15.08.17
 * Time: 14:52
 */

namespace common\components;

use yii;
use common\models\User;
use common\models\UserRole;
use common\models\UserAddress;
use common\models\UserProfile;

class ZohoImplantation extends Zoho
{
    CONST MAX_USER_IN_QUERY = 200;
    CONST DEFAULT_OFFSET = 0;
    
    public function init()
    {
        parent::init();

        ini_set('max_execution_time', 0);
    }

    /**
     * Multiple implantation BIO records to DB
     * @param bool $from
     * @param bool $to
     * @param bool $record_count
     * @return int
     */
    public function agentBios($from = false, $to = false, $record_count = false)
    {
        return $this->multipleImplantation(Zoho::MODULE_AGENT_BIO, $from, $to, $record_count);
    }

    /**
     * Multiple implantation records to DB
     * @param $module
     * @param bool $from
     * @param bool $to
     * @param bool $record_count
     * @return int
     */
    public function multipleImplantation($module, $from = false, $to = false, $record_count = false)
    {
        $from = $from ? $from : self::DEFAULT_OFFSET;
        $to = $to ? $to : self::MAX_USER_IN_QUERY;

        $count = 0;
        while (true) {
            if ($records = $this->setModule($module)->getRecords($from, $to)) {
                foreach ($records as $number => $record) {
                    if ($count !== $record_count) {
                        if ($this->populateObjectsFromZohoResponse($record)->save()) {
                            ++$count;
                        }
                    } else { // stop if users are enough
                        break;
                    }
                }
            } else {
                break;
            }

            $from += self::MAX_USER_IN_QUERY;
            $to += self::MAX_USER_IN_QUERY;
        }

        return $count;
    }
}