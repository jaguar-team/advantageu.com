<?php

/**
 * Created by PhpStorm.
 * User: Vlad
 * Date: 18.04.2017
 * Time: 10:03
 */

namespace common\components;

use Yii;
use yii\base\Object;
use yii\web\UploadedFile;
use yii\imagine\Image;

class UploadPhoto extends Object
{
    CONST DEFAULT_USER_ADMIN_AVATAR = 'default-admin-avatar.png';
    CONST DEFAULT_AGENT_AVATAR = 'default-agent-avatar.png';

    /** @var  object UploadedFile */
    public $_uploaded_file;

    /** @var  object */
    public $_object;

    /** @var bool  */
    public $multiple = false;

    /** @var  string */
    public $property_name;

    /** @var  string */
    public $file_name;

    /** @var  string */
    public $file;

    /** @var  array, Coordinators for crop image */
    public $crop = [];

    /**
     * UploadPhoto constructor.
     * @param array $config
     */
    public function __construct(array $config = [])
    {
        parent::__construct($config);
    }

    public function init()
    {
        parent::init();

        if ($this->multiple) {
            $this->_uploaded_file = UploadedFile::getInstances($this->_object, $this->property_name);
        } else {
            $this->_uploaded_file = UploadedFile::getInstance($this->_object, $this->property_name);
        }
    }

    /**
     * @return $this
     */
    public function userAvatar()
    {
        if (!file_exists(\Yii::getAlias('@dir_img_user'))) {
            mkdir(\Yii::getAlias('@dir_img_user'));
        }

        if (!file_exists(\Yii::getAlias('@dir_user_avatar'))) {
            mkdir(\Yii::getAlias('@dir_user_avatar'));
        }

        $this->file = \Yii::getAlias('@dir_user_avatar').'/'.$this->generateFileName();

        return $this;
    }

    /**
     * @return $this
     */
    public function img()
    {
        if (!file_exists(\Yii::getAlias('@dir_img_upload'))) {
            mkdir(\Yii::getAlias('@dir_img_upload'));
        }

        $this->file = \Yii::getAlias('@dir_img_upload').'/'.$this->generateFileName();

        return $this;
    }

    /**
     * Upload file
     * @return bool
     */
    public function upload()
    {
        if ($this->file && $this->_uploaded_file) {

            if ($this->_uploaded_file->saveAs($this->file)) {
                $this->crop();
                return true;
            }
            return false;
        }

        return false;
    }

    /**
     * @return bool|string
     */
    public function generateFileName()
    {
        if (!$this->_uploaded_file) {
            return false;
        }

        return $this->file_name = time().'_'.\Yii::$app->security->generateRandomString().'.'.$this->_uploaded_file->extension;
    }

    /**
     * Crop image
     */
    public function crop()
    {
        if ($this->file && $this->crop['width'] && $this->crop['height']) {

            $width = $this->crop['width'];
            $height = $this->crop['height'];
            $start = [
                isset($this->crop['start']['x']) && !empty($this->crop['start']['x']) ? $this->crop['start']['x'] : 0,
                isset($this->crop['start']['y']) && !empty($this->crop['start']['y']) ? $this->crop['start']['y'] : 0,
            ];

            return Image::crop($this->file, $width, $height, $start)->save($this->file);
        }

        return false;
    }
}