<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%know_from}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%user_role}}`
 */
class m170406_064859_create_know_from_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%know_from}}', [
            'id' => $this->primaryKey(),
            'id_role' => $this->integer()->notNull(),
            'name' => $this->string(255)->notNull(),
            'created_at' => $this->bigInteger()->notNull(),
            'updated_at' => $this->bigInteger()->notNull(),
        ]);

        // creates index for column `id_role`
        $this->createIndex(
            '{{%idx-know_from-id_role}}',
            '{{%know_from}}',
            'id_role'
        );

        // add foreign key for table `{{%user_role}}`
        $this->addForeignKey(
            '{{%fk-know_from-id_role}}',
            '{{%know_from}}',
            'id_role',
            '{{%user_role}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `{{%user_role}}`
        $this->dropForeignKey(
            '{{%fk-know_from-id_role}}',
            '{{%know_from}}'
        );

        // drops index for column `id_role`
        $this->dropIndex(
            '{{%idx-know_from-id_role}}',
            '{{%know_from}}'
        );

        $this->dropTable('{{%know_from}}');
    }
}
