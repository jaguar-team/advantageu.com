<?php

use yii\db\Migration;

class m170510_130942_alter_know_from_column_from_review_table extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('{{%review}}', 'know_from', $this->integer());

        // creates index for column `know_from`
        $this->createIndex(
            '{{%idx-review-know_from}}',
            '{{%review}}',
            'know_from'
        );

        // add foreign key for table `{{%know_from}}`
        $this->addForeignKey(
            '{{%fk-review-know_from}}',
            '{{%review}}',
            'know_from',
            '{{%know_from}}',
            'id',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        // drops foreign key for table `{{%know_from}}`
        $this->dropForeignKey(
            '{{%fk-review-know_from}}',
            '{{%review}}'
        );

        // drops index for column `know_from`
        $this->dropIndex(
            '{{%idx-review-know_from}}',
            '{{%review}}'
        );
    }
}
