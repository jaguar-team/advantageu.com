<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%review}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%user_profile}}`
 * - `{{%advantage}}`
 */
class m170406_083133_create_review_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%review}}', [
            'id' => $this->primaryKey(),
            'id_user_profile' => $this->integer()->notNull(),
            'advantage' => $this->integer()->notNull(),
            'star' => $this->float()->notNull()->notNull(),
            'body' => $this->text()->notNull(),
            'first_name' => $this->string(255)->notNull(),
            'last_name' => $this->string(255)->notNull(),
            'know_from' => $this->text()->notNull(), //json
            'email' => $this->string(255)->notNull(),
            'status' => $this->boolean()->defaultValue(0)->notNull(),
            'created_at' => $this->bigInteger()->notNull(),
            'updated_at' => $this->bigInteger()->notNull(),
        ]);

        // creates index for column `id_user_profile`
        $this->createIndex(
            '{{%idx-review-id_user_profile}}',
            '{{%review}}',
            'id_user_profile'
        );

        // add foreign key for table `{{%user_profile}}`
        $this->addForeignKey(
            '{{%fk-review-id_user_profile}}',
            '{{%review}}',
            'id_user_profile',
            '{{%user_profile}}',
            'id',
            'CASCADE'
        );

        // creates index for column `advantage`
        $this->createIndex(
            '{{%idx-review-advantage}}',
            '{{%review}}',
            'advantage'
        );

        // add foreign key for table `{{%advantage}}`
        $this->addForeignKey(
            '{{%fk-review-advantage}}',
            '{{%review}}',
            'advantage',
            '{{%advantage}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `{{%user_profile}}`
        $this->dropForeignKey(
            '{{%fk-review-id_user_profile}}',
            '{{%review}}'
        );

        // drops index for column `id_user_profile`
        $this->dropIndex(
            '{{%idx-review-id_user_profile}}',
            '{{%review}}'
        );

        // drops foreign key for table `{{%advantage}}`
        $this->dropForeignKey(
            '{{%fk-review-advantage}}',
            '{{%review}}'
        );

        // drops index for column `advantage`
        $this->dropIndex(
            '{{%idx-review-advantage}}',
            '{{%review}}'
        );

        $this->dropTable('{{%review}}');
    }
}
