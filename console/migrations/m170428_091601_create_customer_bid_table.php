<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%customer_bid}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%customer}}`
 */
class m170428_091601_create_customer_bid_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%customer_bid}}', [
            'id' => $this->primaryKey(),
            'id_customer' => $this->integer()->notNull(),
            'location' => $this->string(255)->notNull(),
            'lead_type' => $this->integer()->notNull(),
            'property_type' => $this->integer()->notNull(),
            'price' => $this->integer()->notNull(),
            'period' => $this->integer()->notNull(),
            'other_action' => $this->boolean()->notNull(),
        ]);

        // creates index for column `id_customer`
        $this->createIndex(
            '{{%idx-customer_bid-id_customer}}',
            '{{%customer_bid}}',
            'id_customer'
        );

        // add foreign key for table `{{%customer}}`
        $this->addForeignKey(
            '{{%fk-customer_bid-id_customer}}',
            '{{%customer_bid}}',
            'id_customer',
            '{{%customer}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `{{%customer}}`
        $this->dropForeignKey(
            '{{%fk-customer_bid-id_customer}}',
            '{{%customer_bid}}'
        );

        // drops index for column `id_customer`
        $this->dropIndex(
            '{{%idx-customer_bid-id_customer}}',
            '{{%customer_bid}}'
        );

        $this->dropTable('{{%customer_bid}}');
    }
}
