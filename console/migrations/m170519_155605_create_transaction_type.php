<?php

use yii\db\Migration;

class m170519_155605_create_transaction_type extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%transaction_type}}', [
            'id'            => $this->primaryKey(),
            'name'          => $this->string(255)->notNull(),
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('{{%transaction_type}}');
    }
}
