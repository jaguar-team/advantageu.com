<?php

use yii\db\Migration;

class m170519_155603_delete_columns_from_transaction_agent_table extends Migration
{
    public function safeUp()
    {
        $this->dropColumn('{{%transaction_agent}}', 'price');
        $this->dropColumn('{{%transaction_agent}}', 'date');
    }

    public function safeDown()
    {
        $this->addColumn('{{%transaction_agent}}', 'price', $this->money()->notNull());
        $this->addColumn('{{%transaction_agent}}', 'date', $this->integer()->notNull());
    }
}
