<?php

use yii\db\Migration;

class m170619_153027_add_columns_to_user_role_table extends Migration
{
    public function safeUp()
    {
		$this->addColumn('{{%user_role}}', 'is_agent', $this->boolean()->defaultValue(0)->notNull());
	    $this->addColumn('{{%user_role}}', 'is_seller', $this->boolean()->defaultValue(0)->notNull());
	    $this->addColumn('{{%user_role}}', 'is_buyer', $this->boolean()->defaultValue(0)->notNull());
	    $this->addColumn('{{%user_role}}', 'default', $this->boolean()->defaultValue(0)->notNull());
    }

    public function safeDown()
    {
        $this->dropColumn('{{%user_role}}', 'is_agent');
	    $this->dropColumn('{{%user_role}}', 'is_seller');
	    $this->dropColumn('{{%user_role}}', 'is_buyer');
	    $this->dropColumn('{{%user_role}}', 'default');
    }
}
