<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%testimonial}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%user_profile}}`
 */
class m170406_075709_create_testimonial_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%testimonial}}', [
            'id' => $this->primaryKey(),
            'id_user_profile' => $this->integer()->notNull(),
            'name' => $this->string(255)->notNull(),
            'body' => $this->text()->notNull(),
            'created_at' => $this->bigInteger()->notNull(),
            'updated_at' => $this->bigInteger()->notNull(),
        ]);

        // creates index for column `id_user_profile`
        $this->createIndex(
            '{{%idx-testimonial-id_user_profile}}',
            '{{%testimonial}}',
            'id_user_profile'
        );

        // add foreign key for table `{{%user_profile}}`
        $this->addForeignKey(
            '{{%fk-testimonial-id_user_profile}}',
            '{{%testimonial}}',
            'id_user_profile',
            '{{%user_profile}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `{{%user_profile}}`
        $this->dropForeignKey(
            '{{%fk-testimonial-id_user_profile}}',
            '{{%testimonial}}'
        );

        // drops index for column `id_user_profile`
        $this->dropIndex(
            '{{%idx-testimonial-id_user_profile}}',
            '{{%testimonial}}'
        );

        $this->dropTable('{{%testimonial}}');
    }
}
