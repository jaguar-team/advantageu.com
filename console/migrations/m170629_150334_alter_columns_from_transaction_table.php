<?php

use yii\db\Migration;

class m170629_150334_alter_columns_from_transaction_table extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('{{%transaction}}', 'id_client', $this->integer()->null());
        $this->alterColumn('{{%transaction}}', 'id_agent', $this->integer()->null());
    }

    public function safeDown()
    {
        $this->alterColumn('{{%transaction}}', 'id_client', $this->integer()->notNull());
        $this->alterColumn('{{%transaction}}', 'id_agent', $this->integer()->notNull());
    }
}
