<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%property_location}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%property}}`
 */
class m170406_120934_create_property_location_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%property_location}}', [
            'id' => $this->primaryKey(),
            'id_property' => $this->integer()->notNull(),
            'city' => $this->string(255)->notNull(),
            'state' => $this->string(255)->notNull(),
            'zipcode' => $this->bigInteger()->notNull(),
            'address_1' => $this->string(255)->notNull(),
            'address_2' => $this->string(255),
            'photo' => $this->string(255)->notNull(),
        ]);

        // creates index for column `id_property`
        $this->createIndex(
            '{{%idx-property_location-id_property}}',
            '{{%property_location}}',
            'id_property'
        );

        // add foreign key for table `{{%property}}`
        $this->addForeignKey(
            '{{%fk-property_location-id_property}}',
            '{{%property_location}}',
            'id_property',
            '{{%property}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `{{%property}}`
        $this->dropForeignKey(
            '{{%fk-property_location-id_property}}',
            '{{%property_location}}'
        );

        // drops index for column `id_property`
        $this->dropIndex(
            '{{%idx-property_location-id_property}}',
            '{{%property_location}}'
        );

        $this->dropTable('{{%property_location}}');
    }
}
