<?php

use yii\db\Migration;

class m170519_155830_add_column_to_property_table extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%property}}', 'id_property_owner_type', $this->integer()->notNull());
        $this->addColumn('{{%property}}', 'id_transaction_type', $this->integer()->notNull());

        // add foreign key for table `{{%property_owner_type}}`
        $this->addForeignKey(
            '{{%fk-property-id_property_owner_type}}',
            '{{%property}}',
            'id_property_owner_type',
            '{{%property_owner_type}}',
            'id',
            'CASCADE'
        );

        // add foreign key for table `{{%transaction_type}}`
        $this->addForeignKey(
            '{{%fk-property-id_transaction_type}}',
            '{{%property}}',
            'id_transaction_type',
            '{{%transaction_type}}',
            'id',
            'CASCADE'
        );

        $this->dropColumn('{{%property}}', 'reo');
        $this->dropColumn('{{%property}}', 'foreclosue');
        $this->dropColumn('{{%property}}', 'short_sale');
    }

    public function safeDown()
    {
        // drops foreign key for table `{{%transaction_type}}`
        $this->dropForeignKey(
            '{{%fk-property-id_transaction_type}}',
            '{{%property}}'
        );

        // drops foreign key for table `{{%property_owner_type}}`
        $this->dropForeignKey(
            '{{%fk-property-id_property_owner_type}}',
            '{{%property}}'
        );

        $this->dropColumn('{{%property}}', 'id_transaction_type');
        $this->dropColumn('{{%property}}', 'id_property_owner_type');
    }
}
