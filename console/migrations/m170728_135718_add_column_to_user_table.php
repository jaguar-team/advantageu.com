<?php

use yii\db\Migration;

class m170728_135718_add_column_to_user_table extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%user}}', 'top', $this->boolean()->defaultValue(0));
    }

    public function safeDown()
    {
        $this->dropColumn('{{%user}}', 'top');
    }
}
