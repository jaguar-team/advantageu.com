<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%agent_role}}`.
 */
class m170406_092605_create_agent_role_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%agent_role}}', [
            'id' => $this->primaryKey(),
            'is_list' => $this->boolean()->defaultValue(0),
            'is_sell' => $this->boolean()->defaultValue(0),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%agent_role}}');
    }
}
