<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%property_status}}`.
 */
class m170406_100357_create_property_status_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%property_status}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull()->unique(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%property_status}}');
    }
}
