<?php

use yii\db\Migration;

class m170512_132757_alter_id_user_and_id_section_from_section_value_table extends Migration
{
    public function safeUp()
    {
        $this->dropIndex('id_user', '{{%section_value}}');
        $this->dropIndex('id_section', '{{%section_value}}');
    }

    public function safeDown()
    {
        $this->createIndex(
            '{{%idx-section_value-id_user}}',
            '{{%section_value}}',
            'id_user'
        );

        $this->createIndex(
            '{{%idx-section_value-id_section}}',
            '{{%section_value}}',
            'id_section'
        );
    }
}
