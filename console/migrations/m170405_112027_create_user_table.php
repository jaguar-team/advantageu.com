<?php

use yii\db\Migration;

class m170405_112027_create_user_table extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%user}}', [
            'id'                    => $this->primaryKey(),
            'id_role'               => $this->integer()->notNull(),
            'auth_key'              => $this->string(32)->notNull(),
            'password_hash'         => $this->string()->notNull(),
            'password_reset_token'  => $this->string()->unique(),
            'email'                 => $this->string()->notNull()->unique(),
            'status'                => $this->boolean()->notNull()->defaultValue(0),
            'created_at'            => $this->bigInteger()->notNull(),
            'updated_at'            => $this->bigInteger()->notNull(),
        ], $tableOptions);

        // creates index for column `id_role`
        $this->createIndex(
            'idx-user-id_role',
            '{{%user}}',
            'id_role'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-user-id_role',
            '{{%user}}',
            'id_role',
            '{{%user_role}}',
            'id',
            'CASCADE'
        );
    }

    public function down()
    {
        // drops foreign key for table `user`
        $this->dropForeignKey(
            'fk-user-id_role',
            '{{%user}}'
        );

        // drops index for column `id_role`
        $this->dropIndex(
            'idx-user-id_role',
            '{{%user}}'
        );

        $this->dropTable('{{%user}}');
    }
}
