<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%transaction_info}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%transaction}}`
 */
class m170406_090136_create_transaction_info_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%transaction_info}}', [
            'id' => $this->primaryKey(),
            'id_transaction' => $this->integer()->unique()->notNull(),
            'mls' => $this->string(255),
            'mls_number' => $this->string(255),
            'financing_type' => $this->string(255),
        ]);

        // creates index for column `id_transaction`
        $this->createIndex(
            '{{%idx-transaction_info-id_transaction}}',
            '{{%transaction_info}}',
            'id_transaction'
        );

        // add foreign key for table `{{%transaction}}`
        $this->addForeignKey(
            '{{%fk-transaction_info-id_transaction}}',
            '{{%transaction_info}}',
            'id_transaction',
            '{{%transaction}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `{{%transaction}}`
        $this->dropForeignKey(
            '{{%fk-transaction_info-id_transaction}}',
            '{{%transaction_info}}'
        );

        // drops index for column `id_transaction`
        $this->dropIndex(
            '{{%idx-transaction_info-id_transaction}}',
            '{{%transaction_info}}'
        );

        $this->dropTable('{{%transaction_info}}');
    }
}
