<?php

use yii\db\Migration;

class m170414_144157_alter_birth_year_and_license_issued_columns_to_user_profile_table extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('{{%user_profile}}', 'birth_year', $this->bigInteger());
        $this->alterColumn('{{%user_profile}}', 'license_issued', $this->bigInteger());
    }

    public function safeDown()
    {
        return false;
    }
}
