<?php

use yii\db\Migration;

class m170824_131601_refact_columns_from_area_table extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%area}}', 'zip_code', $this->bigInteger()->null());
        $this->addColumn('{{%area}}', 'county', $this->string(255)->null());
        $this->addColumn('{{%area}}', 'address_1', $this->string(255)->null());
        $this->addColumn('{{%area}}', 'place_id', $this->string(255)->null());

        $this->alterColumn('{{%area}}', 'state', $this->string(255)->null());
        $this->alterColumn('{{%area}}', 'short_name', $this->string(255)->null());
        $this->alterColumn('{{%area}}', 'city', $this->string(255)->null());
    }

    public function safeDown()
    {
        $this->alterColumn('{{%area}}', 'city', $this->string(255)->notNull());
        $this->alterColumn('{{%area}}', 'short_name', $this->string(255)->notNull());
        $this->alterColumn('{{%area}}', 'state', $this->string(255)->notNull());

        $this->dropColumn('{{%area}}', 'place_id');
        $this->dropColumn('{{%area}}', 'address_1');
        $this->dropColumn('{{%area}}', 'county');
        $this->dropColumn('{{%area}}', 'zip_code');
    }
}
