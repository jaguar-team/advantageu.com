<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user_address`.
 */
class m170626_132004_create_user_address_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('user_address', [
            'id'            => $this->primaryKey(),
            'id_user'       => $this->integer()->notNull(),
            'state'         => $this->string(255),
            'city'          => $this->string(255),
            'address'       => $this->string(255),
            'short_name'    => $this->string(255),
            'zip_code'      => $this->string(255),
            'lat'           => $this->string(255),
            'lng'           => $this->string(255),
            'place_id'      => $this->string(255),
        ]);

        // add foreign key for table `user_profile`
        $this->addForeignKey(
            'fk-user_address-id_user',
            '{{%user_address}}',
            'id_user',
            '{{%user}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-user_address-id_user',
            '{{%user_address}}'
        );

        $this->dropTable('user_address');
    }
}
