<?php

use yii\db\Migration;

class m170523_114205_delete_foreign_key_from_transaction_agent_table extends Migration
{
    public function safeUp()
    {
        // drops foreign key for table `{{%agent_role}}`
        $this->dropForeignKey(
            '{{%fk-transaction_agent-id_role}}',
            '{{%transaction_agent}}'
        );
    }

    public function safeDown()
    {
        // add foreign key for table `{{%agent_role}}`
        $this->addForeignKey(
            '{{%fk-transaction_agent-id_role}}',
            '{{%transaction_agent}}',
            'id_role',
            '{{%agent_role}}',
            'id',
            'CASCADE'
        );
    }
}
