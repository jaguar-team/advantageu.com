<?php

use yii\db\Migration;

class m170518_152947_refact_agent_role_table extends Migration
{
    public function safeUp()
    {
        $this->dropColumn('{{%agent_role}}', 'is_list');
        $this->dropColumn('{{%agent_role}}', 'is_sell');

        $this->addColumn('{{%agent_role}}', 'name', $this->string()->unique()->notNull());
    }

    public function safeDown()
    {
        echo "m170518_152947_refact_agent_role_table cannot be reverted.\n";

        return false;
    }
}
