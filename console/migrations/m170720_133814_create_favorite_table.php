<?php

use yii\db\Migration;

/**
 * Handles the creation of table `favorite`.
 */
class m170720_133814_create_favorite_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%favorite}}', [
            'id'                    => $this->primaryKey(),
            'id_user'               => $this->integer()->notNull(),
            'id_favorite'           => $this->integer()->notNull(),
        ]);

        // add foreign key for table `favorite`
        $this->addForeignKey(
            'fk-favorite-id_user',
            '{{%favorite}}',
            'id_user',
            '{{%user}}',
            'id',
            'CASCADE'
        );

        // add foreign key for table `favorite`
        $this->addForeignKey(
            'fk-favorite-id_favorite',
            '{{%favorite}}',
            'id_favorite',
            '{{%user}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-favorite-id_favorite',
            '{{%user_profile}}'
        );

        $this->dropForeignKey(
            'fk-favorite-id_user',
            '{{%user_profile}}'
        );

        $this->dropTable('{{%favorite}}');
    }
}
