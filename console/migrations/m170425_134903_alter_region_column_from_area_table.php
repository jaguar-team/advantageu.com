<?php

use yii\db\Migration;

class m170425_134903_alter_region_column_from_area_table extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('{{%area}}', 'region', $this->string(255));
    }

    public function safeDown()
    {
        echo "m170425_134903_alter_region_column_from_area_table cannot be reverted.\n";

        return false;
    }
}
