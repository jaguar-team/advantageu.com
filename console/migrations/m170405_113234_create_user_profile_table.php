<?php

use yii\db\Migration;

class m170405_113234_create_user_profile_table extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%user_profile}}', [
            'id'                    => $this->primaryKey(),
            'id_user'               => $this->integer()->notNull()->unique(),
            'company'               => $this->string(255),
            'first_name'            => $this->string(255),
            'last_name'             => $this->string(255),
            'biography'             => $this->text(),
            'phone_number'          => $this->string(255),
            'office_number'         => $this->string(255),
            'photo'                 => $this->string(255),
            'website_url'           => $this->string(255),
            'blog_url'              => $this->string(255),
            'education'             => $this->string(255),
            'license_number'        => $this->string(255),
            'license_state'         => $this->string(255),
            'license_issued'        => $this->date(),
            'brokerage'             => $this->string(255),
            'gender'                => $this->smallInteger(),
            'birth_year'            => $this->date(),
            'email_alternative'     => $this->string(255),
        ], $tableOptions);

        // creates index for column `id_user`
        $this->createIndex(
            'idx-user_profile-id_user',
            '{{%user_profile}}',
            'id_user'
        );

        // add foreign key for table `user_profile`
        $this->addForeignKey(
            'fk-user_profile-id_user',
            '{{%user_profile}}',
            'id_user',
            '{{%user}}',
            'id',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        // drops foreign key for table `user_profile`
        $this->dropForeignKey(
            'fk-user_profile-id_user',
            '{{%user_profile}}'
        );

        // drops index for column `id_user`
        $this->dropIndex(
            'idx-user_profile-id_user',
            '{{%user_profile}}'
        );

        $this->dropTable('{{%user_profile}}');
    }
}
