<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%property_type}}`.
 */
class m170406_100334_create_property_type_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%property_type}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull()->unique(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%property_type}}');
    }
}
