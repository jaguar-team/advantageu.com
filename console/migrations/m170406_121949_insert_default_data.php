<?php

use yii\db\Migration;

class m170406_121949_insert_default_data extends Migration
{
    public function safeUp()
    {
        /** insert roles */
        echo 'Insert roles';

        $this->batchInsert(
            '{{%user_role}}',
            ['id', 'priority', 'is_admin', 'name'],
            [
                [1, 1, 1, 'Administrator'],
                [2, 1, 0, 'Real Estate Agent'],
            ]
        );

        /** insert users */
        echo 'Insert users';

        $this->batchInsert(
            '{{%user}}',
            ['id', 'id_role', 'auth_key', 'password_hash', 'password_reset_token', 'email', 'status', 'created_at', 'updated_at'],
            [
                [
                    1,
                    1,
                    'HP187Mvq7Mmm3CTU80dLkGmni_FUH_lR',
                    '$2y$13$EjaPFBnZOQsHdGuHI.xvhuDp1fHpo8hKRSk6yshqa9c5EG8s3C3lO',
                    'ExzkCOaYc1L8IOBs4wdTGGbgNiG3Wz1I_1402312317',
                    'admin@admin.com',
                    1,
                    time(),
                    time(),
                ],
            ]
        );

        /** insert user profile */
        echo 'Insert users\' profile';

        $this->batchInsert(
            '{{%user_profile}}',
            ['id', 'id_user', 'company', 'first_name', 'last_name'],
            [
                [
                    1,
                    1,
                    'Admin company',
                    'Admin',
                    'Admin',
                ],
            ]
        );

        /** insert sections */
        echo 'Insert sections';

        $this->batchInsert(
            '{{%section}}',
            ['id_role', 'name', 'created_at', 'updated_at'],
            [
                [1, 'Awards', time(), time()],
                [1, 'Certifications', time(), time()],
            ]
        );

        /** insert know from */
        echo 'Insert know from';

        $this->batchInsert(
            '{{%know_from}}',
            ['id_role', 'name', 'created_at', 'updated_at'],
            [
                [1, 'Listed and sold my home', time(), time()],
                [1, 'Listed, but didn\'t sell, my home', time(), time()],
                [1, 'Helped me buy a home', time(), time()],
                [1, 'Showed me homes', time(), time()],
                [1, 'Connected, but did no business', time(), time()],
                [1, 'Never responded to my request', time(), time()],
            ]
        );

        /** insert advantages */
        echo 'Insert advantages';

        $this->batchInsert(
            '{{%advantage}}',
            ['id_role', 'name', 'created_at', 'updated_at'],
            [
                [1, 'Knowledge of the local area', time(), time()],
                [1, 'Finding homes that fit my criteria', time(), time()],
                [1, 'Determining fair market value', time(), time()],
                [1, 'Explaining the closing process', time(), time()],
                [1, 'Negotiating the best price', time(), time()],
                [1, 'Giving me their time/attention', time(), time()],
            ]
        );

        /** insert property type */
        echo 'Insert property type';

        $this->batchInsert(
            '{{%property_type}}',
            ['name'],
            [
                ['Single family'],
                ['Condo'],
                ['Townhome'],
                ['Tic'],
                ['Mobile Home'],
                ['Others'],
            ]
        );

        /** insert property status */
        echo 'Insert property status';

        $this->batchInsert(
            '{{%property_status}}',
            ['name'],
            [
                ['Sold'],
                ['Closed'],
            ]
        );
    }

    public function safeDown()
    {
    }
}
