<?php

use yii\db\Migration;

class m170711_141423_add_column_to_review_table extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%review}}', 'title', $this->string()->notNull());
    }

    public function safeDown()
    {
        $this->dropColumn('{{%review}}', 'title');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170711_141423_add_column_to_review_table cannot be reverted.\n";

        return false;
    }
    */
}
