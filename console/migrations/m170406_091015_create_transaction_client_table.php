<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%transaction_client}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%transaction}}`
 */
class m170406_091015_create_transaction_client_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%transaction_client}}', [
            'id' => $this->primaryKey(),
            'id_transaction' => $this->integer()->unique()->notNull(),
            'first_name' => $this->string(255)->notNull(),
            'last_name' => $this->string(255)->notNull(),
            'email' => $this->string(255)->notNull(),
        ]);

        // creates index for column `id_transaction`
        $this->createIndex(
            '{{%idx-transaction_client-id_transaction}}',
            '{{%transaction_client}}',
            'id_transaction'
        );

        // add foreign key for table `{{%transaction}}`
        $this->addForeignKey(
            '{{%fk-transaction_client-id_transaction}}',
            '{{%transaction_client}}',
            'id_transaction',
            '{{%transaction}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `{{%transaction}}`
        $this->dropForeignKey(
            '{{%fk-transaction_client-id_transaction}}',
            '{{%transaction_client}}'
        );

        // drops index for column `id_transaction`
        $this->dropIndex(
            '{{%idx-transaction_client-id_transaction}}',
            '{{%transaction_client}}'
        );

        $this->dropTable('{{%transaction_client}}');
    }
}
