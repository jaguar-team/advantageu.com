<?php

use yii\db\Migration;

class m170628_161554_refact_review_table extends Migration
{
    public function safeUp()
    {
        //drop foreign and index
        $this->dropForeignKey(
            '{{%fk-review-id_user_profile}}',
            '{{%review}}'
        );
        $this->dropIndex(
            '{{%idx-review-id_user_profile}}',
            '{{%review}}'
        );
        $this->dropForeignKey(
            '{{%fk-review-advantage}}',
            '{{%review}}'
        );
        $this->dropIndex(
            '{{%idx-review-advantage}}',
            '{{%review}}'
        );
        $this->dropForeignKey(
            '{{%fk-review-know_from}}',
            '{{%review}}'
        );
        $this->dropIndex(
            '{{%idx-review-know_from}}',
            '{{%review}}'
        );

        //drop column
        $this->dropColumn('{{%review}}', 'id_user_profile');
        $this->dropColumn('{{%review}}', 'advantage');
        $this->dropColumn('{{%review}}', 'know_from');

        //add new columns
        $this->addColumn('{{%review}}', 'id_user', $this->integer()->notNull());
        $this->addForeignKey(
            '{{%fk-review-user}}',
            '{{%review}}',
            'id_user',
            '{{%user}}',
            'id',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey(
            '{{%fk-review-user}}',
            '{{%review}}'
        );

        $this->dropColumn('{{%review}}', 'id_user');

        $this->addColumn('{{%review}}', 'id_user_profile', $this->integer()->notNull());
        $this->addColumn('{{%review}}', 'advantage', $this->integer()->notNull());
        $this->addColumn('{{%review}}', 'know_from', $this->integer()->notNull());

        // creates index for column `id_user_profile`
        $this->createIndex(
            '{{%idx-review-id_user_profile}}',
            '{{%review}}',
            'id_user_profile'
        );

        // add foreign key for table `{{%user_profile}}`
        $this->addForeignKey(
            '{{%fk-review-id_user_profile}}',
            '{{%review}}',
            'id_user_profile',
            '{{%user_profile}}',
            'id',
            'CASCADE'
        );

        // creates index for column `advantage`
        $this->createIndex(
            '{{%idx-review-advantage}}',
            '{{%review}}',
            'advantage'
        );

        // add foreign key for table `{{%advantage}}`
        $this->addForeignKey(
            '{{%fk-review-advantage}}',
            '{{%review}}',
            'advantage',
            '{{%advantage}}',
            'id',
            'CASCADE'
        );
    }
}
