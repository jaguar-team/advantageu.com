<?php

use yii\db\Migration;

class m170519_154751_add_columns_to_transaction_info_table extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%transaction_info}}', 'selling_price', $this->money()->notNull());
        $this->addColumn('{{%transaction_info}}', 'listing_price', $this->money()->notNull());
        $this->addColumn('{{%transaction_info}}', 'selling_date', $this->integer()->notNull());
        $this->addColumn('{{%transaction_info}}', 'listing_date', $this->integer()->notNull());
    }

    public function safeDown()
    {
        $this->dropColumn('{{%transaction_info}}', 'selling_price');
        $this->dropColumn('{{%transaction_info}}', 'listing_price');
        $this->dropColumn('{{%transaction_info}}', 'selling_date');
        $this->dropColumn('{{%transaction_info}}', 'listing_date');
    }
}
