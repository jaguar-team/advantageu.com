<?php

use yii\db\Migration;

class m170526_085254_add_columns_to_property_location_table extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%property_location}}', 'lat', $this->double()->null());
        $this->addColumn('{{%property_location}}', 'lng', $this->double()->null());
        $this->addColumn('{{%property_location}}', 'place_id', $this->string(255)->null());
        $this->addColumn('{{%property_location}}', 'short_name', $this->string(255)->null());
        $this->alterColumn('{{%property_location}}', 'zipcode', $this->string()->null());
        $this->alterColumn('{{%property_location}}', 'address_1', $this->string(255)->null());
        $this->alterColumn('{{%property_location}}', 'photo', $this->string()->null());
    }

    public function safeDown()
    {
        $this->alterColumn('{{%property_location}}', 'photo', $this->string()->notNull());
        $this->alterColumn('{{%property_location}}', 'address_1', $this->bigInteger()->notNull());
        $this->alterColumn('{{%property_location}}', 'zipcode', $this->string()->notNull());
        $this->dropColumn('{{%property_location}}', 'short_name');
        $this->dropColumn('{{%property_location}}', 'place_id');
        $this->dropColumn('{{%property_location}}', 'lng');
        $this->dropColumn('{{%property_location}}', 'lat');
    }
}
