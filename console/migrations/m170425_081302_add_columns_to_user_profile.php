<?php

use yii\db\Migration;

class m170425_081302_add_columns_to_user_profile extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%user_profile}}', 'lead_type', $this->integer()->defaultValue(1));
        $this->addColumn('{{%user_profile}}', 'status', $this->integer()->defaultValue(1));
        $this->addColumn('{{%user_profile}}', 'sms_send', $this->boolean()->defaultValue(0));
        $this->addColumn('{{%user_profile}}', 'email_send', $this->boolean()->defaultValue(0));
    }

    public function safeDown()
    {
        echo "m170425_081302_add_columns_to_user_profile cannot be reverted.\n";

        return false;
    }
}
