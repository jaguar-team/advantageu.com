<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%property}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%transaction}}`
 * - `{{%property_type}}`
 * - `{{%property_status}}`
 */
class m170406_111816_create_property_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%property}}', [
            'id' => $this->primaryKey(),
            'id_transaction' => $this->integer()->unique()->notNull(),
            'id_type' => $this->integer()->notNull(),
            'id_status' => $this->integer()->notNull(),
            'foreclosue' => $this->boolean()->notNull(),
            'reo' => $this->boolean()->notNull(),
            'short_sale' => $this->boolean()->notNull(),
            'beds' => $this->integer(10)->notNull(),
            'baths' => $this->integer(10)->notNull(),
            'square_feet' => $this->integer()->notNull(),
        ]);

        // creates index for column `id_transaction`
        $this->createIndex(
            '{{%idx-property-id_transaction}}',
            '{{%property}}',
            'id_transaction'
        );

        // add foreign key for table `{{%transaction}}`
        $this->addForeignKey(
            '{{%fk-property-id_transaction}}',
            '{{%property}}',
            'id_transaction',
            '{{%transaction}}',
            'id',
            'CASCADE'
        );

        // creates index for column `id_type`
        $this->createIndex(
            '{{%idx-property-id_type}}',
            '{{%property}}',
            'id_type'
        );

        // add foreign key for table `{{%property_type}}`
        $this->addForeignKey(
            '{{%fk-property-id_type}}',
            '{{%property}}',
            'id_type',
            '{{%property_type}}',
            'id',
            'CASCADE'
        );

        // creates index for column `id_status`
        $this->createIndex(
            '{{%idx-property-id_status}}',
            '{{%property}}',
            'id_status'
        );

        // add foreign key for table `{{%property_status}}`
        $this->addForeignKey(
            '{{%fk-property-id_status}}',
            '{{%property}}',
            'id_status',
            '{{%property_status}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `{{%transaction}}`
        $this->dropForeignKey(
            '{{%fk-property-id_transaction}}',
            '{{%property}}'
        );

        // drops index for column `id_transaction`
        $this->dropIndex(
            '{{%idx-property-id_transaction}}',
            '{{%property}}'
        );

        // drops foreign key for table `{{%property_type}}`
        $this->dropForeignKey(
            '{{%fk-property-id_type}}',
            '{{%property}}'
        );

        // drops index for column `id_type`
        $this->dropIndex(
            '{{%idx-property-id_type}}',
            '{{%property}}'
        );

        // drops foreign key for table `{{%property_status}}`
        $this->dropForeignKey(
            '{{%fk-property-id_status}}',
            '{{%property}}'
        );

        // drops index for column `id_status`
        $this->dropIndex(
            '{{%idx-property-id_status}}',
            '{{%property}}'
        );

        $this->dropTable('{{%property}}');
    }
}
