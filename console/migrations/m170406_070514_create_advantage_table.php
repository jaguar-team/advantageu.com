<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%advantage}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%user_role}}`
 */
class m170406_070514_create_advantage_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%advantage}}', [
            'id' => $this->primaryKey(),
            'id_role' => $this->integer()->notNull(),
            'name' => $this->string(255)->notNull(),
            'created_at' => $this->bigInteger()->notNull(),
            'updated_at' => $this->bigInteger()->notNull(),
        ]);

        // creates index for column `id_role`
        $this->createIndex(
            '{{%idx-advantage-id_role}}',
            '{{%advantage}}',
            'id_role'
        );

        // add foreign key for table `{{%user_role}}`
        $this->addForeignKey(
            '{{%fk-advantage-id_role}}',
            '{{%advantage}}',
            'id_role',
            '{{%user_role}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `{{%role}}`
        $this->dropForeignKey(
            '{{%fk-advantage-id_role}}',
            '{{%advantage}}'
        );

        // drops index for column `id_role`
        $this->dropIndex(
            '{{%idx-advantage-id_role}}',
            '{{%advantage}}'
        );

        $this->dropTable('{{%advantage}}');
    }
}
