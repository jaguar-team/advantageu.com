<?php

use yii\db\Migration;

/**
 * Handles the dropping of table `indexs_from_transaction_agent`.
 */
class m170518_152236_drop_indexs_from_transaction_agent_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->dropIndex('id_transaction', '{{%transaction_agent}}');
        $this->dropIndex('id_role', '{{%transaction_agent}}');
    }

    public function safeDown()
    {
        $this->createIndex(
            '{{%idx-transaction_agent-id_transaction}}',
            '{{%transaction_agent}}',
            'id_transaction'
        );

        // creates index for column `id_role`
        $this->createIndex(
            '{{%idx-transaction_agent-id_role}}',
            '{{%transaction_agent}}',
            'id_role'
        );
    }
}
