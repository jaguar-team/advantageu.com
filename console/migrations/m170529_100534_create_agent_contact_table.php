<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%agent_contact}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%user_profile}}`
 */
class m170529_100534_create_agent_contact_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%agent_contact}}', [
            'id' => $this->primaryKey(),
            'id_user_profile' => $this->integer()->notNull(),
            'full_name' => $this->string(255)->notNull(),
            'email' => $this->string(255)->notNull(),
            'phone' => $this->string(255)->notNull(),
            'message' => $this->text(),
            'created_at' => $this->bigInteger()->notNull(),
            'updated_at' => $this->bigInteger()->notNull(),
        ]);

        // creates index for column `id_user_profile`
        $this->createIndex(
            '{{%idx-agent_contact-id_user_profile}}',
            '{{%agent_contact}}',
            'id_user_profile'
        );

        // add foreign key for table `{{%user_profile}}`
        $this->addForeignKey(
            '{{%fk-agent_contact-id_user_profile}}',
            '{{%agent_contact}}',
            'id_user_profile',
            '{{%user_profile}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `{{%user_profile}}`
        $this->dropForeignKey(
            '{{%fk-agent_contact-id_user_profile}}',
            '{{%agent_contact}}'
        );

        // drops index for column `id_user_profile`
        $this->dropIndex(
            '{{%idx-agent_contact-id_user_profile}}',
            '{{%agent_contact}}'
        );

        $this->dropTable('{{%agent_contact}}');
    }
}
