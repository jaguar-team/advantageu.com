<?php

use yii\db\Migration;

class m170627_081503_refact_user_and_user_profile_table extends Migration
{
    public function safeUp()
    {
        // user profile
        $this->dropColumn('{{%user_profile}}', 'office_number');
        $this->dropColumn('{{%user_profile}}', 'blog_url');
        $this->dropColumn('{{%user_profile}}', 'license_state');
        $this->dropColumn('{{%user_profile}}', 'license_issued');
        $this->dropColumn('{{%user_profile}}', 'brokerage');
        $this->dropColumn('{{%user_profile}}', 'gender');
        $this->dropColumn('{{%user_profile}}', 'birth_year');
        $this->dropColumn('{{%user_profile}}', 'email_alternative');

        // user
        $this->addColumn('{{%user}}', 'certificate', $this->boolean()->defaultValue(0));
    }

    public function safeDown()
    {
        $this->addColumn('{{%user_profile}}', 'office_number', $this->string(255));
        $this->addColumn('{{%user_profile}}', 'blog_url', $this->string(255));
        $this->addColumn('{{%user_profile}}', 'license_state', $this->string(255));
        $this->addColumn('{{%user_profile}}', 'license_issued', $this->integer(255));
        $this->addColumn('{{%user_profile}}', 'brokerage', $this->string(255));
        $this->addColumn('{{%user_profile}}', 'gender', $this->boolean());
        $this->addColumn('{{%user_profile}}', 'birth_year', $this->integer());
        $this->addColumn('{{%user_profile}}', 'email_alternative', $this->string(255));

        $this->dropColumn('{{%user}}', 'certificate');
    }
}
