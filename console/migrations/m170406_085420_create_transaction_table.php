<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%transaction}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%user_profile}}`
 */
class m170406_085420_create_transaction_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%transaction}}', [
            'id' => $this->primaryKey(),
            'id_user_profile' => $this->integer()->notNull(),
            'status' => $this->boolean()->defaultValue(0),
            'created_at' => $this->bigInteger()->notNull(),
            'updated_at' => $this->bigInteger()->notNull(),
        ]);

        // creates index for column `id_user_profile`
        $this->createIndex(
            '{{%idx-transaction-id_user_profile}}',
            '{{%transaction}}',
            'id_user_profile'
        );

        // add foreign key for table `{{%user_profile}}`
        $this->addForeignKey(
            '{{%fk-transaction-id_user_profile}}',
            '{{%transaction}}',
            'id_user_profile',
            '{{%user_profile}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `{{%user_profile}}`
        $this->dropForeignKey(
            '{{%fk-transaction-id_user_profile}}',
            '{{%transaction}}'
        );

        // drops index for column `id_user_profile`
        $this->dropIndex(
            '{{%idx-transaction-id_user_profile}}',
            '{{%transaction}}'
        );

        $this->dropTable('{{%transaction}}');
    }
}
