<?php

use yii\db\Migration;

class m170630_134649_refact_relation_transaction_tables extends Migration
{
    public function safeUp()
    {
        //transaction
        $this->addColumn('{{%transaction}}', 'featured', $this->boolean()->defaultValue(0));

        //transaction info
        $this->alterColumn('{{%transaction_info}}', 'selling_date', $this->integer()->null());
        $this->alterColumn('{{%transaction_info}}', 'selling_price', $this->money()->null());

        //property
        $this->alterColumn('{{%property}}', 'beds', $this->integer()->null());
        $this->alterColumn('{{%property}}', 'baths', $this->integer()->null());
        $this->alterColumn('{{%property}}', 'square_feet', $this->integer()->null());
        $this->addColumn('{{%property}}', 'garage', $this->integer()->null());
        // drops foreign key for table `{{%property_status}}`
        $this->dropForeignKey(
            '{{%fk-property-id_status}}',
            '{{%property}}'
        );
        // drops index for column `id_status`
        $this->dropIndex(
            '{{%idx-property-id_status}}',
            '{{%property}}'
        );
        $this->dropColumn('{{%property}}', 'id_status');
        $this->dropForeignKey(
            '{{%fk-property-id_transaction_type}}',
            '{{%property}}'
        );

        // drops foreign key for table `{{%property_owner_type}}`
        $this->dropForeignKey(
            '{{%fk-property-id_property_owner_type}}',
            '{{%property}}'
        );
        $this->dropColumn('{{%property}}', 'id_transaction_type');
        $this->dropColumn('{{%property}}', 'id_property_owner_type');

    }

    public function safeDown()
    {
        echo "m170630_134649_refact_relation_transaction_tables cannot be reverted.\n";

        return false;
    }
}
