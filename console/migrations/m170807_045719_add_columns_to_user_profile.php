<?php

use yii\db\Migration;

class m170807_045719_add_columns_to_user_profile extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%user_profile}}', 'account_number', $this->integer());
        $this->addColumn('{{%user_profile}}', 'experience', $this->integer());
        $this->addColumn('{{%user_profile}}', 'google_calendar', $this->boolean());
        $this->addColumn('{{%user_profile}}', 'timezone', $this->integer());
        $this->addColumn('{{%user_profile}}', 'phonetic_pronunciation', $this->string(255));
        $this->addColumn('{{%user_profile}}', 'team_name', $this->string(255));
        $this->addColumn('{{%user_profile}}', 'team_info', $this->text());
        $this->addColumn('{{%user_profile}}', 'listing_agent_phone_number', $this->string(255));
        $this->addColumn('{{%user_profile}}', 'listing_agent_mobile_number', $this->string(255));
        $this->addColumn('{{%user_profile}}', 'listing_mobile_carrier', $this->integer());
        $this->addColumn('{{%user_profile}}', 'listing_agent_first_name', $this->string(255));
        $this->addColumn('{{%user_profile}}', 'listing_agent_last_name', $this->string(255));
        $this->addColumn('{{%user_profile}}', 'admin_first_name', $this->string(255));
        $this->addColumn('{{%user_profile}}', 'admin_last_name', $this->string(255));
        $this->addColumn('{{%user_profile}}', 'admin_email', $this->string(255));
        $this->addColumn('{{%user_profile}}', 'admin_phone_number', $this->string(255));
        $this->addColumn('{{%user_profile}}', 'admin_mobile_carrier', $this->integer());
        $this->addColumn('{{%user_profile}}', 'special_designations', $this->text());
        $this->addColumn('{{%user_profile}}', 'average_days_on_market', $this->integer());
        $this->addColumn('{{%user_profile}}', 'average_home_sale_price', $this->double());
        $this->addColumn('{{%user_profile}}', 'total_sale_price', $this->double());
        $this->addColumn('{{%user_profile}}', 'listing_sale_price_ratio', $this->double());
        $this->addColumn('{{%user_profile}}', 'number_home_listed', $this->integer());
        $this->addColumn('{{%user_profile}}', 'number_home_sold', $this->integer());
        $this->addColumn('{{%user_profile}}', 'statements', $this->text());
        $this->addColumn('{{%user_profile}}', 'uvps', $this->text());
        $this->addColumn('{{%user_profile}}', 'advantages', $this->text());
        $this->addColumn('{{%user_profile}}', 'package_level', $this->integer());
        $this->addColumn('{{%user_profile}}', 'facebook_login', $this->string(255));
        $this->addColumn('{{%user_profile}}', 'facebook_password', $this->string(255));
        $this->addColumn('{{%user_profile}}', 'twitter_login', $this->string(255));
        $this->addColumn('{{%user_profile}}', 'twitter_password', $this->string(255));
        $this->addColumn('{{%user_profile}}', 'youtube_login', $this->string(255));
        $this->addColumn('{{%user_profile}}', 'youtube_password', $this->string(255));
        $this->addColumn('{{%user_profile}}', 'schedule_question_1', $this->text());
        $this->addColumn('{{%user_profile}}', 'schedule_question_2', $this->text());
        $this->addColumn('{{%user_profile}}', 'schedule_question_3', $this->text());
        $this->addColumn('{{%user_profile}}', 'schedule_question_4', $this->text());
        $this->addColumn('{{%user_profile}}', 'mls_name', $this->string(255));
        $this->addColumn('{{%user_profile}}', 'mls_website', $this->string(255));
        $this->addColumn('{{%user_profile}}', 'mls_username', $this->string(255));
        $this->addColumn('{{%user_profile}}', 'mls_password', $this->string(255));
        $this->addColumn('{{%user_profile}}', 'redx_username', $this->string(255));
        $this->addColumn('{{%user_profile}}', 'redx_password', $this->string(255));
        $this->addColumn('{{%user_profile}}', 'zip_codes_daily', $this->text());
        $this->addColumn('{{%user_profile}}', 'zip_codes_just', $this->text());
        $this->addColumn('{{%user_profile}}', 'zip_codes_high_turnover', $this->text());
        $this->addColumn('{{%user_profile}}', 'property_avoid', $this->text());
        $this->addColumn('{{%user_profile}}', 'property_list', $this->text());
    }

    public function safeDown()
    {
        $this->dropColumn('{{%user_profile}}', 'account_number');
        $this->dropColumn('{{%user_profile}}', 'experience');
        $this->dropColumn('{{%user_profile}}', 'google_calendar');
        $this->dropColumn('{{%user_profile}}', 'timezone');
        $this->dropColumn('{{%user_profile}}', 'phonetic_pronunciation');
        $this->dropColumn('{{%user_profile}}', 'team_name');
        $this->dropColumn('{{%user_profile}}', 'team_info');
        $this->dropColumn('{{%user_profile}}', 'listing_agent_phone_number');
        $this->dropColumn('{{%user_profile}}', 'listing_agent_mobile_number');
        $this->dropColumn('{{%user_profile}}', 'listing_mobile_carrier');
        $this->dropColumn('{{%user_profile}}', 'listing_agent_first_name');
        $this->dropColumn('{{%user_profile}}', 'listing_agent_last_name');
        $this->dropColumn('{{%user_profile}}', 'admin_first_name');
        $this->dropColumn('{{%user_profile}}', 'admin_last_name');
        $this->dropColumn('{{%user_profile}}', 'admin_email');
        $this->dropColumn('{{%user_profile}}', 'admin_phone_number');
        $this->dropColumn('{{%user_profile}}', 'admin_mobile_carrier');
        $this->dropColumn('{{%user_profile}}', 'special_designations');
        $this->dropColumn('{{%user_profile}}', 'average_days_on_market');
        $this->dropColumn('{{%user_profile}}', 'average_home_sale_price');
        $this->dropColumn('{{%user_profile}}', 'total_sale_price');
        $this->dropColumn('{{%user_profile}}', 'listing_sale_price_ratio');
        $this->dropColumn('{{%user_profile}}', 'number_home_listed');
        $this->dropColumn('{{%user_profile}}', 'number_home_sold');
        $this->dropColumn('{{%user_profile}}', 'statements ');
        $this->dropColumn('{{%user_profile}}', 'uvps');
        $this->dropColumn('{{%user_profile}}', 'advantages');
        $this->dropColumn('{{%user_profile}}', 'package_level');
        $this->dropColumn('{{%user_profile}}', 'facebook_login');
        $this->dropColumn('{{%user_profile}}', 'facebook_password');
        $this->dropColumn('{{%user_profile}}', 'twitter_login');
        $this->dropColumn('{{%user_profile}}', 'twitter_password');
        $this->dropColumn('{{%user_profile}}', 'youtube_login');
        $this->dropColumn('{{%user_profile}}', 'youtube_password');
        $this->dropColumn('{{%user_profile}}', 'schedule_question_1');
        $this->dropColumn('{{%user_profile}}', 'schedule_question_2');
        $this->dropColumn('{{%user_profile}}', 'schedule_question_3');
        $this->dropColumn('{{%user_profile}}', 'schedule_question_4');
        $this->dropColumn('{{%user_profile}}', 'mls_name');
        $this->dropColumn('{{%user_profile}}', 'mls_website');
        $this->dropColumn('{{%user_profile}}', 'mls_username');
        $this->dropColumn('{{%user_profile}}', 'mls_password');
        $this->dropColumn('{{%user_profile}}', 'redx_username');
        $this->dropColumn('{{%user_profile}}', 'redx_password');
        $this->dropColumn('{{%user_profile}}', 'zip_codes_daily');
        $this->dropColumn('{{%user_profile}}', 'zip_codes_just');
        $this->dropColumn('{{%user_profile}}', 'zip_codes_high_turnover');
        $this->dropColumn('{{%user_profile}}', 'property_avoid');
        $this->dropColumn('{{%user_profile}}', 'property_list');
    }
}
