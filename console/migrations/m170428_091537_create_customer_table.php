<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%customer}}`.
 */
class m170428_091537_create_customer_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%customer}}', [
            'id' => $this->primaryKey(),
            'email' => $this->string(255),
            'phone' => $this->string(255),
            'first_name' => $this->string(255),
            'last_name' => $this->string(255),
            'created_at' => $this->bigInteger(),
            'updated_at' => $this->bigInteger(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%customer}}');
    }
}
