<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%language}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%user_profile}}`
 * - `{{%advantage}}`
 */
class m170406_085320_create_language_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%language}}', [
            'id' => $this->primaryKey(),
            'id_user_profile' => $this->integer()->notNull(),
            'name' => $this->string(255)->notNull(),
        ]);

        // creates index for column `id_user_profile`
        $this->createIndex(
            '{{%idx-language-id_user_profile}}',
            '{{%language}}',
            'id_user_profile'
        );

        // add foreign key for table `{{%user_profile}}`
        $this->addForeignKey(
            '{{%fk-language-id_user_profile}}',
            '{{%language}}',
            'id_user_profile',
            '{{%user_profile}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `{{%user_profile}}`
        $this->dropForeignKey(
            '{{%fk-language-id_user_profile}}',
            '{{%language}}'
        );

        // drops index for column `id_user_profile`
        $this->dropIndex(
            '{{%idx-language-id_user_profile}}',
            '{{%language}}'
        );

        $this->dropTable('{{%language}}');
    }
}
