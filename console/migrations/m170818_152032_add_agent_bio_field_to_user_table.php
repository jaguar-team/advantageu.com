<?php

use yii\db\Migration;

class m170818_152032_add_agent_bio_field_to_user_table extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%user}}', 'id_bio', $this->string(255)->null()->unique());
    }

    public function safeDown()
    {
        $this->dropColumn('{{%user}}', 'id_bio');
    }
}
