<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user_role`.
 */
class m170405_110023_create_user_role_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%user_role}}', [
            'id'            => $this->primaryKey(),
            'priority'      => $this->integer()->notNull(),
            'is_admin'      => $this->boolean()->defaultValue(0)->notNull(),
            'name'          => $this->string(255)->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('user_role');
    }
}
