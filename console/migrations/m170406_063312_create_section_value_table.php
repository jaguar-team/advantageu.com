<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%section_value}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%user}}`
 * - `{{%section}}`
 */
class m170406_063312_create_section_value_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%section_value}}', [
            'id' => $this->primaryKey(),
            'id_user' => $this->integer()->unique()->notNull(),
            'id_section' => $this->integer()->unique()->notNull(),
            'value' => $this->string(255)->notNull(),
        ]);

        // creates index for column `id_user`
        $this->createIndex(
            '{{%idx-section_value-id_user}}',
            '{{%section_value}}',
            'id_user'
        );

        // add foreign key for table `{{%user}}`
        $this->addForeignKey(
            '{{%fk-section_value-id_user}}',
            '{{%section_value}}',
            'id_user',
            '{{%user}}',
            'id',
            'CASCADE'
        );

        // creates index for column `id_section`
        $this->createIndex(
            '{{%idx-section_value-id_section}}',
            '{{%section_value}}',
            'id_section'
        );

        // add foreign key for table `{{%section}}`
        $this->addForeignKey(
            '{{%fk-section_value-id_section}}',
            '{{%section_value}}',
            'id_section',
            '{{%section}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `{{%user}}`
        $this->dropForeignKey(
            '{{%fk-section_value-id_user}}',
            '{{%section_value}}'
        );

        // drops index for column `id_user`
        $this->dropIndex(
            '{{%idx-section_value-id_user}}',
            '{{%section_value}}'
        );

        // drops foreign key for table `{{%section}}`
        $this->dropForeignKey(
            '{{%fk-section_value-id_section}}',
            '{{%section_value}}'
        );

        // drops index for column `id_section`
        $this->dropIndex(
            '{{%idx-section_value-id_section}}',
            '{{%section_value}}'
        );

        $this->dropTable('{{%section_value}}');
    }
}
