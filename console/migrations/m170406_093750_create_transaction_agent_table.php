<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%transaction_agent}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%transaction}}`
 * - `{{%agent_role}}`
 */
class m170406_093750_create_transaction_agent_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%transaction_agent}}', [
            'id' => $this->primaryKey(),
            'id_transaction' => $this->integer()->unique()->notNull(),
            'id_role' => $this->integer()->unique()->notNull(),
            'price' => $this->money()->notNull(),
            'agent_license_number' => $this->string(255)->notNull(),
            'agent_name' => $this->string(255)->notNull(),
            'agent_office_name' => $this->string(255)->notNull(),
            'date' => $this->integer()->notNull(),
        ]);

        // creates index for column `id_transaction`
        $this->createIndex(
            '{{%idx-transaction_agent-id_transaction}}',
            '{{%transaction_agent}}',
            'id_transaction'
        );

        // add foreign key for table `{{%transaction}}`
        $this->addForeignKey(
            '{{%fk-transaction_agent-id_transaction}}',
            '{{%transaction_agent}}',
            'id_transaction',
            '{{%transaction}}',
            'id',
            'CASCADE'
        );

        // creates index for column `id_role`
        $this->createIndex(
            '{{%idx-transaction_agent-id_role}}',
            '{{%transaction_agent}}',
            'id_role'
        );

        // add foreign key for table `{{%agent_role}}`
        $this->addForeignKey(
            '{{%fk-transaction_agent-id_role}}',
            '{{%transaction_agent}}',
            'id_role',
            '{{%agent_role}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `{{%transaction}}`
        $this->dropForeignKey(
            '{{%fk-transaction_agent-id_transaction}}',
            '{{%transaction_agent}}'
        );

        // drops index for column `id_transaction`
        $this->dropIndex(
            '{{%idx-transaction_agent-id_transaction}}',
            '{{%transaction_agent}}'
        );

        // drops foreign key for table `{{%agent_role}}`
        $this->dropForeignKey(
            '{{%fk-transaction_agent-id_role}}',
            '{{%transaction_agent}}'
        );

        // drops index for column `id_role`
        $this->dropIndex(
            '{{%idx-transaction_agent-id_role}}',
            '{{%transaction_agent}}'
        );

        $this->dropTable('{{%transaction_agent}}');
    }
}
