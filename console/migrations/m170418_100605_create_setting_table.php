<?php

use yii\db\Migration;

/**
 * Handles the creation of table `setting`.
 */
class m170418_100605_create_setting_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('setting', [
            'id'            => $this->primaryKey(),
            'name'          => $this->string(255)->notNull(),
            'value'         => $this->text()->notNull(),
            'created_at'    => $this->bigInteger()->notNull(),
            'updated_at'    => $this->bigInteger()->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('setting');
    }
}
