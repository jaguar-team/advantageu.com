<?php

use yii\db\Migration;

class m170719_105446_add_column_to_user_table extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%user}}', 'private_account', $this->boolean()->defaultValue(0));
        $this->addColumn('{{%user}}', 'private_property', $this->boolean()->defaultValue(0));
    }

    public function safeDown()
    {
        $this->dropColumn('{{%user}}', 'private_property');
        $this->dropColumn('{{%user}}', 'private_account');
    }
}
