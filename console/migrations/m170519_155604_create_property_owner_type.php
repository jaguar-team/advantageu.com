<?php

use yii\db\Migration;

class m170519_155604_create_property_owner_type extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%property_owner_type}}', [
            'id'            => $this->primaryKey(),
            'name'          => $this->string(255)->notNull(),
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('{{%property_owner_type}}');
    }
}
