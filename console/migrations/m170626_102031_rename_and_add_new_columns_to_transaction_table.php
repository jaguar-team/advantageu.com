<?php

use yii\db\Migration;

class m170626_102031_rename_and_add_new_columns_to_transaction_table extends Migration
{
    public function safeUp()
    {
        

        $this->addColumn('{{%transaction}}', 'id_client', $this->integer()->notNull());
        $this->addColumn('{{%transaction}}', 'id_agent', $this->integer()->notNull());

        //delete column
        $this->dropForeignKey(
            '{{%fk-transaction-id_user_profile}}',
            '{{%transaction}}'
        );

        $this->dropIndex(
            '{{%idx-transaction-id_user_profile}}',
            '{{%transaction}}'
        );

        $this->dropColumn('{{%transaction}}', 'id_user_profile');

        //add foreigns
        $this->addForeignKey(
            '{{%fk-transaction-id_client}}',
            '{{%transaction}}',
            'id_client',
            '{{%user}}',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            '{{%fk-transaction-id_agent}}',
            '{{%transaction}}',
            'id_agent',
            '{{%user}}',
            'id',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey(
            '{{%fk-transaction-id_agent}}',
            '{{%transaction}}'
        );

        $this->dropForeignKey(
            '{{%fk-transaction-id_client}}',
            '{{%transaction}}'
        );

        $this->addColumn('{{%transaction}}', 'id_user_profile', $this->integer()->notNull());

        $this->createIndex(
            '{{%idx-transaction-id_user_profile}}',
            '{{%transaction}}',
            'id_user_profile'
        );

        $this->addForeignKey(
            '{{%fk-transaction-id_user_profile}}',
            '{{%transaction}}',
            'id_user_profile',
            '{{%user_profile}}',
            'id',
            'CASCADE'
        );

        $this->dropColumn('{{%transaction}}', 'id_agent');
        $this->dropColumn('{{%transaction}}', 'id_client');
    }
}
