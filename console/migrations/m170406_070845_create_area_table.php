<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%area}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%user_profile}}`
 */
class m170406_070845_create_area_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%area}}', [
            'id' => $this->primaryKey(),
            'id_user_profile' => $this->integer()->notNull(),
            'state' => $this->string(255)->notNull(),
            'short_name' => $this->string(10)->notNull(),
            'city' => $this->string(100),
            'zip_code' => $this->bigInteger()->notNull(),
        ]);

        // creates index for column `id_user_profile`
        $this->createIndex(
            '{{%idx-area-id_user_profile}}',
            '{{%area}}',
            'id_user_profile'
        );

        // add foreign key for table `{{%user_profile}}`
        $this->addForeignKey(
            '{{%fk-area-id_user_profile}}',
            '{{%area}}',
            'id_user_profile',
            '{{%user_profile}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `{{%user_profile}}`
        $this->dropForeignKey(
            '{{%fk-area-id_user_profile}}',
            '{{%area}}'
        );

        // drops index for column `id_user_profile`
        $this->dropIndex(
            '{{%idx-area-id_user_profile}}',
            '{{%area}}'
        );

        $this->dropTable('{{%area}}');
    }
}
