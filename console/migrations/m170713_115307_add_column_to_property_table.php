<?php

use yii\db\Migration;

class m170713_115307_add_column_to_property_table extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%property}}', 'title', $this->string(255));
    }

    public function safeDown()
    {
        $this->dropColumn('{{%property}}', 'title');
    }
}
