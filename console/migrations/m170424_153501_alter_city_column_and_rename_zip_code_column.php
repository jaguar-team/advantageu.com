<?php

use yii\db\Migration;

class m170424_153501_alter_city_column_and_rename_zip_code_column extends Migration
{
    public function safeUp()
    {
        $this->renameColumn('{{%area}}', 'zip_code', 'region');
        $this->alterColumn('{{%area}}', 'city', 'string not null');
    }

    public function safeDown()
    {
        echo "m170424_153501_alter_and_rename_zip_code_column cannot be reverted.\n";

        return false;
    }
}
