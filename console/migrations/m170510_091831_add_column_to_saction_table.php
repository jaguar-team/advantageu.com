<?php

use yii\db\Migration;

class m170510_091831_add_column_to_saction_table extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%section}}', 'color', $this->string(255)->null());
        $this->addColumn('{{%section}}', 'position', $this->integer(255)->unique()->null());
    }

    public function safeDown()
    {
    }
}
