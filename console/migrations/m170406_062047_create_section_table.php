<?php

use yii\db\Migration;

/**
 * Handles the creation of table `section`.
 */
class m170406_062047_create_section_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%section}}', [
            'id'            => $this->primaryKey(),
            'id_role'       => $this->integer()->notNull(),
            'name'          => $this->string(255)->notNull(),
            'created_at'    => $this->bigInteger()->notNull(),
            'updated_at'    => $this->bigInteger()->notNull(),
        ]);

        // creates index for column `id_role`
        $this->createIndex(
            '{{%idx-section-id_role}}',
            '{{%section}}',
            'id_role'
        );

        // add foreign key for table `{{%user_role}}`
        $this->addForeignKey(
            '{{%fk-section-id_role}}',
            '{{%section}}',
            'id_role',
            '{{%user_role}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `{{%user_role}}`
        $this->dropForeignKey(
            '{{%fk-section-id_role}}',
            '{{%section}}'
        );

        // drops index for column `id_role`
        $this->dropIndex(
            '{{%idx-section-id_role}}',
            '{{%section}}'
        );

        $this->dropTable('section');
    }
}
