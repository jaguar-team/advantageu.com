<?php

use yii\db\Migration;

class m170626_130604_refact_user_profile_table extends Migration
{
    public function safeUp()
    {
        /**$this->addColumn('{{%user_profile}}', 'state', $this->string(255));
        $this->addColumn('{{%user_profile}}', 'city', $this->string(255));
        $this->addColumn('{{%user_profile}}', 'address', $this->string(255));
        $this->addColumn('{{%user_profile}}', 'short_name', $this->string(255));
        $this->addColumn('{{%user_profile}}', 'zip_code', $this->string(255));
        $this->addColumn('{{%user_profile}}', 'lat', $this->string(255));
        $this->addColumn('{{%user_profile}}', 'lng', $this->string(255));**/

        //drop
        $this->dropColumn('{{%user_profile}}', 'lead_type');
        $this->dropColumn('{{%user_profile}}', 'status');
    }

    public function safeDown()
    {
        $this->addColumn('{{%user_profile}}', 'lead_type', $this->boolean()->defaultValue(1));
        $this->addColumn('{{%user_profile}}', 'status', $this->boolean()->defaultValue(1));
    }
}
