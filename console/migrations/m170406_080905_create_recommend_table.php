<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%recommend}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%user_profile}}`
 * - `{{%user_role}}`
 */
class m170406_080905_create_recommend_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%recommend}}', [
            'id' => $this->primaryKey(),
            'id_user_profile' => $this->integer()->notNull(),
            'id_role' => $this->integer()->notNull(),
            'first_name' => $this->string(255)->notNull(),
            'last_name' => $this->string(255)->notNull(),
            'email' => $this->string(255)->notNull(),
            'company' => $this->string(255),
            'phone_number' => $this->string(255),
            'body' => $this->text(),
            'created_at' => $this->bigInteger()->notNull(),
            'updated_at' => $this->bigInteger()->notNull(),
        ]);

        // creates index for column `id_user_profile`
        $this->createIndex(
            '{{%idx-recommend-id_user_profile}}',
            '{{%recommend}}',
            'id_user_profile'
        );

        // add foreign key for table `{{%user_profile}}`
        $this->addForeignKey(
            '{{%fk-recommend-id_user_profile}}',
            '{{%recommend}}',
            'id_user_profile',
            '{{%user_profile}}',
            'id',
            'CASCADE'
        );

        // creates index for column `id_role`
        $this->createIndex(
            '{{%idx-recommend-id_role}}',
            '{{%recommend}}',
            'id_role'
        );

        // add foreign key for table `{{%user_role}}`
        $this->addForeignKey(
            '{{%fk-recommend-id_role}}',
            '{{%recommend}}',
            'id_role',
            '{{%user_role}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `{{%user_profile}}`
        $this->dropForeignKey(
            '{{%fk-recommend-id_user_profile}}',
            '{{%recommend}}'
        );

        // drops index for column `id_user_profile`
        $this->dropIndex(
            '{{%idx-recommend-id_user_profile}}',
            '{{%recommend}}'
        );

        // drops foreign key for table `{{%user_role}}`
        $this->dropForeignKey(
            '{{%fk-recommend-id_role}}',
            '{{%recommend}}'
        );

        // drops index for column `id_role`
        $this->dropIndex(
            '{{%idx-recommend-id_role}}',
            '{{%recommend}}'
        );

        $this->dropTable('{{%recommend}}');
    }
}
