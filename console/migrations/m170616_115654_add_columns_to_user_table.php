<?php

use yii\db\Migration;

class m170616_115654_add_columns_to_user_table extends Migration
{
    public function safeUp()
    {
		$this->addColumn('{{%user}}', 'id_zoho', $this->string(255)->null()->unique());
	    $this->addColumn('{{%user}}', 'id_google', $this->string(255)->null()->unique());
	    $this->addColumn('{{%user}}', 'id_facebook', $this->string(255)->null()->unique());
    }

    public function safeDown()
    {
	    $this->dropColumn('{{%user}}', 'id_zoho');
	    $this->dropColumn('{{%user}}', 'id_google');
	    $this->dropColumn('{{%user}}', 'id_facebook');
    }
}
