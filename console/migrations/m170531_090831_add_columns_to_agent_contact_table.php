<?php

use yii\db\Migration;

class m170531_090831_add_columns_to_agent_contact_table extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%agent_contact}}', 'status', $this->boolean()->defaultValue(0)->notNull());
    }

    public function safeDown()
    {
        $this->dropColumn('{{%agent_contact}}', 'status');
    }
}
