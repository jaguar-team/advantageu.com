<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 016 16.08.17
 * Time: 17:11
 */

namespace console\controllers;

use yii;
use yii\console\Controller;
use yii\console\ExitCode;
use yii\helpers\Console;
use common\components\ZohoImplantation;

class ZohoController extends Controller
{
    public $defaultAction = 'help';

    protected $help_list = [
        'add-agents'   => 'Get all agents from Zoho and pull them into database.'
    ];

    public function actionHelp()
    {
        foreach ($this->help_list as $name => $description) {
            echo "\nThe following commands are available: \n\n\nzoho/$name                    $description";
        }

        return ExitCode::OK;
    }

    /**
     * Implantation all BIOS from Zoho::MODULE_AGENT_BIO
     * @param bool|integer $from
     * @param bool|integer $to
     * @param bool|integer $user_count
     * @return int
     * @throws yii\base\InvalidConfigException
     */
    public function actionImplantationAgentBios($from = false, $to = false, $user_count = false)
    {
        $zoho_implantation = \Yii::createObject(['class' => ZohoImplantation::className()]);
        $this->stdout("WAITING...\n", Console::BOLD);
        $count = $this->ansiFormat($zoho_implantation->agentBios($from, $to, $user_count), Console::FG_YELLOW);
        echo "Numbers of agents added to database: $count \n";
        return ExitCode::OK;
    }
}