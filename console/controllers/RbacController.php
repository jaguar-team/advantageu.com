<?php
/**
 * Created by PhpStorm.
 * User: Vlad
 * Date: 06.04.2017
 * Time: 16:29
 */

namespace console\controllers;

use Yii;
use yii\console\Controller;
use common\rbac\UserGroupRule;

class RbacController extends Controller
{
    public function actionInit()
    {
        $authManager = \Yii::$app->authManager;

        // Create roles
        $agent  = $authManager->createRole('agent');
        $admin  = $authManager->createRole('admin');

        // Create simple, based on action{$NAME} permissions
        $login  = $authManager->createPermission('login');
        $logout = $authManager->createPermission('logout');
        /** Admin panel */
        $loginAdmin  = $authManager->createPermission('loginAdmin');

        // Add permissions in Yii::$app->authManager
        $authManager->add($login);
        /** Admin panel */
        $authManager->add($loginAdmin);


        // Add rule, based on UserExt->group === $user->group
        $userGroupRule = new UserGroupRule();
        $authManager->add($userGroupRule);

        // Add rule "UserGroupRule" in roles
        $agent->ruleName  = $userGroupRule->name;
        $admin->ruleName  = $userGroupRule->name;

        // Add roles in Yii::$app->authManager
        $authManager->add($admin);
        $authManager->add($agent);

        // Add permission-per-role in Yii::$app->authManager
        // Agent
        $authManager->addChild($agent, $login);

        // Admin
        $authManager->addChild($admin, $loginAdmin);
    }
}