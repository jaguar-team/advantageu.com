<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 031 31.08.17
 * Time: 15:33
 */

namespace console\controllers;

use yii\console\Controller;
use yii\console\ExitCode;
use yii\helpers\Console;
use common\components\GooglePlaceImplantation;

class ConvertDataController extends Controller
{
    /**
     * Convert all zip codes from BIOS to user Area
     * @return int
     * @throws \yii\base\InvalidConfigException
     */
    public function actionAllBiosZipCodesToUserArea()
    {
        $google_place_implantation = \Yii::createObject(['class' => GooglePlaceImplantation::className()]);
        $this->stdout("WAITING...\n", Console::BOLD);
        $results = $google_place_implantation->convertAllBiosZipCodesToUserArea();

        if ($results) {
            foreach ($results as $id => $count) {
                echo "ID number $id --- ".$this->ansiFormat($count, Console::FG_YELLOW)." added areas\n";
            }
        } else {
            echo 'Already up to date.';
        }
        return ExitCode::OK;
    }
}