<?php
/**
 * Created by PhpStorm.
 * User: Vlad
 * Date: 25.04.2017
 * Time: 14:06
 */

namespace frontend\models;

use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\data\ActiveDataProvider;
use common\models\User;
use common\models\UserProfile;
use common\models\LoginForm;
use common\models\Customer;
use common\models\CustomerBid;
use frontend\components\AgentSearchStep;

class AgentSearch extends Model
{
    CONST SCENARIO_SEARCH = 'search';
    CONST SCENARIO_SEARCH_MODIFY = 'search_modify';
    CONST SCENARIO_STEP_LOCATION = 'location';
    CONST SCENARIO_STEP_ACTION = 'action';
    CONST SCENARIO_STEP_OTHER_ACTION = 'other_action';
    CONST SCENARIO_STEP_PERIOD = 'period';
    CONST SCENARIO_STEP_PRICE = 'price';
    CONST SCENARIO_STEP_PROPERTY = 'property';
    CONST SCENARIO_STEP_CUSTOMER_INFO = 'customer_info';

    public $area;

    public $lead_type;

    public $property_type;

    public $price;

    public $period;

    public $other_action;

    public $email;

    public $phone;

    public $first_name;

    public $last_name;

    public $location;

    public $step;

    public $_customer;

    public $_customer_bid;

    /**
     * AgentSearch constructor.
     * @param array $config
     */
    public function __construct(Customer $customer, CustomerBid $customer_bid, array $config = [])
    {
        $this->_customer = $customer;
        $this->_customer_bid = $customer_bid;

        parent::__construct($config);
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            //area rules
            ['area', 'required', 'except' => [self::SCENARIO_SEARCH_MODIFY]],
            [['area', 'location'], 'filter', 'filter' => function($areas) {
                if ($areas) {
                    if (is_array($areas)) {
                        return $this->location = $areas;
                    } else {
                        $this->location = json_decode($areas, true);
                        return $this->location;
                    }
                } else {
                    return null;
                }
            }],

            //lead type rules
            ['lead_type', 'required', 'except' => [self::SCENARIO_SEARCH_MODIFY]],
            ['lead_type', 'integer'],

            //property rules
            ['property_type', 'required', 'except' => [self::SCENARIO_SEARCH_MODIFY]],
            ['property_type', 'integer'],

            //price rules
            ['price', 'required', 'except' => [self::SCENARIO_SEARCH_MODIFY]],
            ['price', 'integer'],

            //period rules
            ['period', 'required', 'except' => [self::SCENARIO_SEARCH_MODIFY]],
            ['period', 'integer'],

            //other action rules
            ['other_action', 'required', 'except' => [self::SCENARIO_SEARCH_MODIFY]],
            ['other_action', 'boolean'],

            //email rules
            ['email', 'required', 'except' => [self::SCENARIO_SEARCH_MODIFY]],
            ['email', 'string', 'max' => 255],
            ['email', 'email'],
            ['email', 'trim'],
            ['email', 'unique', 'targetClass' => User::className(), 'message' => 'This email is already exists.'],

            //phone rules
            ['phone', 'required', 'except' => [self::SCENARIO_SEARCH_MODIFY]],
            ['phone', 'match', 'pattern' => '/^\([0-9]{3}\)[0-9]{3}-[0-9]{4}$/i', 'message' => 'Allowable format: (999)999-9999'],

            //first name rules
            ['first_name', 'required', 'except' => [self::SCENARIO_SEARCH_MODIFY]],
            ['first_name', 'trim'],
            ['first_name', 'string', 'max' => 255, 'min' => 1],
            ['first_name', 'match', 'pattern' => '/^[A-z]{1,255}$/i', 'message' => 'Allowable characters: A-z'],

            //last name rules
            ['last_name', 'required', 'except' => [self::SCENARIO_SEARCH_MODIFY]],
            ['last_name', 'trim'],
            ['last_name', 'string', 'max' => 255, 'min' => 1],
            ['last_name', 'match', 'pattern' => '/^[A-z]{1,255}$/i', 'message' => 'Allowable characters: A-z'],

            //location rules
            ['location', 'safe'],
            ['location', 'required', 'except' => [self::SCENARIO_SEARCH_MODIFY]],

            //step rules
            ['step', 'safe', 'except' => [self::SCENARIO_SEARCH_MODIFY]],
        ];
    }

    /**
     * @inheritdoc
     * @return array
     */
    public function scenarios()
    {
        return ArrayHelper::merge([
            self::SCENARIO_SEARCH               => ['lead_type', 'property_type', 'price', 'period', 'other_action', 'location'],
            self::SCENARIO_SEARCH_MODIFY        => ['lead_type', 'property_type', 'price', 'period', 'other_action', 'location'],
            self::SCENARIO_STEP_LOCATION        => ['area'],
            self::SCENARIO_STEP_ACTION          => ['lead_type'],
            self::SCENARIO_STEP_OTHER_ACTION    => ['other_action'],
            self::SCENARIO_STEP_PERIOD          => ['period'],
            self::SCENARIO_STEP_PRICE           => ['price'],
            self::SCENARIO_STEP_PROPERTY        => ['property_type'],
            self::SCENARIO_STEP_CUSTOMER_INFO   => ['email', 'phone', 'first_name', 'last_name'],
        ], parent::scenarios());
    }

    /**
     * @return mixed
     */
    public function register($login = false)
    {
	    $_agent_search_step = \Yii::createObject(['class' => AgentSearchStep::className()]);
    	$search_data = $_agent_search_step->getSearch();
        $password = \Yii::$app->security->generateRandomString(8);

	    $user_model = \Yii::createObject([
		    'class'         => User::className(),
		    'id_role'       => isset($search_data['lead_type']) ? $search_data['lead_type'] : '',
		    'email'         => isset($search_data['email']) ? $search_data['email'] : '',
		    'password_hash' => $password,
	    ]);

	    $user_profile_model = \Yii::createObject([
		    'class'         => UserProfile::className(),
		    'first_name'    => isset($search_data['first_name']) ? $search_data['first_name'] : '',
		    'last_name'     => isset($search_data['last_name']) ? $search_data['last_name'] : '',
		    'phone_number'  => isset($search_data['phone']) ? $search_data['phone'] : '',
	    ]);

	    $user_model->populateRelation('profile',  $user_profile_model);

	    if ($user_model->save()) {

	        if ($login) { //login
                $_login_form = \Yii::createObject([
                    'class'     => LoginForm::className(),
                    'email'     => isset($search_data['email']) ? $search_data['email'] : '',
                    'password'  => $password,
                ]);

                return $_login_form->login();
            }

	        return true;
        }

	    return false;
    }

    /**
     * @return bool|object
     */
    public function search()
    {
        $_agent_search_step = \Yii::createObject(['class' => AgentSearchStep::className()]);
        $this->scenario = self::SCENARIO_SEARCH;

        if (!$search_data = $_agent_search_step->getSearch()) {
            return false;
        }

        $this->load([$this->formName() => $search_data]);

        //construct select
        $select = [
            'u.*',
            'transaction_quantity'          => 'COUNT(t.id)',
            'average_price'                 => 'SUM(ti.selling_price)/COUNT(t.id)',
            'average_days_on_market'        => 'round(SUM(DATEDIFF(FROM_UNIXTIME(ti.selling_date), FROM_UNIXTIME(ti.listing_date)))/COUNT(t.id))'
        ];

        if ($this->property_type) {
            $select['transaction_quantity_by_type'] = 'SUM(CASE WHEN p.id_type = '.$this->property_type.' THEN 1 ELSE 0 END)';
        }

        $query = User::find()
            ->select($select)
            ->alias('u')
            ->joinWith('role ur')
            ->joinWith('areas a')
            ->joinWith('profile up')
            ->joinWith(['transactions t' => function($q_transaction) {
                $q_transaction->joinWith('transactionInfo ti');
                $q_transaction->joinWith(['property p' => function($q_property) {
                    $q_property->joinWith('location pl');
                    $q_property->joinWith('type pt');
                }]);
            }])
            ->where(['ur.is_agent' => User::STATUS_ACTIVE, 'u.status' => User::STATUS_ACTIVE])
            ->groupBy('u.id')
            ->orderBy('u.certificate DESC');

        $data_provider = \Yii::createObject([
            'class'         => ActiveDataProvider::className(),
            'query'         => $query,
            'pagination'    => [
                'pageSize'  => 20,
            ],
            'sort' => [
                'defaultOrder' => [
                    'transaction_quantity' => 'DESC',
                ],
                'attributes' => [
                    'transaction_quantity' => [
                        'label' => 'Transaction quantity',
                    ],
                    'average_price' => [
                        'label' => 'Avg price',
                    ],
                    'average_days_on_market' => [
                        'label' => 'Avg days on market',
                    ],
                ],
            ]
        ]);

        $query->andFilterWhere([
            'a.state'       => isset($this->location['state']) ? $this->location['state'] : '',
            'a.city'        => isset($this->location['city']) ? $this->location['city'] : '',
            //'pl.state'      => isset($this->location['state']) ? $this->location['state'] : '',
            //'pl.city'       => isset($this->location['city']) ? $this->location['city'] : '',
        ]);

        //$query->asArray();

        //var_dump($data_provider->getModels());
        return $data_provider;
    }
}