<?php
/**
 * Created by PhpStorm.
 * User: Vlad
 * Date: 25.04.2017
 * Time: 17:10
 */

namespace frontend\controllers;

use common\components\ZohoImplantation;
use Yii;
use yii\helpers\Url;
use yii\web\Response;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Inflector;
use frontend\models\AgentSearch;
use frontend\components\AgentSearchStep;
use common\models\User;
use common\models\UserRole;
use common\models\UserProfile;
use common\models\LoginForm;
use common\components\GoogleAuth;
use common\components\Zoho;
use dosamigos\google\maps\services\DirectionsClient;

class HomeController extends Controller {
	public $defaultAction = 'home';

	/**
	 * @inheritdoc
	 */
	public function behaviors() {
		return [
			'access' => [
				'class' => AccessControl::className(),
				'only'  => [ 'logout' ],
				'rules' => [
					[
						'allow'   => true,
						'actions' => [ 'sign-out' ],
						'roles'   => [ '@' ],
					],
				],
			],
			'verbs'  => [
				'class'   => VerbFilter::className(),
				'actions' => [
					'home'         => [ 'get', 'post' ],
					'sign-in'      => [ 'post' ],
					'sign-out'     => [ 'post' ],
					'registration' => [ 'post' ],
				],
			],
		];
	}

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

	/**
	 * @return string
	 */
	public function actionHome() {

		//var_dump(@Yii::$app->params);
		//exit();
		//$direction = new DirectionsClient([
			//'params' => [
				//'language' => \Yii::$app->language,
				//'origin' => 'street from',
				//'destination' => 'street to'
			//]
		//]);
		//var_dump($direction->lookup());
		//$place = Yii::$app->googlePlacesSearch->autoComplete('Williamston', 'en', ['components' => 'country:us', 'types' => '(regions)']);
		//var_dump(Yii::$app->googlePlaces->details($place->predictions[0]->place_id));
		//exit();

		//$zoho = \Yii::createObject(['class' => Zoho::className()]);
		//var_dump($zoho->setModule(Zoho::MODULE_AGENT_BIO)->getRecords());
		//$user_model = User::findOne(20);
		//var_dump($user_model->getTransactionRelation()->bestLocation());
		//exit();
		//$blogPosts = Yii::$app->blog->getPosts([
		//	'post_status' => 'publish',
		//	'number' => 10
		//], ['post_title', 'post_content', 'post_author']);
		//var_dump($blogPosts);
		ini_set('xdebug.var_display_max_depth', -1);
		ini_set('xdebug.var_display_max_children', -1);
		ini_set('xdebug.var_display_max_data', -1);
		//$zoho = Yii::createObject(['class' => Zoho::className()]);
		//$res = $zoho->setData([['Home Seller Name' => 'Jaguar Team', 'Lead Type' => 'Seller', 'Lead Source' => 'Referral']])->setModule('CustomModule4')->insertRecord();
		//$user = User::findOne(20);
		//var_dump($user->getTransactions());
		//exit();
		$agent_search = \Yii::createObject( [ 'class'    => AgentSearch::className(),
		                                      'scenario' => AgentSearch::SCENARIO_STEP_LOCATION
		] );
		$google_auth  = \Yii::$app->googleAuth;

		if ( $google_auth->isExistsCode() ) {
			$google_auth->getToken()->setUserInfo()->setUserModel()->setUserProfileModel();
		}

		if ( $agent_search->load(\Yii::$app->request->post()) && $agent_search->validate() ) {
			$agent_search_step = \Yii::createObject(['class' => AgentSearchStep::className()]);
			$agent_search_step->setSearchParam('location', $agent_search->area );
			$agent_search_step->setSearchParam('step', 1);
			$this->redirect( 'agent-search' );
		}

		return $this->render( 'home', [
			'search'      => $agent_search,
			'google_auth' => $google_auth,
		] );
	}

	/**
	 * *
	 * @return mixed|Response
	 */
	public function actionSignIn() {
		$response = [ 'status' => false ];

		if ( \Yii::$app->request->isAjax && \Yii::$app->user->isGuest ) {
			$login_form = \Yii::createObject( [
				'class'      => LoginForm::className(),
				'email'      => \Yii::$app->request->post( 'email' ),
				'password'   => \Yii::$app->request->post( 'password' ),
				'rememberMe' => \Yii::$app->request->post( 'rememberMe' ),
			] );

			if ( $login_form->load( \Yii::$app->request->post() ) && $login_form->login() ) {
				$response['status']       = true;
				$response['redirect_url'] = \Yii::$app->request->referrer;
			}

			$response['errors'][ $login_form->formName() ] = $login_form->errors;
		}

		\Yii::$app->response->format = Response::FORMAT_JSON;

		return $response;
	}

	/**
	 * @return Response
	 */
	public function actionSignOut() {
		\Yii::$app->user->logout();

		return $this->goBack();
	}

	/**
	 * @return mixed
	 */
	public function actionRegistration() {
		$response = [ 'status' => false ];

		if ( \Yii::$app->request->isAjax && \Yii::$app->user->isGuest ) {
			$user_model         = \Yii::createObject( [ 'class' => User::className() ] );
			$user_profile_model = \Yii::createObject( [ 'class' => UserProfile::className() ] );

			if ( $user_model->load( \Yii::$app->request->post() ) && $user_model->validate() &&
			     $user_profile_model->load( \Yii::$app->request->post() ) && $user_profile_model->validate()
			) {

				$user_model->populateRelation( 'profile', $user_profile_model );
				$login_form = \Yii::createObject( [
					'class'    => LoginForm::className(),
					'email'    => $user_model->email,
					'password' => $user_model->password_hash,
				] );

				if ( $user_model->save() ) {
					//save user cookies

					$login_form->login();

					//$response['password'] = $login_form->password;
					$response['status']       = true;
					$response['redirect_url'] = Url::toRoute( [ 'agent/profile', 'id' => $user_model->id ], true );
				}
			}

			$response['errors'][ $user_model->formName() ]         = $user_model->errors;
			$response['errors'][ $user_profile_model->formName() ] = $user_profile_model->errors;
		}

		\Yii::$app->response->format = Response::FORMAT_JSON;

		return $response;
	}
}