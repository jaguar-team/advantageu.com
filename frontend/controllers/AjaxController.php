<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 020 20.07.17
 * Time: 17:34
 */

namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\User;
use common\models\Favorite;

class AjaxController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['add-favorite', 'delete-favorite'],
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'add-favorite'        => ['POST'],
                    'delete-favorite'     => ['POST'],
                ],
            ],
        ];
    }

    /**
     * @param $id
     * @return array
     */
    public function actionDeleteFavorite()
    {
        $response = ['status' => false];
        $id = \Yii::$app->request->post('id');

        if (\Yii::$app->request->isAjax && $id) {
            $favorite_model = Favorite::findOne($id);
            $id_favorite = $favorite_model->id_favorite;
            if ($favorite_model) {
                if ($favorite_model->delete()) {
                    $response['data']['id_favorite'] = $id_favorite;
                    $response['status'] = true;
                } else {
                    $response['errors'] = $favorite_model->errors;
                }
            } else {
                $response['errors'][] = 'Favorite with id '.$id.' not found.';
            }
        }

        \Yii::$app->response->format = Response::FORMAT_JSON;
        return $response;
    }

    /**
     * @param $id
     * @return array
     */
    public function actionAddFavorite()
    {
        $response = ['status' => false];
        $id = \Yii::$app->request->post('id');

        if (\Yii::$app->request->isAjax && $id) {
            $id_user = \Yii::$app->user->identity->id;
            $user_model = User::findOne($id_user);

            if ($user_model) {
                $favorite_model = \Yii::createObject([
                    'class'             => Favorite::className(),
                    'id_user'           => $user_model->id,
                    'id_favorite'       => $id
                ]);

                if ($favorite_model->validate() && $favorite_model->save()) {
                    $response['data']['id'] = $favorite_model->id;
                    $response['status'] = true;
                } else {
                    $response['errors'] = $favorite_model->errors;
                }

            } else {
                $response['errors'][] = 'User is not found.';
            }
        }

        \Yii::$app->response->format = Response::FORMAT_JSON;
        return $response;
    }
}