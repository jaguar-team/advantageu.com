<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 003 03.05.17
 * Time: 13:53
 */

namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\Response;
use frontend\models\AgentSearch;
use frontend\components\AgentSearchStep;
use frontend\components\ResultMatches;
use common\models\User;
use common\models\UserProfile;
use common\models\AgentContact;

class AgentSearchController extends Controller
{
    /** @var string  */
    public $layout = 'search';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'index'             => ['get', 'post'],
                    'result'            => ['get'],
                    'result-update'     => ['get', 'post'],
                    'set-step'          => ['post'],
                ],
            ],
            'access' => [
                'only' => ['result', 'result-matches', 'add-match', 'delete-match'],
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['result', 'result-matches', 'add-match', 'delete-match'],
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Display current step
     * @return string
     */
    public function actionIndex()
    {
        $agent_search_step = \Yii::createObject(['class' => AgentSearchStep::className()]);

        //if search data is completed
        if ($agent_search_step->existsSearchParams(true)) {
            return $this->redirect(['result']);
        }


        if ($agent_search_step->_agent_search) {
            if ($agent_search_step->_agent_search->load(\Yii::$app->request->post()) &&
                $agent_search_step->_agent_search->validate()) {
                $agent_search_step->setNextStep();
            }
        }

        if ($agent_search_step->isLastStep()) {

            if (\Yii::$app->user->isGuest) {
                if ($agent_search_step->_agent_search->register(true)) {
                    $this->redirect(['result', 'best' => true]);
                }
            } else {
                $this->redirect(['result', 'best' => true]);
            }
        }

        //if empty search data
        if (!$agent_search_step->getSearch() || empty($agent_search_step->getSearch())) {
            return $this->goHome();
        }

        return $this->render('/home/search/index', [
            'model'         => $agent_search_step->_agent_search,
            'part'          => $agent_search_step->getStepData('view'),
            'search_data'   => $agent_search_step->getSearch(),
        ]);
    }

    /**
     * Display search result
     * @return string
     */
    public function actionResult($best = false)
    {
        $agent_search_step = \Yii::createObject(['class' => AgentSearchStep::className()]);

        if ($agent_search_step->existsSearchParams()) {
            $agent_search = \Yii::createObject(['class' => AgentSearch::className(), 'scenario' => AgentSearch::SCENARIO_SEARCH]);
            $result_matches = \Yii::createObject(['class' => ResultMatches::className()]);
            $agent_search_data_provider = $agent_search->search();

            //add best results
            if ($best) {
                $best_users = array_slice($agent_search->search()->getModels(), 0, 3);
                foreach ($best_users as $index => $value) {
                    $result_matches->add($value->id);
                }
            }

            return $this->render('/home/search/result', [
                'agent_search' => $agent_search,
                'result_matches' => $result_matches,
                'agent_search_step' => $agent_search_step,
                'agent_search_data_provider' => $agent_search_data_provider,
            ]);
        } else {
            return $this->redirect(['index']);
        }
    }

    /**
     * Update search result
     * @return string
     */
    public function actionResultUpdate()
    {
        $agent_search = \Yii::createObject(['class' => AgentSearch::className(), 'scenario' => AgentSearch::SCENARIO_SEARCH_MODIFY]);
        $agent_search_step = \Yii::createObject(['class' => AgentSearchStep::className()]);

        if ($agent_search->load(\Yii::$app->request->post()) && $agent_search->validate()) {
            foreach (\Yii::$app->request->post($agent_search->formName()) as $key => $value) {
                $agent_search_step->setSearchParam($key, $agent_search->{$key});
            }
        }
        return $this->redirect(['index']);
    }

    /**
     * Change step
     * @return array
     */
    public function actionSetStep()
    {
        $response = ['status' => false];

        if (\Yii::$app->request->isAjax && $step = \Yii::$app->request->post('step')) {
            $agent_search_step = \Yii::createObject(['class' => AgentSearchStep::className()]);
            if ($step < $agent_search_step->getSearchParam('step')) {
                $step = $agent_search_step->setStep($step);
                $response['status'] = true;
                $response['step'] = $step;
            }
        }

        \Yii::$app->response->format = Response::FORMAT_JSON;

        return $response;
    }

    /**
     * @param \yii\base\Action $action
     * @return bool
     */
    public function beforeAction($action)
    {
        $agent_search_step = \Yii::createObject(['class' => AgentSearchStep::className()]);

        // Remove extra params
        if (!$agent_search_step->isLastStep() && $agent_search_step->isCorrectStep()) {
            $agent_search_step->removeExtraParams();
        }

        // Check required params
        if ($action->id == 'index' ||
            $action->id == 'set-step' ||
            $action->id == 'result-matches') {
            $current_step = false;
        } else {
            $current_step = true;
        }

        if (!$agent_search_step->existsSearchParams($current_step)) {
            $this->goHome();
        }

        if (!parent::beforeAction($action)) {
            return false;
        }

        return true;
    }
}