<?php

namespace frontend\controllers;

use common\models\UserAddress;
use Yii;
use yii\web\Controller;
use yii\helpers\Url;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\Response;
use yii\web\NotFoundHttpException;
use common\models\User;
use common\models\Area;
use common\models\Language;
use common\models\Review;
use common\models\Testimonial;
use common\models\SectionValue;
use common\models\Recommend;
use common\models\Transaction;
use common\models\TransactionInfo;
use common\models\TransactionAgent;
use common\models\TransactionClient;
use common\models\TransactionType;
use common\models\Property;
use common\models\PropertyType;
use common\models\PropertyOwnerType;
use common\models\PropertyStatus;
use common\models\PropertyLocation;
use common\models\UploadFileForm;
use common\models\AgentContact;

class AgentController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        //'actions' => ['delete-area', 'delete-language'],
                        //'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'profile'               => ['GET', 'POST'],
                    'delete-address'        => ['POST'],
                    'delete-language'       => ['POST'],
                    'delete-area'           => ['POST'],
                    'delete-section-value'  => ['POST'],
                    'delete-transaction'    => ['POST'],
                    'delete-recommend'      => ['POST'],
                    'delete-testimonial'    => ['POST'],
                    'delete-review'         => ['POST'],
                    'delete-agent-contact'  => ['POST'],
                    'change-review-status'  => ['POST'],
                    'change-contact-status' => ['POST'],
                    'approve-property'      => ['POST'],
                ],
            ],
        ];
    }

    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionProfile($id)
    {
        $user_model = User::findOne($id);

        if (!$user_model) {
            throw new NotFoundHttpException();
        }

        $user_profile_model = $user_model->profile;
        $user_address = \Yii::createObject(['class' => UserAddress::className()]);
        $area_model = \Yii::createObject(['class' => Area::className(), 'scenario' => Area::SCENARIO_SKIP_ID_PROFILE]);
        $language_model = \Yii::createObject(['class' => Language::className(), 'scenario' => Language::SCENARIO_SKIP_ID_PROFILE]);
        $section_value_model = \Yii::createObject(['class' => SectionValue::className(), 'scenario' => SectionValue::SCENARIO_SKIP_ID_USER]);
        $upload_file_form = \Yii::createObject(['class' => UploadFileForm::className(), 'scenario' => UploadFileForm::SCENARIO_USER_AVATAR]);
        $agent_contact_model = \Yii::createObject(['class' => AgentContact::className(), 'scenario' => AgentContact::SCENARIO_SKIP_ID_PROFILE]);

        /** update user data */
        if ($user_model->load(\Yii::$app->request->post()) && $user_model->validate()) {
            if ($user_model->save()) {
                \Yii::$app->session->setFlash('toast-success', 'User with id '.$id.' has been updated.');
            }
        }

        /** update user profile */
        if ($user_profile_model->load(\Yii::$app->request->post()) && $user_profile_model->validate()) {
            if ($user_profile_model->save()) {
                \Yii::$app->session->setFlash('toast-success', 'User with id '.$id.' has been updated.');
            }
        }

        /** upload avatar */
        if ($upload_file_form->load(\Yii::$app->request->post()) && $upload_file_form->validate()) {
            if ($file_name = $upload_file_form->upload()) {
                $user_profile_model->photo = $file_name;
                if ($user_profile_model->save()) {
                    \Yii::$app->session->setFlash('toast-success', 'User with id '.$id.' has been updated.');
                }
            }
        }

        /** save user address */
        if ($user_address->load(\Yii::$app->request->post()) && $user_address->validate()) {
            $user_model->link('addresses', $user_address);
            \Yii::$app->session->setFlash('toast-success', 'Addresses has been added .');
        }

        /** add new area */
        if ($area_model->load(\Yii::$app->request->post()) && $area_model->validate()) {
            $user_profile_model->link('areas', $area_model);
            \Yii::$app->session->setFlash('toast-success', 'Area has been added.');
        }

        /** add language */
        if ($language_model->load(\Yii::$app->request->post()) && $language_model->validate()) {
            $user_profile_model->link('languages', $language_model);
            \Yii::$app->session->setFlash('toast-success', 'Language has been added.');
        }

        /** add section value */
        if ($section_value_model->load(\Yii::$app->request->post()) && $section_value_model->validate()) {
            $user_model->link('sectionValues', $section_value_model);
            \Yii::$app->session->setFlash('toast-success', 'Section value has been added.');
        }

        /** add new request for agent */
        if ($agent_contact_model->load(\Yii::$app->request->post()) && $agent_contact_model->validate()) {
            $user_profile_model->link('agentContacts', $agent_contact_model);
            \Yii::$app->session->setFlash('toast-success', 'Agent contact value has been added.');
        }

        return $this->render('profile', [
            'user_model'                => $user_model,
            'user_profile_model'        => $user_profile_model,
            'upload_file_form'          => $upload_file_form,
            'area_model'                => $area_model,
            'language_model'            => $language_model,
            'section_value_model'       => $section_value_model,
            'agent_contact_model'       => $agent_contact_model,
        ]);
    }

    /**
     * Display all reviews & add new
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionReview($id)
    {
        $user_model = User::findOne($id);
        if (!$user_model) {
            throw new NotFoundHttpException();
        }

        $review_model = \Yii::createObject(['class' => Review::className(), 'scenario' => Review::SCENARIO_SKIP_ID_USER]);

        if ($review_model->load(\Yii::$app->request->post()) && $review_model->validate()) {
            $user_model->link('reviews', $review_model);
            \Yii::$app->session->setFlash('toast-success', 'Reviews has been added.');
        }

        $agent_contact_model = \Yii::createObject(['class' => AgentContact::className(), 'scenario' => AgentContact::SCENARIO_SKIP_ID_PROFILE]);
        /** add new request for agent */
        if ($agent_contact_model->load(\Yii::$app->request->post()) && $agent_contact_model->validate()) {
            $user_model->profile->link('agentContacts', $agent_contact_model);
            \Yii::$app->session->setFlash('toast-success', 'Agent contact value has been added.');
        }

        return $this->render('review', [
            'user_model'            => $user_model,
            'review_model'          => $review_model,
            'agent_contact_model'   => $agent_contact_model,
        ]);
    }

    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionAddProperty($id)
    {
        $user_model = User::findOne($id);

        if (!$user_model) {
            throw new NotFoundHttpException();
        }

        $transaction_model = \Yii::createObject(['class' => Transaction::className()]);
        $transaction_info_model = \Yii::createObject(['class' => TransactionInfo::className()]);
        $property_model = \Yii::createObject(['class' => Property::className()]);
        $property_location_model = \Yii::createObject(['class' => PropertyLocation::className()]);

        if ($transaction_model->load(\Yii::$app->request->post()) && $transaction_model->validate() &&
            $transaction_info_model->load(\Yii::$app->request->post()) && $transaction_info_model->validate() &&
            $property_model->load(\Yii::$app->request->post()) && $property_model->validate() &&
            $property_location_model->load(\Yii::$app->request->post()) && $property_location_model->validate()) {

            $transaction_model->populateRelation('transactionInfo', $transaction_info_model);
            $transaction_model->populateRelation('property', $property_model);
            $property_model->populateRelation('location', $property_location_model);

            /** save transaction */
            $user_model->link('transactions', $transaction_model);

            $this->redirect(['profile', 'id' => $id]);
        }

        return $this->render('add-property', [
            'user_model'                => $user_model,
            'transaction_model'         => $transaction_model,
            'transaction_info_model'    => $transaction_info_model,
            'property_model'            => $property_model,
            'property_location_model'   => $property_location_model,
        ]);
    }

    /**
     * Delete user address
     * @param $id
     * @return mixed
     */
    public function actionDeleteAddress($id)
    {
        $user_address_model = UserAddress::findOne($id);

        if (!$user_address_model) {
            \Yii::$app->session->setFlash('toast-warning', 'Address with id '.$id.' does not exists');
        } else {
            if ($user_address_model->delete()) {
                \Yii::$app->session->setFlash('toast-success', 'Address with id '.$id.' has been deleted.');
            } else {
                \Yii::$app->session->setFlash('toast-error', 'An error has occurred trying to delete address.');
            }
        }

        return $this->run('profile', ['id' => $user_address_model->user->id]);
    }

    /**
     * Delete area
     * @param $id
     * @return mixed
     */
    public function actionDeleteArea($id)
    {
        $area_model = Area::findOne($id);
        $user_id = $area_model->userProfile->user->id;

        if (!$area_model) {
            \Yii::$app->session->setFlash('toast-warning', 'Area with id '.$id.' does not exists');
        } else {
            if ($area_model->delete()) {
                \Yii::$app->session->setFlash('toast-success', 'Area with id '.$id.' has been deleted.');
            } else {
                \Yii::$app->session->setFlash('toast-error', 'An error has occurred trying to delete area.');
            }
        }

        return $this->run('profile', ['id' => $user_id]);
    }

    /**
     * Delete language
     * @param $id
     * @return mixed
     */
    public function actionDeleteLanguage($id)
    {
        $language_model = Language::findOne($id);
        $user_id = $language_model->userProfile->user->id;

        if (!$language_model) {
            \Yii::$app->session->setFlash('toast-warning', 'Language with id '.$id.' does not exists');
        } else {
            if ($language_model->delete()) {
                \Yii::$app->session->setFlash('toast-success', 'Language with id '.$id.' has been deleted.');
            } else {
                \Yii::$app->session->setFlash('toast-error', 'An error has occurred trying to delete language.');
            }
        }

        return $this->run('profile', ['id' => $user_id]);
    }

    public function actionDeleteTransaction($id)
    {
        $transaction_model = Transaction::findOne($id);
        $user_id = $transaction_model->userProfile->user->id;

        if (!$transaction_model) {
            \Yii::$app->session->setFlash('toast-warning', 'Transaction with id '.$id.' does not exists');
        } else {
            if ($transaction_model->delete()) {
                \Yii::$app->session->setFlash('toast-success', 'Transaction with id '.$id.' has been deleted.');
            } else {
                \Yii::$app->session->setFlash('toast-error', 'An error has occurred trying to delete transaction.');
            }
        }

        return $this->run('profile', ['id' => $user_id]);
    }

    public function actionDeleteSectionValue($id)
    {
        $section_value_model = SectionValue::findOne($id);
        $user_id = $section_value_model->user->id;

        if (!$section_value_model) {
            \Yii::$app->session->setFlash('toast-warning', 'Section value with id '.$id.' does not exists');
        } else {
            if ($section_value_model->delete()) {
                \Yii::$app->session->setFlash('toast-success', 'Language with id '.$id.' has been deleted.');
            } else {
                \Yii::$app->session->setFlash('toast-error', 'An error has occurred trying to delete language.');
            }
        }

        return $this->run('profile', ['id' => $user_id]);
    }

    public function actionDeleteReview($id)
    {
        $review_model = Review::findOne($id);
        $user_id = $review_model->userProfile->user->id;

        if (!$review_model) {
            \Yii::$app->session->setFlash('toast-warning', 'Review value with id '.$id.' does not exists');
        } else {
            if ($review_model->delete()) {
                \Yii::$app->session->setFlash('toast-success', 'Review with id '.$id.' has been deleted.');
            } else {
                \Yii::$app->session->setFlash('toast-error', 'An error has occurred trying to delete review.');
            }
        }

        return $this->run('profile', ['id' => $user_id]);
    }

    public function actionChangeReviewStatus($id)
    {
        $review_model = Review::findOne($id);
        $user_id = $review_model->userProfile->user->id;

        if (!$review_model) {
            \Yii::$app->session->setFlash('toast-warning', 'Review value with id '.$id.' does not exists');
        } else {
            if ($review_model->status) {
                $review_model->status = 0;
            } else {
                $review_model->status = 1;
            }

            if ($review_model->save()) {
                \Yii::$app->session->setFlash('toast-success', 'Review status has been updated.');
            }
        }

        return $this->run('profile', ['id' => $user_id]);
    }

    public function actionChangeContactStatus()
    {
        $response = ['status' => false];

        if (\Yii::$app->request->isAjax) {
            $id = \Yii::$app->request->post('id');
            $agent_contact_model = AgentContact::findOne($id);

            if ($agent_contact_model) {
                $agent_contact_model->status = AgentContact::STATUS_READ;
                if ($agent_contact_model->save()) {
                    $response['status'] = true;
                }
            }
        }

        \Yii::$app->response->format = Response::FORMAT_JSON;
        return $response;
    }

    public function actionDeleteAgentContact()
    {
        $response = ['status' => false];

        if (\Yii::$app->request->isAjax) {
            $id = \Yii::$app->request->post('id');
            $agent_contact_model = AgentContact::findOne($id);

            if ($agent_contact_model && $agent_contact_model->delete()) {
                $response['status'] = true;
            }
        }

        \Yii::$app->response->format = Response::FORMAT_JSON;
        return $response;
    }

    /**
     * id - id transaction
     * @return array json
     */
    public function actionApproveProperty()
    {
        $response = ['status' => false];

        if (\Yii::$app->request->isAjax) {
            $id = \Yii::$app->request->post('id');
            $transaction_model = Transaction::findOne($id);

            if ($transaction_model) {
                $transaction_model->status = Transaction::STATUS_ACTIVE;
                if ($transaction_model->validate() && $transaction_model->save()) {
                    $response['status'] = true;
                } else {
                    $response['errors'] = $transaction_model->errors;
                }
            } else {
                $response['errors'][] = 'Transaction with id '.$id.' not found.';
            }
        }

        \Yii::$app->response->format = Response::FORMAT_JSON;
        return $response;
    }
}