<?php

return [
    [
        'id' => '1',
        'id_user_profile' => '2',
        'state' => 'State 1',
        'short_name' => 'SN',
        'city' => 'City 1',
        'region' => 'Region 1',
    ],

    [
        'id' => '2',
        'id_user_profile' => '2',
        'state' => 'State 2',
        'short_name' => 'SN',
        'city' => 'City 2',
        'region' => 'Region 2',
    ],

    [
        'id' => '3',
        'id_user_profile' => '2',
        'state' => 'State 3',
        'short_name' => 'SN',
        'city' => 'City 3',
        'region' => 'Region 3',
    ],

    [
        'id' => '4',
        'id_user_profile' => '2',
        'state' => 'State 4',
        'short_name' => 'SN',
        'city' => 'City 4',
        'region' => 'Region 4',
    ],

    [
        'id' => '5',
        'id_user_profile' => '2',
        'state' => 'State 5',
        'short_name' => 'SN',
        'city' => 'City 5',
        'region' => 'Region 5',
    ],
];
