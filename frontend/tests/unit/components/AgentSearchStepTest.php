<?php

/**
 * Created by PhpStorm.
 * User: Vlad
 * Date: 26.04.2017
 * Time: 17:46
 */

namespace frontend\tests\unit\components;

use Yii;
use frontend\components\AgentSearchStep;

class AgentSearchStepTest extends \Codeception\Test\Unit
{
    public $_agent_search_step;

    public function _before()
    {
        $this->_agent_search_step = \Yii::createObject(['class' => AgentSearchStep::className()]);
    }

    public function _after()
    {
        \Yii::$app->session->remove('search');
    }

    public function testSetSearch()
    {
        $this->_agent_search_step->setSearch(['step' => 1, 'location' => 1]);

        expect('Should be 1', $this->_agent_search_step->getSearchParam('step'))->equals(1);
        expect('Should be 1', $this->_agent_search_step->getSearchParam('location'))->equals(1);
    }

    public function testGetSearch()
    {
        \Yii::$app->session->set('search', ['step' => 1]);
        $search = $this->_agent_search_step->getSearch();
        expect('Shouldn\'t be empty.', $search)->notEmpty();
        expect('Should be array', is_array($search))->true();

        \Yii::$app->session->remove('search');
        $search = $this->_agent_search_step->getSearch();
        expect('Should be empty.', $search)->isEmpty();
        expect('Should be array', is_array($search))->true();
    }
    
    public function testGetSearchParam()
    {
        \Yii::$app->session->set('search', ['step' => 1]);
        expect('Shouldn\'t be empty.', $this->_agent_search_step->getSearchParam('step'))->notEmpty();
        expect('Should be 1.', $this->_agent_search_step->getSearchParam('step'))->equals(1);

        \Yii::$app->session->set('search', []);
        expect('Should be empty.', $this->_agent_search_step->getSearchParam('step'))->isEmpty();
    }

    public function testSetSearchParam()
    {
        $this->_agent_search_step->setSearchParam('step', 1);
        expect('Shouldn\'t be empty.', $this->_agent_search_step->getSearchParam('step'))->notEmpty();
        expect('Should be 1.', $this->_agent_search_step->getSearchParam('step'))->equals(1);

        \Yii::$app->session->set('search', []);
        expect('Should be empty.', $this->_agent_search_step->getSearchParam('step'))->isEmpty();
    }

    public function testGetStepData()
    {
        \Yii::$app->session->set('search', ['step' => 1]);
        $this->_agent_search_step = \Yii::createObject(['class' => AgentSearchStep::className()]);

        expect('Should be '. $this->_agent_search_step->steps[1]['view'], $this->_agent_search_step->getStepData('view'))->equals($this->_agent_search_step->steps[1]['view']);
        expect('Should be null', $this->_agent_search_step->getStepData('not_isset'))->isEmpty();
    }

    public function testSetNextStep()
    {
        \Yii::$app->session->set('search', ['step' => 1]);
        $this->_agent_search_step = \Yii::createObject(['class' => AgentSearchStep::className()]);
        $this->_agent_search_step->_agent_search->lead_type = 1;

        expect('Should be 2.', $this->_agent_search_step->setNextStep())->equals(2);
        expect('Should be 1.', $this->_agent_search_step->getSearchParam('lead_type'))->equals(1);
    }

    public function testSetPreviousStep()
    {
        \Yii::$app->session->set('search', ['step' => 2, 'location' => 'New York', 'lead_type' => 1, 'property_type' => 1]);
        $this->_agent_search_step = \Yii::createObject(['class' => AgentSearchStep::className()]);
        expect('Should be 1.', $this->_agent_search_step->setPreviousStep())->equals(1);
        expect('Should be empty.', $this->_agent_search_step->getSearchParam('lead_type'))->isEmpty();

        \Yii::$app->session->set('search', ['step' => 1, 'location' => 'New York', 'lead_type' => 1]);
        $this->_agent_search_step = \Yii::createObject(['class' => AgentSearchStep::className()]);
        expect('Should be 1.', $this->_agent_search_step->setPreviousStep())->equals(1);
    }

    public function testSetStep()
    {
        \Yii::$app->session->set('search', ['step' => 2, 'location' => 'New York', 'lead_type' => 1, 'property_type' => 1]);
        $this->_agent_search_step = \Yii::createObject(['class' => AgentSearchStep::className()]);

        expect('Should be 1.', $this->_agent_search_step->setStep(1))->equals(1);
        expect('Should be true.', is_array($this->_agent_search_step->getSearch()))->true();
        expect('Should\'t has lead type key.', $this->_agent_search_step->getSearch())->hasntKey('lead_type');
        expect('Should\'t has property_type key.', $this->_agent_search_step->getSearch())->hasntKey('property_type');
    }

    public function testIsLastStep()
    {
        $this->_agent_search_step = \Yii::createObject(['class' => AgentSearchStep::className()]);
        \Yii::$app->session->set('search', ['step' => count($this->_agent_search_step->steps), 'location' => 'New York']);
        $this->_agent_search_step->__construct();
        expect('Should be true.', $this->_agent_search_step->isLastStep())->true();

        \Yii::$app->session->set('search', ['step' => 1, 'location' => 'New York']);
        $this->_agent_search_step->__construct();
        expect('Should be true.', $this->_agent_search_step->isLastStep())->false();
    }

    public function testExistsSearchParam()
    {
        \Yii::$app->session->set('search', ['step' => 1, 'location' => 'New York']);
        $this->_agent_search_step = \Yii::createObject(['class' => AgentSearchStep::className()]);
        expect('Should be true.', $this->_agent_search_step->existsSearchParams())->true();

        \Yii::$app->session->set('search', ['step' => 2, 'location' => 'New York', 'lead_type' => 1]);
        $this->_agent_search_step = \Yii::createObject(['class' => AgentSearchStep::className()]);
        expect('Should be true.', $this->_agent_search_step->existsSearchParams())->true();

        \Yii::$app->session->set('search', ['step' => 2, 'location' => 'New York']);
        $this->_agent_search_step = \Yii::createObject(['class' => AgentSearchStep::className()]);
        expect('Should be false.', $this->_agent_search_step->existsSearchParams())->false();
    }

    public function testRemoveExtraParams()
    {
        \Yii::$app->session->set('search', ['step' => 1, 'location' => 'New York', 'lead_type' => 1, 'property_type' => 1]);
        $this->_agent_search_step = \Yii::createObject(['class' => AgentSearchStep::className()]);
        expect('Should be 1.', $this->_agent_search_step->removeExtraParams())->equals(1);
        expect('Should be empty.', $this->_agent_search_step->getSearchParam('property_type'))->isEmpty();
    }
}