<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 027 27.04.17
 * Time: 15:38
 */

namespace frontend\tests\unit\models;

use Yii;
use common\fixtures\UserRoleFixture as UserRoleFixture;
use common\fixtures\UserFixture as UserFixture;
use common\fixtures\UserProfileFixture as UserProfileFixture;
use common\fixtures\AreaFixture as AreaFixture;
use frontend\models\AgentSearch;

class AgentSearchTest extends \Codeception\Test\Unit
{
    /**
     * @var \frontend\tests\UnitTester
     */
    protected $tester;

    public function _before()
    {
        $this->tester->haveFixtures([
            'user_role' => [
                'class' => UserRoleFixture::className(),
                'dataFile' => codecept_data_dir() . 'user_role.php'
            ],
            'user' => [
                'class' => UserFixture::className(),
                'dataFile' => codecept_data_dir() . 'user.php'
            ],
            'user_profile' => [
                'class' => UserProfileFixture::className(),
                'dataFile' => codecept_data_dir() . 'user_profile.php'
            ],
            'area' => [
                'class' => AreaFixture::className(),
                'dataFile' => codecept_data_dir() . 'area.php'
            ]
        ]);
    }

    public function testScenarioSearchValidate()
    {
        $_agent_search_step = \Yii::createObject([
            'class' => AgentSearch::className(),
            'scenario' => AgentSearch::SCENARIO_SEARCH,
            'step' => 6,
            'location' => [
                'state' => 'State 2',
                'short_name' => 'SN',
                'city' => 'City 2',
                'region' => 'Region 2',
            ],
            'lead_type' => 1,
            'property_type' => 1,
            'price' => 1,
            'period' => 1,
            'other_action' => 1,
        ]);
        expect('Validate should be true', $_agent_search_step->validate())->true();
        expect('Should be empty', $_agent_search_step->errors)->isEmpty();

        $_agent_search_step = \Yii::createObject([
            'class' => AgentSearch::className(),
            'scenario' => AgentSearch::SCENARIO_SEARCH,
        ]);
        expect('Validate should be false', $_agent_search_step->validate())->false();
        expect('Errors should has location key', $_agent_search_step->errors)->hasKey('location');
        expect('Errors should has lead_type key', $_agent_search_step->errors)->hasKey('lead_type');
        expect('Errors should has property_type key', $_agent_search_step->errors)->hasKey('property_type');
        expect('Errors should has price key', $_agent_search_step->errors)->hasKey('price');
        expect('Errors should has period key', $_agent_search_step->errors)->hasKey('period');
        expect('Errors should has other_action key', $_agent_search_step->errors)->hasKey('other_action');
    }

    public function testScenarioStepLocationValidate()
    {
        $_agent_search_step = \Yii::createObject([
            'class' => AgentSearch::className(),
            'scenario' => AgentSearch::SCENARIO_STEP_LOCATION,
            'area' => '{"state" : "State 1", "short_name" : "UA", "city" : "City 1", "region" : "Region 1"}',
        ]);
        expect('Validate should be true', $_agent_search_step->validate())->true();
        expect('Errors should be empty', $_agent_search_step->errors)->isEmpty();
        expect('Should be array', is_array($_agent_search_step->area))->true();
        expect('Area should has state key', $_agent_search_step->area)->hasKey('state');
        expect('Area should has short_name key', $_agent_search_step->area)->hasKey('short_name');
        expect('Area should has city key', $_agent_search_step->area)->hasKey('city');
        expect('Area should has region key', $_agent_search_step->area)->hasKey('region');

        $_agent_search_step = \Yii::createObject([
            'class' => AgentSearch::className(),
            'scenario' => AgentSearch::SCENARIO_STEP_LOCATION,
        ]);
        expect('Validate should be false', $_agent_search_step->validate())->false();
        expect('Errors should has area key', $_agent_search_step->errors)->hasKey('area');
    }

    public function testScenarioStepCustomerInfo()
    {
        $_agent_search_step = \Yii::createObject([
            'class' => AgentSearch::className(),
            'scenario' => AgentSearch::SCENARIO_STEP_CUSTOMER_INFO,
            'email' => 'email@email.com',
            'phone' => '(333)333-3333',
            'first_name' => 'First name',
            'last_name' => 'Last name',
        ]);
        expect('Validate should be true', $_agent_search_step->validate())->true();
        expect('Errors should be empty', $_agent_search_step->errors)->isEmpty();

        $_agent_search_step = \Yii::createObject([
            'class' => AgentSearch::className(),
            'scenario' => AgentSearch::SCENARIO_STEP_CUSTOMER_INFO,
            'email' => 'email',
            'phone' => '(333)333-333344',
            'first_name' => 'f',
            'last_name' => 'l',
        ]);
        expect('Validate should be false', $_agent_search_step->validate())->false();
        expect('Errors should be not empty', $_agent_search_step->errors)->notEmpty();
        expect('Errors should has email key', $_agent_search_step->errors)->hasKey('email');
        expect('Errors should has phone key', $_agent_search_step->errors)->hasKey('phone');
        expect('Errors should has first_name key', $_agent_search_step->errors)->hasKey('first_name');
        expect('Errors should has last_name key', $_agent_search_step->errors)->hasKey('last_name');
    }

    public function testSearch()
    {
        \Yii::$app->session->set('search', [
            'location' => [
                'state' => 'State 1',
                'short_name' => 'SN',
                'city' => 'City 1',
                'region' => 'Region 1',
            ],
            'lead_type'     => 1,
            'property_type' => 1,
            'price'         => 1,
            'period'        => 1,
            'other_action'  => 1,
        ]);

        $_agent_search = \Yii::createObject([
            'class' => AgentSearch::className(),
            'scenario' => AgentSearch::SCENARIO_SEARCH,
        ]);

        $_agent_search->search();
    }
}