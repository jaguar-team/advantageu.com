<?php
namespace frontend\widgets;

use Yii;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\base\Widget;
use yii\widgets\LinkPager;
use yii\data\Pagination;

class BlogPostsWidget extends Widget
{
    public $pageSize = 3;

    public $posts = [];

    public $pager = [];

    public function run()
    {
        parent::run();

        $content = $this->renderList($this->posts);
        echo $content;
    }

    public function getPosts()
    {
        

        //var_dump($blogPosts); exit();

        return $blogPosts;
    }

    public function renderPager($blogPosts)
    {
        $pagination = new Pagination(['totalCount' => count($blogPosts), 'pageSize' => $this->pageSize]);
        if ($pagination === false || count($blogPosts) <= 0) {
            return '';
        }
        /* @var $class LinkPager */
        $pager = $this->pager;
        $class = ArrayHelper::remove($pager, 'class', LinkPager::className());
        $pager['pagination'] = $pagination;
        $pager['view'] = $this->getView();

        return $class::widget($pager);
    }

    public function renderList($blogPosts)
    {
        if(empty($blogPosts)){
            return '<h3 class="text-center">There are no posts yet</h3>';
        };

        $page = (\Yii::$app->getRequest()->getQueryParam('page') ? \Yii::$app->getRequest()->getQueryParam('page') : 1);
        $offset = ($page - 1) * $this->pageSize;

        $posts = [];
        for($i = $offset; $i < $this->pageSize + $offset; $i++){
            if(isset($blogPosts[$i])){
                $posts[] = $this->renderBlogPost($blogPosts[$i]);
            }
        }


        return "<div class=\"blog_full\">\n".implode("\n", $posts)."\n</div>\n
                <div class=\"clearfix\"></div>\n
	            <div id=\"au-pagination\">\n".
	            	$this->renderPager($blogPosts)."\n
	            </div>\n
	            <div class=\"clearfix\"></div>";
    }

    public function renderBlogPost($post) {
        $block = "<div class=\"block\">
                <a href=\"".$post['link']."\">
                    <div class=\"img\">
                        ".( !empty($post['post_thumbnail']) ? Html::img($post['post_thumbnail']['link']) : Html::img(Url::to(['@uri_img/no-thumbnail.jpg'], true)) )."
                    </div>
                    <h4 class=\"blog-heading\">".Html::encode($post['post_title'])."</h4>
                </a>
            </div>";
        return $block;
    }
}