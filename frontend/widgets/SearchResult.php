<?php
namespace frontend\widgets;

use Yii;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\base\Widget;
use yii\widgets\LinkPager;
use yii\widgets\LinkSorter;
use common\models\CustomerBid;

class SearchResult extends Widget
{
	public $dataProvider;

	public $itemClass = null;

	public $pager = [];

    public $sorter = [];

    public $resultMatches;

	public function init()
    {
        parent::init();
    }

    public function run()
    {
    	parent::run();
        $this->dataProvider->pagination->pageSize = 6;
    	$models = $this->dataProvider->getModels();
    	$content = $this->renderList($models);
    	echo $content;
    }

    public function renderList($models)
    {
    	$listItems = [];
    	foreach ($models as $key => $model) {
    		$listItems[] = $this->renderListItem($model);
    	}
    	if (empty($listItems)) {
    		$listItems[] = $this->renderEmptyItem();
    	}

    	$pager = $this->renderPager();

        $sorter = $this->renderSorter();

    	return "<div class='filters'>".$sorter."</div>\n<ul>\n".implode("\n", $listItems)."\n</ul>\n".$pager;
    }

    public function isFavourite($model, $id = false){
        foreach (\Yii::$app->user->identity->favorites as $favourite) {
            if ($favourite->id_favorite == $model->id)
                return ($id ? true : $favourite->id);
        }
        return false;
    }

    public function renderListItem($model)
    {
    	$img = Html::decode(
    		'<div class="profile-photo-wrap">'.
    		Html::a(
	            Html::img($model->profile->getPhotoUrl(), []),
	        	Url::toRoute(['agent/profile', 'id' => $model->id], true), ['target' => '_blank', 'data-pjax' => 'false']
	        ).
	        '</div>'
    	);
        $name = Html::a(
    		Html::encode( $model->profile->getFullName(true) ),
    		Url::toRoute(['agent/profile', 'id' => $model->id], true), ['target' => '_blank', 'data-pjax' => 'false']
    	);
    	$company = ($model->profile->company) ? Html::decode(
    		'<div class="stat">
				<i class="hi hi-user"></i>
				<span title="'.Html::encode( $model->profile->company).'">'.Html::encode( $model->profile->company).'</span>
			</div>'
    	) : '';
        $profile_link = '<div class="stat"><span>'.
            Html::a('View Profile',
                Url::toRoute(['agent/profile', 'id' => $model->id], true), ['target' => '_blank', 'data-pjax' => 'false'])
            .'</span></div>';

        if ( $this->isFavourite($model) ){
            $add_link = Html::a(
                'Added to Matches',
                Url::toRoute('javascript:void(0)', true),
                [
                    'class' => 'complete add-to-matches',
                    'onclick'   => sprintf('removeFromMatches(event, %s)', $this->isFavourite($model, true))
                ]
            );
        } else {
            $add_link = Html::a(
                'Add to Matches',
                Url::toRoute('javascript:void(0)', true),
                [   
                    'class'     => 'add-to-matches',
                    'onclick'   => sprintf('addToMatches(event, %s)', $model->id)
                ]
            );
        }
    	
    	$badges = $this->renderBadges($model);

    	return Html::decode('
	    	<li class="'.($this->itemClass ? $this->itemClass : 'agent-card').'">
	    		<div class="slug-wrap">'
	    			.$img.
	    			'<div class="info">
						<h3>'.$name.'</h3>
						<div class="stats">'
							.$company
                            .$add_link
                            .$profile_link.
						'</div>							
					</div>
	    		</div>'	    		
				.$badges.'
	    	</li>');    	
    }

    public function renderEmptyItem(){
    	return '<li class="agent-card">
                    <p class="text-center"><strong>Thank you, We are pre-screening agents for you at the moment.</strong></p>
                    <p class="text-center">An AdvantageU Advocate will contact you shortly.</p>
                </li>';
    }

    public function ifSortedBy($attribute){
        if( isset(\Yii::$app->request->queryParams['sort']) && ( (\Yii::$app->request->queryParams['sort'] == $attribute) || (\Yii::$app->request->queryParams['sort'] == '-'.$attribute) ) ) {
            return true;
        }
        return false;
    }

    public function renderBadges($model)
    {
        $customer_bid = \Yii::createObject(['class' => CustomerBid::className()]);
    	$badges = '
			<div class="comparison-stat '.( $this->ifSortedBy('transaction_quantity') ? 'filtered' : '' ).'">
				<div class="val">'.( $model->transaction_quantity ? $model->transaction_quantity : 0 ).'</div>
				<label>'.sprintf('%s listings %s', Yii::$app->agentSearchStep->getSearchParam('location')['city'], ( Yii::$app->agentSearchStep->getSearchParam('lead_type') == 2 ) ? 'sold' : 'closed' ).'</label>
			</div>
             <div class="comparison-stat '.( $this->ifSortedBy('average_price') ? 'filtered' : '' ).'">
                <div class="val">$ '.( $model->average_price ? $this->renderPrice($model->average_price) : 0 ).'</div>
                <label>'.sprintf('Average %s price', ( Yii::$app->agentSearchStep->getSearchParam('lead_type') == 2 ) ? 'sold' : 'purchase' ).'</label>
            </div>
            <div class="comparison-stat '.( $this->ifSortedBy('average_days_on_market') ? 'filtered' : '' ).'">
                <div class="val">'.( $model->average_days_on_market ? $model->average_days_on_market : 0 ).'</div>
                <label>Average days on market</label>
            </div>
            <div class="comparison-stat '.( $this->ifSortedBy('transaction_quantity_by_type') ? 'filtered' : '' ).'">
                '.Html::a('Get Matched Now', Url::toRoute(['agent/profile', 'id' => $model->id, '#' => 'contact_agent'], true), ['class' => 'btn', 'data-pjax' => 'false']).'
            </div>';
    	return '<div class="badges">'.$badges.'</div>';
    }

    public function renderPager()
    {
        $pagination = $this->dataProvider->getPagination();
        if ($pagination === false || $this->dataProvider->getCount() <= 0) {
            return '';
        }
        /* @var $class LinkPager */
        $pager = $this->pager;
        $class = ArrayHelper::remove($pager, 'class', LinkPager::className());
        $pager['pagination'] = $pagination;
        $pager['view'] = $this->getView();

        return $class::widget($pager);
    }

    public function renderSorter()
    {
        $sort = $this->dataProvider->getSort();
        if ($sort === false || empty($sort->attributes) || $this->dataProvider->getCount() <= 0) {
            return '';
        }
        /* @var $class LinkSorter */
        $sorter = $this->sorter;
        $class = ArrayHelper::remove($sorter, 'class', LinkSorter::className());
        $sorter['sort'] = $sort;
        $sorter['linkOptions'] = ['class' => 'outline-btn', 'data-pjax' => 'true'];
        //$sorter['attributes'] = ['transaction_quantity', ];
        $sorter['view'] = $this->getView();

        return $class::widget($sorter);
    }

    public function renderPrice($price){
        if( $price ){
            $num = 0;
            $affix = '';
            if( $price / 1000 > 1 ){
                if( $price / 1000000 > 1 ){                    
                    $num = round($price, -5) / 1000000;
                    $affix = 'm';
                }
                else{
                    $num = round($price, -2) / 1000;
                    $affix = 'k';
                }               
            } else {
                $num = $price;
            }
            return number_format( $num, 1, '.', '' ).$affix;
        }
        return 0;
    }
}