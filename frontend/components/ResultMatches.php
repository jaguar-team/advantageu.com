<?php

namespace frontend\components;

use Yii;
use yii\base\Object;
use common\models\User;

class ResultMatches extends Object
{
	CONST SESSION_NAME = 'search-result-matches';

    /**
     * ResultMatches constructor.
     * @param array $config
     */
	public function __construct(array $config = [])
    {
        parent::__construct($config);
    }

    /**
     * @param $id
     */
    public function add($id)
    {
    	$ids = $this->get();
    	array_push($ids, $id);
    	$ids = array_unique($ids);

    	return $this->set($ids);

    }

    /**
     * @param $id_delete
     */
    public function delete($id_delete)
    {
    	$ids = $this->get();

    	foreach ($ids as $index => $id) {
    		if ($id == $id_delete) {
    			unset($ids[$index]);
    		}
    	}

    	return $this->set($ids);
    }

    /**
     * @param $value
     */
    public function set($value)
    {
    	return \Yii::$app->session->set(ResultMatches::SESSION_NAME, $value);
    }

    /**
     * @param bool $object
     * @param bool $count
     * @return array|int|mixed|string|\yii\db\ActiveRecord[]
     */
    public function get($object = false, $count = false)
    {
    	$ids = \Yii::$app->session->get(ResultMatches::SESSION_NAME, []);

    	if ($object || $count) {
    		$query = User::find()->where(['id' => $ids]);

    		if ($object) {
    			return $query->indexBy('id')->all();
    		}

    		if ($count) {
    			return $query->count();
    		}

    	} else {
    		return $ids;
    	}
    }
}