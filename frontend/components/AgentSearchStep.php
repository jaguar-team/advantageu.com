<?php
/**
 * Created by PhpStorm.
 * User: Vlad
 * Date: 25.04.2017
 * Time: 18:54
 */

namespace frontend\components;

use Yii;
use yii\base\Object;
use frontend\models\AgentSearch;

class AgentSearchStep extends Object
{
    /** @var null|string  */
    public $step;

    /** @var object  */
    public $_agent_search;

    /** @var array  */
    public $steps = [
        1   => ['view' => '_action', 'scenario' => AgentSearch::SCENARIO_STEP_ACTION, 'label' => 'Buying or Selling', 'step' => 1],
        2   => ['view' => '_property', 'scenario' => AgentSearch::SCENARIO_STEP_PROPERTY, 'label' => 'Property Details', 'step' => 2],
        3   => ['view' => '_price', 'scenario' => AgentSearch::SCENARIO_STEP_PRICE, 'label' => 'Estimated Value', 'step' => 3],
        4   => ['view' => '_period', 'scenario' => AgentSearch::SCENARIO_STEP_PERIOD, 'label' => 'Timeframe', 'step' => 4],
        5   => ['view' => '_other_action', 'scenario' => AgentSearch::SCENARIO_STEP_OTHER_ACTION, 'label' => 'Other Action', 'step' => 5],
        6   => ['view' => '_customer_info', 'scenario' => AgentSearch::SCENARIO_STEP_CUSTOMER_INFO, 'label' => 'Contact Info', 'step' => 6],
    ];

    /**
     * AgentSearchStep constructor.
     * @param array $config
     */
    public function __construct(array $config = [])
    {
        $this->step = $this->getSearchParam('step');

        if ($this->step) {
            $this->_agent_search = \Yii::createObject([
                'class' => AgentSearch::className(),
                'scenario'  => $this->steps[$this->step]['scenario']
            ]);
        }

        parent::__construct($config);
    }

    /**
     * Set next step
     * Set current step data
     * @return null|string
     */
    public function setNextStep()
    {
        foreach ($this->_agent_search->getAttributes() as $name => $attribute) {
            if ($this->_agent_search->isAttributeActive($name)) {

                $this->setSearchParam($name, $attribute);
            }
        }

        if ($this->step < $this->getLastStep()) {
            $this->setSearchParam('step', ++$this->step);
        }
        return $this->step = $this->getSearchParam('step');
    }

    /**
     * Set previous step
     * Delete current step data
     * @return null|string
     */
    public function setPreviousStep()
    {

        if (($this->step - 1) > 0) {
            $this->setSearchParam('step', --$this->step);
            $this->step = $this->getSearchParam('step');
            $this->removeExtraParams();
        }

        return $this->step;
    }

    /**
     * @param $number
     * @return null|string
     * @throws \yii\base\InvalidConfigException
     */
    public function setStep($number)
    {
        if ($this->step > 1 && $number > 0) {
            $this->setSearchParam('step', $number);
            $this->step = $this->getSearchParam('step');
            $this->removeExtraParams();
        }

        return $this->step;
    }

    /**
     * Check last step
     * @return bool
     */
    public function isLastStep()
    {
        if (\Yii::$app->user->isGuest) {
            return $this->step == $this->getLastStep() ? true : false;
        } else {
            return $this->step >= $this->getLastStep() ? true : false;
        }
    }

    /**
     * Check correct step
     * @return bool
     */
    public function isCorrectStep()
    {
        if (!$this->step) {
            return false;
        }

        return in_array($this->step, array_keys($this->steps));
    }

    /**
     * Check required params
     * @return bool
     * @throws \yii\base\InvalidConfigException
     */
    public function existsSearchParams($current_step = false)
    {
        if (!$this->getSearchParam('location') && !$this->getSearchParam('step')) {
            return false;
        }

        /** init $step */
        $step = $current_step ? $this->step + 1 : $this->step;
        if (!\Yii::$app->user->isGuest && $step >= count($this->steps)) {
            $step = $this->getLastStep();
        }

        for ($i = 1; $i < $step; $i++) {
            $agent_search = \Yii::createObject([
                'class' => AgentSearch::className(),
                'scenario'  => $this->steps[$i]['scenario']
            ]);

            foreach ($agent_search->getAttributes() as $name => $attribute) {
                if ($agent_search->isAttributeActive($name)) {
                    if ($this->getSearchParam($name) == NULL) {
                        return false;
                    }
                }
            }

        }
        return true;
    }

    /**
     * @param $name
     * @return bool|null
     */
    public function getStepData($name)
    {
        if (!$this->step) {
            return false;
        }

        return isset($this->steps[$this->step][$name]) ? $this->steps[$this->step][$name] : NULL;
    }

    public function getSteps()
    {
        $steps = $this->steps;

        if (!\Yii::$app->user->isGuest) {
            unset($steps[count($steps)]);
        }

        return $steps;
    }

    /**
     * @return int
     */
    public function getLastStep()
    {
        return \Yii::$app->user->isGuest ? count($this->steps) : (count($this->steps) - 1);
    }

    /**
     * Set search data
     * @param $params
     */
    public function setSearch($params)
    {
        return \Yii::$app->session->set('search', $params);
    }

    /**
     * Get search data
     * Default empty array []
     * @return mixed
     */
    public function getSearch()
    {
        return \Yii::$app->session->get('search', []);
    }

    /**
     * Get search property value
     * @param $name
     * @return string|null
     */
    public function getSearchParam($name)
    {
        $search = $this->getSearch();

        return isset($search[$name]) ? $search[$name] : NULL;
    }

    /**
     * Set search param
     * @param $name
     * @param $value
     */
    public function setSearchParam($name, $value)
    {
        $search = $this->getSearch();
        $search[$name] = $value;
        return \Yii::$app->session->set('search', $search);
    }

    /**
     * @param $name
     */
    public function removeSearchParam($name)
    {
        $search = $this->getSearch();
        unset($search[$name]);

        return $this->setSearch($search);
    }

    /**
     * Remove extra params
     * @return null|string
     */
    public function removeExtraParams()
    {
        $current_step = $this->step;
        $count_step = count($this->steps);

        for ($i = $count_step; $i >= $current_step; $i--) {
            $this->setSearchParam('step', $i);
            $this->step = $this->getSearchParam('step');

            $_agent_search = \Yii::createObject([
                'class'     => AgentSearch::className(),
                'scenario'  => $this->getStepData('scenario'),
            ]);

            foreach ($_agent_search->getAttributes() as $name => $attribute) {
                if ($_agent_search->isAttributeActive($name)) {
                    $this->removeSearchParam($name);
                }
            }
        }
        return $this->step;
    }

    /**
     * @return mixed
     */
    public function clearSearch()
    {
        return \Yii::$app->session->remove('search');
    }
}