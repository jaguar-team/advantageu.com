$(document)
	.ready(function(){

		$('.matches ul').perfectScrollbar();

		$(document).on('click', '.modify', function(event){
			event.preventDefault();
			$(event.target).closest('.search_results').toggleClass('edit');
		});

	})
	.on({
		'pjax:send': function(event){	

			if($.isFunction($.fn.select2) && event.target.id == 'updateResult'){
				$('.select2').select2("destroy");	
			}		
		},
		'pjax:complete': function(event){

			if(event.target.id == 'answers'){
				setIndicatorStep(_current_step);
				
				if($.isFunction($.perfectScrollbar))
					$('.matches ul').perfectScrollbar();

				//History.pushState({currentStep: _current_step}, null, window.location.href);
			}

			if($.isFunction($.fn.select2) && event.target.id == 'updateResult')
				$('.select2').select2();
		},
		'pjax:popstate': function(event){
			event.preventDefault();
			event.stopPropagation();
			event.stopImmediatePropagation();
			if(event.direction == 'back'){
				let step = ((_current_step > 1) ? (_current_step - 1) : false);
  				setStep(event, step);
			}
		}
	});

/*History.Adapter.bind(window, "popstate", function (event) {
  	if(typeof(event.state) != 'undefined'){
  		let step = ((_current_step > 1) ? (_current_step - 1) : false);
  		setStep(event, step);
  	}
});*/

function searchAnswer(event, input_id, value){
	event.preventDefault();
	
	let el = ((event.target.tagName == 'A') ? $(event.target) : $(event.target).closest('a')),
		input = $('#' + input_id);
	let form = input.closest('form');

	el.addClass('selected');
	input.val(value);
	$('#loader').show();	
	form.trigger('submit');
}

function setIndicatorStep(step){
	let indicator = $('.progress-indicator span');
	let container = indicator.closest('.figures-wrap'),
		step_num = +indicator.data('step_num');

	indicator.css({'width': 'calc(100% / ' + step_num +' * ' + +(step - 1) + ')'});

	container.find('.figure').removeClass('current completed');
	container.find('.figure[data-step="'+ +step +'"]').addClass('current');
	container.find('.figure[data-step="'+ +step +'"] ~ .figure').addClass('completed');
}

function setStep(event, step){
	if(!step){
		//History.pushState({}, null, _base_url);
		window.location.href = _base_url;
		return false;
	}
	if(step > _current_step) 
		return false;
	let data = {
		step: step
	};
	data[yii.getCsrfParam()] = yii.getCsrfToken();
	$.ajax({
		method: "post",
		url: _base_url + '/agent-search/set-step',
		data: data,
		beforeSend: function(){
			$('#answers').append(loader).addClass('loading');
		},
		success: function(resp){
			$.pjax.reload({container: '#answers'});
		},
		error: function(xhr, error, message){
			console.log(message);
		}
	});
}

function addToMatches(event, user_id){
	event.preventDefault();
	if(ajaxActive)
		return false;
	let data = {
		id: user_id
	};
	let block = $(event.target).closest('li');
	data[yii.getCsrfParam()] = yii.getCsrfToken();
	$.ajax({
		url: _base_url + 'ajax/add-favorite',
		method: "POST",
		data: data,
		beforeSend: function(){
			ajaxActive = true;
			block.addClass('loading').append(loader);
		},
		success: function(resp){
			ajaxActive = false;
			if(resp.status){
				$(event.target).html("Added to matches").addClass('complete').attr('onclick', 'removeFromMatches(event, '+resp.data.id+')');
				$('.match-count').each(function(){
					$(this).html(+$(this).text() + 1);
				});
			}
			block.removeClass('loading').find('.loader').remove();
		},
		error: function(xhr, error, message){
			ajaxActive = false;
			console.log(xhr, error, message);
			addToMatches(event, user_id);
		}
	});
}