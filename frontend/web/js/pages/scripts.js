/* some magical jquery goodness */

$ = jQuery;

$(document).ready(function(){

//set first agent as active on choose an agent page
$('#field_y53bof').val($('.agent-item.active .hidden-name').attr('data-name'));

$("#other-short-sale, #other-commercial, #other-multi-family, #other-rental, #other-apartments, #other-land").prop( "disabled", true );

$("#frm_field_88_container").on("mouseup touchend", rangeChange);
$("#frm_field_84_container").on("mouseup touchend", rangeChange);
$("#frm_field_90_container").on("mouseup touchend", rangeChange);
$("#frm_field_92_container").on("mouseup touchend", rangeChange);

function rangeChange() { 
	console.log(parseInt($(this).parent().attr('id').slice(-1)) + 1);
		
	var $tabs = $(this).parent().parent();
	var nextTabNum = parseInt($(this).parent().attr('id').slice(-1)) + 1;
	var $tabPanel = $('#tablist1-panel' + nextTabNum);
	
	// remove hidden mobile class from any other panel as we'll want that panel to be open at mobile size
	$tabs.find('.responsive-tabs__panel--closed-accordion-only').removeClass('responsive-tabs__panel--closed-accordion-only');
	
	// close current panel and remove active state from its (hidden on desktop) heading
	$tabs.find('.responsive-tabs__panel--active').toggle().removeClass('responsive-tabs__panel--active').attr('aria-hidden','true').prev().removeClass('responsive-tabs__heading--active');
							
	//make this tab panel active
	$tabPanel.toggle().addClass('responsive-tabs__panel--active').attr('aria-hidden','false');

	//make the hidden heading active
	//$tabHeading.addClass('responsive-tabs__heading--active');

	//remove active state from currently active tab list item
	$tabs.find('.responsive-tabs__list__item--active').removeClass('responsive-tabs__list__item--active');

	//make this tab active
	$('#tablist1-tab' + nextTabNum).addClass('responsive-tabs__list__item--active');
	
}
	
			if($(this).scrollTop() >= menuTop + 0) { 
				$('#main-navigation-bar').addClass('sticky');
				$('#header').addClass('headerPush');
				//gridResize();
			}else{
				$('#main-navigation-bar').removeClass('sticky');
				$('#header').removeClass('headerPush');
				//gridResize();
			}
	
	$('.page-hero').on('click', '.jcarousel li', function(){
    $('html, body').animate({
        scrollTop: $("#quiz-banner").offset().top
    }, 1000);
	});
	
	try {
	    var menuTop = $('#main-navigation-bar').offset().top;
	}
	catch(e) {

	}
	
	$(window).scroll(function(){
	
			if($(this).scrollTop() >= menuTop + 0) { 
				$('#main-navigation-bar').addClass('sticky');
				$('#header').addClass('headerPush');
				//gridResize();
			}else{
				$('#main-navigation-bar').removeClass('sticky');
				$('#header').removeClass('headerPush');
				//gridResize();
			}
	});

/*$(function() {
	
	$('.frm_submit input').removeAttr('disabled');
	
	var image = new Image();
	image.src = "https://advantageu.com/wp-content/themes/wscleanslate/img/check-dark.gif";
	//var imgStyle = "url(" + image.src + ")";

    $('.jcarousel-agent').on('jcarousel:create jcarousel:reload', function() {
        var element = $(this),
            width = element.innerWidth();

        if (width >= 1150) {
            width = width / 3;
        } else if (width > 768) {
            width = width / 2;
        }

        element.jcarousel('items').css('width', width + 'px');
    }).jcarousel();
	
    $('.jcarousel-prev').jcarouselControl({
        target: '-=1'
    });

    $('.jcarousel-next').jcarouselControl({
        target: '+=1'
    });

    $('.jcarousel').jcarousel({
        vertical: true,
		wrap: 'circular',
    animation: {
        duration: 700,
        easing:   'swing',
        }
    })
            .on('jcarousel:targetin', 'li', function() {
                $(this).addClass('target');
				$('.page-hero .jcarousel li img').attr('src', image.src);
            })
            .on('jcarousel:targetout', 'li', function() {
                $(this).removeClass('target');
            })
        .jcarouselAutoscroll({
            interval: 1500,
            target: '+=1',
            autostart: true
        })
	;
});*/
	
	$('.post-type-archive-featured-agent .agent-item').on('click', 'div', function(){
		$('.post-type-archive-featured-agent .agent-item').removeClass('active');
		$('.agent-single').removeClass('active');
		
		$(this).parent().addClass('active');
		var selectAgent = $(this).parent().attr('data-agent');
		$('#agent-single-' + selectAgent).addClass('active');
		$('#field_y53bof').val($('.agent-item.active .hidden-name').attr('data-name'));
	});
	
	$('.cta-callout').on('click', '#matched-button', function(){
		var popup = '';
		popup = $(this).parent().parent().parent().find('.agent-popup');
		if(popup != '') {
			popup.fadeIn('400', function() {
                $(this).css("display", "block");
                $(this).css("opacity", 100);
                });
		}
		
	});
	
	$('.cta-callout').on('click', '#popup-close', function(){
		$('.agent-popup').hide();
		$('.agent-popup').css("opacity", 0);
	});
	
	$('#quiz-box #frm_form_15_container').on('click', '.select-item', function(){
		$('.select-item').removeClass('selected');
		$(this).addClass('selected');
		$('input[name="item_meta[141]"]').val(this.id);
	});
	
	    $('input[type="range"]').rangeslider({ polyfill: false });
		$('input[type="range"]').after('<div class="output">' + $(this).val() + '</div>');
		
		$('#frm_field_92_container .rangeslider__handle').prepend('<div id="value-tracker">$500,000</div>');
		
$("#frm_field_92_container #rangevalue").mousemove(function () {
    $("#value-tracker").text($("#rangevalue").val());
});

//$('#quiz-form-title').text($('.quiz-display .frm_primary_label').text());

	$('#main-navigation-bar #nav').meanmenu({meanScreenWidth:767, meanMenuContainer: '#main-navigation-bar'});

	// center menu
	if($( window ).width() > 768){
	
	}

	// homepage images
	$('.home #circle-images').cycle({
		
	});
	$('#value-tracker').text('$' + ReplaceNumberWithCommas($('input[name="item_meta[92]"]').val()));
	if($('input[name="item_meta[92]"]').val() == '1000000') { $('#value-tracker').text('> $1 M'); }
	if($('input[name="item_meta[98]"]').val() != '.5') {
		$('.frm_form_fields.frm_page_num_1').css('margin-bottom', '4em');
		$('.frm_page_num_1 .frm_submit').show();
	}
});

$(document).on('input', 'input[name="item_meta[98]"]', function() {
	$('.frm_form_fields').css('margin-bottom', '4em');
	$('.frm_page_num_1 .frm_submit').show();
});

$(document).on('input', 'input[name="item_meta[92]"]', function() {
	$('#value-tracker').text('$' + ReplaceNumberWithCommas($(this).val()));
	if($(this).val() == '1000000') { $('#value-tracker').text('> $1 M'); }
});

$(window).load(function(){
	gridResize();
});

$(window).resize(function(){
	if($( window ).width() > 768) { gridResize(); }
	else {
		$('.twocol').height('auto');
		$('.tencol').height('auto');
	}
});

function gridResize() {
	$('.resource-display').each(function(i){
		var maxHeight = 0;
			$(this).find('.twocol').each(function(){
				$(this).height("auto");
					if($(this).height() > maxHeight) maxHeight = $(this).height();
			});
			$(this).find('.tencol').each(function(){
				$(this).height("auto");
					if($(this).height() > maxHeight) maxHeight = $(this).height();
			});
				$(this).find('.twocol').each(function(){ $(this).height(maxHeight); });
				$(this).find('.tencol').each(function(){ $(this).height(maxHeight); });
	});
	
	$('#main-navigation-bar').each(function(i){
		var maxHeight = 0;
			$(this).find('#logo-bar').each(function(){
				$(this).height("auto");
					if($(this).height() > maxHeight) maxHeight = $(this).height();
			});
			$(this).find('#nav').each(function(){
				$(this).height("auto");
					if($(this).height() > maxHeight) maxHeight = $(this).height();
			});
				$(this).find('#logo-bar').each(function(){ $(this).height(maxHeight); });
				$(this).find('#nav').each(function(){ $(this).height(maxHeight); });
	});
	
	$('.blog .resource-display').each(function(i){
		var maxHeight = 0;
			$(this).find('.fourcol').each(function(){
				$(this).height("auto");
					if($(this).height() > maxHeight) maxHeight = $(this).height();
			});
				$(this).find('.fourcol').each(function(){ $(this).height(maxHeight); });
	});
	
}

function ReplaceNumberWithCommas(yourNumber) {
	try { var n= yourNumber.toString().split(".");
    //Seperates the components of the number
	
    //Comma-fies the first part
    n[0] = n[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    //Combines the two sections
    return n.join(".");
	}
	catch(e) {}
}