let _URL = window.URL || window.webkitURL,
	ajaxActive = false,
	loader = '<div class="loader">' +
		    '<div class="loader-container">' +
		        '<i class="fa fa-spinner fa-pulse fa-fw"></i>' +
		        '<span class="sr-only">Loading...</span>' +
		    '</div>' +
		'</div>';
	cbr_replace();
$(document).ready(function(){	
	(typeof($.pjax) != 'undefined') ? $.pjax.defaults.scrollTo = false : false;

	hashCheck();

	autoComplete();

	$('[data-toggle="tooltip"]').tooltip();

	$('.select2').select2();

	$('.datepicker').datepicker({
		autoclose: true,
		endDate: new Date()
	}).on('changeDate', function(event) {
		$(event.target).datepicker('hide')
	});

	$(window).on('hashchange', function() {
		hashCheck();
	});

	$(document).on('submit', '#login_form', function(event){
		registration(event, 'sign-in');
	});

	$(document).on('submit', '#signup_form', function(event){
		registration(event, 'registration');
	});

	$(document).on('click', '.notifications', function(e){
		$(this).parent().toggleClass('open');
	});

	$(document).on('click', function (e) {
	    if (!$('li.dropdown').is(e.target) 
	        && $('li.dropdown').has(e.target).length === 0 
	        && $('.open').has(e.target).length === 0
	    ) {
	        $('li.dropdown').removeClass('open');
	    }
	});

	$(document).on('show.bs.modal', '#contact_agent_result', function(event){
		let modal = $(event.target);
		let button = $(event.relatedTarget);
		modal.find('.modal-head img').attr('src', button.data('img'));
		modal.find('.modal-head h3').html('Contact ' + button.data('name'));
		modal.find('textarea').attr('placeholder', 'Hi, '+ button.data('name') +', I saw your profile on HomeLight and I\'d like to schedule a time to talk.')
	});

	$(document).on('show.bs.modal', '#login_open', function(event){
		window.location.hash = '';
	});

	$('#contact_agent_result').appendTo("body");

	$(document).on({
		'pjax:error': function(event, textStatus, error, options){
			event.preventDefault();
			console.log(error);
			//alert("Check ur console for errors");
		},
		'pjax:send': function(event){	
			if(!$(event.target).hasClass('loading'))
				$(event.target).append(loader).addClass('loading');
		},
		'pjax:complete': function(event){
			
			autoComplete();	

			let container = $(event.target);
			container.removeClass('loading');
			container.find('.loader').remove();
			$('.datepicker').datepicker('update');
			//container.find('input, textarea').val('');
			if( container.data('related_container') ){
				
				$.pjax.reload({ container: $('#' + container.data('related_container')) });
			}
		},
		'pjax:timeout': function(event){
			event.preventDefault();
		}
	});

	if(typeof(GoogleService) != 'undefined'){
		$('#to_map').on('shown.bs.tab', function(event){
			GoogleService.initMap("map");
			GoogleService.setMarkers();
		});
	}

	$(document).on('afterValidate', 'form.area_validation', validateArea);

	$(document).on('hide.bs.modal', '.modal.edit_section', function(e){
		$.pjax.reload({container: '#' + $(this).data('related_section')});
	});

	$(document).on('click', '[do="submit_editable"]', function(e){
		e.preventDefault();
		if($(e.target).closest('form').length)
			$(e.target).closest('form').yiiActiveForm('submitForm');
		else $(e.target).closest('.editor').find('form').yiiActiveForm('submitForm');
	});

	$(document).on('click', '[do="cancel_editable"]', function(e){
		e.preventDefault();
		$(e.target).closest('.editable').removeClass('editing');
	});

	$(document).on('click', '.input-group-addon', function(e){
		$(e.target).closest('.input-group').find('input').trigger('focus');
	});

	$(document).on('submit', 'form[data-pjax="false"]', function(event){
		event.stopPropagation();
		$(event.target).append(loader).addClass('loading');
	});
	

	//set first agent as active on choose an agent page
	$('#field_y53bof').val($('.agent-item.active .hidden-name').attr('data-name'));

	$("#other-short-sale, #other-commercial, #other-multi-family, #other-rental, #other-apartments, #other-land").prop( "disabled", true );

	$("#frm_field_88_container").on("mouseup touchend", rangeChange);
	$("#frm_field_84_container").on("mouseup touchend", rangeChange);
	$("#frm_field_90_container").on("mouseup touchend", rangeChange);
	$("#frm_field_92_container").on("mouseup touchend", rangeChange);

	
	if($(this).scrollTop() >= menuTop + 0) { 
		$('#main-navigation-bar').addClass('sticky');
		$('#header').addClass('headerPush');
		//gridResize();
	}else{
		$('#main-navigation-bar').removeClass('sticky');
		$('#header').removeClass('headerPush');
		//gridResize();
	}
	
	$('.page-hero').on('click', '.jcarousel li', function(){
	    $('html, body').animate({
	        scrollTop: $("#quiz-banner").offset().top
	    }, 1000);
	});
	
	try {
	    var menuTop = $('#main-navigation-bar').offset().top;
	}
	catch(e) {

	}
	
	$(window).scroll(function(){
	
		if($(this).scrollTop() >= menuTop + 0) { 
			$('#main-navigation-bar').addClass('sticky');
			$('#header').addClass('headerPush');
			//gridResize();
		}else{
			$('#main-navigation-bar').removeClass('sticky');
			$('#header').removeClass('headerPush');
			//gridResize();
		}
	});
	$('.post-type-archive-featured-agent .agent-item').on('click', 'div', function(){
		$('.post-type-archive-featured-agent .agent-item').removeClass('active');
		$('.agent-single').removeClass('active');
		
		$(this).parent().addClass('active');
		var selectAgent = $(this).parent().attr('data-agent');
		$('#agent-single-' + selectAgent).addClass('active');
		$('#field_y53bof').val($('.agent-item.active .hidden-name').attr('data-name'));
	});
	
	$('.cta-callout').on('click', '#matched-button', function(){
		var popup = '';
		popup = $(this).parent().parent().parent().find('.agent-popup');
		if(popup != '') {
			popup.fadeIn('400', function() {
                $(this).css("display", "block");
                $(this).css("opacity", 100);
                });
		}
		
	});

	
	$('.cta-callout').on('click', '#popup-close', function(){
		$('.agent-popup').hide();
		$('.agent-popup').css("opacity", 0);
	});
	
	$('#quiz-box #frm_form_15_container').on('click', '.select-item', function(){
		$('.select-item').removeClass('selected');
		$(this).addClass('selected');
		$('input[name="item_meta[141]"]').val(this.id);
	});
	
    $('input[type="range"]').rangeslider({ polyfill: false });
	$('input[type="range"]').after('<div class="output">' + $(this).val() + '</div>');
	
	$('#frm_field_92_container .rangeslider__handle').prepend('<div id="value-tracker">$500,000</div>');

	$("#frm_field_92_container #rangevalue").mousemove(function () {
	    $("#value-tracker").text($("#rangevalue").val());
	});

	//$('#quiz-form-title').text($('.quiz-display .frm_primary_label').text());

	// center menu
	if($( window ).width() > 768){
	
	}

	// homepage images
	$('.home #circle-images').cycle({
		
	});
	$('#value-tracker').text('$' + ReplaceNumberWithCommas($('input[name="item_meta[92]"]').val()));
	if($('input[name="item_meta[92]"]').val() == '1000000') { $('#value-tracker').text('> $1 M'); }
	if($('input[name="item_meta[98]"]').val() != '.5') {
		$('.frm_form_fields.frm_page_num_1').css('margin-bottom', '4em');
		$('.frm_page_num_1 .frm_submit').show();
	}

	$(document).on('input', 'input[name="item_meta[98]"]', function() {
		$('.frm_form_fields').css('margin-bottom', '4em');
		$('.frm_page_num_1 .frm_submit').show();
	});

	$(document).on('input', 'input[name="item_meta[92]"]', function() {
		$('#value-tracker').text('$' + ReplaceNumberWithCommas($(this).val()));
		if($(this).val() == '1000000') { $('#value-tracker').text('> $1 M'); }
	});

	$(window).load(function(){
		gridResize();
	});

	$(window).resize(function(){
		if($( window ).width() > 768) { gridResize(); }
		else {
			$('.twocol').height('auto');
			$('.tencol').height('auto');
		}
	});
});

function rangeChange() { 	
		
	var $tabs = $(this).parent().parent();
	var nextTabNum = parseInt($(this).parent().attr('id').slice(-1)) + 1;
	var $tabPanel = $('#tablist1-panel' + nextTabNum);
	
	// remove hidden mobile class from any other panel as we'll want that panel to be open at mobile size
	$tabs.find('.responsive-tabs__panel--closed-accordion-only').removeClass('responsive-tabs__panel--closed-accordion-only');
	
	// close current panel and remove active state from its (hidden on desktop) heading
	$tabs.find('.responsive-tabs__panel--active').toggle().removeClass('responsive-tabs__panel--active').attr('aria-hidden','true').prev().removeClass('responsive-tabs__heading--active');
							
	//make this tab panel active
	$tabPanel.toggle().addClass('responsive-tabs__panel--active').attr('aria-hidden','false');

	//make the hidden heading active
	//$tabHeading.addClass('responsive-tabs__heading--active');

	//remove active state from currently active tab list item
	$tabs.find('.responsive-tabs__list__item--active').removeClass('responsive-tabs__list__item--active');

	//make this tab active
	$('#tablist1-tab' + nextTabNum).addClass('responsive-tabs__list__item--active');
	
}

function gridResize() {
	$('.resource-display').each(function(i){
		var maxHeight = 0;
			$(this).find('.twocol').each(function(){
				$(this).height("auto");
					if($(this).height() > maxHeight) maxHeight = $(this).height();
			});
			$(this).find('.tencol').each(function(){
				$(this).height("auto");
					if($(this).height() > maxHeight) maxHeight = $(this).height();
			});
				$(this).find('.twocol').each(function(){ $(this).height(maxHeight); });
				$(this).find('.tencol').each(function(){ $(this).height(maxHeight); });
	});
	
	
	
	$('.blog .resource-display').each(function(i){
		var maxHeight = 0;
			$(this).find('.fourcol').each(function(){
				$(this).height("auto");
					if($(this).height() > maxHeight) maxHeight = $(this).height();
			});
				$(this).find('.fourcol').each(function(){ $(this).height(maxHeight); });
	});
	
}

function ReplaceNumberWithCommas(yourNumber) {
	try { var n= yourNumber.toString().split(".");
    //Seperates the components of the number	
    //Comma-fies the first part
    n[0] = n[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    //Combines the two sections
    return n.join(".");
	}
	catch(e) {}
}

function autoComplete(){

	let searchBoxes = $('.profile_areas');
	if (searchBoxes.length){
		if(typeof(google) != 'undefined'){
			searchBoxes.each(function(){
				let searchBox = new google.maps.places.Autocomplete(this),
					$this = $(this);

				$this.on('keydown', event => {
					if(event.which == 13){
						event.preventDefault();
						return false;
					}
				});				

			    searchBox.setComponentRestrictions({'country':'us'});
			    ( $this.data('type') ? searchBox.setTypes([$this.data('type')]) : false );
			    searchBox.addListener('place_changed', function() {			    	
			        let place = searchBox.getPlace();
					console.log(place);
			        setSearchboxData($this, place);
			    });
			    $this.closest('.area_loading').removeClass('loading').find('.loader').remove();
			});
		} else {
			$.ajax({
				url: 'https://maps.googleapis.com/maps/api/js?key=AIzaSyDbgI-qDa-U_JWtQbuqj7kOdBK4vYBCJQ8&libraries=places&language=en&callback=autoComplete',
				method: 'GET',
				crossDomain: true
			});
		}
	}
}

function setSearchboxData($searchBox, place){

	if($searchBox.data('stringify')){
		$('#' + $searchBox.data('string_input')).val(parseArea(place, true))
		return false;
	}
	let data = parseArea(place);

	( $searchBox.data('city-id') && data.city ) 			? $('#' + $searchBox.data('city-id')).val(data.city) 				: false;
	//( $searchBox.data('city-id') && !data.city )			? $('#' + $searchBox.data('city-id')).val(data.region)				: false;
    ( $searchBox.data('state-id') && data.state ) 			? $('#' + $searchBox.data('state-id')).val(data.state) 				: false;
    ( $searchBox.data('region-id') && data.region ) 		? $('#' + $searchBox.data('region-id')).val(data.region) 			: false;
    ( $searchBox.data('short-id') && data.short_name ) 		? $('#' + $searchBox.data('short-id')).val(data.short_name) 		: false;
    ( $searchBox.data('address-id')  && data.address ) 		? $('#' + $searchBox.data('address-id')).val(data.address) 			: false;
    ( $searchBox.data('zipcode-id')  && data.zipcode ) 		? $('#' + $searchBox.data('zipcode-id')).val(data.zipcode) 			: false;
    ( $searchBox.data('photo-id')  && data.photo ) 			? $('#' + $searchBox.data('photo-id')).val(data.photo) 				: false;
    ( $searchBox.data('lat-id') && data.lat ) 				? $('#' + $searchBox.data('lat-id')).val(data.lat) 					: false;
    ( $searchBox.data('lng-id') && data.lng ) 				? $('#' + $searchBox.data('lng-id')).val(data.lng) 					: false;
    ( $searchBox.data('place_id-id') && data.place_id ) 	? $('#' + $searchBox.data('place_id-id')).val(data.place_id) 		: false;
	

	if( ($searchBox.data('zipcode-id') && !data.zipcode) || ($searchBox.data('photo-id') && !data.photo) ){
    	let geocoder = new google.maps.Geocoder;
	    
	    let latlng = new google.maps.LatLng(data.lat, data.lng);
		geocoder.geocode({'latLng': latlng}, function(results) {
			
			for(let i = 0; i < results.length; i++){
	      		for(let j = 0; j < results[i].address_components.length; j++){
	          		for(let k = 0; k < results[i].address_components[j].types.length; k++){
	              		if(results[i].address_components[j].types[k] == "postal_code"){

	                  		data.zipcode = results[i].address_components[j].short_name;	 
	                  		( $searchBox.data('zipcode-id')  && data.zipcode ) ? $('#' + $searchBox.data('zipcode-id')).val(data.zipcode) : false;
	                  		( $searchBox.data('place_id-id') && data.place_id ) ? $('#' + $searchBox.data('place_id-id')).val(data.place_id) : false;
	                  		if( $searchBox.data('photo-id') ){
	                  			if(data.route && data.city && data.short_name){	                  				
	                  				$('#' + $searchBox.data('photo-id')).val('https://maps.googleapis.com/maps/api/streetview?size=260x190&location=' + data.route + ',' + data.city + ',' + data.short_name + '&fov=100&key=AIzaSyDbgI-qDa-U_JWtQbuqj7kOdBK4vYBCJQ8');
	                  			} else{
	                  				$('#' + $searchBox.data('photo-id')).remove();
	                  			}
	                  		}
	                  		
	                  	}
	                }
	            }
	        }

	    });			    
	}
	return false;
}

function parseArea(place, to_string = false){
	let area = {};
	for(i in place.address_components){
		/*if ( place.address_components[i].types[0] == 'country' || place.address_components[i].types[0] == 'postal_code' || place.address_components[i].types[0] == 'street_address' || place.address_components[i].types[0] == 'route' || place.address_components[i].types[0] == 'intersection' || place.address_components[i].types[0] == 'administrative_area_level_3' || place.address_components[i].types[0] == 'administrative_area_level_4' || place.address_components[i].types[0] == 'administrative_area_level_5' )
			continue;*/
		if ( place.address_components[i].types[0] == 'administrative_area_level_1' ) {
			area['short_name'] = place.address_components[i].short_name;
			area['state'] = place.address_components[i].long_name;
		}
		if ( place.address_components[i].types[0] == 'administrative_area_level_2' )
			area['region'] = place.address_components[i].long_name;
		if ( place.address_components[i].types[0] == 'locality' || place.address_components[i].types[0] == 'sublocality_level_1' || place.address_components[i].types[0] == 'sublocality' )
			area['city'] = place.address_components[i].long_name;
		if ( place.address_components[i].types[0] == 'route' )
			area['route'] = place.address_components[i].long_name;
		if ( place.address_components[i].types[0] == 'street_number' )
			area['street_number'] = place.address_components[i].long_name;
		if ( place.address_components[i].types[0] == 'postal_code' )
			area['zipcode'] = place.address_components[i].long_name;
	}
	if ( place.place_id )
		area['place_id'] = place.place_id;
	if (area['route']){
		area['address'] = area['route'];
		if (area['street_number']){
			area['address'] = area['street_number'] + ', ' + area['route'];
		}
	}
	if (!area.region)
		area.region = area.city;

	area.lat = place['geometry']['location'].lat();
	area.lng = place['geometry']['location'].lng();
	
	return (to_string) ? JSON.stringify(area) : area;	
}

function validateArea(event, attribute, messages){
	let area_field = $(event.target).find('.area_field'),
		invalid_attributes = [];
	for (let i in messages){
		if( $(messages[i].input).parents('.area_field').length )
			invalid_attributes.push(messages[i].name);
	}
	if(invalid_attributes.length) 
		area_field.removeClass('has-success').addClass('has-error').append('<p class="help-block help-block-error">'+invalid_attributes.join(', ')+(invalid_attributes.length > 1 ? ' are': ' is')+' invalid.</p>');
}

function setParam(event, param_input_id, val){
	event.preventDefault();
	$('#' + param_input_id).val(val);
	$(event.target).parent().children().removeClass('selected');
	$(event.target).addClass('selected');
}

function triggerFileInput(event, input_id, preview_img_id = null, submit = false){
	let input = $('#' + input_id);
	event.preventDefault();
	input.trigger('click');
	
	input.on('change', function(){	
		if (preview_img_id){	
			let file;
		    if ((file = this.files[0])) {
		        $('#' + preview_img_id).attr('src', _URL.createObjectURL(file));
		    }	
		}
		if (submit){
			input.closest('form').yiiActiveForm('submitForm');
		}
	})		
}

function editById(event, modal_id, action, data_id){
	event.preventDefault();
	$('#' + modal_id).modal('show', data_id);
	$('#' + modal_id).find('form').attr('action', _base_url + 'agent/'+action+'?id=' + data_id);	
}

function readMessage(event, message_id, isNew, message_count){
	event.preventDefault();
	let item = $(event.target).closest('li'),
		counter = $('.message_count');
	( item.hasClass('open') ) ? item.removeClass('open') : item.addClass('open');
	if (isNew){	
		let data = {
			id: message_id
		};
		data[yii.getCsrfParam()] = yii.getCsrfToken();
		$.ajax({
			url: _base_url + 'agent/change-contact-status',
			method: 'POST',
			data: data,
			success: function(resp){
				if(resp.status){
					item.removeClass('active');
					( message_count ) ? counter.html(message_count - 1) : false;
				}
				
			}
		});
	}	
}

function cbr_replace()
{
	var $inputs = $('input[type="checkbox"].cbr, input[type="radio"].cbr').filter(':not(.cbr-done)'),
		$wrapper = '<div class="cbr-replaced"><div class="cbr-input"></div><div class="cbr-state"><span></span></div></div>';

	$inputs.each(function(i, el)
	{
		var $el = jQuery(el),
			is_radio = $el.is(':radio'),
			is_checkbox = $el.is(':checkbox'),
			is_disabled = $el.is(':disabled'),
			styles = ['primary', 'secondary', 'success', 'danger', 'warning', 'info', 'purple', 'blue', 'red', 'gray', 'pink', 'yellow', 'orange', 'turquoise'];

		if( ! is_radio && ! is_checkbox)
			return;

		$el.after( $wrapper );
		$el.addClass('cbr-done');

		var $wrp = $el.next();
		$wrp.find('.cbr-input').append( $el );

		if(is_radio)
			$wrp.addClass('cbr-radio');

		if(is_disabled)
			$wrp.addClass('cbr-disabled');

		if($el.is(':checked'))
		{
			$wrp.addClass('cbr-checked');
		}


		// Style apply
		jQuery.each(styles, function(key, val)
		{
			var cbr_class = 'cbr-' + val;

			if( $el.hasClass(cbr_class))
			{
				$wrp.addClass(cbr_class);
				$el.removeClass(cbr_class);
			}
		});


		// Events
		$wrp.on('click', function(ev)
		{
			if(is_radio && $el.prop('checked') || $wrp.parent().is('label'))
				return;

			if(jQuery(ev.target).is($el) == false)
			{
				$el.prop('checked', ! $el.is(':checked'));
				$el.trigger('change');
			}
		});

		$el.on('change', function(ev)
		{
			$wrp.removeClass('cbr-checked');

			if($el.is(':checked'))
				$wrp.addClass('cbr-checked');

			cbr_recheck();
		});
	});
}

function cbr_recheck()
{
	var $inputs = $("input.cbr-done");

	$inputs.each(function(i, el)
	{
		var $el = $(el),
			is_radio = $el.is(':radio'),
			is_checkbox = $el.is(':checkbox'),
			is_disabled = $el.is(':disabled'),
			$wrp = $el.closest('.cbr-replaced');

		if(is_disabled)
			$wrp.addClass('cbr-disabled');

		if(is_radio && ! $el.prop('checked') && $wrp.hasClass('cbr-checked'))
		{
			$wrp.removeClass('cbr-checked');
		}
	});
}

function ifAccept(event){
	let checkbox = $(event.target),
		submit = $(event.target).closest('form').find('.submit-btn');
	if(checkbox.is(':checked')){
		submit.removeClass('disabled');
	} else {
		submit.addClass('disabled');
	}
	
	
}

function registration(event, action){
	event.preventDefault();
	let form = $(event.target);
	let inputs = form.serializeArray();
	let data = {};
	
	for(let i in inputs){
		data[inputs[i].name] = inputs[i].value;
	};
	$.ajax({
		method: "POST",
		data: data,
		url: _base_url + 'home/' + action,
		beforeSend: function(){
			$('#loader').show();
		},
		success: function(resp){
			if(!resp.status){
				for (let model_name in resp.errors){
					for(let attr_name in resp.errors[model_name]){
						$('[name="' + model_name + '[' + attr_name + ']"]').closest('.form-group').removeClass('has-success').addClass('has-error').find('.help-block').html(resp.errors[model_name][attr_name]);
					}
				}
				form.removeClass('loading').find('.loader').remove();
			} 
			else {				
				window.location.href = resp.redirect_url;
			}
		},
		error: function(xhr, error, message){
			$('#loader').hide();
			alert("Internal server error occured.\nPlease, try again\nCheck your console for details");
			console.log(xhr);
			console.log(error);
			console.log(message);
		}
	});
}

function menuToggle(event, menu_id){
	$('#' + menu_id).toggleClass('open');
}

function editField(event, input_id){
	event.preventDefault();
	let $this = $(event.target);
	$('.editable').removeClass('editing');
	$this.closest('.editable').addClass('editing');
	$('#' + input_id).trigger('focus');
}

function scrollToElement(event){
	$('html, body').animate({
        scrollTop: $($(event.target).attr('href')).offset().top - $("#main-navigation-bar").height() * 2
    }, 400);
}

function increase(event, input_id){
	event.preventDefault();
	$('#' + input_id).val( function(i, oldval) {
        return ++oldval;
    });
}

function decrease(event, input_id){
	event.preventDefault();
	$('#' + input_id).val( function(i, oldval) {
        return (oldval != 0) ? --oldval : 0;
    });
}

function transactionApprove(event, trans_id){
	event.preventDefault();
	let data = {
			id: trans_id
		},
		block = $(event.target).closest('.panel');

	data[yii.getCsrfParam()] = yii.getCsrfToken();
	$.ajax({
		method: "POST",
		url: _base_url + 'agent/approve-property',
		data: data,
		beforeSend: function(){
			block.addClass('loading').append(loader);
		},
		success: function(resp){
			if(resp.status){
				block.addClass('approved');
				$(event.target).addClass('complete').html('approved');
			}
			block.removeClass('loading').find('.loader').remove();
		},
		error: function(xhr,error,message){
			console.log(error);
			block.removeClass('loading').find('.loader').remove();
		}
	});
}

function toMap(event, target_id){
	event.preventDefault();
	if(target_id){
		$('#' + target_id).tab('show');
	}
}

function submitForm(event){
	$(event.target).closest('form').trigger('submit');
}

function removeFromMatches(event, fav_id, remove = false){
	event.preventDefault();
	if(ajaxActive)
		return false;
	let data = {
		id: fav_id
	};
	let block = $(event.target).closest('li');
	data[yii.getCsrfParam()] = yii.getCsrfToken();
	$.ajax({
		url: _base_url + 'ajax/delete-favorite',
		method: "POST",
		data: data,
		beforeSend: function(){
			ajaxActive = true;
			block.addClass('loading').append(loader);
		},
		success: function(resp){
			ajaxActive = false;
			if(resp.status){
				$('.match-count').each(function(){
					$(this).html(+$(this).text() - 1);
				});
				if(remove){
					$(event.target).closest('li').hide(500).remove();
					if(typeof(resp.data) && resp.data.is_last)
						$(event.target).closest('.panel').append('<ul><li class="text-center">No favorites yet!</li></ul>');
				}
				else
					$(event.target).html("Add to matches").removeClass('complete').attr('onclick', 'addToMatches(event, '+resp.data.id_favorite+')');
			}
			block.removeClass('loading').find('.loader').remove();
		},
		error: function(xhr, error, message){
			ajaxActive = false;
			console.log(xhr, error, message);
			removeFromMatches(event, user_id, remove);
		}
	});
}

function hashCheck() {
	if(window.location.hash == '#login_open' && !_is_logged_in)
		$('#signup_modal').modal('show');
	if(window.location.hash == '#contact_agent')
		$('#contact_agent').modal('show');
	if(window.location.hash == '#new_review')
		$('html, body').animate({scrollTop: $("#new_review_container").offset().top - 100}, 1000);
	if(window.location.hash == '#search' && !_is_logged_in){
		$('html, body').animate({
			scrollTop: $("#main_search").closest('form').offset().top - 120
		}, 500);
		$('#main_search').trigger('focus');
	}
	return;
}

function oneStrExpand(event) {
	event.preventDefault();
	$(event.target).css({'white-space' : 'normal'}).tooltip('destroy');
}