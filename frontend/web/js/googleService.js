let GoogleService = {
    bounds: null,
    dataProvider: null,
    photoRequest: [],
    markers : [],
    isAvailable: function(){
        if (typeof(google) != 'undefined'){
            return true;
        }
        return false;
    },
    init: function(dataProvider = null){
        if (this.isAvailable()){
            this.bounds = new google.maps.LatLngBounds;
            if (typeof(dataProvider) == 'string'){
                dataProvider = JSON.parse(dataProvider);
            }
            this.dataProvider = dataProvider;
            for (let i in dataProvider) {                   
                this.bounds.extend(new google.maps.LatLng(+dataProvider[i].lat, +dataProvider[i].lng)); 
            }           
        } else {
            //alert('Google Services are not available at the moment!');
        }        
    },
    initMap: function(map_id){
        let block = document.getElementById(map_id);
        if (this.isAvailable() && block){
            let options = {
                zoom: 16,
                center: this.bounds.getCenter(),
                streetViewControl: false,
                mapTypeControl: false,
                scrollwheel: false,
            }
            if(this.dataProvider && this.dataProvider.length){
                this.map = new google.maps.Map(block, options);
                this.map.fitBounds(this.bounds);
            } else {
                block.innerHTML = '<img src="' + _base_url + '/img/no_map.jpg" width="100%" height="100%" />';
            }
        }
    },
    setMarkers: function(){
        if (this.isAvailable() && this.map){
            for(let i in this.markers){
                this.markers[i].setMap(null);
            }
            this.markers = [];
            if(this.dataProvider){
                for (let i in this.dataProvider){
                    let marker = new google.maps.Marker({
                        position: new google.maps.LatLng(+this.dataProvider[i].lat, +this.dataProvider[i].lng),
                        //icon: 'https://d3ld1cdy4ojgst.cloudfront.net/assets/frog/google-marker-21x36.png',
                        icon: _base_url+'img/pin.png',
                        animation: google.maps.Animation.DROP,
                    });
                    /*let infoContent = this.dataProvider[i].beds + ' Bedrooms ' + this.dataProvider[i].property_type + ' Home<br>Sold on ' + this.dataProvider[i].selling_date + '<br><br>' + this.dataProvider[i].address_1 + '<br>' + this.dataProvider[i].city;
                    let infowindow = new google.maps.InfoWindow({
                        content: infoContent
                    });
                    marker.addListener('click', function() {
                        infowindow.open(this.map, marker);
                    });*/
                    this.markers.push(marker);
                }        
            }            
            for ( let i in this.markers ){
                this.markers[i].setMap(this.map);
            }
        }
    },
    downloadPhotos: function(){        
        if (this.isAvailable()){
            $('.photos_download').html('<i class="fa fa-spinner fa-pulse"></i>');
            
            for (let i in this.dataProvider){
                if (this.dataProvider[i].photo)
                    $('#photo_' + this.dataProvider[i].id ).html('<img src="' + this.dataProvider[i].photo + '" />');
                else 
                    $('#photo_' + this.dataProvider[i].id ).html('<img src="https://maps.googleapis.com/maps/api/streetview?size=200x100&location='+ this.dataProvider[i].address_1+','+ this.dataProvider[i].city+','+ this.dataProvider[i].state+'&fov=110&key=AIzaSyDbgI-qDa-U_JWtQbuqj7kOdBK4vYBCJQ8" />');
            }
        }
    },
    getStaticMap: function(target_element_id){
        if (this.isAvailable()){
            let marker_request = '';
            for (let i in this.dataProvider){
                marker_request += 'markers=icon:https://d3ld1cdy4ojgst.cloudfront.net/assets/frog/google-marker-21x36.png|'
                +this.dataProvider[i].lat+','+this.dataProvider[i].lng+'&';
            }
            let request = `https://maps.googleapis.com/maps/api/staticmap?center=`+this.bounds.getCenter().lat()+`,`+this.bounds.getCenter().lng()+`&`+marker_request+`size=438x502&style=feature:poi%7Celement:labels.icon%7Cvisibility:off&style=feature:poi.business%7Cvisibility:off&style=feature:poi.park%7Celement:geometry%7Ccolor:0xb1cea4&style=feature:transit%7Cvisibility:off&style=feature:landscape.man_made%7Celement:geometry%7Ccolor:0xedece8&style=feature:road.highway%7Celement:labels.icon%7Cvisibility:off&style=feature:water%7Celement:geometry%7Ccolor:0xc9dcea&key=AIzaSyDbgI-qDa-U_JWtQbuqj7kOdBK4vYBCJQ8`;
            $('#' + target_element_id).html('<img src="'+request+'" />');
        }
    }
}
