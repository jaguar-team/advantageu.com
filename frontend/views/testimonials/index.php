<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 021 21.06.17
 * Time: 16:24
 */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

$this->title = 'Testimonials | AdvantageU';

?>
<style type="text/css">
	#background-bar img{
		display: none;
	}
</style>
<div class="blog">
<div class="page">
    <div class="the-content">
        <div class="page-hero acf-row-0" style="background-image: url('/img/pages/testimonial-header.jpg');">
            <?= Html::img('@uri_img/pages/testimonial-header.jpg', ['id' => 'hero-image', 'alt' => 'Home Selling Testimonials']) ?>
            <div class="wrap">
                <div class="headingWrap"><h1 id="page-heading">Testimonials | AdvantageU</h1></div>
            </div>
        </div>
        <div style="background-image: url()" class="text-block resource-display acf-row acf-row-1"><div class="wrap"><h3>See what people have to say about us</h3><h2>Customer Testimonials</h2>

            <div class=" four-column-display resource-display acf-row no-top-pad acf-row-2" style="padding-bottom: 0;"><div class="wrap"><div class="threecol first">
            	<?= Html::img(Url::to(['@uri_img/pages/Revised-Testimonials-1.jpg'], true), ['alt' => 'Home Seller']) ?>
            </div><div class="threecol ">
            	<?= Html::img(Url::to(['@uri_img/pages/Revised-Testimonials-2.jpg'], true), ['alt' => 'For Sale By Owner']) ?>
            </div><div class="threecol ">
            	<?= Html::img(Url::to(['@uri_img/pages/Revised-Testimonials-3.jpg'], true), ['alt' => 'Sold My House']) ?>
            </div>
            	<div class="threecol ">
            		<?= Html::img(Url::to(['@uri_img/pages/Revised-Testimonials-4.jpg'], true), ['alt' => 'Found a Real Estate Agent']) ?>
            	</div><div class="clearfix"></div></div></div>


            <div class="testimonial-single">
                <p>
                    <?= Html::img(Url::to(['@uri_img/pages/default.png'], true), ['class' => 'testimonial-thumb']) ?>
                    			<p>My name is Marlene and I did speak with the AdvantageU rep Robin and also the realtor Christina,  I did have a very favorable experience with both.  Robin was professional and knowledgeable about your agent Matching service.  I normally do not talk to someone who calls &#8220;out of the blue&#8221;, but as I told Robin, the way she represented your service and herself made me believe that what she was promoting was legitimate and I felt comfortable talking with her.  The same goes for Christina, and appreciate all the efforts she has already put forth on my husband and my behalf.  We are not ready to list our home at this time but will consider you when we are.</p>
                </p>
                <h4>- Marlene S.</h4>
            </div>

            <div class="testimonial-single">
                <p>
                    <?= Html::img(Url::to(['@uri_img/pages/default.png'], true), ['class' => 'testimonial-thumb']) ?>
                    			<p>Since we were not in the area we thought we should sell our house. After putting on zillow we saw their was activity. When advantage U called they connected me with Jason who sold the home quickly . He was very persistent with follow up and the transaction went smoothly with Jason. I would recommend Jason and Advantage U in the future.</p>
                </p>
                <h4>- Rebecca. Smithville, TX</h4>
            </div>

            <div class="testimonial-single">
                <p>
                    <?= Html::img(Url::to(['@uri_img/pages/default.png'], true), ['class' => 'testimonial-thumb']) ?>
                     			<p>I started as a FSBO before meeting Joe W. through Advantage U. He convinced me that to really net what I needed I had to do renovations. Originally I was looking for 340000 however after it was all said and done my home sold for 370000. Joe did an excellent Job and I would recommend Advantage U and Joe W.</p>
                </p>
                <h4>- Travis K. Wake Forest , NC</h4>
            </div>

            <div class="testimonial-single">
                <p>
                    <?= Html::img(Url::to(['@uri_img/pages/default.png'], true), ['class' => 'testimonial-thumb']) ?>
                     			<p>I started to sell my home FSBO and had no one come to any showings. My phone was ringing off the hook with agents and I finally gave in and decided to give  Advantage U a chance. They Set up an appointment with Bill K. and it was sold in less than two weeks. He was very active in pursuing the sell.</p>
                </p>
                <h4>- Adam P. Mauldin, SC</h4>
            </div>

            <div class="testimonial-single">
                <p>
                    <?= Html::img(Url::to(['@uri_img/pages/default.png'], true), ['class' => 'testimonial-thumb']) ?>
                     			<p>Advantage U called after I was selling my home FSBO for two weeks and had low offers. After becoming swamped with handling it all on my own I decided to let them set me up with Suzanne who helped me sell the home in two weeks. I was able to net what I wanted and would recommend both Suzanne and Advantage U.</p>
                </p>
                <h4>- Matthew K. Charlotte NC</h4>
            </div>

            <div class="testimonial-single">
                <p>
                    <?= Html::img(Url::to(['@uri_img/pages/default.png'], true), ['class' => 'testimonial-thumb']) ?>
                     			<p>When we received a phone call from Advantage U we were trying to close on our house on our own. We needed help finishing the job and Gary and Ryan really provided us with the help we needed with the finishing touches. They were professional and I would recommend them in the future.</p>
                </p>
                <h4>- Wendell H. Mechanicsville, VA</h4>
            </div>

            <div class="testimonial-single">
                <p>
                    <?= Html::img(Url::to(['@uri_img/pages/default.png'], true), ['class' => 'testimonial-thumb']) ?>			<p>I was looking at 4 other agents when Advantage U called and matched me with John. We had to make a move pretty soon so we were in a bind when Advantage U called he was able to help sell the home so we could move on schedule. I would recommend your service and John&#8217;s service in the future.</p>
                </p>
                <h4>- Jack B. Roberts, WI</h4>
            </div>

            <div class="testimonial-single">
                <p>
                    <?= Html::img(Url::to(['@uri_img/pages/default.png'], true), ['class' => 'testimonial-thumb']) ?> 			<p>Advantage you had very great timing when they called me and I felt I was qualified very well. JJ was the agent they recommend. He was very professional and sold 3 units fast within a month and I would recommend Advantage U going forward.</p>
                </p>
                <h4>- Fang H. Medford, OR</h4>
            </div>

            <div class="testimonial-single">
                <p>
                    <?= Html::img(Url::to(['@uri_img/pages/default.png'], true), ['class' => 'testimonial-thumb']) ?>		<p>I would recommend Advantage U to friends and family. Once I spoke with Will I knew I would give the listing to the agent you sent which was Chad Bishop. Very fast turn around and for more money than I was asking for. Chad was able to sell my home in Florida and I live in New Jersey. He went above and beyond and I did not have to worry throughout the process.</p>
                </p>
                <h4>- Ondra M. Miramar, FL</h4>
            </div>

            <div class="clearfix"></div></div></div>

        <div class="text-callout testimonial-display acf-row acf-row-"><div class="wrap"><div class="cycle-slideshow" data-cycle-slides="> div"><div><h2 class="testimonial-content">They provided incredible service! They sold my home in 14 days and saved me $3,514.</h2><h2 class="testimonial-name">&#8212; Judy D’Ambrose, Webster NY</h2></div><div><h2 class="testimonial-content">They sold my house in 4 days and I was able to pocket an extra $5,713.</h2><h2 class="testimonial-name">&#8212; Steve Johnston, Greece NY</h2></div><div><h2 class="testimonial-content">They sold our home in 18 days, we saved money and they helped us purchase our new home.</h2><h2 class="testimonial-name">&#8212; Mike and Diane Osewalt, Rochester NY</h2></div><div><h2 class="testimonial-content">We received a contract for sale in 4 days of signing up and saved $2,250.</h2><h2 class="testimonial-name">&#8212; Rick and Suzy Kasper, Rush NY</h2></div><div><h2 class="testimonial-content">I was planning to move from NJ to join my family in Atlanta, GA. I’d lived in my home for 20 </h2><h2 class="testimonial-name">&#8212; Paula, Atlanta GA</h2></div><div><h2 class="testimonial-content">I was happy with the advantageU matching service and how much professionalism the agent Bobby s</h2><h2 class="testimonial-name">&#8212; John P. Penfield, NY</h2></div><div><h2 class="testimonial-content">I used the advantage U matching service, I appreciated the concept and would recommend the free</h2><h2 class="testimonial-name">&#8212; Alan H.  De Pere, WI</h2></div><div><h2 class="testimonial-content">I was happy with the program and I would definitely recommend AdvantageU service. I was able to</h2><h2 class="testimonial-name">&#8212; Senarath B. Antelope, CA</h2></div><div><h2 class="testimonial-content">I was very pleased with this service. My house was on the market for 50 days with no serious of</h2><h2 class="testimonial-name">&#8212; John M. Cedar Rapids, IA</h2></div><div><h2 class="testimonial-content">Before using the free AdvantageU service I tried  For Sale By Owner for 1 week because I neede</h2><h2 class="testimonial-name">&#8212; Dan B. Grand Rapids, MI</h2></div><div><h2 class="testimonial-content">I was happy with Advantage U setting me up with John and I would recommend him to other people </h2><h2 class="testimonial-name">&#8212; Robert W. Tallahassee, FL</h2></div><div><h2 class="testimonial-content">The service provided to me by Advantage U was good and I am glad they picked out TJ. I was not </h2><h2 class="testimonial-name">&#8212; Crystal H. Myrtle Beach, SC</h2></div><div><h2 class="testimonial-content">I would recommend Advantage U to friends and family. Once I spoke with Will I knew I would give</h2><h2 class="testimonial-name">&#8212; Ondra M. Miramar, FL</h2></div><div><h2 class="testimonial-content">Advantage you had very great timing when they called me and I felt I was qualified very well. J</h2><h2 class="testimonial-name">&#8212; Fang H. Medford, OR</h2></div><div><h2 class="testimonial-content">I was looking at 4 other agents when Advantage U called and matched me with John. We had to mak</h2><h2 class="testimonial-name">&#8212; Jack B. Roberts, WI</h2></div><div><h2 class="testimonial-content">When we received a phone call from Advantage U we were trying to close on our house on our own.</h2><h2 class="testimonial-name">&#8212; Wendell H. Mechanicsville, VA</h2></div><div><h2 class="testimonial-content">Advantage U called after I was selling my home FSBO for two weeks and had low offers. After bec</h2><h2 class="testimonial-name">&#8212; Matthew K. Charlotte NC</h2></div><div><h2 class="testimonial-content">I started to sell my home FSBO and had no one come to any showings. My phone was ringing off th</h2><h2 class="testimonial-name">&#8212; Adam P. Mauldin, SC</h2></div><div><h2 class="testimonial-content">I started as a FSBO before meeting Joe W. through Advantage U. He convinced me that to really n</h2><h2 class="testimonial-name">&#8212; Travis K. Wake Forest , NC</h2></div><div><h2 class="testimonial-content">Since we were not in the area we thought we should sell our house. After putting on zillow we s</h2><h2 class="testimonial-name">&#8212; Rebecca. Smithville, TX</h2></div><div><h2 class="testimonial-content">My name is Marlene and I did speak with the AdvantageU rep Robin and also the realtor Christina</h2><h2 class="testimonial-name">&#8212; Marlene S.</h2></div></div></div><div style="background-image: url(/img/pages/AdvantageU-CTA.jpg)" class="back-cover"></div></div></div>
    <div class="cta-callout testimonial-display acf-row acf-row-8 bot-bord" style="text-align: center; background-image: url('/img/pages/Blog-Page-callout.jpg');">
        <div class="wrap">
            <h2><em>Get Our Educational Resources</em> <span style="color: #ff9e13;">RIGHT TO YOUR INBOX.</span></h2>
            <p>AdvantageU provides free tools and the definitive A to Z list curated<br />
                from our extensive experience to save you time time and money.</p>
            <div class="frm_forms  with_frm_style frm_style_formidable-style" id="frm_form_16_container" >
                <form enctype="multipart/form-data" method="post" class="frm-show-form  frm_pro_form " id="form_p5d7e9" >
                    <div class="frm_form_fields ">
                        <fieldset>
                            <legend class="frm_hidden">Educational Resources Request Form</legend>

                            <div id="frm_field_234_container" class="frm_form_field form-field  frm_required_field frm_top_container frm_first frm_two_thirds">
                                <label for="field_2vvfgm" class="frm_primary_label">Your Full First and Last Name
                                    <span class="frm_required">*</span>
                                </label>
                                <input type="text" id="field_2vvfgm" name="item_meta[234]" value=""  data-reqmsg="This field cannot be blank."  />



                            </div>
                            <div id="frm_field_161_container" class="frm_form_field form-field  frm_required_field frm_top_container frm_first frm_two_thirds">
                                <label for="field_bt6dxk" class="frm_primary_label">Email Address
                                    <span class="frm_required">*</span>
                                </label>
                                <input type="email" id="field_bt6dxk" name="item_meta[161]" value=""  placeholder="Enter Email" data-reqmsg="This field cannot be blank." data-invmsg="Email Address is invalid"  />



                            </div>
                            <div id="frm_field_168_container" class="frm_form_field form-field  frm_required_field frm_top_container frm_third">
                                <label for="field_x9f3s9" class="frm_primary_label">Zip Code
                                    <span class="frm_required">*</span>
                                </label>
                                <input type="text" id="field_x9f3s9" name="item_meta[168]" value=""  placeholder="Zip Code" data-reqmsg="This field cannot be blank."  />



                            </div>
                            <input type="hidden" name="item_key" value="" />
                            <div class="frm_submit">

                                <input type="submit" value="Submit"  class="frm_final_submit" />
                            </div></fieldset>
                    </div>
                </form>
            </div>
        </div>

    </div>


</div>
</div>