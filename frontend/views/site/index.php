<?php
    use yii\helpers\Html;
    use yii\helpers\Url;
 ?>
<div class="page">
    <div class="the-content">
        <div class="page-hero page-hero-index acf-row-0">
            <img alt="Home" id="hero-image" src="img/AdvantageU-Home-header.jpg">
            <div class="wrap">
                <div class="headingWrap">
                    <h1>You want to sell your house.</h1>
                    <h2>We help you get there smarter, faster and for fair market value.</h2>
                    <div class="caro-wrap">
                        <div class="jcarousel" data-jcarousel="true" data-jcarouselautoscroll="true">
                            <ul style="left: 0px; top: -351px;">
                                <li class="">
                                    <?= Html::img( Url::to(['@uri_img/check-dark.gif'], true ), ['alt' => 'Checkmark'] ) ?>
                                    <h3 class="rotating-check">Gauge your need to sell.</h3>
                                </li>
                                <li class="">
                                    <?= Html::img( Url::to(['@uri_img/check-dark.gif'], true ), ['alt' => 'Checkmark'] ) ?>
                                    <h3 class="rotating-check">Calculate your selling expenses.</h3>
                                </li>
                                <li class="">
                                    <?= Html::img( Url::to(['@uri_img/check-dark.gif'], true ), ['alt' => 'Checkmark'] ) ?>
                                    <h3 class="rotating-check">Plan your selling strategy.</h3>
                                </li>
                                <li class="">
                                    <?= Html::img( Url::to(['@uri_img/check-dark.gif'], true ), ['alt' => 'Checkmark'] ) ?>
                                    <h3 class="rotating-check">Determine your home’s fair market value (FMV) and set a price.</h3>
                                </li>
                                <li class="target">
                                    <?= Html::img( Url::to(['@uri_img/check-dark.gif'], true ), ['alt' => 'Checkmark'] ) ?>
                                    <h3 class="rotating-check">Advertise and market the home.</h3>
                                </li>
                                <li class="">
                                    <?= Html::img( Url::to(['@uri_img/check-dark.gif'], true ), ['alt' => 'Checkmark'] ) ?>
                                    <h3 class="rotating-check">Prepare and stage the home.</h3>
                                </li>
                                <li class="">
                                    <?= Html::img( Url::to(['@uri_img/check-dark.gif'], true ), ['alt' => 'Checkmark'] ) ?>
                                    <h3 class="rotating-check">Set up showings and open houses.</h3>
                                </li>
                                <li class="">
                                    <?= Html::img( Url::to(['@uri_img/check-dark.gif'], true ), ['alt' => 'Checkmark'] ) ?>
                                    <h3 class="rotating-check">Review purchase offers.</h3>
                                </li>
                                <li class="">
                                    <?= Html::img( Url::to(['@uri_img/check-dark.gif'], true ), ['alt' => 'Checkmark'] ) ?>
                                    <h3 class="rotating-check">Make counteroffers and negotiate.</h3>
                                </li>
                                <li class="">
                                    <?= Html::img( Url::to(['@uri_img/check-dark.gif'], true ), ['alt' => 'Checkmark'] ) ?>
                                    <h3 class="rotating-check">Get through escrow.</h3></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <a class="anchor" id="quiz-banner"></a>
        <div class="quiz-banner-display bot-bord acf-row acf-row-1">
            <div class="wrap">
                <h3>Get the Homesellers’ Advantage</h3>
                <h2>Ultimate Homeselling Checklist</h2>
                <div class="section-content">The world’s most complete list of how to sell your home quickly and for fair market value.</div>
                <span id="quiz-banner-green">Get Access to the Ultimate Home Checklist</span>
            </div>
        </div>

       </div>
</div>
<div class="resource-display resource-display-index acf-row acf-row-7">
    <div class="wrap">
        <h2>homesellers’ Resources PICKED especially for you.</h2>
        <h3>How to sell fast and find the right agent.</h3>
        <div class="fourcol first">
            <div class="eightcol first">
                <a href="###https://advantageu.com/increase-market-value-of-home/">
                    <img alt="market value of home" src="img/advantageu-home-interior--240x105.jpg">
                </a>
            </div>
            <div class="fourcol last">
                <h4><a href="#https://advantageu.com/increase-market-value-of-home/">8 Improvements that  Increase the Market Value of Your Home</a></h4>
            </div>
            <div class="clearfix"></div>
            <div class="section-content">If you’re preparing to sell your house – maybe your family is expanding and it’s time to start looking for a house with more room! – you need to analyze how you can increase the market value of your home. You first need to create a budget on how much money you can spend on...</div>
        </div>
        <div class="fourcol ">
            <div class="eightcol first">
                <a href="#https://advantageu.com/fall-decorations-show-your-house/">
                    <?= Html::img( Url::to(['@uri_img/33207185_xxl-240x105.jpg'], true ), ['alt' => 'show your house'] ) ?>
                </a>
            </div>
            <div class="fourcol last">
                <h4><a href="#https://advantageu.com/fall-decorations-show-your-house/">How to Choose Tasteful Fall Decorations When You Show your House</a></h4>
            </div>
            <div class="clearfix"></div>
            <div class="section-content">It’s time for you to move – maybe your employer is relocating you to a different office – so your current house is for sale and your listing is in place. Now it’s time to show your house. You want your home to be inviting, but Halloween and Thanksgiving are just around the corner and...</div>
        </div>
        <div class="fourcol last">
            <div class="eightcol first">
                <a href="#https://advantageu.com/avoid-handcuffed-real-estate-listing-agreement/">
                    <?= Html::img( Url::to(['@uri_img/advantageu-contract-shaking-hands--240x105.jpg'], true ), ['alt' => 'listing agreement'] ) ?>
                </a>
            </div>
            <div class="fourcol last">
                <h4><a href="#https://advantageu.com/avoid-handcuffed-real-estate-listing-agreement/">How to Avoid Being Handcuffed By a Real Estate Listing Agreement</a></h4>
            </div>
            <div class="clearfix"></div>
            <div class="section-content">When you’re ready to sell your home, you’ll eventually meet with a real estate agent who’s going to offer you a listing agreement. The listing agreement is a contract that legally binds you to sell with that particular agent for a specified amount of time. Most states offer a standard form that the agent and...</div>
        </div>
        <div class="clearfix"></div>
        <div style="text-align: center; margin-top: 4em;"><a href="#https://advantageu.com/resources/" class="linkButton">More Resources</a></div>
    </div>
</div>
<div id="testimonials">
    <div class="container">
        <div class="row testimonials-row">
            <header>
                <h2 id="testimonials-header">What People Are Saying About AU Certified Agents</h2>
            </header>
            <?= Html::img( Url::to(['@uri_img/1.jpeg'], true ), ['alt' => 'people', 'id' => 'testimonials-main-img'] ) ?>
            <div id="testimonials-main-text">Since we were not in the area we shoud sell our house. After putting on zillow we s <br><span id="testimonials-main-text-signature"> - Ebecca Smithville</span></div>
            <h2 id="testimonials-top-agent">Top Selling Agents in Rochester</h2>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 testimonials-agents">

                <?= Html::a( 
                    Html::img( Url::to(['@uri_img/1.jpeg'], true ), ['alt' => 'people', 'id' => 'testimonials-main-img'] ), 
                    [''], ['class' => 'testimonials-agents-link']
                ) ?>
                <h4 class="testimonials-agents-name">Richard Leasure</h4>
                <span class="testimonials-agents-town">Rochester, NY</span>
                <p class="testimonials-agents-description">Ron ranks in the top 2% of agents in San Francisco in production, and he sells his listings 30 days faster than the average agent.</p>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 testimonials-agents">

                <?= Html::a( 
                    Html::img( Url::to(['@uri_img/1.jpeg'], true ), ['alt' => '', 'class' => 'testimonials-agents-photo'] ), 
                    [''], ['class' => 'testimonials-agents-link']
                ) ?>

                <h4 class="testimonials-agents-name">Richard Leasure</h4>
                <span class="testimonials-agents-town">Rochester, NY</span>
                <p class="testimonials-agents-description">Ron ranks in the top 2% of agents in San Francisco in production, and he sells his listings 30 days faster than the average agent.</p>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 testimonials-agents">

                <?= Html::a( 
                    Html::img( Url::to(['@uri_img/1.jpeg'], true ), ['alt' => '', 'class' => 'testimonials-agents-photo'] ), 
                    [''], ['class' => 'testimonials-agents-link']
                ) ?>

                <h4 class="testimonials-agents-name">Richard Leasure</h4>
                <span class="testimonials-agents-town">Rochester, NY</span>
                <p class="testimonials-agents-description">Ron ranks in the top 2% of agents in San Francisco in production, and he sells his listings 30 days faster than the average agent.</p>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 testimonials-agents">

                <?= Html::a( 
                    Html::img( Url::to(['@uri_img/1.jpeg'], true ), ['alt' => '', 'class' => 'testimonials-agents-photo'] ), 
                    [''], ['class' => 'testimonials-agents-link']
                ) ?>

                <h4 class="testimonials-agents-name">Richard Leasure</h4>
                <span class="testimonials-agents-town">Rochester, NY</span>
                <p class="testimonials-agents-description">Ron ranks in the top 2% of agents in San Francisco in production, and he sells his listings 30 days faster than the average agent.</p>
            </div>
        </div>
    </div>
</div>



