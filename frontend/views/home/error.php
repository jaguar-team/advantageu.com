<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

$this->title = $name;

$this->registerCssFile('@web/css/error.css');

?>
    
<?php if($exception->statusCode >= 300 & $exception->statusCode < 400): ?>
    <?= $this->render('errors/3xx', [
        'status_code'   => $exception->statusCode,
        'name'          => $name,
        'message'       => $message,
    ]);
    ?>
<?php endif; ?>

<?php if($exception->statusCode >= 400 & $exception->statusCode < 500): ?>
    <?= $this->render('errors/4xx', [
        'status_code'   => $exception->statusCode,
        'name'          => $name,
        'message'       => $message,
    ]);
    ?>
<?php endif; ?>

<?php if($exception->statusCode >= 500 & $exception->statusCode < 600): ?>
    <?= $this->render('errors/5xx', [
        'status_code'   => $exception->statusCode,
        'name'          => $name,
        'message'       => $message,
    ]);
    ?>
<?php endif; ?>
