<?php
    use yii\helpers\Html;
    use yii\helpers\Url;
    use yii\bootstrap\ActiveForm;
	use yii\widgets\Pjax;
    use yii\widgets\MaskedInput;
    use \monitorbacklinks\yii2wp\Wordpress;
    use common\models\User;

    $this->title = 'Home';

    $blogPosts = \Yii::$app->blog->getPosts(
        [
        'post_status' => 'publish',
        'number'      => 3
        ],
        [
            'link',
            'post_thumbnail',
            'post_title',
            'post_content'
        ]
    );
    $this->registerCss('.dark-fade{display:none;}')
?>
<div class="page">
    <div class="the-content">
        <div class="page-hero page-hero-index acf-row-0">
            <?= Html::img(Url::to('@uri_img/AdvantageU-Home-header.jpg', true), ['id' => 'hero-image', 'alt' => 'Home']); ?>
            <div class="wrap">
                <div class="search-agent">
	                <?php $form = ActiveForm::begin([
                        'action' => Url::to(\Yii::$app->homeUrl, true),
                        'validateOnBlur' => false,
                        'options' => ['class' => 'area_validation']
                    ]); ?>

						<div class="search-box">
							<h1>Get Matched with Top Agents in your Area</h1>
							<p>We research and prequalify top performing real estate agents</p>
							<div class="search-input-wrap loading area_loading pjax_inline form-group area_field">

					  			<input id="main_search" class="profile_areas" placeholder="Enter the city, address or zip where you're buying or selling" tabindex="1" data-stringify="true" data-string_input="<?= Html::getInputId($search, 'area') ?>">

                                <div class="hidden">
                                    <?= @$form->field($search, 'area')->hiddenInput()->label(false); ?>
                                </div>

                                <div class="loader">
                                    <div class="loader-container">
                                        <i class="fa fa-spinner fa-pulse fa-fw"></i>
                                        <span class="sr-only">Loading...</span>
                                    </div>
                                </div>

							</div>

							<?= Html::submitButton(Html::decode('<span class="search_btn_text">GET MATCHED</span>'), ['id' => 'search-go', 'tabindex' => 2]) ?>
						</div>

					<?php ActiveForm::end(); ?>
					<div class="clearfix"></div>
				</div>
            </div>
        </div>
        <a class="anchor" id="quiz-banner"></a>
        <div class="quiz-banner-display bot-bord acf-row acf-row-1">
            <div class="wrap">
                <h3>Get the Home Sellers’ Advantage</h3>
                <h2>Ultimate Home Selling Checklist </h2>
                <div class="section-content">The world’s most complete list of how to sell your home quickly and for fair market value.</div>
                <?= Html::a('Get Access to the Ultimate Home Selling Checklist ', Url::toRoute(['ultimate-checklist/index'], true), ['id' => 'quiz-banner-green']) ?>
            </div>
        </div>

    </div>
</div>

<div class="resource-display resource-display-index acf-row acf-row-7">
    <div class="wrap">
        <h2>Home Sellers’ Resources PICKED especially for you.</h2>
        <h3>How to sell fast and find the right agent.</h3>
        <div class="feed">
            <?php foreach ($blogPosts as $key => $post) : ?>
                <div class="block">
                    <a href="<?= $post['link'] ?>">
                        <div class="img">
                            <?=(!empty($post['post_thumbnail'])) ? Html::img($post['post_thumbnail']['link']) : Html::img(Url::to(['@uri_img/no-thumbnail.jpg'], true)) ?>
                        </div>
                        <div class="title">
                            <?= Html::encode($post['post_title']) ?>
                        </div>
                        <div class="clearfix"></div>
                        <div class="content">
                            <?= Html::encode( mb_strimwidth($post['post_title'], 0, 300, '...') ) ?>
                        </div>
                    </a>
                </div>
            <?php endforeach; ?>

        </div>
        <div style="text-align: center; margin-top: 4em;">
            <?= Html::a('More Resources', Url::toRoute(['resources/index'], true), ['class' => 'linkButton']) ?>
        </div>
    </div>
</div>
<div id="testimonials">
    <div class="container">
        <div class="row testimonials-row">
            <header>
                <h2 id="testimonials-header">Agent’s who’ve been certified by AdvantageU, get results for their clients!</h2>
            </header>
            <div id="testimonials-main-text"><i>"Advantage U called after I was selling my home FSBO for two weeks and had low offers. After becoming swamped with handling it all on my own I decided to let them set me up with Suzanne who helped me sell the home in two weeks. I was able to net what I wanted and would recommend both Suzanne and Advantage U."</i><br><span id="testimonials-main-text-signature"> - Ebecca Smithville</span></div>
            <a class="linkButton" href="https://advantageu.com/testimonials/index" style="display:block; margin: 0 auto 40px; white-space: nowrap;overflow: hidden; text-overflow: ellipsis; padding: 0 15px; width: 95%; max-width: 400px; text-align: center;"> Read More Testimonials</a>
            <h2 id="testimonials-top-agent">Top Agents</h2>
            <?php foreach (User::find()->agent()->top(4)->all() as $key => $agent) :
                $best_location = $agent->getTransactionRelation()->bestLocation();
                ?>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 testimonials-agents">

                    <?= Html::a(
                        Html::img( $agent->profile->getPhotoUrl(), ['alt' => 'people', 'class' => 'testimonials-main-img'] ),
                        Url::toRoute(['agent/profile', 'id' => $agent->id], true), ['class' => 'testimonials-agents-link']
                    ) ?>
                    <h4 class="testimonials-agents-name"><?= Html::encode($agent->profile->getFullName(true)) ?></h4>
                    <?php if($best_location['count']) : ?>
                        <div style="padding-left: 140px;">
                            <span class="testimonials-agents-town"><?= $best_location['best_location'] ?></span>
                        </div>
                    <?php endif ?>
                    <p class="testimonials-agents-description"><?= Html::encode($agent->profile->first_name) ?> is an AdvantageU Certified Agent</p>
                </div>
            <?php endforeach ?>
        </div>
    </div>
</div>
