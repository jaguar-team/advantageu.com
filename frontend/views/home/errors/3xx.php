<?php

use yii\helpers\Html;
use yii\helpers\Url;

?>

<div class="error flexbox">
	<div class="left_part">
		<div class="error_code">
			<?= $status_code ?>
		</div>
		<div class="notification">
			<div class="title">Oops!</div>
			<div><?= $name ?></div>
			<div class="message">
				<?= $message ?>
			</div>
		</div>
		<?= Html::a('Go back home', Url::to(\Yii::$app->homeUrl, true), ['class' => 'btn']) ?>
	</div>

	<div class="right_part">
		<?= Html::img(Url::to(['@uri_img/building.png'], true)) ?>
	</div>	
</div>