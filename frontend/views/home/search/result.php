<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 028 28.04.17
 * Time: 15:38
 */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\Pjax;
use frontend\widgets\SearchResult;
use rmrevin\yii\fontawesome\FA;

$property_labels = $agent_search->_customer_bid->propertyTypeLabels();

$itemClass = "agent-card ".($agent_search_step->getSearchParam('lead_type') == 2 ? 'seller' : '');

?>

<div class="answers-content">

	<?php Pjax::begin([
		'options' => [
			'class' => 'interstitial-complete'
		],
		'clientOptions' => [
			'scrollTo' 	=> 0,
		]
		
	]) ?>

		<?= SearchResult::widget([
			'dataProvider' => $agent_search_data_provider,
			'itemClass' => /*$itemClass*/'agent-card seller',
			'resultMatches' => $result_matches,
			'pager' => [
                'options' => [
                    'class' => 'pagination pagination-sm no-margin',
                    'style' => 'float: right',
                ],
                'nextPageLabel' => FA::icon('angle-right'),
                'prevPageLabel' => FA::icon('angle-left'),
            ],
		]) ?>

	<?php Pjax::end() ?>
	
	<div class="clearfix"></div>

	<?= $this->render('part/_right_sidebar') ?>
</div>
