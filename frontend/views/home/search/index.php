<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\bootstrap\ActiveForm;
use common\models\UserRole;

//$this->title('Agent search');
//$lead_type_labels = $model->_customer_bid->leadTypeLabels();
$lead_type_labels = UserRole::find()->buyer()->seller(true)->indexBy('id')->all();

$period_labels = $model->_customer_bid->periodLabels();
$property_labels = $model->_customer_bid->propertyTypeLabels();
$price_labels = $model->_customer_bid->priceLabels();
$other_action = $model->_customer_bid->otherActionLabels();
$matches = $model->search()->getModels();

function getStepClass($current_step, $step){
	if($current_step == $step)
		return "current";
	elseif($current_step > $step)
		return "completed";
	return "";
}

?>

<div class="figures-wrap flexbox">

	<?php 
	$steps_length = count(\Yii::$app->agentSearchStep->getSteps());

	foreach (array_reverse(\Yii::$app->agentSearchStep->getSteps()) as $step) : ?>
		<div class="figure <?= getStepClass($search_data['step'], $step['step']) ?>" data-step="<?= $step['step'] ?>" style="width: calc(100% / <?= $steps_length ?>);">			
			<a href="#" onclick="setStep(event, <?= $step['step'] ?>)">
				
			</a>
			<span><?= $step['label'] ?></span>
		</div>
	<?php endforeach ?>

	<div class="progress-indicator" style="left: calc(100% / <?= $steps_length * 2 ?>); right: calc(100% / <?= $steps_length * 2 ?>);">
		<span style="width: calc(100% / <?= $steps_length - 1 ?> * <?= $search_data['step'] - 1 ?>)" data-step_num="<?= $steps_length - 1 ?>"></span>
	</div>			
						
</div>



<?php
 	Pjax::begin([
		'options' => ['id' => 'answers', 'data-related_container' => 'updateResult', 'class' => 'answers-content'],
		'timeout' => 2000,
		'formSelector' => 'form:not('.( \Yii::$app->user->isGuest ? '#customer_info' : '#other_action' ).')'
	]); 
	$this->registerJs('_current_step = '.$search_data['step'].';', yii\web\View::POS_HEAD);
?>
	<div class="question">
		<main class="col-md-12 animated rent-question-active">
	    	<?= $this->render('part/'.$part, [
	    		'model' 			=> $model,
	    		'lead_type_labels' 	=> $lead_type_labels,
	    		'period_labels'		=> $period_labels,
	    		'property_labels'	=> $property_labels,
	    		'price_labels'		=> $price_labels,
	    		'other_action'		=> $other_action,
	    		'search_data'		=> $search_data
	    	]); ?>
	    </main>
	    
		<div class="clearfix"></div>
	</div>	
	<?= 
		$this->render('part/_right_sidebar', [
			'matches' => $matches
		]); 
	?>
<?php Pjax::end() ?>