<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 028 28.04.17
 * Time: 15:38
 */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\widgets\ActiveForm;
use frontend\widgets\SearchResult;
use rmrevin\yii\fontawesome\FA;
use common\models\UserRole;

$location = \Yii::$app->agentSearch->getSearchParam('location');

$lead_type_labels = UserRole::find()->buyer()->seller(true)->indexBy('id')->all();
$period_labels = $agent_search->_customer_bid->periodLabels();
$property_labels = $agent_search->_customer_bid->propertyTypeLabels();
$price_labels = $agent_search->_customer_bid->priceLabels();
$other_action = $agent_search->_customer_bid->otherActionLabels();

?>
<div class="question" id="main-window">
		<?= $this->render('part/_search_navigation', [
			'property_labels' => $property_labels,
		]) ?>

		<?php Pjax::begin([
			'options' => [
				//'id' => 'content',
				'class' => 'interstitial-complete'
			]
			
		]) ?>

			<header>
			    <div class="close-btn"></div>
			    <div class="filter-wrap">
			        <h2>Search</h2>
			        <div class="sub-heading">
			            <ul>
			                <li><i class="hi-circle"></i><span>Update your search criteria to refresh your matches.</span></li>
			            </ul>
			        </div>
			    </div>
			</header>

			<?php $form = ActiveForm::begin() ?>

				<div class="input-wrap">
					<label>Looking to</label>
					<ul>
						<?php foreach ($lead_type_labels as $key => $value) : ?>
							<a onclick="setParam(event, 'lead_type', '<?= $key ?>')" class="outline-btn <?= Html::encode(\Yii::$app->agentSearch->getSearchParam('lead_type') == $key ? 'selected' : '') ?>" >
								<?php 
									if ($value->is_seller) {
										echo "Sell";
									} elseif ($value->is_buyer) {
										echo "Buy";
									}
								?>
							</a>
						<?php endforeach; ?>
					</ul>
				</div>
				<div class="input-wrap">
					<label>
						<?php
							if ($lead_type_labels[\Yii::$app->agentSearch->getSearchParam('lead_type')]->is_buyer) {
								echo "Where are you looking to buy?";
							} elseif ($lead_type_labels[\Yii::$app->agentSearch->getSearchParam('lead_type')]->is_seller) {
								echo "Property Address";
							}
						?>
					</label>
					<input type="text" id="search" data-type="address" value="<?= Html::encode( ( isset(\Yii::$app->agentSearch->getSearchParam('location')['route'])  ? \Yii::$app->agentSearch->getSearchParam('location')['route'].', ' : '').\Yii::$app->agentSearch->getSearchParam('location')['city'].', '.\Yii::$app->agentSearch->getSearchParam('location')['short_name'] ) ?>">
				</div>

				<div class="input-wrap">
					<label>Property Type</label>
					<ul>
						<?php foreach ($property_labels as $val => $label) : ?>
							<a onclick="setParam(event, 'property_type', '<?= $val ?>')" class="outline-btn <?= Html::encode(\Yii::$app->agentSearch->getSearchParam('property_type') == $val ? 'selected' : '') ?>" ><?= Html::encode( $label['name'] ) ?></a>
						<?php endforeach; ?>
					</ul>
				</div>

				<div class="input-wrap">
					<label>Price Range</label>
					<ul>
						<?php foreach ($price_labels as $val => $label) : ?>
							<a onclick="setParam(event, 'price', '<?= $val ?>')" class="outline-btn <?= Html::encode(\Yii::$app->agentSearch->getSearchParam('price') == $val ? 'selected' : '') ?>" ><?= Html::encode( $label['name'] ) ?></a>
						<?php endforeach; ?>
					</ul>
				</div>

				<div class="input-wrap">
					<label>How soon are you looking to <action><?= Yii::$app->agentSearch->getSearchParam('lead_type') == 1 ? 'buy?' : 'sell' ?></action>?</label>
					<ul>
						<?php foreach ($period_labels as $val => $label) : ?>
							<a onclick="setParam(event, 'period', '<?= $val ?>')" class="outline-btn <?= Html::encode(\Yii::$app->agentSearch->getSearchParam('period') == $val ? 'selected' : '') ?>" ><?= Html::encode( $label['name'] ) ?></a>
						<?php endforeach; ?>
					</ul>
				</div>

				<?php $agent_search->location = json_encode($agent_search->location); ?>

				<?= $form->field($agent_search, 'lead_type')->hiddenInput(['id' => 'lead_type'])->label(false) ?>

				<?= $form->field($agent_search, 'property_type')->hiddenInput(['id' => 'property_type'])->label(false) ?>

				<?= $form->field($agent_search, 'price')->hiddenInput(['id' => 'price'])->label(false) ?>

				<?= $form->field($agent_search, 'period')->hiddenInput(['id' => 'period'])->label(false) ?>

				<?= $form->field($agent_search, 'other_action')->hiddenInput(['id' => 'other_action', 'value' => Yii::$app->agentSearch->getSearchParam('other_action')])->label(false) ?>

				<?= $form->field($agent_search, 'location', ['template' => '{input}'])->hiddenInput(['id' => 'area_input'])->label(false) ?>

				<div class="section-cta">
					<?= Html::submitButton('Refresh Matches', ['class' => 'btn blue-btn']) ?>
				</div>
			<?php ActiveForm::end() ?>
		<?php Pjax::end() ?>
		<div class="clearfix"></div>
</div>