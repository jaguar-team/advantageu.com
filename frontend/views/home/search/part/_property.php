<?php
/**
 * Created by PhpStorm.
 * User: Vlad
 * Date: 25.04.2017
 * Time: 17:42
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
?>

<div class="question-heading">
	What kind of home are you looking <?= Html::encode( ($search_data['lead_type'] == 2) ? 'to sell' : 'for'); ?>?
</div>
<div class="answers flexbox type">
	<?php foreach ($property_labels as $val => $label) : ?>
		<div class="answer">
			<a href="#" onclick="searchAnswer(event, '<?= Html::getInputId($model, 'property_type') ?>', '<?= $val ?>')">

				<div class="content">
					<?= Html::img(Url::to(['@uri_img/search/'.$label['favicon'].'.png'], true)) ?>
				</div>
				<span><?= Html::encode($label['name']) ?></span>
				<i class="hi hi-check trans3 checky"></i>
			</a>
		</div>
	<?php endforeach; ?>
</div>
	

<?php $form = ActiveForm::begin([
	'options' => [ 'data-pjax' => 'true', 'class' => 'hidden' ],
	'validateOnBlur' => false,
]); ?>

	<?= $form->field($model, 'property_type')->hiddenInput()->label(false) ?>

<?php ActiveForm::end(); ?>
