<?php
/**
 * Created by PhpStorm.
 * User: Vlad
 * Date: 25.04.2017
 * Time: 17:44
 */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

?>

<div class="question-heading">
	Are you also looking to <?= Html::encode( ($search_data == 2) ? 'sell' : 'buy' ) ?> a property?
</div>
<div class="answers buttons flexbox">
	<?php foreach ($other_action as $val => $label) : ?>

		<div class="answer">
			<a href="#" onclick="searchAnswer(event, '<?= Html::getInputId($model, 'other_action') ?>', '<?= $val ?>')">
				<div class="content">
					<span><?= Html::encode( $label['name'] ) ?></span>
				</div>
				<i class="hi hi-check trans3 checky"></i>			
			</a>
		</div>

	<?php endforeach; ?>
</div>	

<?php $form = ActiveForm::begin([
	'options' => [ 'data-pjax' => ( \Yii::$app->user->isGuest ? 'true' : 'false' ) , 'class' => 'hidden', 'id' => 'other_action'],
	'validateOnBlur' => false,
]); ?>

	<?= $form->field($model, 'other_action')->hiddenInput()->label(false) ?>

<?php ActiveForm::end(); ?>
