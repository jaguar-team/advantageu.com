<?php 

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
use yii\widgets\ActiveForm;
use rmrevin\yii\fontawesome\FA;

$agentSearch = \Yii::$app->agentSearch;
$agentSearchStep = \Yii::$app->agentSearchStep;

$lead_type_labels = common\models\UserRole::find()->buyer()->seller(true)->indexBy('id')->select(['name'])->column();
//$lead_type_labels = $agentSearch->_customer_bid->leadTypeLabels();
$property_labels = [];
foreach ($agentSearch->_customer_bid->propertyTypeLabels() as $key => $label) {
      $property_labels[$key] = $label['name'];
}
$price_labels = [];
foreach ($agentSearch->_customer_bid->priceLabels() as $key => $label) {
      $price_labels[$key] = $label['name'];
}
$period_labels = [];
foreach ($agentSearch->_customer_bid->periodLabels() as $key => $label) {
      $period_labels[$key] = $label['name'];
}
$other_action = [];
foreach ($agentSearch->_customer_bid->otherActionLabels() as $key => $label) {
      $other_action[$key] = $label['name'];
}
$chosen_location = json_encode($agentSearchStep->getSearchParam('location'));

?>
<div class="left-bar">
      <div class="head">
            <?php if(!$agentSearchStep->isLastStep(true)) : ?>
                  <h2 class="lb-head">You are doing great! Just a few more steps and you will see your matches.</h2>
            <?php else : ?>
                  <h2 class="lb-head">Looks like you have ...</h2>
                  <span class="match_count"><span class="match-count"><?= (!\Yii::$app->user->isGuest) ? count(\Yii::$app->user->identity->favorites) : 0 ?></span> Matches</span>
            <?php endif; ?>
            <?= Html::img(Url::to(['@uri_img/advocate.png'], true), ['alt' => 'call-men', 'class' => 'call-men-img']) ?>
            
            <span class="advocate">AU&#160;Advocate</span>
            <button class="call-btn">Call Now</button>
            <div class="clearfix"></div>
      </div>
      <div class="update_result">
            <h2 class="lb-search-head">search</h2>

            <div class="search_results">

                  <?php $form = ActiveForm::begin([
                        'action' => Url::toRoute(['agent-search/result-update'], true),
                        'options' => [
                              'data-pjax' => 'true',
                              'validateOnBlur' => false,
                              'class' => 'area_validation'
                        ]
                  ]) ?>
                        <?php Pjax::begin([
                              'options' => ['id' => 'updateResult'],
                              'timeout' => 2000 
                        ]) ?>
                              <div class="result_block editable">
                                    <p class="element">
                                          <?php 
                                                if( $agentSearchStep->getSearchParam('location') ) 
                                                      echo Html::encode( (isset($agentSearchStep->getSearchParam('location')['route'])  ? $agentSearchStep->getSearchParam('location')['route'].', ' : '').@$agentSearchStep->getSearchParam('location')['city'].', '.@$agentSearchStep->getSearchParam('location')['short_name'] );
                                                else 
                                                      echo 'Nothing chosen yet';
                                          ?>
                                                
                                    </p>
                                    <?php if( $agentSearchStep->getSearchParam('location') ) : ?>
                                          <div class="editor row">
                                                <div class="edit_field">
                                                      <div class="form-group loading area_loading pjax_inline area_field">

                                                            <input id="search" type="text" class="profile_areas form-control" data-stringify="true" data-string_input="<?= Html::getInputId($agentSearch, 'location') ?>"
                                                                  value="<?= Html::encode( ( isset($agentSearchStep->getSearchParam('location')['route'])  ? $agentSearchStep->getSearchParam('location')['route'].', ' : '').( isset($agentSearchStep->getSearchParam('location')['city'])  ? $agentSearchStep->getSearchParam('location')['city'].', ' : '').( isset($agentSearchStep->getSearchParam('location')['short_name'])  ? $agentSearchStep->getSearchParam('location')['short_name'] : '') ) ?>">

                                                            <div class="loader">
                                                                  <div class="loader-container">
                                                                        <i class="fa fa-spinner fa-pulse fa-fw"></i>
                                                                        <span class="sr-only">Loading...</span>
                                                                  </div>
                                                            </div>

                                                            <div class="hidden">
                                                                  <?= $form->field($agentSearch, 'location', ['template' => '{input}'])->hiddenInput(['value' => $chosen_location])->label(false) ?>
                                                            </div>
                                                      </div>                     
                                                </div>
                                                
                                                <div class="edit_controls">
                                                      <?= Html::a( '×', 'javascript:void(0)', ['do' => 'cancel_editable']) ?>
                                                </div>
                                          </div>
                                          <div class="actions">
                                                <?= Html::a( FA::icon('pencil-square-o'), 'javascript:void(0)', 
                                                      [
                                                            'onclick' => 'editField(event, "search")',
                                                            'class' => 'edit_trigger'
                                                      ]
                                                ) ?>
                                          </div>
                                    <?php endif ?>
                              </div>
                  
                              <div class="result_block editable">                                                  
                                    <p class="element">
                                          <?php 
                                                if( $agentSearchStep->getSearchParam('lead_type') ) 
                                                      echo Html::encode($lead_type_labels[$agentSearchStep->getSearchParam('lead_type')]);
                                                else 
                                                      echo 'Nothing chosen yet';
                                          ?>
                                                
                                    </p>
                                    <?php if( $agentSearchStep->getSearchParam('lead_type') ) : ?>
                                          <div class="editor row">
                                                <div class="edit_field">
                                                      <?= $form->field($agentSearch, 'lead_type', ['template' => '{input}'])->dropDownList($lead_type_labels, ['class' => 'select2', 'value' => $agentSearchStep->getSearchParam('lead_type')])->label(false) ?>                      
                                                </div>
                                                <div class="edit_controls">
                                                      <?= Html::a( '×', 'javascript:void(0)', ['do' => 'cancel_editable']) ?>
                                                </div>                                                
                                          </div>
                                          <div class="actions">
                                                <?= Html::a( FA::icon('pencil-square-o'), 'javascript:void(0)', 
                                                      [
                                                            'onclick' => 'editField(event, "'.Html::getInputId($agentSearch, 'lead_type').'")',
                                                            'class' => 'edit_trigger'
                                                      ]
                                                ) ?>
                                          </div>
                                    <?php endif ?>
                              </div>

                              <div class="result_block editable">                        
                                    <p class="element">
                                          <?php 
                                                if( $agentSearchStep->getSearchParam('property_type') ) 
                                                      echo Html::encode($property_labels[$agentSearchStep->getSearchParam('property_type')]);
                                                else 
                                                      echo 'Nothing chosen yet';
                                          ?>
                                                
                                    </p>
                                    <?php if( $agentSearchStep->getSearchParam('property_type') ) : ?>
                                          <div class="editor row">
                                                <div class="edit_field">
                                                      <?= $form->field($agentSearch, 'property_type', ['template' => '{input}'])->dropDownList($property_labels, ['class' => 'select2', 'value' => $agentSearchStep->getSearchParam('property_type')])->label(false) ?>                      
                                                </div>
                                                <div class="edit_controls">
                                                      <?= Html::a( '×', 'javascript:void(0)', ['do' => 'cancel_editable']) ?>
                                                </div>
                                          </div>
                                          <div class="actions">
                                                <?= Html::a( FA::icon('pencil-square-o'), 'javascript:void(0)', 
                                                      [
                                                            'onclick' => 'editField(event, "'.Html::getInputId($agentSearch, 'property_type').'")',
                                                            'class' => 'edit_trigger'
                                                      ]
                                                ) ?>
                                          </div>
                                    <?php endif ?>
                              </div>

                              <div class="result_block editable">
                                    <p class="element">
                                          <?php 
                                                if( $agentSearchStep->getSearchParam('price') ) 
                                                      echo Html::encode($price_labels[$agentSearchStep->getSearchParam('price')]);
                                                else 
                                                      echo 'Nothing chosen yet';
                                          ?>
                                                
                                    </p>
                                    <?php if( $agentSearchStep->getSearchParam('price') ) : ?>
                                          <div class="editor row">
                                                <div class="edit_field">
                                                      <?= $form->field($agentSearch, 'price', ['template' => '{input}'])->dropDownList($price_labels, ['class' => 'select2', 'value' => $agentSearchStep->getSearchParam('price')])->label(false) ?>                      
                                                </div>
                                                <div class="edit_controls">
                                                      <?= Html::a( '×', 'javascript:void(0)', ['do' => 'cancel_editable']) ?>
                                                </div>
                                          </div>
                                          <div class="actions">
                                                <?= Html::a( FA::icon('pencil-square-o'), 'javascript:void(0)', 
                                                      [
                                                            'onclick' => 'editField(event, "'.Html::getInputId($agentSearch, 'price').'")',
                                                            'class' => 'edit_trigger'
                                                      ]
                                                ) ?>
                                          </div>
                                    <?php endif ?>
                              </div>
                  
                              <div class="result_block editable">
                                    <p class="element">
                                          <?php 
                                                if( $agentSearchStep->getSearchParam('period') ) 
                                                      echo Html::encode($period_labels[$agentSearchStep->getSearchParam('period')]);
                                                else 
                                                      echo 'Nothing chosen yet';
                                          ?>
                                                
                                    </p>
                                    <?php if( $agentSearchStep->getSearchParam('period') ) : ?>
                                          <div class="editor row">
                                                <div class="edit_field">
                                                      <?= $form->field($agentSearch, 'period', ['template' => '{input}'])->dropDownList($period_labels, ['class' => 'select2', 'value' => $agentSearchStep->getSearchParam('period')])->label(false) ?>                      
                                                </div>
                                                <div class="edit_controls">
                                                      <?= Html::a( '×', 'javascript:void(0)', ['do' => 'cancel_editable']) ?>
                                                </div>
                                          </div>
                                          <div class="actions">
                                                <?= Html::a( FA::icon('pencil-square-o'), 'javascript:void(0)', 
                                                      [
                                                            'onclick' => 'editField(event, "'.Html::getInputId($agentSearch, 'period').'")',
                                                            'class' => 'edit_trigger'
                                                      ]
                                                ) ?>
                                          </div>
                                    <?php endif ?>
                              </div>
                 
                              <div class="result_block editable">                        
                                    <p class="element">
                                          <?php 
                                                if( $agentSearchStep->getSearchParam('other_action') !=null ) 
                                                      echo Html::encode($other_action[$agentSearchStep->getSearchParam('other_action')]);
                                                else 
                                                      echo 'Nothing chosen yet';
                                          ?>
                                                
                                    </p>
                                    <?php if( $agentSearchStep->getSearchParam('other_action') !=null ) : ?>
                                          <div class="editor row">
                                                <div class="edit_field">
                                                      <?= $form->field($agentSearch, 'other_action', ['template' => '{input}'])->dropDownList($other_action, ['class' => 'select2', 'value' => $agentSearchStep->getSearchParam('other_action')])->label(false) ?>                      
                                                </div>
                                                <div class="edit_controls">
                                                      <?= Html::a( '×', 'javascript:void(0)', ['do' => 'cancel_editable']) ?>
                                                </div>
                                          </div>
                                          <div class="actions">
                                                <?= Html::a( FA::icon('pencil-square-o'), 'javascript:void(0)', 
                                                      [
                                                            'onclick' => 'editField(event, "'.Html::getInputId($agentSearch, 'other_action').'")',
                                                            'class' => 'edit_trigger'
                                                      ]
                                                ) ?>
                                          </div>
                                    <?php endif ?>                                                   
                              </div>
                        <?php Pjax::end() ?>

                        <div class="actions">
                              <?= Html::submitButton('New Search', ['class' => 'btn']) ?>
                              <a href="#" class="modify">modify</a>
                        </div>

                  <?php ActiveForm::end() ?>

            </div>
      </div>
</div>