<?php 

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

?>
<div class="right-bar">
	<?php if (isset($matches)) : ?>

		<div class="matches">
			<?php if(!empty($matches)) : ?>
				<h3>Potential Matches</h3>				
				<ul>
					<?php 
						$i=0; 
						foreach ($matches as $key => $agent) : 
							if($i > 20) break(1);
					?>
						<li class="<?= ($agent->certificate ? 'certificated' : '') ?>">
							<a href="<?= Url::toRoute(['agent/profile', 'id' => $agent->id]) ?>">
								<div class="img">
									<?= Html::img($agent->profile->getPhotoUrl(), []); ?>
								</div>
								<div class="name">
									<?= $agent->profile->getFullName() ?>
								</div>
							</a>
						</li>
					<?php
						$i++;
					endforeach 
					?>				
				</ul>
			<?php else : ?>
				<h3>No Potential Matches</h3>
			<?php endif ?>
		</div>
	<?php else : ?>
		<?= Html::a('View Your Account', Url::toRoute(['agent/profile', 'id' => (isset(\Yii::$app->user->identity) ? \Yii::$app->user->identity->id : '') ]), []) ?>
	<?php endif; ?>
	<!--<div class="certificate"></div>-->
</div>
