<?php
/**
 * Created by PhpStorm.
 * User: Vlad
 * Date: 25.04.2017
 * Time: 17:43
 */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
?>

<div class="question-heading">
	<h2 class="question-ask">How soon are you looking to <?= Html::encode( ($search_data['lead_type'] == 2) ? 'sell' : 'buy' ) ?>?</h2>
</div>
<div class="answers buttons flexbox">
	<?php foreach ($period_labels as $val => $label) : ?>
		<div class="answer">
			<a href="#" onclick="searchAnswer(event, '<?= Html::getInputId($model, 'period') ?>', '<?= $val ?>')">
				<div class="content">
					<span><?= Html::encode($label['name']) ?></span>
				</div>
				<i class="hi hi-check trans3 checky"></i>
			</a>
		</div>
	<?php endforeach; ?>
</div>


<?php $form = ActiveForm::begin([
	'options' => [ 'data-pjax' => 'true', 'class' => 'hidden' ],
	'validateOnBlur' => false,
]); ?>

	<?= $form->field($model, 'period')->hiddenInput()->label(false) ?>

<?php ActiveForm::end(); ?>
