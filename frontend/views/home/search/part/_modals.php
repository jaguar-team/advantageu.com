<?php 
	use yii\helpers\Html;
	use yii\helpers\Url;
	use rmrevin\yii\fontawesome\FA;
	use yii\bootstrap\ActiveForm;
	use yii\widgets\Pjax;
	use yii\widgets\MaskedInput;

	function showModal($id, &$view_obj)
	{
		if ( isset(\Yii::$app->request->queryParams['modal_after_pjax']) && \Yii::$app->request->queryParams['modal_after_pjax'] == $id ){
			$view_obj->registerJs('$("body").css("padding-right", "0px");
	    		$("body").removeClass("modal-open");
	    		$(".modal-backdrop").remove();
	    		$("#'.$id.'").modal("show")');
			return true;
		} 
		else return false;
	}
	
?>

<div class="modal fade reviews" id="contact_agent_result" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-head">
				<img src="">
				<h3></h3>
			</div>
			<div class="modal-body">
				<div class="left-content">
					<p>HomeLight will connect you with Keith and, upon your request, up to two additional top agents that meet your criteria. We use historical and public real estate transaction data to locate the premier real estate agents in your area.</p>
					<div class="accordion" id="accordion" role="tablist" aria-multiselectable="true">
					  	<div class="block">
						    <div class="heading" role="tab" id="headingOne">
						      	<h4>
							        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
							          	Only the Best Real Estate Agents
							          	<div class="icon">
								          	<?= FA::icon('chevron-down') ?>
								          	<?= FA::icon('chevron-up') ?>
								        </div>
							        </a>
						      	</h4>
						    </div>
						    <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
						      	<p>
						        	By evaluating millions of home records, our team is able to identify the top 5% of all agents based on sales volume, negotiation ability, and closing rates.
						      	</p>
						    </div>
					  	</div>

					  	<div class="block">
						    <div class="heading" role="tab" id="headingTwo">
						      	<h4>
							        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseOne">
							          	100% Free
							          	<div class="icon">
								          	<?= FA::icon('chevron-down') ?>
								          	<?= FA::icon('chevron-up') ?>
								        </div>
							        </a>
						      	</h4>
						    </div>
						    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
						      	<p>
						        	Our real estate agent matching service is completely free with no obligation for you. We are not paid to advertise for real estate agents,we will only recommend a real estate agent if they are proven objectively to perform better than the average real estate agent in your area.
						      	</p>
						    </div>
					  	</div>

					  	<div class="block">
						    <div class="heading" role="tab" id="headingThree">
						      	<h4>
							        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseOne">
							          	Why HomeLight?
							          	<div class="icon">
								          	<?= FA::icon('chevron-down') ?>
								          	<?= FA::icon('chevron-up') ?>
								        </div>
							        </a>
						      	</h4>
						    </div>
						    <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
						      	<p>
						        	HomeLight is the only real estate agent comparison service that uses a complete and objective evaluation of the real estate agent's historical sales performance, negotiation ability, and closing rates. Using the right real estate agent, especially one with a proven track record of success like the agents recommended by our team of data scientists, can save you thousands of dollars!
						      	</p>
						    </div>
					  	</div>
					  
					</div>
					<div class="security">
						<p>We take user privacy seriously. Your data is 100% secure with us.</p>
						<?= Html::img('@uri_img/secure-icon.png') ?>
						<div class="no-spam-wrap">
							<i class="hi hi-ban"></i>
							<span>NO SPAM</span>
						</div>
					</div>
					<p class="immediate-service">For Immediate Service between 9am-5pm PST:</p>
					<a href="tel:(855) 999-7959">Call us at (855) 999-7959</a>

				</div>
				<div class="right-content">
					<?php $form =  ActiveForm::begin([
						/*'action' => Url::toRoute([
							'agent/profile', 'id' => $user_model->id,
							'transaction-page' => ( isset(Yii::$app->request->queryParams['transaction-page']) ) ? Yii::$app->request->queryParams['transaction-page'] : '',
							'testimonial-page' => ( isset(Yii::$app->request->queryParams['testimonial-page']) ) ? Yii::$app->request->queryParams['testimonial-page'] : '',
							'review-page' => ( isset(Yii::$app->request->queryParams['review-page']) ) ? Yii::$app->request->queryParams['review-page'] : ''
						], true),*/
						'fieldConfig' => [
							'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",							
						],
						'options' => [
							'data-pjax' => 'true',
							'class' => 'col-md-12 form-horizontal'
						]						
					]) ?>		
						<?= $form->field($agent_contact_model, 'full_name')->textInput(['placeholder' => 'Jane Smith'])->label(false) ?>

						<?= $form->field($agent_contact_model, 'email')->widget(MaskedInput::className(), [
						    'clientOptions' => [
						    	'alias' => 'email',
						    ],
						    'options' => [
						    	'placeholder' => 'jsmith@gmail.com',
						    	'class' => 'form-control'
						    ]
						])->label(false) ?>

						<?= $form->field($agent_contact_model, 'phone')->widget(MaskedInput::className(), [
						 	'mask' => '(999)999-9999',
						 	'options' => [
						    	'placeholder' => '(888)867-5309',
						    	'class' => 'form-control'
						    ]
						])->label(false) ?>

						<!--<select class="required dropdown-menu" name="reason[dropdown]" id="reason_dropdown">
							<option value="">Choose a reason for contacting Keith</option>
							<option value="interested_in_agents_listing">I have a question about one of Keith's listings.</option>
							<option value="interested_in_agent_representing_me">I'm a buyer and want to talk to Keith about representing me.</option>
							<option value="interested_in_selling">I'd like to talk to Keith about selling my home.</option>
							<option value="interested_in_contacting">I'm trying to get in touch with Keith.</option>
							<option value="none">None of the above.</option>
						</select>-->



						<?= $form->field($agent_contact_model, 'message')->textarea(['rows' => 5])->label(false) ?>

						<?= Html::submitButton('Send', ['class' => 'main-button orange small']) ?>
						<button class="main-button small" data-dismiss="modal">Cancel</button>

					<?php ActiveForm::end() ?>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
</div>