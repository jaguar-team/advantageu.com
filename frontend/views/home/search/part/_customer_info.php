<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 027 27.04.17
 * Time: 16:19
 */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\widgets\MaskedInput;

?>

<div class="question-heading">
	<span>Get Matched Analysis ALMOST complete!</span><br>
	<div class="text">You will see a personalized list of AdvantageU Certified<br> Agents in your market that you contact.</div>
</div>
<div class="answers light">
	<?php $form = ActiveForm::begin(['options' => ['id' => 'customer_info','data-pjax' => 'false']]) ?>

		<?= $form->field($model, 'email')->widget(MaskedInput::className(), [
		    'clientOptions' => [
		    	'alias' => 'email'
		    ],
		    'options' => [
		    	'placeholder' => 'Email Address'
		    ]
		])->label(false) ?>

			<?= $form->field($model, 'phone')->widget(MaskedInput::className(), [
				 'mask' => '(999)999-9999',
				 'options' => [
			    	'placeholder' => 'Phone'
			    ]
			])->label(false) ?>

			<?= $form->field($model, 'first_name')->textInput(['placeholder' => 'First Name'])->label(false) ?>

			<?= $form->field($model, 'last_name')->textInput(['placeholder' => 'Last Name'])->label(false) ?>

			<div class="form-group">
				<?= Html::submitButton('Generate Matches', ['class' => 'btn', 'data-pjax' => 'false']) ?>
			</div>

	<?php ActiveForm::end(); ?>
</div>	
