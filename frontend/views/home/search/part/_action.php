<?php
/**
 * Created by PhpStorm.
 * User: Vlad
 * Date: 25.04.2017
 * Time: 17:44
 */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use common\models\UserRole;
?>

<div class="question-heading">
	Are you looking to buy or sell?
</div>
<div class="answers flexbox">

	<?php foreach (UserRole::find()->seller()->buyer(true)->_default()->all() as $key => $value) : ?>

		<div class="answer">
			<a href="#" onclick="searchAnswer(event, '<?= Html::getInputId($model, 'lead_type') ?>', '<?= $value->id ?>')">
				<?php if($value->is_seller) : ?>
					<div class="content">
						<?= Html::img(Url::to(['@uri_img/search/sell.png'], true)) ?>
					</div>
					<span>Sell</span>
				<?php elseif ($value->is_buyer) : ?>
					<div class="content">
						<?= Html::img(Url::to(['@uri_img/search/buy.png'], true)) ?>
					</div>
					<span>Buy</span>
				<?php endif; ?>
				<i class="hi hi-check trans3 checky"></i>
			</a>
		</div>

	<?php endforeach; ?>
</div>
<?php $form = ActiveForm::begin([
	'options' => [ 'data-pjax' => 'true', 'class' => 'hidden' ],
	'validateOnBlur' => false,
]); ?>

	<?= $form->field($model, 'lead_type')->hiddenInput()->label(false) ?>

<?php ActiveForm::end(); ?>
