<?php
/**
 * Created by PhpStorm.
 * User: Vlad
 * Date: 25.04.2017
 * Time: 17:43
 */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
?>

<div class="question-heading">
	What is our estimated Property Value
</div>
<div class="answers buttons flexbox">
	<?php foreach ($price_labels as $val => $label) : ?>
		<div class="answer">
			<a href="#" onclick="searchAnswer(event, '<?= Html::getInputId($model, 'price') ?>', '<?= $val ?>')">
				<div class="content">
					<span><?= Html::encode($label['name']) ?></span>
				</div>
				<i class="hi hi-check checky"></i>
			</a>
		</div>
	<?php endforeach; ?>
</div>	

<?php $form = ActiveForm::begin([
	'options' => [ 'data-pjax' => 'true', 'class' => 'hidden' ],
	'validateOnBlur' => false,
]); ?>

	<?= $form->field($model, 'price')->hiddenInput()->label(false) ?>

<?php ActiveForm::end(); ?>