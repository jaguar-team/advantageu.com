<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\helpers\Url;
use rmrevin\yii\fontawesome\FA;

?>

<div class="top-bar testSC">
    <div class="wrap">
        <a href="tel:855 451-2150 ">(855) 451-2150 </a><span class="topMeta  hidden-xs">|</span>
        <?= Html::a('Testimonials', Url::toRoute(['testimonials/index'], true), ['class' => 'topMeta']) ?> <span class="topMeta hidden-xs">|</span>
        <?= Html::a('Contact', Url::toRoute(['contact/index'], true), ['class' => 'topMeta']) ?> <span class="topMeta hidden-xs">|</span>
        <?php if(\Yii::$app->user->isGuest){ ?>
            <a class="topMeta pull-right" style="display: inline-block;" href="#" data-toggle="modal" data-target="#signup_modal">Log in</a>
        <?php } else { ?>
            <span class="dropdown  pull-right">
                <a href="#" class="dropdown-toggle" type="button" id="user_dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                    <?= \Yii::$app->user->identity->profile->getFullName() ?>
                    <span class="caret"></span>
                </a>
                <ul class="dropdown-menu" aria-labelledby="user_dropdown">
                    <li>
                        <?= Html::a(FA::icon('user').'Profile', Url::toRoute(['agent/profile', 'id' => \Yii::$app->user->id ], true)) ?>
                    </li>
                    <li>
                        <?= Html::a(FA::icon('lock').'Logout', Url::toRoute(['home/sign-out'], true), ['data-method' => "POST"]) ?>
                    </li>
                </ul>
            </span>
        <?php } ?>
    </div>
</div>

<div id="main-navigation-bar" class="fullwidth">
    <div class="dark-fade"></div>
    <div id="background-bar">
        <?= Html::img( Url::to(['@uri_img/AdvantageU-Home-header.jpg'], true), ['alt' => 'page header'] ) ?>
    </div>

    <div class="wrap">
        <div class="threecol first " id="logo-bar">
            <div id="logo">
                <?= Html::a( Html::img( Url::to(['@uri_img/AdvantageU-Logo.png'], true ), ['alt' => 'AdvantageU'] ), Url::to(Yii::$app->homeUrl), [] ); ?>
            </div>
        </div>
        <div class="menu-toggle visible-xs" onclick="menuToggle(event, 'menu-primary')">
            <?= FA::icon('bars') ?>
        </div>
        <nav id="navigation" class="ninecol last"> 
            <div class="menu-primary-container">
                <ul id="menu-primary" class="menu">
                    <li>
                        <?= Html::a('About Us', Url::toRoute(['about-us/index'], true)) ?>
                    </li>
                    <li>
                        <?= Html::a('Sell Your House', Url::toRoute(['sell-your-house/index'], true)) ?>
                    </li>
                    <li>
                        <?= Html::a('Ultimate Checklist', Url::toRoute(['ultimate-checklist/index'], true)) ?>
                    </li>
                    <li>
                        <?= Html::a('Resources', Url::toRoute(['resources/index'], true)) ?>
                    </li>
                    <li id="menu-item-416" class="menu-item menu-item-type-post_type_archive menu-item-object-testimonial menu-item-416">
                        <?= Html::a('Testimonials', Url::toRoute(['testimonials/index'], true)) ?>
                    </li>
                    <li id="menu-item-417" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-417">
                        <?= Html::a('Contact', Url::toRoute(['contact/index'], true)) ?>
                    </li>
                    <li class="get_matched">
                        <a href="<?= (!\Yii::$app->agentSearch->search() ? Url::toRoute([\Yii::$app->homeUrl, '#' => 'search'], true) : Url::toRoute(['agent-search/result'], true)) ?>" >
                            Get Matched
                            <?php if(\Yii::$app->agentSearch->search()) : ?>
                                <div class="match_count"><?= Html::encode(\Yii::$app->agentSearch->search()->getTotalCount()) ?></div>
                            <?php endif ?>
                        </a>
                    </li>
                </ul>
            </div>
        </nav> 
        <div class="clearfix"></div>
    </div>


</div>
