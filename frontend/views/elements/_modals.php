<?php
    use yii\helpers\Html;
    use yii\helpers\Url;
    use yii\bootstrap\ActiveForm;
	use yii\widgets\Pjax;
    use yii\widgets\MaskedInput;

    $login_form = \Yii::$app->loginForm;
    $user_model = \Yii::$app->userIdentity;
    $user_role_model = \Yii::$app->userRole;
    $user_profile_model = \Yii::$app->userProfile;
    $google_auth = \Yii::$app->googleAuth;

    function showLayoutModal($id, &$google_auth, &$view_obj)
    {
        if ( $google_auth->isShowPassword() ){
            $view_obj->registerJs('$("body").css("padding-right", "0px");
                $("body").removeClass("modal-open");
                $(".modal-backdrop").remove();
                $("#'.$id.'").modal("show")');
            return true;
        } 
        else return false;
    }
?>


<div class="modal fade login_form <?= ( showLayoutModal('signup_modal', $google_auth, $this) ? 'in' : '' ) ?>" id="signup_modal" tabindex="-1" role="dialog" style="<?= ( showLayoutModal('signup_modal', $google_auth, $this) ) ? 'display: block;' : '' ?>">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body signup_form">
                <h2>Welcome to AdvantageU</h2>
                <div class="controls">
                    <ul role="tablist">
                        <li role="presentation" class="<?= ($google_auth->isShowPassword() ? '' : 'active') ?>">
                            <a href="#signin" aria-controls="signin" role="tab" data-toggle="tab">Sign In</a>
                        </li>
                        <li role="presentation" class="<?= ($google_auth->isShowPassword() ? 'active' : '') ?>">
                            <a href="#signup" aria-controls="signup" role="tab" data-toggle="tab">New Account</a>
                        </li>
                    </ul>
                </div>
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane <?= ($google_auth->isShowPassword() ? '' : 'active') ?>" id="signin">
                        <?php $form = ActiveForm::begin([
                            'action' => Url::to(['home/sign-in'], true),
                            'options' => [
                                'id'    => 'login_form',
                                'class' => 'col-md-12',
                                'tag' => 'false',
                                'data-pjax' => 'false',
                            ]
                        ]) ?>

                            <?= @$form->field($login_form, 'email')->widget(MaskedInput::className(), [
                                'clientOptions' => [
                                    'alias' => 'email',
                                ],
                                'options' => [
                                    'placeholder' => 'jsmith@gmail.com',
                                    'class' => 'form-control'
                                ]
                            ])->label(false) ?>

                            <?= @$form->field($login_form, 'password')->passwordInput()->label(false) ?>

                            <?= @$form->field($login_form, 'rememberMe')->checkbox(['class' => 'cbr']) ?>

                            <div class="row">
                                <div class="col-sm-6">
                                    <?= Html::submitButton('Submit', ['class' => 'submit-btn']) ?>
                                </div>
                            </div>

                        <?php ActiveForm::end() ?>
                    </div>
                    <div role="tabpanel" class="tab-pane <?= ($google_auth->isShowPassword() ? 'active' : '') ?>" id="signup">
                        <?php $form = ActiveForm::begin([
                            'options' => [
                                'class' => 'col-md-12',
                                'tag' => 'false',
                                'id' => 'signup_form',
                                'data-pjax' => 'false',
                            ]
                        ]) ?>
                            <div class="row">
                                <div class="col-md-12 <?= ($google_auth->isShowPassword() ? 'hidden' : '') ?>">
                                    <?= @$form->field($user_model, 'email')->widget(MaskedInput::className(), [
                                        'clientOptions' => [
                                            'alias' => 'email',
                                        ],
                                        'options' => [
                                            'placeholder' => 'jsmith@gmail.com',
                                            'class' => 'form-control',
                                        ]
                                    ])->label(false) ?>
                                </div>
                                <div class="col-md-12">
                                    <?php if($google_auth->isShowPassword())
                                        echo "<h3>Google+ authentification is complete! You only need to enter password.</h3>" ?>
                                    <?= $form->field($user_model, 'password_hash')->passwordInput()->label(false) ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <label><input type="checkbox" class="cbr"> I am a landlord or industry professional</label>
                            </div>


                            <?php if(!$google_auth->isShowPassword()) : ?>
                                <h3>Professional Information</h3>
                            <?php endif; ?>
                            
                            <div class="row">

                                <div class="col-md-12">
                                    <?= @$form->field($user_model, 'id_role')->hiddenInput(['value' => $user_role_model::find()->agent()->_default()->one()->id ])->label(false) ?>
                                    
                                </div>

                                <div class="col-sm-6 <?= ($google_auth->isShowPassword() ? 'hidden' : '') ?>">
                                    <?= @$form->field($user_profile_model, 'first_name')->textInput([
                                        'placeholder' => 'First Name', 
                                        'class' => 'form-control'
                                    ])->label(false) ?>
                                </div>

                                <div class="col-sm-6 <?= ($google_auth->isShowPassword() ? 'hidden' : '') ?>">
                                    <?= @$form->field($user_profile_model, 'last_name')->textInput([
                                        'placeholder' => 'Last Name',
                                        'class' => 'form-control' 
                                    ])->label(false) ?>
                                </div>
                            </div>
                            <div class="row <?= ($google_auth->isShowPassword() ? 'hidden' : '') ?>">
                                <div class="col-sm-6">
                                    <?= @$form->field($user_profile_model, 'phone_number')->widget(MaskedInput::className(), [
                                        'mask' => '(999)999-9999',
                                        'options' => [
                                            'placeholder' => '(888)867-5309',
                                            'class' => 'form-control'
                                        ]
                                    ])->label(false) ?>
                                </div>
                                

                            </div>

                            <div class="row">
                                <div class="col-sm-6">
                                    <?= Html::submitButton('Submit', ['class' => 'submit-btn disabled']) ?>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label><input type="checkbox" onchange="ifAccept(event)" class="cbr"> <strong>I accept AdvantageU Terms of Use</strong></label>
                                    </div>                                    
                                </div>
                            </div>
                            <div class="hidden">
                                <hr>
                                <div class="connect_options <?= ($google_auth->isShowPassword() ? 'hidden' : '') ?>">
                                    <strong>Or connect with:</strong>
                                    <?= Html::a(Html::img('@uri_img/google_share.png'), Url::to($google_auth->getUrl()), []) ?>
                                    <!--<?//= Html::img('@uri_img/facebook_share.png') ?>-->
                                </div>
                            </div>


                        <?php ActiveForm::end() ?>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>