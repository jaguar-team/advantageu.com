<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\MaskedInput;
use yii\widgets\Pjax;
use rmrevin\yii\fontawesome\FA;

use common\models\Area;
use common\models\Language;
use common\models\UploadFileForm;
use common\models\UserAddress;

/* @var $this yii\web\View */
/* @var $user_profile_model common\models\UserProfile */
/* @var $upload_file_form common\models\UploadFileForm */
/* @var $area_model common\models\Area */
/* @var $user_address common\models\UserAddress */
/* @var $language_model common\models\Language */

$area_model = \Yii::createObject(['class' => Area::className()]);
$language_model = \Yii::createObject(['class' => Language::className()]);
$upload_file_form = \Yii::createObject(['class' => UploadFileForm::className()]);
$user_address = \Yii::createObject(['class' => UserAddress::className()]);

$areas_served = $user_profile_model->getAreas()->select(['city'])->indexBy('id')->column();
$languages = $user_profile_model->getLanguages()->select(['name'])->indexBy('id')->column();

$i = 0;
$areas = '';
foreach ($areas_served as $key => $city) {
	$areas.= ( ++$i < count($areas_served) ) ? $city.', ' : $city;
} 
?>
<div class="header">
	<div class="container">
		<div class="row">
			<!--<div class="searchbox flexbox col-sm-8 col-xs-12">
				
			</div>-->
			<?php if ($is_owner) : ?>
				<div class="actions hidden pull-right col-lg-4 col-md-5 col-sm-6 col-xs-12">
					<ul class="flexbox">
						<li class="notifications dropdown">
							<?= Html::a( 
								sprintf('<span class="message_count">%d</span>Orders', $user_profile_model->getAgentContacts(0, 'unread', false, true)), ['#'], 
								[
									'data-toggle' => "modal", 'data-target' => "#notifications", 
									'class' => 'notifications dropdown-toggle',
									'aria-expanded' => 'false'
								]
							)?>
							<ul class="dropdown-menu">
								<li class="top">
									<p class="small">
										You have <strong class="message_count"><?= Html::encode($user_profile_model->getAgentContacts(0,'unread', false, true)) ?></strong> new notifications.
									</p>
								</li>									
								<li>
									<?php if( $user_profile_model->getAgentContacts(0, 'all', false, true) ) : ?>
										<ul class="dropdown-menu-list">
											<?php foreach ($user_profile_model->getAgentContacts(20, 'all', true) as $key => $message) : ?>
												<li class="<?= ($message['status']) ? '' : 'active' ?>">
													<a href="#" onclick="readMessage(event, <?= $message['id'] ?>, <?= ($message['status']) ? 'false' : 'true' ?>, <?= $user_profile_model->getAgentContacts(0, 'unread', false, true) ?>)">															
														<span class="line">
															<strong><?= Html::encode( $message['full_name'] ) ?></strong>
															<span class="light small">- <?= \Yii::$app->formatter->asRelativeTime($message['updated_at']) ?></span>
														</span>													
														<span class="line small desc">
															<?= Html::encode($message['message']) ?>
														</span>
														<div class="info">
															<span class="line">
																Email: <?= Html::encode($message['email']) ?>
															</span>
															<span class="line">
																Phone: <?= Html::encode($message['phone']) ?>
															</span>
														</div>

													</a>
												</li>
											<?php endforeach; ?>											
										</ul>
									<?php else : ?>
										<li class="empty">
											<p>You have no messages yet..</p>
										</li>
									<?php endif ?>
								</li>
							</ul>
						</li>
						<li class="text-right"><?= Html::a( 'Manage My Account', ['#'], ['data-toggle' => "modal", 'data-target' => "#edit_profile"]) ?></li>
						
						<li class="text-left"><?= Html::a( 'Your favourites: 10', ['#'], [])?></li>
					</ul>
				</div>
			<?php endif; ?>
		</div>
		<div class="user_info flexbox <?= ($user_model->certificate) ? 'certified' : '' ?>">
			
			<div class="user_photo">
				<?php Pjax::begin([
					'options' => ['class' => 'img'],
					'enablePushState' => false
				]) ?>
					<?php $form = ActiveForm::begin([
						'action' => Url::toRoute(['agent/profile', 'id' => $user_model->id], true),
						'options' => [
							'data-pjax' => 'true'
						]
					]) ?>
						<?= ($is_owner) ? Html::a( Html::img($user_profile_model->getPhotoUrl(), ['id' => 'agent-profile-img']), 'javascript:void(0)', ['onclick' => 'triggerFileInput(event, "'.Html::getInputId($upload_file_form, 'user_avatar').'", null, true)'] ) : Html::img($user_profile_model->getPhotoUrl()) ?>
						<div class="hidden">
							<?= $form->field($upload_file_form, 'user_avatar')->fileInput(['accept' => 'image/*']) ?>
						</div>
					<?php ActiveForm::end() ?>
				<?php Pjax::end() ?>
			</div>
				
			<div class="personal_info row">

				<?php Pjax::begin(['options' => ['class' => 'pjax_inline col-sm-6 col-xs-12']]) ?>
					<div class="<?= ($is_owner ? 'editable' : '') ?>">
				    	<?php $form = ActiveForm::begin([
				    		'action' => Url::toRoute(['agent/profile', 'id' => $user_model->id], true),
				    		'options' => [
				    			'data-pjax' => 'true'
				    		]
				    	]) ?>
							<h3 class="element">
								<?= Html::encode($user_profile_model->getFullName()) ?>
								
							</h3>
							<?php if($is_owner) : ?>
								<div class="editor x2 row">
									<div class="col-sm-10 col-xs-9">
										<?= $form->field($user_profile_model, 'first_name')->textInput()->label(false) ?>
									
										<?= $form->field($user_profile_model, 'last_name')->textInput()->label(false) ?>
									</div>
									<div class="edit_controls col-sm-2 col-xs-3">
										<?= Html::a( 'Save', 'javascript:void(0)', ['do' => 'submit_editable']) ?>
										<?= Html::a( 'Cancel', 'javascript:void(0)', ['do' => 'cancel_editable']) ?>
									</div>
								</div>
								<div class="actions">
									<?= Html::a( FA::icon('pencil-square-o'), 'javascript:void(0)', 
										[
											'onclick' => 'editField(event, "'.Html::getInputId($user_profile_model, 'first_name').'")',
											'class' => 'edit_trigger'
										]
									) ?>
								</div>
							<?php endif ?>
						<?php ActiveForm::end() ?>									
					</div>
				<?php Pjax::end() ?>

				<?php if($is_owner) : ?>
					<?php Pjax::begin(['options' => ['class' => 'pjax_inline col-sm-6 col-xs-12']]) ?>
						<div>
					    	<?php $form = ActiveForm::begin([
					    		'action' => Url::toRoute(['agent/profile', 'id' => $user_model->id], true),
					    		'options' => [
					    			'data-pjax' => 'true',
					    			'class' => 'form-horizontal'
					    		]
					    	]) ?>
					    		<div class="col-sm-12 col-xs-6">
									<?= $form->field($user_model, 'private_property', [
										'checkboxTemplate' => "{beginWrapper}\n<div class=\"checkbox\">\n{beginLabel}\n{input}\n{labelTitle}\n{endLabel}\n</div>\n{error}\n{endWrapper}\n{hint}",
									])->checkbox([
						                'class'=>'iswitch iswitch-secondary',
						                'onchange' => 'submitForm(event)'
						            ], false) ?>
					           </div>

					           <div class="col-sm-12 col-xs-6">
						            <?= $form->field($user_model, 'private_account', [
										'checkboxTemplate' => "{beginWrapper}\n<div class=\"checkbox\">\n{beginLabel}\n{input}\n{labelTitle}\n{endLabel}\n</div>\n{error}\n{endWrapper}\n{hint}",
									])->checkbox([
						                'class'=>'iswitch iswitch-secondary',
						                'onchange' => 'submitForm(event)'
						            ], false) ?>
						        </div>

							<?php ActiveForm::end() ?>									
						</div>
					<?php Pjax::end() ?>
				<?php endif ?>
				
				<h4 class="col-xs-12"><?= Html::encode($user_model->role->name) ?></h4>
				<div class="col-sm-6 col-xs-12">

					<li class="<?= ($is_owner ? 'editable' : '') ?>">
											
						<i class="icon-location-pin2"></i>
						<?php Pjax::begin([
							'options' => [
								'class' => 'pjax_inline',
								'data-related_container' => 'profile_address'
							]
						]) ?>	
							<span class="element" data-toggle="tooltip" data-placement="top" title="Double click to expand!" ondblclick="oneStrExpand(event)">
								<?php									
									echo Html::encode( ($addresses != '') ? $addresses : 'No addresses yet');
									echo (!$is_matched ? '<span class="add-to-matches">Get Matched</span>' : '');
								?>
							</span>
							<?php if($is_owner) : ?>
								<div class="editor row">
									<div class="col-sm-10 col-xs-9">
				                    	
										<?php $form = ActiveForm::begin([
											'action' => Url::toRoute(['agent/profile', 'id' => $user_model->id], true),
											'options' => [
												'class' => 'area_validation',
												'data-area-model' => 'useraddress',
												'data-pjax' => 'true',
												'validateOnBlur' => false
											]
										]) ?>
											<div class="form-group area_field">
				                            	<input class="form-control profile_areas" type="text"
				                            		data-type="address"
				                            		data-city-id="<?= Html::getInputId($user_address, 'city') ?>" 
													data-state-id="<?= Html::getInputId($user_address, 'state') ?>" 
													data-zipcode-id="<?= Html::getInputId($user_address, 'zip_code') ?>"
													data-address-id="<?= Html::getInputId($user_address, 'address') ?>"
													data-lat-id="<?= Html::getInputId($user_address, 'lat') ?>" 
													data-lng-id="<?= Html::getInputId($user_address, 'lng') ?>"
													data-place_id-id="<?= Html::getInputId($user_address, 'place_id') ?>"
													data-short-id="<?= Html::getInputId($user_address, 'short_name') ?>" />
											</div>

											<div class="hidden">
							                    <?= $form->field($user_address, 'city')->hiddenInput()->label(false) ?>
												<?= $form->field($user_address, 'state')->hiddenInput()->label(false) ?>
												<?= $form->field($user_address, 'zip_code')->hiddenInput()->label(false) ?>
												<?= $form->field($user_address, 'address')->hiddenInput()->label(false) ?>
												<?= $form->field($user_address, 'lat')->hiddenInput()->label(false) ?>
												<?= $form->field($user_address, 'lng')->hiddenInput()->label(false) ?>
												<?= $form->field($user_address, 'place_id')->hiddenInput()->label(false) ?>
												<?= $form->field($user_address, 'short_name')->hiddenInput()->label(false) ?>													
											</div>
											      
										<?php ActiveForm::end() ?>	

										<ul class="areas_list">
											<?php foreach ($user_model->addresses as $key => $address) : ?>
												<li>
													<?= $address['address'].', '.$address['city'].', '.$address['short_name'] ?>
													<?= Html::a('×', Url::toRoute(['agent/delete-address', 'id' => $address['id']], true), ['class' => 'remove', 'data-pjax' => 'true', 'data-method' => 'POST']) ?>
												</li>
											<?php endforeach ?>
										</ul>
									</div>
									<div class="edit_controls col-sm-2 col-xs-3">
										<?= Html::a( 'Save', 'javascript:void(0)', ['do' => 'submit_editable']) ?>
										<?= Html::a( 'Done', 'javascript:void(0)', ['do' => 'cancel_editable']) ?>
									</div>
								</div>
								<div class="actions">
									<?= Html::a( FA::icon('pencil-square-o'), 'javascript:void(0)', 
										[
											'onclick' => 'editField(event, "office_address")',
											'class' => 'edit_trigger'
										]
									) ?>
								</div>
							<?php endif ?>
						<?php Pjax::end() ?>
					</li>

					<?php if($user_model->role->is_agent) : ?>
						<li class="<?= ($is_owner ? 'editable' : '') ?>">
							<i class="icon-map5"></i>
							<?php Pjax::begin(['options' => ['class' => 'pjax_inline']]) ?>
								<span class="element" data-toggle="tooltip" data-placement="top" title="Double click to expand!" ondblclick="oneStrExpand(event)">
									Real Estate Market: <?= Html::encode( (!empty($areas_served)) ? $areas : 'No served areas yet') ?>
								</span> 
								<?php if($is_owner) : ?>
									<div class="editor row">
										<div class="col-sm-10 col-xs-9">
					                    	
				                    		<?php
				                    			$form = ActiveForm::begin([
				                    				'action' => Url::toRoute(['agent/profile', 'id' => $user_model->id], true),
													'options' => [
														'class' => 'area_validation',
														'data-area-model' => 'area',
														'data-pjax' => 'true',
														'validateOnBlur' => false
													]
												])
				                    		?>
				                    			<div class="form-group area_field">
					                            	<input class="form-control profile_areas" type="text" id="served"
														data-type="(regions)"
					                            		data-city-id="<?= Html::getInputId($area_model, 'city') ?>" 
														data-state-id="<?= Html::getInputId($area_model, 'state') ?>"
														data-zipcode-id="<?= Html::getInputId($area_model, 'zip_code') ?>"
														data-region-id="<?= Html::getInputId($area_model, 'region') ?>"
														data-short-id="<?= Html::getInputId($area_model, 'short_name') ?>"													
													/>
												</div>

						                        <div class="hidden">
								                    <?= $form->field($area_model, 'city')->hiddenInput(['class' => 'hidden'])->label(false) ?>
								                    <?= $form->field($area_model, 'short_name')->hiddenInput(['class' => 'hidden'])->label(false) ?>
													<?= $form->field($area_model, 'zip_code')->hiddenInput(['class' => 'hidden'])->label(false) ?>
								                    <?= $form->field($area_model, 'state')->hiddenInput(['class' => 'hidden'])->label(false) ?>
								                    <?= $form->field($area_model, 'region')->hiddenInput(['class' => 'hidden'])->label(false) ?>  

							                   	</div>  
							                <?php ActiveForm::end() ?>
							                <ul class="areas_list">
												<?php foreach ($areas_served as $key => $area) : ?>
													<li>
														<?= $area ?>
														<?= Html::a('×', Url::toRoute(['agent/delete-area', 'id' => $key], true), ['class' => 'remove', 'data-pjax' => 'true', 'data-method' => 'POST']) ?>
													</li>
												<?php endforeach ?>
											</ul>                    

										</div>
										<div class="edit_controls col-sm-2 col-xs-3">
											<?= Html::a( 'Save', 'javascript:void(0)', ['do' => 'submit_editable']) ?>
											<?= Html::a( 'Done', 'javascript:void(0)', ['do' => 'cancel_editable']) ?>
										</div>
									</div>
									<div class="actions">
										<?= Html::a( FA::icon('pencil-square-o'), 'javascript:void(0)', 
											[
												'onclick' => 'editField(event, "served")',
												'class' => 'edit_trigger'
											]
										) ?>
									</div>
								<?php endif ?>
							<?php Pjax::end() ?>
						</li>
					<?php endif ?>
						
					<li class="<?= ($is_owner ? 'editable' : '') ?>">
						<i class="icon-earth"></i>
						<?php Pjax::begin(['options' => ['class' => 'pjax_inline']]) ?>
							<span class="element" data-toggle="tooltip" data-placement="top" title="Double click to expand!" ondblclick="oneStrExpand(event)">
								Languages: 								
								<?php 
									$i = 1;
									$langs = '';
									foreach ($languages as $key => $language) {
										if ($i< count($languages)) {
											if( $i + 1 < count($languages) ){
												$langs.= $language.', ';
											} else {
												$langs.= $language.' and ';
											}
										}
										else $langs.= $language;
										$i++;
									} 
									echo (!empty($languages)) ? $langs : 'No languages yet';
								?>
							</span>
							<?php if($is_owner) : ?>
								<div class="editor row">
									<div class="col-sm-10 col-xs-9">
										
			                    		<?php
			                    			$form = ActiveForm::begin([
			                    				'action' => Url::toRoute(['agent/profile', 'id' => $user_model->id], true),
												'options' => [
													'data-pjax' => 'true',
													'validateOnBlur' => false
												]
											])
			                    		?>

			                    			<?= $form->field($language_model, 'name')->textInput(['value' => null, 'placeholder' => 'Enter language'])->label(false) ?>

			                    		<?php ActiveForm::end() ?>
										<ul class="areas_list">
											<?php foreach ($languages as $key => $language) : ?>
												<li>
													<?= $language ?>
													<?= Html::a('×', Url::toRoute(['agent/delete-language', 'id' => $key], true), ['class' => 'remove', 'data-pjax' => 'true', 'data-method' => 'POST']) ?>
												</li>
											<?php endforeach ?>
										</ul>               
									</div>
									<div class="edit_controls col-sm-2 col-xs-3">
										<?= Html::a( 'Save', 'javascript:void(0)', ['do' => 'submit_editable']) ?>
										<?= Html::a( 'Done', 'javascript:void(0)', ['do' => 'cancel_editable']) ?>
									</div>
								</div>
								<div class="actions">
									<?= Html::a( FA::icon('pencil-square-o'), 'javascript:void(0)', 
										[
											'onclick' => 'editField(event, "'.Html::getInputId($language_model, 'name').'")',
											'class' => 'edit_trigger'
										]
									) ?>
								</div>
							<?php endif ?>
						<?php Pjax::end() ?>
					</li>
					

					<?php Pjax::begin(['options' => ['class' => 'pjax_inline']]) ?>
						<li class="<?= ($is_owner ? 'editable' : '') ?>">
							<i class="icon-earth-globe"></i>
							
							<span class="element">
													
								<?php 
									if($is_matched){
										echo ($user_profile_model->website_url ? Html::a($user_profile_model->website_url, $user_profile_model->website_url) : 'No website yet');	
									}

									else echo 'XXXXXXXXXXX.XXX';
								?>
							</span>
							<?= (!$is_matched ? '<span class="add-to-matches">Get Matched</span>' : '') ?>

							<?php if($is_owner) : ?>
								<div class="editor row">
									<div class="col-sm-10 col-xs-9">
										<div class="form-group area_field">
				                    		<?php
				                    			$form = ActiveForm::begin([
				                    				'action' => Url::toRoute(['agent/profile', 'id' => $user_model->id], true),
													'options' => [
														'data-pjax' => 'true',
														'validateOnBlur' => false
													]
												])
				                    		?>

				                    		<?= $form->field($user_profile_model, 'website_url')->textInput()->label(false) ?>

				                    		<?php ActiveForm::end() ?>  
					                    </div>
									</div>
									<div class="edit_controls col-sm-2 col-xs-3">
										<?= Html::a( 'Save', 'javascript:void(0)', ['do' => 'submit_editable']) ?>
										<?= Html::a( 'Cancel', 'javascript:void(0)', ['do' => 'cancel_editable']) ?>
									</div>
								</div>
								<div class="actions">
									<?= Html::a( FA::icon('pencil-square-o'), 'javascript:void(0)', 
										[
											'onclick' => 'editField(event, "'.Html::getInputId($user_profile_model, 'website_url').'")',
											'class' => 'edit_trigger'
										]
									) ?>
								</div>
							<?php endif ?>
						</li>
					<?php Pjax::end() ?>

					<?php if($user_model->role->is_agent) : ?>
						<li>
							<i class="icon-heart5"></i>
							<span><?= $user_model->getAddedFavorites()->count() ?> interests</span>
						</li>
					<?php endif ?>
				</div>

				<div class="col-sm-6 col-xs-12">
					<?php Pjax::begin(['options' => ['class' => 'pjax_inline']]) ?>
						<li class="<?= ($is_owner ? 'editable' : '') ?>">
							<i class="icon-book2"></i>
							
							<span class="element">Education: 
								<?php 
									if($is_matched){
										echo ($user_profile_model->education ? Html::encode($user_profile_model->education) : 'No education yet');	
									}

									else echo 'XXXXXXXXXX';
								?>									
							</span>
							<?= (!$is_matched ? '<span class="add-to-matches">Get Matched</span>' : '') ?>

							<?php if($is_owner) : ?>
								<div class="editor row">
									<?php
										$form = ActiveForm::begin([
											'action' => Url::toRoute(['agent/profile', 'id' => $user_model->id], true),
											'options' => [
												'data-pjax' => 'true'
											]
										]);
									?>
										<div class="col-sm-10 col-xs-9">
											<?= $form->field($user_profile_model, 'education')->textInput(['value' => $user_profile_model->education])->label(false) ?>
										
										</div>
										<div class="edit_controls col-sm-2 col-xs-3">
											<?= Html::a( 'Save', 'javascript:void(0)', ['do' => 'submit_editable']) ?>
											<?= Html::a( 'Cancel', 'javascript:void(0)', ['do' => 'cancel_editable']) ?>
										</div>
									<?php ActiveForm::end() ?>  
								</div>
								<div class="actions">
									<?= Html::a( FA::icon('pencil-square-o'), 'javascript:void(0)', 
										[
											'onclick' => 'editField(event, "'.Html::getInputId($user_profile_model, 'education').'")',
											'class' => 'edit_trigger'
										]
									) ?>
								</div>
							<?php endif ?>
						</li>
					<?php Pjax::end() ?>

					<?php if($user_model->role->is_agent) :
						Pjax::begin(['options' => ['class' => 'pjax_inline']]) 
					?>
						<li class="<?= ($is_owner ? 'editable' : '') ?>">

							<i class="icon-contact_mail"></i>

							<span class="element">
								License #
								<?php 
									if($is_matched){
										echo ($user_profile_model->license_number != '' ? Html::encode($user_profile_model->license_number) : 'No license yet');	
									}

									else echo 'XXXXXXXXXX';
								?>							
							</span>	

							<?= (!$is_matched ? '<span class="add-to-matches">Get Matched</span>' : '') ?>

							<?php if($is_owner) : ?>
								<div class="editor row">
									<?php
										$form = ActiveForm::begin([
											'action' => Url::toRoute(['agent/profile', 'id' => $user_model->id], true),
											'options' => [
												'data-pjax' => 'true'
											]
										]);
									?>
										<div class="col-sm-10 col-xs-9">
											<?= $form->field($user_profile_model, 'license_number')->textInput()->label(false) ?>
										
										</div>
										<div class="edit_controls col-sm-2 col-xs-3">
											<?= Html::a( 'Save', 'javascript:void(0)', ['do' => 'submit_editable']) ?>
											<?= Html::a( 'Cancel', 'javascript:void(0)', ['do' => 'cancel_editable']) ?>
										</div>
									<?php ActiveForm::end() ?>  
								</div>
								<div class="actions">
									<?= Html::a( FA::icon('pencil-square-o'), 'javascript:void(0)', 
										[
											'onclick' => 'editField(event, "'.Html::getInputId($user_profile_model, 'license_number').'")',
											'class' => 'edit_trigger'
										]
									) ?>
								</div>
							<?php endif ?>								
						</li>
					<?php Pjax::end(); endif ?>
									
					<li>
						<i class="icon-envelope-o"></i>
						
						<?php 
							if($is_matched){	
								echo Html::a($user_model->email, 'mailto:'.$user_model->email, ['class' => 'element']);
							}

							else echo '<span class="element">XXXXX@XXXXX.XXX</span>';
						?>	
						<?= (!$is_matched ? '<span class="add-to-matches">Get Matched</span>' : '') ?>										
					</li>

					<?php Pjax::begin(['options' => ['class' => 'pjax_inline']]) ?>		
						<li class="<?= ($is_owner ? 'editable' : '') ?>">
							<i class="icon-mobile2"></i>

							<?php 
								if($is_matched){	
									echo ($user_profile_model->phone_number) ? '<a class="element" href="tel:'.Html::encode( $user_profile_model->phone_number ).'">'.Html::encode( $user_profile_model->phone_number ).'</a>'  : 'No phone number yet';
								}

								else echo '<span class="element">(XXX)XXX-XXXX</span>';
							?>	

							<?= (!$is_matched ? '<span class="add-to-matches">Get Matched</span>' : '') ?>

							<?php if($is_owner) : ?>
								<div class="editor row">
									<?php
										$form = ActiveForm::begin([
											'action' => Url::toRoute(['agent/profile', 'id' => $user_model->id], true),
											'options' => [
												'data-pjax' => 'true'
											]
										]);
									?>
										<div class="col-sm-10 col-xs-9">
											<?= $form->field($user_profile_model, 'phone_number')->widget(MaskedInput::className(), [
												'mask' => '(999)999-9999',
												'options' => [
											    	'placeholder' => '(___)___-____',
											    	'class' => 'form-control'
											    ]									
											])->label(false) ?>
										
										</div>
										<div class="edit_controls col-sm-2 col-xs-3">
											<?= Html::a( 'Save', 'javascript:void(0)', ['do' => 'submit_editable']) ?>
											<?= Html::a( 'Cancel', 'javascript:void(0)', ['do' => 'cancel_editable']) ?>
										</div>
									<?php ActiveForm::end() ?>  
								</div>
								<div class="actions">
									<?= Html::a( FA::icon('pencil-square-o'), 'javascript:void(0)', 
										[
											'onclick' => 'editField(event, "'.Html::getInputId($user_profile_model, 'phone_number').'")',
											'class' => 'edit_trigger'
										]
									) ?>
								</div>
							<?php endif ?>
						</li>
					<?php Pjax::end() ?>

					<?php if($user_model->role->is_agent) : 
						Pjax::begin(['options' => ['class' => 'pjax_inline']]) 
					?>
						<li class="<?= ($is_owner ? 'editable' : '') ?>">
							<i class="icon-office"></i>
							
							<span class="element">
													
								<?php 
									if($is_matched){
										echo ($user_profile_model->company ? Html::a($user_profile_model->company, $user_profile_model->company) : 'No company yet');	
									}

									else echo 'XXXXXXXXXX';
								?>
							</span>
							<?= (!$is_matched ? '<span class="add-to-matches">Get Matched</span>' : '') ?>

							<?php if($is_owner) : ?>
								<div class="editor row">
									<div class="col-sm-10 col-xs-9">
										<div class="form-group area_field">
				                    		<?php
				                    			$form = ActiveForm::begin([
				                    				'action' => Url::toRoute(['agent/profile', 'id' => $user_model->id], true),
													'options' => [
														'data-pjax' => 'true',
														'validateOnBlur' => false
													]
												])
				                    		?>

				                    		<?= $form->field($user_profile_model, 'company')->textInput()->label(false) ?>

				                    		<?php ActiveForm::end() ?>  
					                    </div>
									</div>
									<div class="edit_controls col-sm-2 col-xs-3">
										<?= Html::a( 'Save', 'javascript:void(0)', ['do' => 'submit_editable']) ?>
										<?= Html::a( 'Cancel', 'javascript:void(0)', ['do' => 'cancel_editable']) ?>
									</div>
								</div>
								<div class="actions">
									<?= Html::a( FA::icon('pencil-square-o'), 'javascript:void(0)', 
										[
											'onclick' => 'editField(event, "'.Html::getInputId($user_profile_model, 'company').'")',
											'class' => 'edit_trigger'
										]
									) ?>
								</div>
							<?php endif ?>
						</li>
					<?php Pjax::end(); endif ?>

				</div>					
			</div>				
		</div>
	</div>
</div>