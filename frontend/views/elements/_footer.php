<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\helpers\Url;

?>

<div id="footer" class="fullwidth acf-row">
    <div class="wrap">

        <?= Html::img( Url::to(['@uri_img/AdvantageU-Logo.png'], true ), ['alt' => 'AdvantageU'] ) ?>
        <h2>(888) 998-2254</h2>
        <p><span style="color: #434242;">© 2017 AdvantageU. AdvantageU is a registered trademark. All Rights Reserved.</span> Rochester, New York 14650 | info@advantageu.com</p>

    </div>

</div>
<div id="social-bar">
    <div class="socItem" style="background: #54a33d;">

        <?= Html::a( Html::img( Url::to(['@uri_img/AdvantageU-facebook.jpg'], true ), ['alt' => 'AdvantageU FaceBook'] ), 'https://www.facebook.com/myadvantageU/', ['target' => '_blank'] ); ?>
    </div>
    <div class="socItem" style="background: #9dd58c;">

        <?= Html::a( Html::img( Url::to(['@uri_img/AdvantageU-twitter.jpg'], true ), ['alt' => 'AdvantageU Twitter'] ), 'https://twitter.com/myadvantageU', ['target' => '_blank'] ); ?>
    </div>
    <div class="socItem" style="background: #56a63e;">

        <?= Html::a( Html::img( Url::to(['@uri_img/AdvantageU-youtube.jpg'], true ), ['alt' => 'AdvantageU YouTube'] ), 'https://www.youtube.com/channel/UCPJTQFS9glLUaOMlGTCOwpw', ['target' => '_blank'] ); ?>
    </div>
    <div class="socItem" style="background: #498d35;">

        <?= Html::a( Html::img( Url::to(['@uri_img/AdvantageU-linkedin.jpg'], true ), ['alt' => 'AdvantageU LinkedIn'] ), 'https://www.linkedin.com/company/advantage-u', ['target' => '_blank'] ); ?>
    </div>
    
</div>