<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 029 29.06.17
 * Time: 15:52
 */

use yii\bootstrap\ActiveForm;

use yii\helpers\Html;
use yii\helpers\Url;

use yii\widgets\Pjax;
use yii\widgets\MaskedInput;

use rmrevin\yii\fontawesome\FA;

?>

<?php if(!\Yii::$app->user->isGuest) : ?>
<div class="panel">
	
	<h3>Your Favorite Agents</h3>
	<?php if(!empty(\Yii::$app->user->identity->favorites)) : ?>
		<?php foreach (\Yii::$app->user->identity->favorites as $favorite) { ?>

			<li class="slug-wrap">
				<div class="profile-photo-wrap">
		    		<?= Html::a(
			            Html::img($favorite->favoriteUser->profile->getPhotoUrl(), []),
			        	Url::toRoute(['agent/profile', 'id' => $favorite->favoriteUser->id], true), ['target' => '_blank', 'data-pjax' => 'false']
			        ) ?>
			    </div>
					<div class="info">
						<h3>
							<?=  Html::a($favorite->favoriteUser->profile->getFullName(true),
					    			Url::toRoute(['agent/profile', 'id' => $favorite->favoriteUser->id], true), ['target' => '_blank', 'data-pjax' => 'false']
					    	) ?>						
						</h3>
						<div class="stats">
							<div class="stat">
								<i class="hi hi-user"></i>
								<span><?= Html::encode( $favorite->favoriteUser->profile->company) ?></span>
							</div>
							<?= Html::a('Remove', 'javascript:void(0)', ['class' => 'add-to-matches', 'onclick' => 'removeFromMatches(event, '.$favorite->id.', true)']) ?>
							<div class="stat">
								<span><?= Html::a( 'View profile', Url::toRoute(['agent/profile', 'id' => $favorite->favoriteUser->id], true), ['target' => '_blank', 'data-pjax' => 'false']) ?></span>
							</div>
						</div>
						
				</div>
			</li> 

		<?php } ?>
	<?php else : ?>
		<ul>
			<li class="text-center">No favorites yet!</li>
		</ul>
	<?php endif ?>
</div>
<?php endif ?>

<?php if(!$is_owner) : ?>
	<div class="panel">
		<a href="#" class="btn" data-toggle="modal" data-target="#contact_agent">Contact</a>
	</div>
<?php endif ?>
<?php if($user_model->role->is_agent) : ?>
	<?php foreach ($user_model->role->getSections()->all() as $key => $section) : ?>
		<?php Pjax::begin([
				'options' => ['id' => 'section_'.$section->id],
				'timeout' => 500000 
			]) ?>
			<div class="panel">		
				<div class="label"></div>
				<h3><?= Html::encode($section->name) ?></h3>
				<ul>
					<?php if(!empty($section->getSectionValues()->where(['id_user' => $user_model->id])->indexBy('id')->asArray()->all())) : ?>
						<?php foreach ($section->getSectionValues()->where(['id_user' => $user_model->id])->indexBy('id')->asArray()->all() as $id => $value) : ?>
							<li><?= Html::encode($value['value']) ?></li>
						<?php endforeach ?>
					<?php else : ?>
						<li class="text-center">No <?= Html::encode($section->name) ?> yet!</li>
					<?php endif ?>
				</ul>
				<?php if($is_owner) : ?>
					<div class="edit">
						<a href="#" data-toggle="modal" data-target="#edit_section_<?= $section->id ?>"><?= FA::icon('pencil-square-o') ?></a>
					</div>	
				<?php endif ?>	
			</div>
		<?php Pjax::end() ?>
	<?php endforeach ?>
<?php endif ?>

<div class="panel statistics hidden">
	<h3>Agent’s Success Metrics</h3>
	<ul>
		<li><span>$10m</span>Days on Market</li>
		<li><span>150%</span>List to Sold Ratio</li>
		<li><span>41</span>Total Listing Volume</li>
	</ul>
</div>

<div class="panel favorites hidden">
	<div class="label"></div>
	<h3>Your Favorites</h3>
	<ul>
		<li>343 State Street - Anthony Ricketts</li>
		<li>Jane Dow  - Your Favorite Agent</li>
		<li>17 East Squire Drive - Tom Real  (AU Cert)</li>
	</ul>
</div>

<div class="panel">
	<h3><?= ($is_owner ? 'Your' : $user_model->profile->getFullName(true)) ?>'s Reviews & Testimonials</h3>
	<ul>
		<?php if(!empty($user_model->reviews)) : ?>
			<?php foreach ($user_model->reviews as $review) : ?>
				<li><?= Html::encode( $review['first_name'].' '. $review['last_name'].' Review' ) ?></li>
			<?php endforeach ?>
		<?php else : ?>
			<li class="text-center">No reviews or testimonials yet!</li>
		<?php endif ?>
	</ul>
	
</div>