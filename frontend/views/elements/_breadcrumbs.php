<?php 
	use yii\helpers\Html;
	use yii\widgets\Breadcrumbs;
	use rmrevin\yii\fontawesome\FA;
?>

			
<div class="breadcrumb-env">
	<?= Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        'options' => [
        	'class' => 'breadcrumb bc-1',
        ],
        'homeLink' => [
        	'url'=> Yii::$app->homeUrl,
        	'label'=> 'Home',
        	'template' => '<li>'.Html::decode(FA::icon('home')).' {link}</li>'
        ],
    ]) ?>				
		
</div>
	