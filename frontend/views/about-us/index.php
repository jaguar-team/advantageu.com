<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 021 21.06.17
 * Time: 16:24
 */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

$this->title = 'AU';

?>

<div class="page">
    <div class="the-content">
        <div class="page-hero page-hero-about-us acf-row-0">
            <?= Html::img(Url::to(['@uri_img/About-Us.jpg'], true), ['alt' => 'About Us']) ?>
            <div class="wrap">
                <div class="headingWrap">
                    <h1 id="page-heading">About Us</h1>
                </div>
            </div>
        </div>
        <div  class="text-block resource-display acf-row acf-row-1 fix-background-image">
            <div class="wrap">
                <h2>Empower Yourself</h2>
                <div>
                    <p>AdvantageU Home Sellers’ Resource was established to help residential home sellers navigate the complex world of real estate. Our aim is to help you: Reduce time &amp; effort, discover available options, limit uncertainty &amp; risk and make smart decisions.</p>
                    <p>The AdvantageU Home Sellers’ Resource gives you the tools you need to make smarter choices about selling your home. Our proprietary “<strong>Get Matched</strong>” service connects you with only the best real estate agents in your area. These matches are exclusively tailored to your unique situation and needs.</p>
                    <p><strong>AdvantageU</strong> information and resources are always free.</p>
                </div>
            </div>
        </div>
        <div class="four-image-display resource-display acf-row  acf-row-2">
            <div class="wrap">
                <h2><em>Real Experiences</em> REAL HOMESELLERS</h2>
                <div class="threecol first">
                    <?= Html::img(Url::to(['@uri_img/Layer-62.png'], true), ['alt' => 'Home Real Estate']) ?>
                </div>
                <div class="threecol">
                    <?= Html::img(Url::to(['@uri_img/Layer-63.png'], true), ['alt' => 'House Sold by Real Estate Broker']) ?>
                </div>
                <div class="threecol">                    
                    <?= Html::img(Url::to(['@uri_img/Layer-64.png'], true), ['alt' => 'Home sold by property brokers']) ?>
                </div>
                <div class="threecol">
                    <?= Html::img(Url::to(['@uri_img/Layer-65.png'], true), ['alt' => 'Home sold on MLS listing']) ?>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="text-callout testimonial-display acf-row acf-row-5">
            <div class="wrap">
                <div class="cycle-slideshow" data-cycle-slides="> div" id="cycle-slideshow-about-us">
                    <div class="cycle-slide cycle-sentinel" id="cycle-sentinel-about-us">
                        <h2 class="testimonial-content" style="visibility: hidden;">They provided incredible service! They sold my home in 14 days and saved me $3,514.</h2>
                        <h2 class="testimonial-name" style="visibility: hidden;">— Judy D’Ambrose, Webster NY</h2>
                    </div>
                    <div class="cycle-slide cycle-slide1-about-us" id="cycle-slide1">
                        <h2 class="testimonial-content">They provided incredible service! They sold my home in 14 days and saved me $3,514.</h2>
                        <h2 class="testimonial-name">— Judy D’Ambrose, Webster NY</h2>
                    </div>
                    <div class="cycle-slide cycle-slide2-about-us" id="cycle-slide2" >
                        <h2 class="testimonial-content">They sold my house in 4 days and I was able to pocket an extra $5,713.</h2>
                        <h2 class="testimonial-name">— Steve Johnston, Greece NY</h2>
                    </div>
                    <div class="cycle-slide cycle-slide3-about-us" id="cycle-slide3" >
                        <h2 class="testimonial-content">They sold our home in 18 days, we saved money and they helped us purchase our new home.</h2>
                        <h2 class="testimonial-name">— Mike and Diane Osewalt, Rochester NY</h2>
                    </div>
                    <div class="cycle-slide cycle-slide4-about-us" id="cycle-slide4">
                        <h2 class="testimonial-content">We received a contract for sale in 4 days of signing up and saved $2,250.</h2>
                        <h2 class="testimonial-name">— Rick and Suzy Kasper, Rush NY</h2>
                    </div>
                    <div class="cycle-slide cycle-slide5-about-us" id="cycle-slide5">
                        <h2 class="testimonial-content">I was planning to move from NJ to join my family in Atlanta, GA. I’d lived in my home for 20 </h2>
                        <h2 class="testimonial-name">— Paula, Atlanta GA</h2>
                    </div>
                    <div class="cycle-slide cycle-slide6-about-us" id="cycle-slide6">
                        <h2 class="testimonial-content">I was happy with the advantageU matching service and how much professionalism the agent Bobby s</h2>
                        <h2 class="testimonial-name">— John P. Penfield, NY</h2>
                    </div>
                    <div class="cycle-slide cycle-slide7-about-us" id="cycle-slide7">
                        <h2 class="testimonial-content">I used the advantage U matching service, I appreciated the concept and would recommend the free</h2>
                        <h2 class="testimonial-name">— Alan H.  De Pere, WI</h2>
                    </div>
                    <div class="cycle-slide cycle-slide8-about-us" id="cycle-slide8">
                        <h2 class="testimonial-content">I was happy with the program and I would definitely recommend AdvantageU service. I was able to</h2>
                        <h2 class="testimonial-name">— Senarath B. Antelope, CA</h2>
                    </div>
                    <div class="cycle-slide cycle-slide9-about-us" id="cycle-slide9">
                        <h2 class="testimonial-content">I was very pleased with this service. My house was on the market for 50 days with no serious of</h2>
                        <h2 class="testimonial-name">— John M. Cedar Rapids, IA</h2>
                    </div>
                    <div class="cycle-slide cycle-slide10-about-us" id="cycle-slide10">
                        <h2 class="testimonial-content">Before using the free AdvantageU service I tried &nbsp;For Sale By Owner for 1 week because I neede</h2>
                        <h2 class="testimonial-name">— Dan B. Grand Rapids, MI</h2>
                    </div>
                    <div class="cycle-slide cycle-slide11-about-us" id="cycle-slide11">
                        <h2 class="testimonial-content">I was happy with Advantage U setting me up with John and I would recommend him to other people </h2>
                        <h2 class="testimonial-name">— Robert W. Tallahassee, FL</h2>
                    </div>
                    <div class="cycle-slide cycle-slide12-about-us" id="cycle-slide12">
                        <h2 class="testimonial-content">The service provided to me by Advantage U was good and I am glad they picked out TJ. I was not </h2>
                        <h2 class="testimonial-name">— Crystal H. Myrtle Beach, SC</h2>
                    </div>
                    <div class="cycle-slide cycle-slide13-about-us" id="cycle-slide13">
                        <h2 class="testimonial-content">I would recommend Advantage U to friends and family. Once I spoke with Will I knew I would give</h2>
                        <h2 class="testimonial-name">— Ondra M. Miramar, FL</h2>
                    </div>
                    <div class="cycle-slide cycle-slide14-about-us" id="cycle-slide14">
                        <h2 class="testimonial-content">Advantage you had very great timing when they called me and I felt I was qualified very well. J</h2>
                        <h2 class="testimonial-name">— Fang H. Medford, OR</h2>
                    </div>
                    <div class="cycle-slide cycle-slide15-about-us" id="cycle-slide15">
                        <h2 class="testimonial-content">I was looking at 4 other agents when Advantage U called and matched me with John. We had to mak</h2>
                        <h2 class="testimonial-name">— Jack B. Roberts, WI</h2>
                    </div>
                    <div class="cycle-slide cycle-slide16-about-us" id="cycle-slide16">
                        <h2 class="testimonial-content">When we received a phone call from Advantage U we were trying to close on our house on our own.</h2>
                        <h2 class="testimonial-name">— Wendell H. Mechanicsville, VA</h2>
                    </div>
                    <div class="cycle-slide cycle-slide17-about-us" id="cycle-slide17">
                        <h2 class="testimonial-content">Advantage U called after I was selling my home FSBO for two weeks and had low offers. After bec</h2>
                        <h2 class="testimonial-name">— Matthew K. Charlotte NC</h2>
                    </div>
                    <div class="cycle-slide cycle-slide18-about-us" id="cycle-slide18">
                        <h2 class="testimonial-content">I started to sell my home FSBO and had no one come to any showings. My phone was ringing off th</h2>
                        <h2 class="testimonial-name">— Adam P. Mauldin, SC</h2>
                    </div>
                    <div class="cycle-slide cycle-slide19-about-us" id="cycle-slide19"
                        <h2 class="testimonial-content">I started as a FSBO before meeting Joe W. through Advantage U. He convinced me that to really n</h2>
                        <h2 class="testimonial-name">— Travis K. Wake Forest , NC</h2>
                    </div>
                    <div class="cycle-slide cycle-slide20-about-us" id="cycle-slide20">
                        <h2 class="testimonial-content">Since we were not in the area we thought we should sell our house. After putting on zillow we s</h2>
                        <h2 class="testimonial-name">— Rebecca. Smithville, TX</h2>
                    </div>
                    <div class="cycle-slide cycle-slide21-about-us" id="cycle-slide21">
                        <h2 class="testimonial-content">My name is Marlene and I did speak with the AdvantageU rep Robin and also the realtor Christina</h2>
                        <h2 class="testimonial-name">— Marlene S.</h2>
                    </div>
                </div>
            </div>
            <div class="back-cover back-cover-about-us"></div>
        </div>
    </div>
    <div class="text-block  resource-display acf-row acf-row-6 fix-background-image">
        <div class="wrap">
            <h2><em>Isaiah Colton</em> FOUNDER &amp; CEO</h2>
            <div>
                <p>
                    <?= Html::img(Url::to(['@uri_img/1016028_150x150.jpg'], true), ['alt' => 'face', 'class' => 'alignleft size-thumbnail wp-image-313 wp-image-313-about-us']) ?>
                    When a homeowner wants to get their home sold, there’s no easy way for them to identify, evaluate and hire a real estate agent. The homeowner simply wants to get their home sold fast, for the best price, the least cost and the least hassle. Most home sellers simply don’t have the information they need to select the best agent to match their individual needs.</p>
                <p>Our mission is to take the guess work and risk out of that process for home sellers. We pull back the curtain and accurately assess a real estate agent’s past performance and potential success with your home.</p>
                <p>If we determine an agent has a proven track record of success and shows a commitment to customer satisfaction, only then will he or she be considered to be a part of the List Assist network.</p>
            </div>
        </div>
    </div>
    <div class="text-block no-pad resource-display acf-row acf-row-7 fix-background-image">
        <div class="wrap">
            <h2><em>Our</em> CLIENT CARE SPECIALISTS</h2>
            <div>
                <p>
                    <?= Html::img(Url::to(['@uri_img/team-1024x494.jpg'], true), ['alt' => 'Best Real Estate Agents', 'class' => 'aligncenter wp-image-317 wp-image-317-about-us size-large']) ?>
                </p>
                <p><strong>Advantage<span class="orange-color-for-text">U</span></strong> understands the challenges homeowners face from limitations on time, resources and knowledge. We’ve developed a platform that can bolster the success of anyone trying to sell their home. The team at <strong>Advantage<span class="orange-color-for-text">U</span></strong>, uses our proprietary Home Sellers Resources to help homeowners make better choices to help sell their home quickly and with a fair market value. Our aim is to help you: <em>Reduce time &amp; effort, discover available options, limit uncertainty &amp; risk and make smart decisions.</em></p>
            </div>
        </div>
    </div>
    <div class="text-block  resource-display acf-row acf-row-8  fix-background-image">
        <div class="wrap">
            <h2><em>Our Company</em> For the Community</h2>
            <div>
                <p>At <strong>Advantage<span class="orange-color-for-text">U</span></strong>, doing the right thing is at the core of our purpose, values and principles. That includes investing in the communities in which we live, work and serve. In fact, two key points found in our Corporate Code of Ethics posted on the walls of our headquarters building reads as follows:</p>
                <p>•&nbsp; We believe in superior service to our consumers, our community, and to each other as members of the business community.</p>
                <p>•&nbsp; We believe in the untapped potential of every human being.</p>
            </div>
        </div>
    </div>
    <div class="four-image-display resource-display acf-row no-bottom-pad acf-row-9">
        <div class="wrap">
            <div class="threecol first">
                <?= Html::img(Url::to(['@uri_img/about-1.jpg'], true), ['alt' => 'AdvantageU team']) ?>
            </div>
            <div class="threecol">
                <?= Html::img(Url::to(['@uri_img/about-2.jpg'], true), ['alt' => 'AdvantageU team']) ?>
            </div>
            <div class="threecol">
                <?= Html::img(Url::to(['@uri_img/about-3.jpg'], true), ['alt' => 'AdvantageU team volunteering']) ?>
            </div>
            <div class="threecol">
                <?= Html::img(Url::to(['@uri_img/about-4.jpg'], true), ['alt' => 'Best Real Estate Agents volunteering']) ?>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="text-block  resource-display acf-row acf-row-5  fix-background-image">
        <div class="wrap">
            <div>
                <div class="align-center">
                   

                    <a href="http://www.rochesterhabitat.org/" target="_blank">
                        <?= Html::img(Url::to(['@uri_img/habitat-for-humanity.jpg'], true), ['alt' => 'habitat-for-humanity', 'class' => 'alignnone size-full wp-image-324 wp-image-324-about-us']) ?>
                    </a>
                    <a href="https://www.hillside.com/" target="_blank">
                        <?= Html::img(Url::to(['@uri_img/hillside.jpg'], true), ['alt' => 'hillside', 'class' => 'alignnone size-full wp-image-325 wp-image-325-about-us']) ?>
                    </a>
                </div>
                <p>With those values, it only makes sense that we would support <a class="color-black" href="http://www.rochesterhabitat.org/" target="_blank">Flower City Habitat for Humanity</a>.</p>
                <p>Families who own homes enjoy wealth accumulation, improved health, education and security. Better behavior, increased work productivity, enhanced civic and political participation are also shown. It truly is a hand-up, not a hand out.&nbsp; That’s why we love this program–we have the chance to help break the cycle of poverty–graduation rates for youth who have lived in our Flower City Habitat for Humanity houses is 98% in a city where the average is under 48%.</p>
                <p>The stability of home-ownership can also help stabilize the surrounding neighborhood providing positive benefits to more than just one homeowner.</p>
                <p>Make <a class="color-black" href="https://www.hillside.com/" target="_blank">Hillside Children Center</a> your Charity of Choice on <a class="color-black"  href="https://www.amazon.com/" target="_blank">Amazon.com</a> through <strong>Amazon Smiles</strong>, where Amazon sends a percentage of sales from select purchases to your charity organization.</p>
            </div>
        </div>
    </div>
    <span class="anchor" id="checklist-quiz"></span>


    <div id="quiz-box" class="quiz-display bot-bord acf-row acf-row-6 quiz-box-about-us">
        <div class="dark-fade"></div>
        <div class="dark-fade"></div>
        <div class="wrap">
            <div class="quiz">
                <div class="frm_forms  with_frm_style frm_style_formidable-style" id="frm_form_19_container">
                    <form enctype="multipart/form-data" method="post" class="frm-show-form " id="form_65itlq5">
                        <div class="frm_form_fields ">
                            <fieldset>
                                <legend class="frm_hidden">Quick Quiz</legend>

                                <input type="hidden" name="frm_action" value="create">
                                <input type="hidden" name="form_id" value="19">
                                <input type="hidden" name="frm_hide_fields_19" id="frm_hide_fields_19" value="">
                                <input type="hidden" name="frm_helpers_19" id="frm_helpers_19" value="[]">
                                <input type="hidden" name="form_key" value="65itlq5">
                                <input type="hidden" name="item_meta[0]" value="">
                                <input type="hidden" id="frm_submit_entry_19" name="frm_submit_entry_19" value="b10622b93c">
                                <input type="hidden" name="_wp_http_referer" value="/about-us/">
                                <div class="sixcol first">
                                    <?= Html::img(Url::to(['@uri_img/Quiz-devices.png'], true), ['alt' => 'Image Here', 'id' => 'submit-devices']) ?>
                                </div>
                                <div class="sixcol last">
                                    <div id="frm_field_180_container" class="frm_form_field  frm_top_container  form-field">

                                        <?php $this->registerJs('$(\'#quiz-heading\').text("We’ve found your a-z ultimate checklist");', yii\web\View::POS_LOAD) ?>
                                        <p></p>
                                        <style>
                                            .quiz-display .dark-fade,
                                            .home .quiz-display .dark-fade {
                                                display: block
                                            }
                                        </style>
                                        <h3 id="submit-page-right-sub">Know what to do, when.</h3>
                                        <h2 id="submit-page-heading">get your checklist now!</h2>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div id="frm_field_183_container" class="frm_form_field  frm_top_container  form-field">
                                    <div class="sixcol last">
                                        <p id="submit-page-desc">This list is tailored to make sure your homeselling experience is a success. AdvantageU provides free tools and the definitive A to Z list curated from our extensive experience to save you time time and money.</p>
                                    </div>
                                </div>
                                <input type="hidden" name="item_meta[220]" id="field_ou5wui" value="About Us" data-frmval="About Us">
                                <div class="sixcol email-section last">
                                    <div id="frm_field_181_container" class="frm_form_field form-field  frm_required_field frm_top_container">
                                        <label for="field_dy4go34" class="frm_primary_label">Email Address
                                            <span class="frm_required">*</span>
                                        </label>
                                        <input type="email" id="field_dy4go34" name="item_meta[181]" value="" placeholder="*Enter Email">

                                    </div>
                                    <div id="frm_field_182_container" class="frm_form_field form-field  frm_required_field frm_top_container">
                                        <label for="field_eqna42" class="frm_primary_label">Zip Code
                                            <span class="frm_required">*</span>
                                        </label>
                                        <input type="text" id="field_eqna42" name="item_meta[182]" value="" placeholder="*Zip Code">

                                    </div>
                                </div>
                                <input type="hidden" name="item_meta[184]" id="field_mje0dx5" value="">
                                <input type="hidden" name="item_key" value="">
                                <?php $this->registerJs('$(\'#quiz-box #frm_form_2_container\').on(\'click\', \'.select-item\', function() {
                                            $(\'.select-item\').removeClass(\'selected\');
                                            $(this).addClass(\'selected\');
                                            $(\'input[name="item_meta[104]"]\').val(this.id);
                                        });', yii\web\View::POS_LOAD) ?>

                                <div class="sixcol first submit-contain" style="text-align: right;">
                                    <div class="frm_submit">
                                        <input type="submit" value="Get List Now!">
                                        <?= Html::img(Url::to(['@uri_img/ajax_loader.gif'], true), ['alt' => 'Sending', 'class' => 'frm_ajax_loading']) ?>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>

    <div class="multi-columns text-block resource-display bot-bord acf-row acf-row-7 resource-display-about-us">
        <div class="wrap">
            <h2><em>AdvantageU</em> By the Numbers</h2>
            <div class="column-item">
                <?= Html::img(Url::to(['@uri_img/section-check.png'], true), ['alt' => 'Checkmark', 'class' => 'shadowed']) ?>
                <h5>25%</h5>
                <p>More
                    <br> Homes Sold
                    <br> in 2016</p>
            </div>
            <div class="column-item">
                <?= Html::img(Url::to(['@uri_img/section-check.png'], true), ['alt' => 'Checkmark', 'class' => 'shadowed']) ?>
                <h5>$3,500</h5>
                <p>Average
                    <br> Savings per
                    <br> Home Sold</p>
            </div>
            <div class="column-item">
                <?= Html::img(Url::to(['@uri_img/section-check.png'], true), ['alt' => 'Checkmark', 'class' => 'shadowed']) ?>
                <h5>65</h5>
                <p>AdvantageU
                    <br> Resource Experts
                    <br> at Your Disposal</p>
            </div>
            <div class="column-item">
                <?= Html::img(Url::to(['@uri_img/section-check.png'], true), ['alt' => 'Checkmark', 'class' => 'shadowed']) ?>
                <h5>90%</h5>
                <p>of Homes are Sold
                    <br> by top 10%
                    <br> Expert Agents</p>
            </div>
            <div class="column-item">
                <?= Html::img(Url::to(['@uri_img/section-check.png'], true), ['alt' => 'Checkmark', 'class' => 'shadowed']) ?>
                <h5>30 Days</h5>
                <p>Average
                    <br> Time to sell
                    <br> a Home</p>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>