<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 021 21.06.17
 * Time: 16:24
 */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

$this->title = 'Home Selling Resources | What Sells Houses | AdvantageU';

?>
<style type="text/css">
	#background-bar img{
		display: none;
	}
</style>

<div class="page">

    <div class="the-content">
        <div class="page-hero acf-row-0" style="background-image: url(/img/pages/Contact-Page-header.jpg);">
        	<?= Html::img(Url::to('@uri_img/pages/Contact-Page-header.jpg'), ['id' => 'hero-image', 'alt' => 'Contact']) ?>
        <div class="wrap"><div class="headingWrap"><h1 id="page-heading">Contact</h1></div></div></div><div style="background-image: url()" class="text-block  resource-display acf-row acf-row-1"><div class="wrap"><h2><em>Get</em><br /> IN TOUCH</h2><div><p style="text-align: center;"> Rochester, New York 14650-1002<br />
        info@advantageu.com  |  855-451-2150  |  www.AdvantageU.com</p>
    </div></div></div><div class="cta-callout testimonial-display acf-row acf-row-8 bot-bord" style="text-align: center; background-image: url('/img/pages/Blog-Page-callout.jpg');">
        <div class="wrap">
            <h2><em>Let us know</em> <span style="color: #ff9e13; display: block;">HOW WE CAN HELP.</span></h2>
            <div class="frm_forms  with_frm_style frm_style_formidable-style" id="frm_form_17_container" >
                
                <div id='crmWebToEntityForm' style='width:600px;margin:auto;'>
                   <META HTTP-EQUIV ='content-type' CONTENT='text/html;charset=UTF-8'>
                   <form action='https://crm.zoho.com/crm/WebForm'  name=WebForm2125054000005086013 method='POST' onSubmit='javascript:document.charset="UTF-8"; return checkMandatory()' accept-charset='UTF-8'>

                     <!-- Do not remove this code. -->
                    <input type='text' style='display:none;' name='xnQsjsdp' value='1f60d39d1969161d1225059c15c97bec13ad5d65632006f83c3c16e37766bdb9'/>
                    <input type='hidden' name='zc_gad' id='zc_gad' value=''/>
                    <input type='text' style='display:none;' name='xmIwtLD' value='a26254201fcf378aca39e793eb1c6f4acc0e1c31af692531f4bf171016072dfc'/>
                    <input type='text' style='display:none;'  name='actionType' value='Q3VzdG9tTW9kdWxlMg=='/>

                    <input type='text' style='display:none;' name='returnURL' value='https&#x3a;&#x2f;&#x2f;resources.advantageu.com&#x2f;thank-get-matched&#x2f;' /> 
                     <!-- Do not remove this code. -->
                    <style>
                        tr , td { 
                            padding:6px;
                            border-spacing:0px;
                            border-width:0px;
                            }
                    </style>
                    <table style='width:600px;color:black'>

                    <tr><td colspan='2' style='text-align:left;color:black;font-size:14px;'><strong>AdvantageU Contact</strong></td></tr>

                    <tr><td  style='nowrap:nowrap;text-align:left;font-size:12px;width:200px;'>AdvantageU Lead Name<span style='color:red;'>*</span></td><td style='width:250px;' ><input type='text' style='width:250px;'  maxlength='120' name='NAME' /></td></tr>

                    <tr><td  style='nowrap:nowrap;text-align:left;font-size:12px;width:200px;'>Email</td><td style='width:250px;' ><input type='text' style='width:250px;'  maxlength='100' name='Email' /></td></tr>

                    <tr><td  style='nowrap:nowrap;text-align:left;font-size:12px;width:200px;'>Address</td><td style='width:250px;' ><input type='text' style='width:250px;'  maxlength='255' name='COBJ2CF2' /></td></tr>

                    <tr><td  style='nowrap:nowrap;text-align:left;font-size:12px;width:200px;'>City</td><td style='width:250px;' ><input type='text' style='width:250px;'  maxlength='255' name='COBJ2CF3' /></td></tr>

                    <tr><td  style='nowrap:nowrap;text-align:left;font-size:12px;width:200px;'>Zipcode</td><td style='width:250px;' ><input type='text' style='width:250px;'  maxlength='5' name='COBJ2CF51' /></td></tr>

                    <tr><td  style='nowrap:nowrap;text-align:left;font-size:12px;width:200px;'>Comments </td><td> <textarea name='COBJ2CF7' maxlength='2000' style='width:250px;'>&nbsp;</textarea></td></tr>

                    <tr style='display:none;' ><td style='nowrap:nowrap;text-align:left;font-size:12px;width:50%'>How did you hear about us&#x3f;</td><td style='width:250px;'>
                        <select style='width:250px;' name='COBJ2CF9'>
                            <option value='-None-'>-None-</option>
                            <option value='AdvantageU&#x20;Advocate'>AdvantageU Advocate</option>
                            <option selected value='AdvantageU&#x20;Website'>AdvantageU Website</option>
                            <option value='Postcard'>Postcard</option>
                            <option value='Google'>Google</option>
                            <option value='Facebook'>Facebook</option>
                            <option value='YouTube'>YouTube</option>
                            <option value='Real&#x20;Estate&#x20;Agent'>Real Estate Agent</option>
                            <option value='Friend'>Friend</option>
                            <option value='Campaign&#x20;More&#x20;Space'>Campaign More Space</option>
                            <option value='Campaign&#x20;The&#x20;Next&#x20;Milestone'>Campaign The Next Milestone</option>
                            <option value='Campaign&#x20;Thinking&#x20;About&#x20;Success'>Campaign Thinking About Success</option>
                            <option value='Campaign&#x20;Ultimate&#x20;Home&#x20;Seller&#x20;Checklist'>Campaign Ultimate Home Seller Checklist</option>
                            <option value='Campaign&#x20;Boy&#x20;and&#x20;Dog'>Campaign Boy and Dog</option>
                            <option value='Campaign&#x20;What&#x20;Matters&#x20;Most'>Campaign What Matters Most</option>
                            <option value='Campaign&#x20;Common&#x20;Mistakes'>Campaign Common Mistakes</option>
                            <option value='Campaign&#x20;Reveal&#x20;Possibilities'>Campaign Reveal Possibilities</option>
                            <option value='Campaign&#x20;Advantage&#x20;u&#x20;promo&#x20;video&#x20;1'>Campaign Advantage u promo video 1</option>
                            <option value='Campaign&#x20;Get&#x20;Matched'>Campaign Get Matched</option>
                            <option value='Campaign&#x20;Request&#x20;Call&#x20;Back'>Campaign Request Call Back</option>
                            <option value='Campaign&#x20;AU&#x20;Call&#x20;Center'>Campaign AU Call Center</option>
                        </select></td></tr>

                    <tr><td colspan='2' style='text-align:center; padding-top:15px;'>
                        <input style='font-size:12px;color:#131307' type='submit' value='Submit' />
                        <!--<input type='reset' style='font-size:12px;color:#131307' value='Reset' />-->
                        </td>
                    </tr>
                   </table>
                    <script>
                      var mndFileds=new Array('NAME');
                      var fldLangVal=new Array('AdvantageU Lead Name');
                        var name='';
                        var email='';

                      function checkMandatory() {
                        for(i=0;i<mndFileds.length;i++) {
                          var fieldObj=document.forms['WebForm2125054000005086013'][mndFileds[i]];
                          if(fieldObj) {
                            if (((fieldObj.value).replace(/^\s+|\s+$/g, '')).length==0) {
                             if(fieldObj.type =='file')
                                { 
                                 alert('Please select a file to upload.'); 
                                 fieldObj.focus(); 
                                 return false;
                                } 
                            alert(fldLangVal[i] +' cannot be empty.'); 
                              fieldObj.focus();
                              return false;
                            }  else if(fieldObj.nodeName=='SELECT') {
                             if(fieldObj.options[fieldObj.selectedIndex].value=='-None-') {
                                alert(fldLangVal[i] +' cannot be none.'); 
                                fieldObj.focus();
                                return false;
                               }
                            } else if(fieldObj.type =='checkbox'){
                             if(fieldObj.checked == false){
                                alert('Please accept  '+fldLangVal[i]);
                                fieldObj.focus();
                                return false;
                               } 
                             } 
                             try {
                                 if(fieldObj.name == 'Last Name') {
                                name = fieldObj.value;
                                }
                            } catch (e) {}
                            }
                        }
                         }
                       
                </script>
                    </form>
                </div>
            </div>
            <p><strong>Advantage<span style="color: #ff9e13;">U</span></strong> provides free tools and the definitive A to Z list curated<br />
                from our extensive experience to save you time time and money.</p>
        </div>

    </div>
    </div>
</div>