<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 021 21.06.17
 * Time: 16:24
 */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

$this->title = 'Checklist for Selling a Home | How to Sell Your House Fast';

?>
<style type="text/css">
	#background-bar img{
		display: none;
	}
</style>
<div class="page">

		<div class="the-content">
			<div class="page-hero acf-row-0" style="background-image: url(/img/pages/Checklist-Page-2.jpg);">
				<?= Html::img(Url::to(['@uri_img/pages/Checklist-Page-2.jpg'], true), ['id' => 'hero-image', 'alt' => 'Checklist for Selling a House']) ?>
			<div class="wrap">
			<div class="headingWrap">
			<h1 id="page-heading">Checklist for Selling a House</h1>
			</div>
			</div>
			</div>
			<div style="background-image: url()" class="text-block  resource-display acf-row acf-row-1">
			<div class="wrap">
			<h2>Your Customized List Includes</h2>
			<div>
			</div>
			<div class="spacer" style="height: 4em;">
			</div>
			</div>
			</div>
			<div class="lift-col four-column-display resource-display acf-row no-top-pad acf-row-2">
			<div class="wrap">
			<div class="threecol short-col">
			<?= Html::img(Url::to(['@uri_img/pages/Checklist-Page-1-1.jpg'], true), ['alt' => 'sell your using the home selling checklist']) ?>
			<h5>Preparing for the Sale</h5>
			<p>Get asked the right questions to prepare for the home selling process – including expense calculations, selling options, and home staging.</p>
			</div>
			<div class="threecol short-col">
			<?= Html::img(Url::to(['@uri_img/pages/Checklist-Page-2-2.jpg'], true), ['alt' => 'sell your house at the right price']) ?>
			<h5>Pricing Your Home</h5>
			<p>Consider which documents, records, and reports will help determine your home’s fair market value (FMV) in order to set a marketable price.</p>
			</div>

			<div class="threecol short-col">
			<?= Html::img(Url::to(['@uri_img/pages/Checklist-Page-3-1.jpg'], true), ['alt' => 'sell my home']) ?>
			<h5>NEGOTIATING THE SALE</h5>
			<p>Think through options that will help you negotiate the best possible price for your home. Maintain asking price by offering buyer incentives.</p>
			</div>


			<!--<h5>Negotiating the Sale</h5>
			<p>Think through options that will help you negotiate the best possible price for your home. Maintain asking price by offering buyer incentives.</p></div>-->
			<div class="clearfix">
			</div>
			</div>
			</div>
			<div class="text-callout acf-row acf-row-4">
			<div class="wrap">
			<h3>
			<div style="text-align: center;">Get the World’s Most Complete List of How to  Sell Your Home Quickly and for fair market value.</div>
			</h3>
			</div>
			<div style="background-image: url(/img/pages/large-heading-back.jpg)" class="back-cover"></div>
			</div>
			</div>
			<div style="background-image: url(/img/pages/Boy-Dad-Parallax.jpg)" class="split-text-block resource-display acf-row acf-row-5">
			<div class="wrap close-wrap">
			<h5>Ultimate Home Seller's Checklist ZOHO form</h5>
			<div class="sixcol first"><p>

			<?= Html::img(Url::to(['@uri_img/pages/Couple-Hugging.jpg'], true), [
				'alt' => 'sell your house the market value of home',
				'width' => '852',
				'height' => '549',
				'srcset' => '/img/pages/Couple-Hugging.jpg 852w, /img/pages/Couple-Hugging-300x193.jpg 300w, /img/pages/Couple-Hugging-768x495.jpg 768w',
				'sizes' => '(max-width: 852px) 100vw, 852px'
				] ) ?>
			</p>
<p style="line-height: 1.75;"><strong>Avoid Mistakes and Maximize Value</strong><br />
The Ultimate Home Sellers’ Checklist helps you avoid mistakes and maximize value when selling your home. This checklist for selling a house will guide you in weighing the pros and cons of selling, and calculating selling expenses. It will also walk you through data to determine your home’s fair market value (FMV), and advise you on how to maintain the price you want during negotiations. Start marking off your way through a successful home sale!</p>
</div>
<div class="sixcol last"><p style="line-height: 1.75;"><strong>Stay on Track and Sell Fast</strong><br />
AdvantageU is excited to offer you the ultimate checklist for selling your home. This powerful resource will guide you through an exhaustive list of instructions, expectations, and ideas you can use to check off online or download as you work to sell your home. This resource will organize the home selling process for you so that you can be confident without missing a single step. Allow this list to keep you on track to get your home sold as fast as possible.</p>
<p>
	<?= Html::img(Url::to(['@uri_img/pages/Jumping-Asian-Couple.jpg'], true), ['alt' => 'Checklist-Page-b', 'class' => 'alignnone size-full wp-image-367']) ?>
</p>
</div>
<div class="clearfix"></div></div></div>
<div class="cta-callout testimonial-display acf-row acf-row-8 bot-bord" style="text-align: center;
    background-image: url(/img/pages/Blog-Page-callout.jpg);">
<div class="frm_forms  with_frm_style frm_style_formidable-style" id="frm_form_17_container">
	<!-- Note :
   - You can modify the font style and form style to suit your website.
   - Code lines with comments “Do not remove this code”  are required for the form to work properly, make sure that you do not remove these lines of code.
   - The Mandatory check script can modified as to suit your business needs.
   - It is important that you test the modified form before going live.-->

   <h5>Get the Ultimate Home Sellers' Checklist</h5><br/>
<div id='crmWebToEntityForm' style='width:600px;margin:auto;'>
   <META HTTP-EQUIV ='content-type' CONTENT='text/html;charset=UTF-8'>
   <form style="background-image: url(/img/pages/Blog-Page-callout.jpg);" action='https://crm.zoho.com/crm/WebForm'  name=WebForm2125054000005121001 method='POST' onSubmit='javascript:document.charset="UTF-8"; return checkMandatory()' accept-charset='UTF-8'>

	 <!-- Do not remove this code. -->
	<input type='text' style='display:none;' name='xnQsjsdp' value='1f60d39d1969161d1225059c15c97bec13ad5d65632006f83c3c16e37766bdb9'/>
	<input type='hidden' name='zc_gad' id='zc_gad' value=''/>
	<input type='text' style='display:none;' name='xmIwtLD' value='a26254201fcf378aca39e793eb1c6f4a2238c60f95d28a8c1c6344702f59a0dd'/>
	<input type='text' style='display:none;'  name='actionType' value='Q3VzdG9tTW9kdWxlMg=='/>

	<input type='text' style='display:none;' name='returnURL' value='https&#x3a;&#x2f;&#x2f;resources.advantageu.com&#x2f;downloads&#x2f;AdvantageU-A-Z-Ultimate-Checklist.pdf' />
	 <!-- Do not remove this code. -->
	<style>
		tr , td {
			padding:20px;
			border-spacing:0px;
			border-width:0px;
			}
	</style>



	<table style='width:600px'>



	<tr><td  style='nowrap:nowrap;text-align:left;font-size:12px;font-family:Arial;width:200px;'>Name<span style='color:red;'>*</span></td><td style='width:250px;' ><input type='text' style='width:250px;'  maxlength='120' name='NAME' /></td></tr>

	<tr><td  style='nowrap:nowrap;text-align:left;font-size:12px;font-family:Arial;width:200px;'>Mobile</td><td style='width:250px;' ><input type='text' style='width:250px;'  maxlength='30' name='COBJ2CF6' /></td></tr>

	<tr><td  style='nowrap:nowrap;text-align:left;font-size:12px;font-family:Arial;width:200px;'>Email</td><td style='width:250px;' ><input type='text' style='width:250px;'  maxlength='100' name='Email' /></td></tr>

	<tr><td  style='nowrap:nowrap;text-align:left;font-size:12px;font-family:Arial;width:200px;'>Zipcode</td><td style='width:250px;' ><input type='text' style='width:250px;'  maxlength='5' name='COBJ2CF51' /></td></tr>

	<tr style='display:none;' ><td style='nowrap:nowrap;text-align:left;font-size:12px;font-family:Arial;width:50%'>How did you hear about us&#x3f;</td><td style='width:250px;'>
		<select style='width:250px;' name='COBJ2CF9'>
			<option value='-None-'>-None-</option>
			<option value='AdvantageU&#x20;Advocate'>AdvantageU Advocate</option>
		<option selected value='AdvantageU&#x20;Website'>AdvantageU Website</option>
			<option value='Postcard'>Postcard</option>
			<option value='Google'>Google</option>
			<option value='Facebook'>Facebook</option>
			<option value='YouTube'>YouTube</option>
			<option value='Real&#x20;Estate&#x20;Agent'>Real Estate Agent</option>
			<option value='Friend'>Friend</option>
			<option value='Campaign&#x20;More&#x20;Space'>Campaign More Space</option>
			<option value='Campaign&#x20;The&#x20;Next&#x20;Milestone'>Campaign The Next Milestone</option>
			<option value='Campaign&#x20;Thinking&#x20;About&#x20;Success'>Campaign Thinking About Success</option>
			<option value='Campaign&#x20;Ultimate&#x20;Home&#x20;Seller&#x20;Checklist'>Campaign Ultimate Home Seller Checklist</option>
			<option value='Campaign&#x20;Boy&#x20;and&#x20;Dog'>Campaign Boy and Dog</option>
			<option value='Campaign&#x20;What&#x20;Matters&#x20;Most'>Campaign What Matters Most</option>
			<option value='Campaign&#x20;Common&#x20;Mistakes'>Campaign Common Mistakes</option>
			<option value='Campaign&#x20;Reveal&#x20;Possibilities'>Campaign Reveal Possibilities</option>
			<option value='Campaign&#x20;Advantage&#x20;u&#x20;promo&#x20;video&#x20;1'>Campaign Advantage u promo video 1</option>
			<option value='Campaign&#x20;Get&#x20;Matched'>Campaign Get Matched</option>
			<option value='Campaign&#x20;Request&#x20;Call&#x20;Back'>Campaign Request Call Back</option>
			<option value='Campaign&#x20;AU&#x20;Call&#x20;Center'>Campaign AU Call Center</option>
		</select></td></tr>

	<tr><td colspan='2' style='text-align:center; padding-top:15px;'>
		<input type='submit' value='Download Now' class="linkButton" />
	    </td>
	</tr>
   </table>
	<script>
 	  var mndFileds=new Array('NAME');
 	  var fldLangVal=new Array('AdvantageU Lead Name');
		var name='';
		var email='';

 	  function checkMandatory() {
		for(i=0;i<mndFileds.length;i++) {
		  var fieldObj=document.forms['WebForm2125054000005121001'][mndFileds[i]];
		  if(fieldObj) {
			if (((fieldObj.value).replace(/^\s+|\s+$/g, '')).length==0) {
			 if(fieldObj.type =='file')
				{
				 alert('Please select a file to upload.');
				 fieldObj.focus();
				 return false;
				}
			alert(fldLangVal[i] +' cannot be empty.');
   	   	  	  fieldObj.focus();
   	   	  	  return false;
			}  else if(fieldObj.nodeName=='SELECT') {
  	   	   	 if(fieldObj.options[fieldObj.selectedIndex].value=='-None-') {
				alert(fldLangVal[i] +' cannot be none.');
				fieldObj.focus();
				return false;
			   }
			} else if(fieldObj.type =='checkbox'){
 	 	 	 if(fieldObj.checked == false){
				alert('Please accept  '+fldLangVal[i]);
				fieldObj.focus();
				return false;
			   }
			 }
			 try {
			     if(fieldObj.name == 'Last Name') {
				name = fieldObj.value;
 	 	 	    }
			} catch (e) {}
		    }
		}
	     }

</script>
	</form>
</div>
</div>
<p><strong>Advantage<span style="color: #ff9e13;">U</span></strong> provides free tools and the definitive A to Z list curated<br />
                from our extensive experience to save you time time and money.</p>
</div>

<!-- <div style="background-image: url(/img/pages/check-bg.jpg)" class="triple-cta resource-display acf-row acf-row-6">
<div class="wrap"><h2>Do you know how you want to sell your home?</h2>
<div class="fourcol first"><a class="linkButton" href="#">USE AGENT</a></div><div class="fourcol "><a class="linkButton" href="#">ON YOUR OWN</a></div><div class="fourcol "><a class="linkButton" href="#">I DON’T KNOW</a><span class="lux">

<?= Html::img(Url::to(['@uri_img/pages/info.jpg'], true), ['alt' => 'Info', 'class' => 'alignnone size-full wp-image-367']) ?>
<strong>Take Our Quiz</strong><br />to find out if you need an agent.</span></div>
<div class="clearfix"></div></div></div><span class="anchor" id="checklist-quiz"></span>
</div></div> -->
<div class="text-callout acf-row acf-row-5">
<div class="wrap"><h3><strong>90%</strong> OF HOMES ARE SOLD BY THE TOP <strong>10%</strong> OF AGENTS</h3><a href="#">Learn More</a></div><div style="background-image: url(/img/pages/AdvantageU-CTA.jpg)" class="back-cover"></div></div></div>
<div style="background-image: url(/img/pages/AdvantageU-Checkmark.jpg)" class="resource-display acf-row acf-row-6"><div class="wrap"><h2>homesellers’ Resources PICKED especially for you.</h2><h3>How to sell fast and find the right agent.</h3><div class="fourcol first"><div class="eightcol first"><a href="#">
	<?= Html::img(Url::to(['@uri_img/pages/advantageu-home-interior--240x105.jpg'], true), ['alt' => 'market value of home']) ?>
</a></div><div class="fourcol last"><h4><a href="#">8 Improvements that  Increase the Market Value of Your Home</a></h4></div><div class="clearfix"></div><div class="section-content">If you’re preparing to sell your house – maybe your family is expanding and it’s time to start looking for a house with more room! – you need to analyze how you can increase the market value of your home. You first need to create a budget on how much money you can spend on...</div></div><div class="fourcol "><div class="eightcol first"><a href="#">
	<?= Html::img(Url::to(['@uri_img/pages/33207185_xxl-240x105.jpg'], true), ['alt' => 'show your house']) ?>
</a></div><div class="fourcol last"><h4><a href="#">How to Choose Tasteful Fall Decorations When You Show your House</a></h4></div><div class="clearfix"></div><div class="section-content">It’s time for you to move – maybe your employer is relocating you to a different office – so your current house is for sale and your listing is in place. Now it’s time to show your house. You want your home to be inviting, but Halloween and Thanksgiving are just around the corner and...</div></div><div class="fourcol last"><div class="eightcol first"><a href="#">
	<?= Html::img(Url::to(['@uri_img/pages/advantageu-contract-shaking-hands--240x105.jpg'], true), ['alt' => 'listing agreement']) ?>
</a></div><div class="fourcol last"><h4><a href="#">How to Avoid Being Handcuffed By a Real Estate Listing Agreement</a></h4></div><div class="clearfix"></div><div class="section-content">When you’re ready to sell your home, you’ll eventually meet with a real estate agent who’s going to offer you a listing agreement. The listing agreement is a contract that legally binds you to sell with that particular agent for a specified amount of time. Most states offer a standard form that the agent and...</div></div><div class="clearfix"></div><div style="text-align: center; margin-top: 4em;"><a href="#" class="linkButton">More Resources</a></div></div></div><div class="text-callout testimonial-display acf-row acf-row-7"><div class="wrap"><div class="cycle-slideshow" data-cycle-slides="> div"><div><h2 class="testimonial-content">They provided incredible service! They sold my home in 14 days and saved me $3,514.</h2><h2 class="testimonial-name">&#8212; Judy D’Ambrose, Webster NY</h2></div><div><h2 class="testimonial-content">They sold my house in 4 days and I was able to pocket an extra $5,713.</h2><h2 class="testimonial-name">&#8212; Steve Johnston, Greece NY</h2></div><div><h2 class="testimonial-content">They sold our home in 18 days, we saved money and they helped us purchase our new home.</h2><h2 class="testimonial-name">&#8212; Mike and Diane Osewalt, Rochester NY</h2></div><div><h2 class="testimonial-content">We received a contract for sale in 4 days of signing up and saved $2,250.</h2><h2 class="testimonial-name">&#8212; Rick and Suzy Kasper, Rush NY</h2></div><div><h2 class="testimonial-content">I was planning to move from NJ to join my family in Atlanta, GA. I’d lived in my home for 20 </h2><h2 class="testimonial-name">&#8212; Paula, Atlanta GA</h2></div><div><h2 class="testimonial-content">I was happy with the advantageU matching service and how much professionalism the agent Bobby s</h2><h2 class="testimonial-name">&#8212; John P. Penfield, NY</h2></div><div><h2 class="testimonial-content">I used the advantage U matching service, I appreciated the concept and would recommend the free</h2><h2 class="testimonial-name">&#8212; Alan H.  De Pere, WI</h2></div><div><h2 class="testimonial-content">I was happy with the program and I would definitely recommend AdvantageU service. I was able to</h2><h2 class="testimonial-name">&#8212; Senarath B. Antelope, CA</h2></div><div><h2 class="testimonial-content">I was very pleased with this service. My house was on the market for 50 days with no serious of</h2><h2 class="testimonial-name">&#8212; John M. Cedar Rapids, IA</h2></div><div><h2 class="testimonial-content">Before using the free AdvantageU service I tried  For Sale By Owner for 1 week because I neede</h2><h2 class="testimonial-name">&#8212; Dan B. Grand Rapids, MI</h2></div><div><h2 class="testimonial-content">I was happy with Advantage U setting me up with John and I would recommend him to other people </h2><h2 class="testimonial-name">&#8212; Robert W. Tallahassee, FL</h2></div><div><h2 class="testimonial-content">The service provided to me by Advantage U was good and I am glad they picked out TJ. I was not </h2><h2 class="testimonial-name">&#8212; Crystal H. Myrtle Beach, SC</h2></div><div><h2 class="testimonial-content">I would recommend Advantage U to friends and family. Once I spoke with Will I knew I would give</h2><h2 class="testimonial-name">&#8212; Ondra M. Miramar, FL</h2></div><div><h2 class="testimonial-content">Advantage you had very great timing when they called me and I felt I was qualified very well. J</h2><h2 class="testimonial-name">&#8212; Fang H. Medford, OR</h2></div><div><h2 class="testimonial-content">I was looking at 4 other agents when Advantage U called and matched me with John. We had to mak</h2><h2 class="testimonial-name">&#8212; Jack B. Roberts, WI</h2></div><div><h2 class="testimonial-content">When we received a phone call from Advantage U we were trying to close on our house on our own.</h2><h2 class="testimonial-name">&#8212; Wendell H. Mechanicsville, VA</h2></div><div><h2 class="testimonial-content">Advantage U called after I was selling my home FSBO for two weeks and had low offers. After bec</h2><h2 class="testimonial-name">&#8212; Matthew K. Charlotte NC</h2></div><div><h2 class="testimonial-content">I started to sell my home FSBO and had no one come to any showings. My phone was ringing off th</h2><h2 class="testimonial-name">&#8212; Adam P. Mauldin, SC</h2></div><div><h2 class="testimonial-content">I started as a FSBO before meeting Joe W. through Advantage U. He convinced me that to really n</h2><h2 class="testimonial-name">&#8212; Travis K. Wake Forest , NC</h2></div><div><h2 class="testimonial-content">Since we were not in the area we thought we should sell our house. After putting on zillow we s</h2><h2 class="testimonial-name">&#8212; Rebecca. Smithville, TX</h2></div><div><h2 class="testimonial-content">My name is Marlene and I did speak with the AdvantageU rep Robin and also the realtor Christina</h2><h2 class="testimonial-name">&#8212; Marlene S.</h2></div></div></div><div style="background-image: url(/img/pages/AdvantageU-CTA.jpg)" class="back-cover"></div></div></div><div style="background-image: url()" class="split-text-block resource-display acf-row acf-row-8"><div class="wrap "><h2>Here is where</h2><h3>AdvantageU Home Sellers’ Resource can really help.</h3><div class="sixcol first"><p>We are a Consumer Advocate for home sellers. We cut through all the advertising hype and over abundance of agents to <strong>identify only the best</strong>. We use our extensive knowledge of the real estate market to find, interview and evaluate top agents all across the country.</p>
</div><div class="sixcol last"><p>We don&#8217;t use impersonal computer algorithms to find your perfect agent. We have a live seller-support team dedicated to personally understanding your unique needs. Then they develop a list of prequalified, top agents in your area who match your needs and introduce those agents to you! Only the best.</p>
</div><div class="clearfix"></div><div style="text-align: center; margin-top: 2em;"><a href="#" class="linkButton">GET MATCHED</a></div></div></div>		</div>
	</div>
