<?php 
use yii\bootstrap\ActiveForm;
use yii\widgets\Pjax;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\MaskedInput;
use kartik\rating\StarRating;
use rmrevin\yii\fontawesome\FA;
?>
<?php 

function showModal($id, &$view_obj)
{
	if ( isset(\Yii::$app->request->queryParams['modal_after_pjax']) && \Yii::$app->request->isPjax && \Yii::$app->request->queryParams['modal_after_pjax'] == $id ){
		$view_obj->registerJs('$("body").css("padding-right", "0px");
    		$("body").removeClass("modal-open");
    		$(".modal-backdrop").remove();
    		$("#'.$id.'").modal("show")');
		return true;
	} 
	else return false;
}
?>


<?php foreach ($sections as $key => $section) : ?>
	<div class="modal fade edit_section" id="edit_section_<?= $key ?>" tabindex="-1" role="dialog" data-related_section="section_<?= $key ?>">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<?php Pjax::begin() ?>
					<div class="modal-body">
						<h2><?= $section->name ?></h2>
						
						<ul class="section_values">
							<?php foreach ($section->getSectionValues()->where(['id_user' => $user_model->id])->indexBy('id')->asArray()->all() as $k => $section_value) : ?>
								<li>
									<?= $section_value['value'] ?>
									<?= Html::a('delete', Url::toRoute(['agent/delete-section-value', 'id' => $k], true), ['data-method' => 'POST', 'data-pjax' => 'true', 'class' => 'remove_link']) ?>
								</li>
							<?php endforeach; ?>
						</ul>
					
						<?php $form = ActiveForm::begin([    
								'action' => Url::toRoute(['agent/profile', 'id' => $user_model->id], true),                  
					            'options' => [				                
					                'tag' => 'false',
					                'data-pjax' => 'true',
					                'class' => ''
					            ],
					        ]) ?>
					        <?= $form->field($section_value_model, 'value')->textInput()->label('New certification') ?>
					        <div class="hidden">
					        	<?= $form->field($section_value_model, 'id_section', ['template' => '{input}'])->hiddenInput(['class' => 'hidden', 'value' => $key])->label(false); ?>
					        </div>

					        <?= Html::submitButton('Add', ['class' => 'main-button primary', 'data-pjax' => 'true']) ?>
					        <button type="button" class="main-button close_after_submit" data-dismiss="modal">Done</button>		

				       	<?php ActiveForm::end() ?>						 
					</div>
				<?php Pjax::end() ?>  
			</div>
		</div>
	</div>
<?php endforeach; ?>


<!-- Modal Success Popup -->
<div class="modal fade success-popup" id="success" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
				<h4 class="modal-title" id="myModalLabel">Thank You !</h4>
			</div>
			<div class="modal-body text-center">
			 	
			 	<?= Html::img(Url::toRoute(['@uri_img/check-true.jpg'], true)) ?>
			  	<p class="lead"><?= (\Yii::$app->user->identity && \Yii::$app->user->identity->isOwner()) ? 'Information successfully saved.' : 'Your message successfully sent.' ?> Thank you!</p>
			  	<div class="submit-buttons" style="margin-top: 30px;">
			  		<button class="main-button small" data-dismiss="modal">Close</button>
			  	</div>
			</div>
		  
		</div>
	</div>
</div>
