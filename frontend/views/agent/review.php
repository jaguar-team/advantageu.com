<?php

use yii\bootstrap\ActiveForm;

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
use yii\widgets\MaskedInput;
use kartik\rating\StarRating;
use rmrevin\yii\fontawesome\FA;

$user_profile_model = $user_model->profile;

$this->registerCssFile('@web/css/profile.css');

$starCaptions = [];
for ($i = 0; $i <= 50; $i++){
	$starCaptions[(string)($i/10)] = ((double)$i/10).' out of 5 stars';
}

$reviews = $user_model->getReviews()->select('*')->asArray()->all();

$is_owner = (!\Yii::$app->user->isGuest && $user_model->isOwner());
$is_matched = true || $is_owner;

function getAddresses($addresses_arr, $is_matched){
	$i = 1;
	$addresses = '';
	foreach ($addresses_arr as $key => $address) {
		if ($i < count($addresses_arr)) {
			
			$addresses.= ($is_matched ? $address['address'] : 'XXXXXXXXXX').', '.$address['city'].', '.$address['short_name'].', '.($is_matched ? $address['zip_code']: 'XXXXXX').'; ';

		}
		else $addresses.= ($is_matched ? $address['address'] : 'XXXXXXXXXX').', '.$address['city'].', '.$address['short_name'].', '.($is_matched ? $address['zip_code']: 'XXXXXX');
		$i++;
	} 
	return $addresses;
}

$addresses = getAddresses($user_model->addresses, $is_matched);

$this->title = 'Add review';
$this->params['breadcrumbs'][] = ['label' => 'User profile', 'url' => ['agent/profile', 'id' => $user_model->id]];
$this->params['breadcrumbs'][] = $this->title;
?>

<section class="profile review_page">

		<div class="container">
			<?php echo $this->render('/elements/_breadcrumbs', array()); ?>
		</div>

		<?= $this->render('/elements/_profile_header', [
			'user_model'			=> $user_model,
			'user_profile_model' 	=> $user_profile_model,
			'is_owner'				=> $is_owner,
			'is_matched'			=> $is_matched,
			'addresses'				=> $addresses
		]) ?>

		<div class="profile_body">
			<div class="container">
				<div class="row">
					<div class="col-sm-8">
				    	<?php $form = ActiveForm::begin([
							'options' => [
								'data-pjax' => 'true',
								'validateOnBlur' => false,
								'class' => 'new_review'
							]
						]) ?>
							<h3 id="new_review_container">New Review</h3>
							<div class="row">
								<div class="col-sm-6">
									<?= $form->field($review_model, 'star')->widget(StarRating::className(), [
										'class' => 'inline_rating',
										'pluginOptions' => [
										    	'step' => 0.1,
										        'showClear' => false,
										        'filledStar' => Html::decode( FA::icon('star') ),
												'emptyStar' => Html::decode( FA::icon('star-o') ),
												'size' => 'sm',										
												'starCaptions' => $starCaptions
										    ],
									])->label(false) ?>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-6">
									<?= $form->field($review_model, 'email')->widget(MaskedInput::className(), [
										'options' => [
									    	'placeholder' => 'jane@smith.com',
									    	'class' => 'form-control',
									    	'value' => ''
									    ],
									    'clientOptions' => [
									    	'alias' => 'email',
									    ]									
									])->label(false) ?>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-6">
									<?= $form->field($review_model, 'first_name')->textInput(['placeholder' => 'Jane', 'value' => ''])->label(false) ?>
								</div>
								<div class="col-sm-6">
									<?= $form->field($review_model, 'last_name')->textInput(['placeholder' => 'Smith', 'value' => ''])->label(false) ?>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-12">
									<?= $form->field($review_model, 'title')->textInput(['placeholder' => 'Title', 'value' => ''])->label(false) ?>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-12">
									<?= $form->field($review_model, 'body')->textarea(['rows' => 5, 'placeholder' => 'Your message..', 'value' => ''])->label(false) ?>
								</div>
							</div>
							<?= Html::submitButton('Submit review', ['class' => 'reviews_btn pull-right']) ?>
						<?php ActiveForm::end() ?>
					</div>

					<div class="sidepanel col-sm-4">

						<?= $this->render('/elements/_sidepanel', [
							'user_model'			=> $user_model,
							'user_profile_model' 	=> $user_profile_model,
							'is_owner'				=> $is_owner,
							'is_matched'			=> $is_matched
						]) ?>

					</div>

				</div>
			</div>
		</div>
</section>

<?= $this->render('_contact_agent', [
	'user_model' 				=> $user_model,
	'user_profile_model' 		=> $user_profile_model,
	
	'agent_contact_model'		=> $agent_contact_model,
]); ?>