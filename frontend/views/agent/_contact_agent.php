<?php 
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\MaskedInput;
use rmrevin\yii\fontawesome\FA;
?>

<div class="modal fade reviews" id="contact_agent" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-head">
				<?= Html::img($user_profile_model->getPhotoUrl()) ?>
				<h3>Contact <?= Html::encode($user_profile_model->first_name) ?></h3>
			</div>
			<div class="modal-body">
				<div class="left-content">
					<p>AdvantageU will connect you with <?= Html::encode($user_profile_model->first_name) ?></p>
					<p>To jump-start the process, one of our Consumer Advocates will assess your needs and immediately relay that information to the real estate agent. That helps ensure your agent understands your preferences and parameters right from the start, making the process more efficient and effective.</p>
					<div class="accordion" id="accordion" role="tablist" aria-multiselectable="true">
					  	<div class="block">
						    <div class="heading" role="tab" id="headingOne">
						      	<h4>
							        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
							          	AdvantageU certified Agent
							          	<div class="icon">
								          	<?= FA::icon('chevron-down') ?>
								          	<?= FA::icon('chevron-up') ?>
								        </div>
							        </a>
						      	</h4>
						    </div>
						    <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
						      	<p>
						        	To be successful, a real estate agent must have the latest knowledge and skills in today's competitive market. They must be able to engage with their clients, be up-to-the-minute on the latest market dynamics, have a fine tuned network and be negotiation powerhouses. In short, they must stand above and beyond the average agent.
						      	</p>
						      	<p>
						      		The AdvantageU Certification provides recognition for those agents who have a proven record of providing exceptional srevice. The AdvantageU criteria is set by a team of real estate professionals and is quickly becoming recognized and trusted by consumers nationwide.
						      	</p>
						      	<p>
						      		Agents certified by AdvantageU will receive and display our exclusive logo indicating their certified status and our recognition of their knowledge, skills and attention to excellent client service.
						      	</p>
						    </div>
					  	</div>

					  	<div class="block">
						    <div class="heading" role="tab" id="headingTwo">
						      	<h4>
							        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseOne">
							          	FREE Resources
							          	<div class="icon">
								          	<?= FA::icon('chevron-down') ?>
								          	<?= FA::icon('chevron-up') ?>
								        </div>
							        </a>
						      	</h4>
						    </div>
						    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
						      	<p>
						        	Here is a list of free resources:
						        	<?= Html::a('Home AU Sellers\' Blog',  Url::toRoute(['resources/index'], true)) ?>
						        	<?= Html::a('Ultimate Home Sellers\' Checklist',  Url::toRoute(['ultimate-checklist/index'], true)) ?>
						        	<a href="http://advantageu.com/magazine">Home Sellers' Magazine</a>
						        	<a href="http://advantageu.com/calculators">Real Estate Calculators</a>
						      	</p>
						    </div>
					  	</div>
					  
					</div>
					<div class="security">
						<p>We take user privacy seriously. Your data is 100% secure with us.</p>
						<?= Html::img('@uri_img/secure-icon.png') ?>
						<div class="no-spam-wrap">
							<i class="hi hi-ban"></i>
							<span>NO SPAM</span>
						</div>
					</div>
					<p class="immediate-service">Consult with an Advocate: 9am - 5m EST</p>
					<a href="https://advantageu.com/about-us/">About Advantage</a>

				</div>
				<div class="right-content">
					<?php $form =  ActiveForm::begin([
						'action' => Url::toRoute(['agent/profile', 'id' => $user_model->id], true),
						'fieldConfig' => [
							'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",							
						],
						'options' => [
							'data-pjax' => 'true',
							'class' => 'col-md-12 form-horizontal'
						]						
					]) ?>		
						<?= $form->field($agent_contact_model, 'full_name')->textInput(['placeholder' => 'Jane Smith'])->label(false) ?>

						<?= $form->field($agent_contact_model, 'email')->widget(MaskedInput::className(), [
						    'clientOptions' => [
						    	'alias' => 'email',
						    ],
						    'options' => [
						    	'placeholder' => 'jsmith@gmail.com',
						    	'class' => 'form-control'
						    ]
						])->label(false) ?>

						<?= $form->field($agent_contact_model, 'phone')->widget(MaskedInput::className(), [
						 	'mask' => '(999)999-9999',
						 	'options' => [
						    	'placeholder' => '(888)867-5309',
						    	'class' => 'form-control'
						    ]
						])->label(false) ?>

						
						<?= $form->field($agent_contact_model, 'message')->textarea(['rows' => 5, 'placeholder' => 'Hi, '.Html::encode($user_profile_model->getFullName()).', I saw your profile on AdvantageU and I\'d like to schedule a time to talk.'])->label(false) ?>

						<?= Html::submitButton('Send', ['class' => 'main-button orange small']) ?>
						<button class="main-button small" data-dismiss="modal">Cancel</button>

					<?php ActiveForm::end() ?>
				</div>
			</div>
		</div>
	</div>
</div>