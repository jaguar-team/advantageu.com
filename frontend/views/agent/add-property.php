<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 029 29.06.17
 * Time: 15:52
 */

use yii\bootstrap\ActiveForm;

use yii\helpers\Html;
use yii\helpers\Url;

use yii\widgets\Pjax;
use yii\widgets\MaskedInput;

use rmrevin\yii\fontawesome\FA;

use common\models\PropertyType;

$property_types = [];
foreach (PropertyType::find()->asArray()->all() as $key => $type) {
	$property_types[$type['id']] = $type['name'];
}

$users = [];
if ($user_model->role->is_agent){

	foreach ($user_model->find()->buyer()->seller(true)->all() as $key => $user) {
		$users[$user->getRole()->one()->name][$user->id] = $user->email.'  ('.$user->profile->getFullName().')';
	}

} elseif ($user_model->role->is_buyer || $user_model->role->is_seller) {

	foreach ($user_model->find()->agent()->all() as $key => $user) {
		$users[$user->getRole()->one()->name][$user->id] = $user->email.'  ('.$user->profile->getFullName().')';
	}

}
$this->title = 'Add property';
$this->params['breadcrumbs'][] = ['label' => 'User profile', 'url' => ['agent/profile', 'id' => $user_model->id]];
$this->params['breadcrumbs'][] = $this->title;

function dateFormatConverter($dateFormat){
	$date_array = explode('-', $dateFormat);
	$js_format = [];
	if (count($date_array) > 1){
		foreach ($date_array as $key => $char) {
			$char = strtolower($char);
			switch ($char) {
				case 'y':
					$js_format[] = 'yyyy';
					break;
				case 'm':
					$js_format[] = 'mm';
					break;
				case 'd':
					$js_format[] = 'dd';
					break;
				default:
					$js_format[] = 'yyyy';
					break;
			}
		}
	}
	else {
		$date_array = explode('/', $dateFormat);
		foreach ($date_array as $key => $char) {
			$char = strtolower($char);
			switch ($char) {
				case 'y':
					$js_format[] = 'yyyy';
					break;
				case 'm':
					$js_format[] = 'mm';
					break;
				case 'd':
					$js_format[] = 'dd';
					break;
				default:
					$js_format[] = 'yyyy';
					break;
			}
		}
	}
	return implode('/', $js_format);
}
?>
<section class="create_property">
	<div class="container">
		<?php echo $this->render('/elements/_breadcrumbs', array()); ?>
			<?php $form = ActiveForm::begin([
				'layout' => 'horizontal',
				'fieldConfig' => [
			        'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}<div class='clearfix'></div>",
			        'horizontalCssClasses' => [
			            'label' => 'col-sm-4',
			            'offset' => 'col-sm-offset-4',
			            'wrapper' => 'col-sm-8',
			            'error' => '',
			            'hint' => '',
			        ],
			    ],
				'options' => [
					'class' => 'area_validation',
					'data-area-model' => 'propertylocation',
					'data-pjax' => 'true',
					'validateOnBlur' => 'false'
				]
			]) ?>
				<div class="row">
					<div class="col-xs-12">
						<?php
							if ($user_model->role->is_agent) {
								echo $form->field($transaction_model, 'id_client', [
									'horizontalCssClasses' => [
										'label' => 'col-sm-2',
							            'offset' => 'col-sm-offset-2',
							            'wrapper' => 'col-sm-10',
									]									
								])->dropDownList($users, ['class' => 'select2 form-control', 'prompt' => 'Choose your client...', 'decode' => true])->label('Choose your client'); 
							} elseif ($user_model->role->is_buyer || $user_model->role->is_seller) {
								echo $form->field($transaction_model, 'id_agent', [
									'horizontalCssClasses' => [
										'label' => 'col-sm-2',
							            'offset' => 'col-sm-offset-2',
							            'wrapper' => 'col-sm-10',
									]
								])->dropDownList($users, ['class' => 'select2 form-control', 'prompt' => 'Choose your client...', 'decode' => true])->label('Choose your agent');
							}
						?>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-6">
						<div class="form-group area_field">
							<label class="control-label col-sm-4">Address</label>
							<div class="col-sm-8 loading area_loading pjax_inline">
					        	<input class="form-control profile_areas" type="text" 
					        		data-type="address"
					        		data-city-id="<?= Html::getInputId($property_location_model, 'city') ?>" 
									data-state-id="<?= Html::getInputId($property_location_model, 'state') ?>" 
									data-zipcode-id="<?= Html::getInputId($property_location_model, 'zipcode') ?>"
									data-address-id="<?= Html::getInputId($property_location_model, 'address_1') ?>"
									data-lat-id="<?= Html::getInputId($property_location_model, 'lat') ?>" 
									data-lng-id="<?= Html::getInputId($property_location_model, 'lng') ?>"
									data-place_id-id="<?= Html::getInputId($property_location_model, 'place_id') ?>"
									data-short-id="<?= Html::getInputId($property_location_model, 'short_name') ?>"
									data-photo-id="<?= Html::getInputId($property_location_model, 'photo') ?>" />

								<div class="loader">
                                    <div class="loader-container">
                                        <i class="fa fa-spinner fa-pulse fa-fw"></i>
                                        <span class="sr-only">Loading...</span>
                                    </div>
                                </div>

							</div>
							<div class="hidden">
					            <?= $form->field($property_location_model, 'city')->hiddenInput()->label(false) ?>
								<?= $form->field($property_location_model, 'state')->hiddenInput()->label(false) ?>
								<?= $form->field($property_location_model, 'zipcode')->hiddenInput()->label(false) ?>
								<?= $form->field($property_location_model, 'address_1')->hiddenInput()->label(false) ?>
								<?= $form->field($property_location_model, 'lat')->hiddenInput()->label(false) ?>
								<?= $form->field($property_location_model, 'lng')->hiddenInput()->label(false) ?>
								<?= $form->field($property_location_model, 'place_id')->hiddenInput()->label(false) ?>
								<?= $form->field($property_location_model, 'short_name')->hiddenInput()->label(false) ?>
								<?= $form->field($property_location_model, 'photo')->hiddenInput()->label(false) ?>									
							</div>   
						</div>

					</div>   
					<div class="col-sm-6">
						<?= $form->field($property_model, 'id_type')->dropDownList($property_types, ['prompt' => 'Choose property type'])->label('Property type') ?>
					</div>
				</div>

				<div class="row">
					<div class="col-sm-6">
						<?= $form->field($property_model, 'beds', [
							'template' =>  '{label}
					    		{beginWrapper}
					    			<div class="input-group">						    				
					    				<div class="input-group-addon"><i class="icon-bed2"></i></div>
					    				{input}{error}{hint}
					    				<div class="input-group-addon" onclick="decrease(event, `'.Html::getInputId($property_model, 'beds').'`)"><i class="icon-minus"></i></div>
					    				<div class="input-group-addon" onclick="increase(event, `'.Html::getInputId($property_model, 'beds').'`)"><i class="icon-plus"></i></div>
					    			</div>
					    		{endWrapper}
					    		<div class="clearfix"></div>',
							])->widget(MaskedInput::className(), [
							'mask' => '9{1,}',
							'options' => [
						    	'placeholder' => '',
						    	'class' => 'form-control'
						    ],
						    'clientOptions' => [
						    	'autoUnmask' => true,
						    	'allowMinus' => false,
						    ]									
						]) ?>
					</div>
					<div class="col-sm-6">
						<?= $form->field($property_model, 'baths', [
							'template' =>  '{label}
					    		{beginWrapper}
					    			<div class="input-group">						    				
					    				<div class="input-group-addon"><i class="icon-man-woman"></i></div>
					    				{input}{error}{hint}
					    				<div class="input-group-addon" onclick="decrease(event, `'.Html::getInputId($property_model, 'baths').'`)"><i class="icon-minus"></i></div>
					    				<div class="input-group-addon" onclick="increase(event, `'.Html::getInputId($property_model, 'baths').'`)"><i class="icon-plus"></i></div>
					    			</div>
					    		{endWrapper}
					    		<div class="clearfix"></div>',
							])->widget(MaskedInput::className(), [
							'mask' => '9{1,}',							
							'options' => [
						    	'placeholder' => '',
						    	'class' => 'form-control'
						    ],
						    'clientOptions' => [
						    	'autoUnmask' => true,
						    	'allowMinus' => false,
						    ]										
						]) ?>
					</div>
				</div>

				<div class="row">

					<div class="col-sm-6">
						<?= $form->field($property_model, 'garage', [
							'template' =>  '{label}
					    		{beginWrapper}
					    			<div class="input-group">						    				
					    				<div class="input-group-addon"><i class="icon-directions_car"></i></div>
					    				{input}{error}{hint}
					    				<div class="input-group-addon" onclick="decrease(event, `'.Html::getInputId($property_model, 'garage').'`)"><i class="icon-minus"></i></div>
					    				<div class="input-group-addon" onclick="increase(event, `'.Html::getInputId($property_model, 'garage').'`)"><i class="icon-plus"></i></div>
					    			</div>
					    		{endWrapper}
					    		<div class="clearfix"></div>',
							])->widget(MaskedInput::className(), [
							'mask' => '9{1,}',

							'options' => [
						    	'placeholder' => '',
						    	'class' => 'form-control'
						    ],
						    'clientOptions' => [
						    	'autoUnmask' => true,
						    	'allowMinus' => false,
						    ]									
						]) ?>
					</div>

					<div class="col-sm-6">
						<?= $form->field($property_model, 'square_feet', [
							'template' =>  '{label}
					    		{beginWrapper}
					    			<div class="input-group">						    				
					    				<div class="input-group-addon"><i class="icon-transform"></i></div>
					    				{input}{error}{hint}
					    			</div>
					    		{endWrapper}
					    		<div class="clearfix"></div>',
							])->widget(MaskedInput::className(), [
							'mask' => '9{1,}',

							'options' => [
						    	'placeholder' => '',
						    	'class' => 'form-control'
						    ],
						    'clientOptions' => [
						    	'autoUnmask' => true,
						    	'allowMinus' => false,
						    ]									
						]) ?>
					</div>

				</div>

				<div class="row">
					<div class="col-sm-6">
						<?= $form->field($transaction_info_model, 'listing_price', [
							'template' =>  '{label}
					    		{beginWrapper}
					    			<div class="input-group">						    				
					    				<div class="input-group-addon">$</div>
					    				{input}{error}{hint}
					    			</div>
					    		{endWrapper}
					    		<div class="clearfix"></div>',
							])->widget(MaskedInput::className(), [
							'mask' => '9{1,}',
							'options' => [
						    	'placeholder' => '',
						    	'class' => 'form-control'
						    ],
						    'clientOptions' => [
						    	'autoUnmask' => true,
						    ]									
						]) ?>
					</div>
					
					<div class="col-sm-6">
						<?= $form->field($transaction_info_model, 'listing_date', [
							'template' => '{label}
								{beginWrapper}
								<div class="input-group date">
									<div class="input-group-addon">
										<i class="icon-calendar3"></i>
									</div>
									{input}{error}{hint}									
								</div>
								{endWrapper}
								<div class="clearfix"></div>'
						])->textInput([
							'class' => 'form-control datepicker',
							'readonly' => 'readonly',
							'value' => ($transaction_info_model->listing_date) ? \Yii::$app->formatter->asDate($transaction_info_model->listing_date, 'php:m/d/Y') : null
						]) ?>
					</div>
				</div>

				<div class="row">
					<div class="col-sm-6">
						<?= $form->field($transaction_info_model, 'selling_price', [
							'template' =>  '{label}
					    		{beginWrapper}
					    			<div class="input-group">						    				
					    				<div class="input-group-addon">$</div>
					    				{input}{error}{hint}
					    			</div>
					    		{endWrapper}
					    		<div class="clearfix"></div>',
							])->widget(MaskedInput::className(), [
							'mask' => '9{1,}',
							'options' => [
						    	'placeholder' => '',
						    	'class' => 'form-control'
						    ],
						    'clientOptions' => [
						    	'autoUnmask' => true,
						    ]									
						]) ?>
					</div>
					
					<div class="col-sm-6">
						<?= $form->field($transaction_info_model, 'selling_date', [
							'template' => '{label}
								{beginWrapper}
								<div class="input-group date">
									<div class="input-group-addon">
										<i class="icon-calendar3"></i>
									</div>
									{input}{error}{hint}									
								</div>
								{endWrapper}
								<div class="clearfix"></div>'
						])->textInput([
							'class' => 'form-control datepicker',
							'readonly' => 'readonly',
							'value' => ($transaction_info_model->selling_date) ? \Yii::$app->formatter->asDate($transaction_info_model->selling_date, 'php:m/d/Y') : null
						]) ?>
					</div>
				</div>

				<div class="row">
					<div class="col-sm-6">
						<?= $form->field($transaction_info_model, 'mls')->textInput() ?>
					</div>
					
					<div class="col-sm-6">
						<?= $form->field($transaction_info_model, 'mls_number')->textInput() ?>
					</div>
				</div>

				<div class="row">
					<div class="col-sm-6">
						<?= $form->field($property_model, 'title')->textInput() ?>
					</div>
					<div class="col-sm-6">
						<?= $form->field($transaction_model, 'featured', [
							'checkboxTemplate' => "{beginWrapper}\n<div class=\"checkbox\">\n{beginLabel}\n{input}\n{labelTitle}\n{endLabel}\n</div>\n{error}\n{endWrapper}\n{hint}",
						])->checkbox([
			                'class'=>'iswitch iswitch-secondary',
			                //'label'=> 'Do you want to make it featured?',
			            ], false) ?>
					</div>
				</div>
				
				<?= Html::submitButton('save',['class' => 'btn btn-success pull-right']) ?>
					
			<?php ActiveForm::end() ?>
	</div>
</section>