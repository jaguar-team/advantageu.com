<?php

use yii\bootstrap\ActiveForm;

use yii\helpers\Html;
use yii\helpers\Url;

use yii\widgets\Pjax;
use yii\widgets\LinkPager;
use yii\widgets\Breadcrumbs;

use kartik\rating\StarRating;
use rmrevin\yii\fontawesome\FA;

$starCaptions = [];
for ($i = 0; $i <= 50; $i++){
	$starCaptions[(string)($i/10)] = ((double)$i/10).' out of 5 stars';
}

$this->title = 'User profile';

$user_profile_model = $user_model->profile;


$sections = $user_model->role->getSections()->indexBy('id')->all();

$user_role_model = $user_model->role;
$roles = $user_role_model::find()->select(['name'])->indexBy('id')->column();

\Yii::$app->formatter->locale = 'en-US';

$is_owner = (!\Yii::$app->user->isGuest && $user_model->isOwner());
$is_matched = true || $is_owner;

$this->params['breadcrumbs'][] = $this->title;

function getAddresses($addresses_arr, $is_matched){
	$i = 1;
	$addresses = '';
	foreach ($addresses_arr as $key => $address) {
		if ($i < count($addresses_arr)) {
			
			$addresses.= ($is_matched ? $address['address'] : 'XXXXXXXXXX').', '.$address['city'].', '.$address['short_name'].', '.($is_matched ? $address['zip_code']: 'XXXXXX').'; ';

		}
		else $addresses.= ($is_matched ? $address['address'] : 'XXXXXXXXXX').', '.$address['city'].', '.$address['short_name'].', '.($is_matched ? $address['zip_code']: 'XXXXXX');
		$i++;
	} 
	return $addresses;
}

$addresses = getAddresses($user_model->addresses, $is_matched);

?>


<section class="profile">
	<div class="container">
		<?php echo $this->render('/elements/_breadcrumbs', array()); ?>
	</div>
	
	<?= $this->render('/elements/_profile_header', [
		'user_model'			=> $user_model,
		'user_profile_model' 	=> $user_profile_model,

		'is_matched'			=> $is_matched,
		'is_owner'				=> $is_owner,
		'addresses'				=> $addresses
	]) ?>
	
	<div class="profile_body">
		<div class="container">
			<div class="row">
				<div class="col-sm-8">

					<div class="panel no-padding">
						<!-- Nav tabs -->
					  	<ul class="nav nav-tabs" role="tablist">
						    <li role="presentation" class="active">
						    	<a href="#overview" aria-controls="home" role="tab" data-toggle="tab">Overview</a>
						    </li>
							<?php if($user_role_model->is_agent) : ?>
								<li role="presentation">
									<a href="#office" aria-controls="profile" role="tab" data-toggle="tab">Office/Branches</a>
								</li>
								<li role="presentation">
									<a href="#map_view" aria-controls="settings" role="tab" data-toggle="tab" id="<?= ($is_matched ? 'to_map' : '') ?>">View on Map</a>
								</li>
							<?php endif ?>
					  	</ul>

						  <!-- Tab panes -->
					  	<div class="tab-content">
						    <div role="tabpanel" class="tab-pane active" id="overview">
						    	<?php Pjax::begin(['options' => ['class' => 'pjax_inline']]) ?>
						    		<div class="<?= ($is_owner ? 'editable' : '') ?>">
								    	<?php $form = ActiveForm::begin([
								    		'options' => [
								    			'data-pjax' => 'true'
								    		]
								    	]) ?>
								    		<p class="element">
								    			<?php 
								    				if( $user_profile_model->biography != '' ) 
								    					echo Html::decode($user_model->profile->biography); 
								    				else 
								    					echo ($is_owner) ? 'You have no biograpy yet!' : Html::encode($user_model->profile->getFullName(true)).' has no biograpy yet!';
								    			?>
								    				
								    		</p>
								    		<?php if($is_owner) : ?>
									    		<div class="editor row">
													<div class="col-sm-10 col-xs-9">
														<?= $form->field($user_profile_model, 'biography')->textarea(['rows' => 5])->label(false) ?>				
													</div>
													<div class="edit_controls col-sm-2 col-xs-3">
														<?= Html::a( 'Save', 'javascript:void(0)', ['do' => 'submit_editable']) ?>
														<?= Html::a( 'Cancel', 'javascript:void(0)', ['do' => 'cancel_editable']) ?>
													</div>
												</div>
												<div class="actions">
													<?= Html::a( FA::icon('pencil-square-o'), 'javascript:void(0)', 
														[
															'onclick' => 'editField(event, "'.Html::getInputId($user_profile_model, 'biography').'")',
															'class' => 'edit_trigger'
														]
													) ?>
												</div>
											<?php endif ?>

										<?php ActiveForm::end() ?>
									</div>
								<?php Pjax::end() ?>						    	

						    	<div class="statistic flexbox">						    		
						    		<div class="block">
						    			<div class="value">
						    				<?= Html::encode( $user_model->getTransactionRelation()->totalNumber() ) ?>
						    			</div>
						    			<div class="info">
						    				<span>Total properties</span> | <a href="javascript:void(0)">Click to zoom</a>
						    			</div>
						    			<div class="statistic_name">
						    				Total Real Estate Sales
						    			</div>
						    		</div>
						    		<div class="block">
						    			<div class="value">
						    				<?= Html::encode( $user_model->getTransactionRelation()->active()->totalNumber() ) ?>
						    			</div>
						    			<div class="info">
						    				<span>Sold in 2017</span> | <a href="javascript:void(0)">Click to zoom</a>
						    			</div>
						    			<div class="statistic_name">
						    				Approved properties
						    			</div>
						    		</div>
						    		<div class="block">
						    			<div class="value">
						    				<?= Html::encode( $user_model->getTransactionRelation()->buy()->totalNumber() ) ?>
						    			</div>
						    			<div class="info">
						    				<span>Sold in 2017</span> | <a href="javascript:void(0)">Click to zoom</a>
						    			</div>
						    			<div class="statistic_name">
						    				Real Estate Purchases
						    			</div>
						    		</div>

						    	</div>

						    	<div class="reviews">
						    		<div class="title">
						    			Client reviews
						    		</div>
						    		<div class="rating">
						    			<?=  StarRating::widget([
										    'name' => 'revies_rating',
										    'value' => $user_model->averageRatingReviews,
										    'pluginOptions' => [
										        'readonly' => true,
										        'showClear' => false,
										        'filledStar' => Html::decode( FA::icon('star') ),
        										'emptyStar' => Html::decode( FA::icon('star-o') ),
        										'size' => 'sm',
        										'starCaptions' => $starCaptions
										    ],
										]); ?>
										<div class="row">

											<div class="col-md-4 col-sm-5 col-xs-12">
												<?php for ($i=5; $i>=1; $i--) : ?>
													<div class="progress-group">
														<label><?= $i ?> star</label>
														<div class="progress">
														  	<div class="progress-bar" role="progressbar" aria-valuenow="<?= isset($user_model->performanceReviews[$i]) ? $user_model->performanceReviews[$i]['percent'] : 0 ?>"
														  		aria-valuemin="0" aria-valuemax="100" style="width:<?= isset($user_model->performanceReviews[$i]) ? $user_model->performanceReviews[$i]['percent'] : 0 ?>%">
														    	<span class="sr-only"><?= isset($user_model->performanceReviews[$i]) ? $user_model->performanceReviews[$i]['percent'] : 0 ?>% Complete</span>
														  	</div>
														</div>
														<div class="value">
															<?= isset($user_model->performanceReviews[$i]) ? $user_model->performanceReviews[$i]['percent'] : 0 ?>%
														</div>
													</div>
												<?php endfor ?>
												<div class="clearfix"></div>
											</div>

											<div class="col-md-8 col-sm-7 col-xs-12">
												<p>Share your thoughts with other customers</p>
												<?= Html::a('Write a customer review', Url::toRoute(['agent/review', 'id' => $user_model->id, '#' => 'new_review']), ['class' => 'reviews_btn']) ?>
											</div>

										</div>
										<?= Html::a('See all reviews', Url::toRoute(['agent/review', 'id' => $user_model->id]), ['class' => 'see_all']) ?>
						    		</div>
						    	</div>

						    </div>

							<?php if($user_role_model->is_agent) : ?>
								<div role="tabpanel" class="tab-pane" id="office">
									<div class="name"><?= Html::encode($user_profile_model->company) ?></div>
									<address>

										<?php
											Pjax::begin([
												'options' => ['id' => 'profile_address']
											]);
												$this->registerJsFile('@web/js/googleService.js');
												$script = '
													GoogleService.init('.json_encode($user_model->getAddresses()->select('*')->asArray()->all()).');
													GoogleService.setMarkers();																		    		
												';
												$this->registerJs($script);
												echo ($addresses != '') ? sprintf('<a href="#" onclick="toMap(event, `%s`)">%s</a>', ($is_matched ? 'to_map' : null), Html::encode($addresses)) : 'No addresses yet';
												echo (!$is_matched ? '<span class="add-to-matches">Get Matched</span>' : '');
											Pjax::end();
										?>
									</address>
									<div class="contacts">
										<span>
											<i class="icon-earth-globe"></i>
											<a href="mailto:<?= Html::encode( $user_model->email ) ?>">Contact Email</a>
										</span>
										<?php if($user_profile_model->phone_number) : ?>
											<span>
												<i class="icon-mobile2"></i>
												<a href="tel:<?= Html::encode( $user_profile_model->phone_number ) ?>"><?= Html::encode( $user_profile_model->phone_number ) ?></a>

											</span>
										<?php endif ?>
									</div>
								</div>

								<div role="tabpanel" class="tab-pane" id="map_view">
									<?php if($is_matched) : ?>
										<div id="map" style="height: 600px;">

										</div>
									<?php else : ?>
										<?= Html::img(Url::to(['@uri_img/no_map.jpg'], true), ['width' => '100%', 'height' => '100%']) ?>
									<?php endif ?>
								</div>
							<?php endif ?>
					  	</div>
					</div>

					<div class="transactions">	
						<?= ($is_owner) ? Html::a('List your property', Url::toRoute(['agent/add-property', 'id' => $user_model->id], true), ['class' => 'btn pull-right',]) : '' ?>
						<h2><?= Html::encode($user_model->profile->getFullName()) ?>’S PROPERTIES</h2>
						<div class="clearfix"></div>
						<?php 
						if( !empty($user_model->getTransactions()->all()) ) :
							Pjax::begin();
								foreach ($user_model->getTransactions(5)->indexBy('id')->all() as $trans_id => $transaction) : 
									if(!$is_owner && $user_model->private_property) {
										echo '<div class="panel">
											<h3 class="text-center">No properties yet!</h3>
										</div>';
										break(1);
									}
									$location = $transaction->property->location; 
						?>
									<div class="panel<?= ($transaction->status) ? ' approved' : '' ?><?= ($transaction->featured) ? ' featured' : '' ?>">
										<div class="img">
											<?= Html::img( isset($location->photo) ? $location->photo : Url::to(['@uri_img/no_thumb.png'], true) ) ?>
										</div>
										<div class="content">
											<div class="top">
												<div class="title">
													<?= Html::a($transaction->property->title, [''], ['data-pjax' => 'false']) ?>
													<div class="pull-right">
														<?php if ($transaction->status) : ?>
															<a class="btn-approve complete" href="javascript:void(0)">Approved</a>														
														<?php elseif( $is_owner ) : ?>

															<?php if($user_model->role->is_buyer || $user_model->role->is_seller) : ?>
																<a class="btn-approve" href="javascript:void(0)" onclick="transactionApprove(event, '<?= $trans_id ?>')">Approve this property</a>
															<?php else : ?>
																<a class="btn-approve" href="javascript:void(0)">Not Approved</a>
															<?php endif ?>
															
														<?php endif ?>
														
													</div>
												</div>
												<address>
													<i class="icon-location-pin2"></i>
													<span>
														<?php echo sprintf('%s, %s, %s', $location->address_1, $location->city, $location->short_name); ?>												
													</span>
												</address>
												<ul class="transaction_details flexbox">
													<li>
														<i class="icon-bed2"></i><?= Html::encode($transaction->property->beds) ?> Bedrooms
													</li>
													<li>
														<i class="icon-man-woman"></i><?= Html::encode($transaction->property->baths) ?> Bathrooms
													</li>
													<li>
														<i class="icon-directions_car"></i><?= Html::encode($transaction->property->garage) ?> Garage
													</li>
												</ul>
												<div class="price">
													$<?= Html::encode( number_format($transaction->getTransactionInfo()->one()->listing_price, 2, ',', '') ) ?> <small>Guide Price</small>
												</div>
											</div>
											<div class="bot">
												<div class="listed_by">
													Listed on <?= \Yii::$app->formatter->asDate($transaction->getTransactionInfo()->one()->listing_date, 'medium') ?> by
													<?php 
														$person = ( ($user_model->role->is_agent) ? $transaction->client : $transaction->agent );

														if($person && (!$person->private_property || $is_owner)){
															echo Html::a($person->profile->getFullName(), Url::toRoute(['agent/profile', 'id' => $person ->id], true), ['data-pjax' => 'false']);
															echo Html::a( Html::img($person->profile->getPhotoUrl()),  Url::toRoute(['agent/profile', 'id' => $person ->id], true), ['class' => 'agent_img pull-right', 'data-pjax' => 'false'] );
														} else {
															echo Html::a('Unknown User', 'javascript:void(0)');
															echo Html::a( Html::img(Url::to(['@uri_img/noavatar.png'], true)),  'javascript:void(0)', ['class' => 'agent_img pull-right'] );
														}									
														
													?>
												</div>
											</div>
										</div>
										<div class="clearfix"></div>
									</div>
						<?php 
								endforeach; 
							
								echo LinkPager::widget([
									'pagination' => $user_model->pagination,
								]);	
							Pjax::end(); else : 
						?>
							<div class="panel">
								<h3 class="text-center">No properties yet!</h3>
							</div>
						<?php endif ?>				
					</div>

				</div>

				<div class="sidepanel col-sm-4">

					<?= $this->render('/elements/_sidepanel', [
						'user_model'			=> $user_model,
						'user_profile_model' 	=> $user_profile_model,
						'section_value_model'	=> $section_value_model,
						'is_owner'				=> $is_owner
					]) ?>

				</div>	
			</div>
		</div>
	</div>

	<?= $this->render('_contact_agent', [
		'user_model' 				=> $user_model,
		'user_profile_model' 		=> $user_profile_model,
		
		'agent_contact_model'		=> $agent_contact_model,
	]); ?>

	<?= $this->render('modals', [
		'user_model' 				=> $user_model,
		'user_profile_model' 		=> $user_profile_model,
		
		'section_value_model'		=> $section_value_model,

		'sections'					=> $sections,
		'roles'						=> $roles
	]); ?>

</section>

