<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\helpers\Url;
use frontend\assets\AppAsset;
use common\widgets\Alert;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= \Yii::$app->language ?>">
    <head>
        <meta charset="<?= \Yii::$app->charset ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>

        <!-- Google Tag Manager -->
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-TW9CF7C');</script>
        <!-- End Google Tag Manager -->

        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>

            
    </head>
    <body>
        <!-- Google Tag Manager (noscript) -->
        <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TW9CF7C"
        height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <!-- End Google Tag Manager (noscript) -->

        <?php 

        $this->beginBody();

        $this->registerJs('
            _base_url = "'.Url::to(\Yii::$app->homeUrl, true).'";
            _is_logged_in = '.(\Yii::$app->user->isGuest ? 'false' : 'true').';
        ', yii\web\View::POS_HEAD);

        echo $this->render('/elements/_header');

        echo Alert::widget();

        echo $content;

        echo $this->render('/elements/_footer');

        echo $this->render('/elements/_modals');

        $this->endBody() 

        ?>

    </body>
</html>
<?php $this->endPage() ?>
