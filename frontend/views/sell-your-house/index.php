<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 021 21.06.17
 * Time: 16:24
 */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

$this->title = 'AU';

$script = "
    $('#field_mje0dx').val($('.select-item.selected').attr('id'));
    $('#other-short-sale, #other-commercial, #other-multi-family, #other-rental, #other-apartments, #other-land').prop('disabled', true);
    $('#quiz-box #frm_form_2_container').on('click', '.select-item', function() {
        $('.select-item').removeClass('selected');
        $(this).addClass('selected');
        $('#field_mje0dx').val(this.id);
    });";
$this->registerJs($script, yii\web\View::POS_LOAD);

$blogPosts = \Yii::$app->blog->getPosts(
    [
    'posts_per_page'    => 9,
    'offset'            => 0,
    'post_status'       => 'publish',
    ],
    [
        'link',
        'post_thumbnail',
        'post_title',
        'post_content'
    ]
);
?>

<div class="page">

    <div class="the-content">
        <div class="page-hero acf-row-0 page-hero-sell-your-house">
            <?= Html::img(Url::to(['@uri_img/Sell-Your-House.jpg'], true), ['alt' => 'Sell Your House', 'id' => 'hero-image']) ?>
            <div class="wrap">
                <div class="headingWrap">
                    <h1 id="page-heading">Sell Your House</h1></div>
            </div>
        </div>
        <div class="text-block text-block-sell-your-house  resource-display acf-row acf-row-1">
            <div class="wrap">
                <h2>Our Process</h2>
                <div>
                    <p>AdvantageU Home Sellers’ Resource was established to help residential home sellers navigate the complex world of real estate. Our aim is to help you: Reduce time &amp; effort, discover available options, limit uncertainty &amp; risk and make smart decisions.</p>
                    <p><strong>Advantage<span class="orange-color-for-text">U</span></strong> information and resources are always free.</p>
                </div>
            </div>
        </div>
        <div class=" four-column-display resource-display acf-row no-top-pad acf-row-2">
            <div class="wrap">
                <div class="threecol first">
                    <?= Html::img(Url::to(['@uri_img/Sell-Your-House-1-1.jpg'], true), ['alt' => 'sell your house']) ?>
                    <h5>Tell Us About You</h5>
                    <p>Know what to do when and download the most comprehensive Ultimate Home Seller Checklist.</p>
                </div>
                <div class="threecol ">
                    <?= Html::img(Url::to(['@uri_img/Sell-Your-House-2-1.jpg'], true), ['alt' => 'sell your house']) ?>
                    <h5>Educational Resources</h5>
                    <p>Learn everything from calculating selling expenses and marketing strategies to staging and getting through escrow.</p>
                </div>
                <div class="threecol ">
                    <?= Html::img(Url::to(['@uri_img/Sell-Your-House-3-1.jpg'], true), ['alt' => 'sell your house']) ?>
                    <h5>Get <br>Listed FREE</h5>
                    <p>Get nationwide  exposure and showcase your home on AdvantageU. Upload photos and list your home for free.<br/><em>Coming Soon!</em></em></p>
                </div>
                <div class="threecol ">
                    <?= Html::img(Url::to(['@uri_img/Sell-Your-House-4-1.jpg'], true), ['alt' => 'sell your house']) ?>
                    <h5>Get <br>Matched</h5>
                    <p>Get Matched with the right Real Estate Agent based on your unique seller profile and sell your home faster and with a fair market value</p>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>


        <div id="heading-7" class="standalone-heading resource-display acf-row acf-row-7">
            <div class="wrap">
                <div class="headingContainer">
                    <?= Html::img(Url::to(['@uri_img/section-check.png'], true), ['alt' => 'Checkmark', 'class' => 'shadowed']) ?>
                    <h2><em>Educational</em> RESOURCES</h2>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <div style="background-image: url(https://resources.advantageu.com/wp-content/uploads/2016/07/AdvantageU-Checkmark.jpg)" class="resource-display acf-row acf-row-8">
            <div class="wrap">
                <h2>HOMESELLERS’ RESOURCES PICKED Especially FOR YOU.</h2>
                <h3></h3>
                <div class="fourcol first">
                    <div class="eightcol first">
                        <a href="https://resources.advantageu.com/increase-market-value-of-home/"><img alt="market value of home" src="https://resources.advantageu.com/wp-content/uploads/2016/10/advantageu-home-interior--240x105.jpg"></a>
                    </div>
                    <div class="fourcol last">
                        <h4><a href="https://resources.advantageu.com/increase-market-value-of-home/">8 Improvements that  Increase the Market Value of Your Home</a></h4></div>
                    <div class="clearfix"></div>
                    <div class="section-content">If you’re preparing to sell your house – maybe your family is expanding and it’s time to start looking for a house with more room! – you need to analyze how you can increase the market value of your home. You first need to create a budget on how much money you can spend on...</div>
                </div>
                <div class="fourcol ">
                    <div class="eightcol first">
                        <a href="https://resources.advantageu.com/fall-decorations-show-your-house/"><img alt="show your house" src="https://resources.advantageu.com/wp-content/uploads/2016/10/33207185_xxl-240x105.jpg"></a>
                    </div>
                    <div class="fourcol last">
                        <h4><a href="https://resources.advantageu.com/fall-decorations-show-your-house/">How to Choose Tasteful Fall Decorations When You Show your House</a></h4></div>
                    <div class="clearfix"></div>
                    <div class="section-content">It’s time for you to move – maybe your employer is relocating you to a different office – so your current house is for sale and your listing is in place. Now it’s time to show your house. You want your home to be inviting, but Halloween and Thanksgiving are just around the corner and...</div>
                </div>
                <div class="fourcol last">
                    <div class="eightcol first">
                        <a href="https://advantageu.com/avoid-handcuffed-real-estate-listing-agreement/"><img alt="listing agreement" src="https://resources.advantageu.com/wp-content/uploads/2016/10/advantageu-contract-shaking-hands--240x105.jpg"></a>
                    </div>
                    <div class="fourcol last">
                        <h4><a href="https://resources.advantageu.com/avoid-handcuffed-real-estate-listing-agreement/">How to Avoid Being Handcuffed By a Real Estate Listing Agreement</a></h4></div>
                    <div class="clearfix"></div>
                    <div class="section-content">When you’re ready to sell your home, you’ll eventually meet with a real estate agent who’s going to offer you a listing agreement. The listing agreement is a contract that legally binds you to sell with that particular agent for a specified amount of time. Most states offer a standard form that the agent and...</div>
                </div>
                <div class="clearfix"></div>
                <div style="text-align: center; margin-top: 4em;"><a href="https://advantageu.com/resources/index" class="linkButton">More Resources</a></div>
            </div>
        </div>


        <div id="heading-9" class="standalone-heading resource-display acf-row acf-row-9">
            <div class="wrap">
                <div class="headingContainer">
                    <?= Html::img(Url::to(['@uri_img/section-check.png'], true), ['alt' => 'Checkmark', 'class' => 'shadowed']) ?>
                    <h2><em>GET MATCHED</em></h2>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <div style-q="background-image: url()" class="text-block  resource-display acf-row acf-row-10">
            <div class="wrap">
                <div class="twocol first" style="height: 204px;"><img alt="market value of home" id="section-image" src="https://resources.advantageu.com/wp-content/uploads/2016/07/Final-AU-SEAL.gif"></div>
                <div class="section-image-content tencol last" style="height: 204px;">
                    <h2><em>Consider using a top agent prequalified by</em> <strong style="color: #6e6e6e; text-transform: none;">Advantage<span style="color: #fdb000;">U</span>.</strong></h2>
                    <div>
                        <p>We strive for absolute excellence when matching you with a real estate agent. Agents must pass our intensive vetting process designed to ensure every agent is committed to deliver excellent service that brings results. We don’t simply accept any agent who’s willing to pay to be part of our matching service. This is why agents recommended by AdvantageU have a proven track record of success.</p>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>

    </div>
</div>
