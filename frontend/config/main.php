<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'homeUrl' => '/',
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log', 'jsUrlManager'],
    'controllerNamespace' => 'frontend\controllers',
    'defaultRoute' => 'home',
    'components' => [
        'request' => [
            'baseUrl' => '',
            'csrfParam' => '_csrf-frontend',
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
            'loginUrl' => ['', '#' => 'login_open']
        ],
        'agentSearch' => [
            'class' => 'frontend\models\AgentSearch',
        ],
        'agentSearchStep' => [
            'class' => 'frontend\components\AgentSearchStep',
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'advanced-frontend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'home/error',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                //Home controller
                '/'                                     => 'home/home',
                // Agent controller
                //'agent/<id:\d+>'                      => 'agent/profile',
                //'agent/review/<id:\d+>'               => 'agent/review',
                //'agent/delete-language/<id:\d+>'      => 'agent/delete-language',
                //'agent/delete-area/<id:\d+>'          => 'agent/delete-area',
                ['class' => 'common\components\url\UserUrlRule'],
            ],
        ],
        'assetManager' => [
            'basePath' => __DIR__ . '/../web/assets',
            'appendTimestamp' => true,
            'bundles' => [
                'dosamigos\google\maps\MapAsset' => [
                    'options' => [
                        'key'       => 'AIzaSyDbgI-qDa-U_JWtQbuqj7kOdBK4vYBCJQ8',
                        'language'  => 'en',
                        'version'   => '3.1.18',
                        'libraries' => 'places'
                    ]
                ]
            ]
        ],
    ],
    'params' => $params,
];
