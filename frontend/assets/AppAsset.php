<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [

        'css/icomoon.css',
        'css/mkboilerplate.css',
        'css/fromWP.css', 
        'css/favicons.css',   
        'css/datepicker.min.css', 
        'js/select2/select2.css',
        'css/pages/rangeslider.css',
        'css/pages/meanmenu.min.css',
        'css/pages/dashicons.min.css',
        'css/pages/admin-bar.min.css',
        'css/pages/frm_fonts.css',
        'css/pages/style.css',
        'css/style.css',
        'css/profile.css'
    ];
    public $js = [
        'js/pages/compat.min.js',
        'js/pages/events.js',        
        'js/pages/frontend.min.js',
        'js/joinable.js',
        'js/jCarousel.js',
        'js/rangeslider.min.js',
        'js/jquery.meanmenu.min.js',
        'js/cycle-2.js',
        'js/bootstrap-datepicker.js',
        'js/pages/scripts.js',
        'js/select2/select2.min.js',
        'js/main.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
        '\rmrevin\yii\fontawesome\AssetBundle',
        '\mgcode\assets\HistoryJsAsset',
        'dosamigos\google\maps\MapAsset'
    ];
}
