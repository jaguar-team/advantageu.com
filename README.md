# AdvantageU
## Installation

```
$ git clone https://bitbucket.org/jaguar-team/advantageu.com.git advantageu.com
$ composer install
$ php init
```
Create database __advantageu.com__

Move to
```
$ cd /advantageu.com/common/config
```
and open __main-local.php__ file to rewrite config for database. Example:
```
<?php
return [
    'components' => [
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=localhost;dbname=advantageu.com',
            'username' => 'root',
            'password' => '',
            'charset' => 'utf8',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => false,
        ],
    ],
];

```
After you should install database tables and data:
```
$ php yii/migrate
$ php yii migrate --migrationPath=@yii/rbac/migrations/
```
If you have errors while composer install, try to write the following command

```
$ composer global require "fxp/composer-asset-plugin:^1.2.0"
```
### Apache 
Install virtual host
```
$ cd /your_apache_dir/conf/extra
```
If you use XAMPP
```
$ cd /dir_to_xampp/apache/conf/extra
```
Open file __httpd-vhosts.conf__ and paste the following code
```
<VirtualHost *:80>
   DocumentRoot "C:\your_xampp_dir\htdocs\ladvantageu.com"
   ServerName local-advantageu.com
   ServerAlias www.local-advantageu.com
   <Directory "C:\your_xampp_dir\htdocs\advantageu.com">
       Options Indexes FollowSymLinks Includes ExecCGI
       AllowOverride All
       Require all granted
   </Directory>
</VirtualHost>

<VirtualHost *:80>
   ServerName localhost
   DocumentRoot "C:\your_xampp_dir\htdocs"
   <Directory "C:\your_xampp_dir\htdocs">
       Options Indexes FollowSymLinks Includes execCGI
       AllowOverride All
       Allow From All
       Order Allow,Deny
   </Directory>
</VirtualHost>
```
For __WINDOWS__, move to __C:\Windows\System32\drivers\etc__ and add the following line:
```
127.0.0.1 local-advantageu.com
```