<?php
/**
 * Created by PhpStorm.
 * User: Vlad
 * Date: 18.04.2017
 * Time: 14:53
 */

namespace backend\models;

use Yii;
use yii\base\Model;
use common\models\Setting;

class GeneralSettingForm extends Model
{
    public $_setting;

    public $site_name;

    public $site_description;

    public $site_logo;

    public $site_url;

    public $admin_email;

    public $admin_default_role;

    public $date_format;

    public $time_format;

    public $default_date_format = ['F j, y', 'y-m-d', 'm/d/y', 'd/m/y'];

    public $default_time_format = ['g:i a', 'g:i A', 'H:i'];

    /**
     * GeneralSettingForm constructor.
     * @param Setting $setting
     * @param array $config
     */
    public function __construct(Setting $setting, array $config = [])
    {
        $this->_setting = $setting;

        foreach ($this->getAttributes() as $name => $value) {
            if ($this->isAttributeActive($name)) {
                $this->{$name} = $this->_setting->getValueByName($name);
            }
        }

        parent::__construct($config);
    }

    public function rules()
    {
        return [
            //site name rules
            ['site_name', 'string', 'max' => 100],

            //site description rules
            ['site_description', 'string', 'max' => 100],

            //site logo rules
            ['site_logo', 'file', 'extensions' => 'png, jpg, jpeg'],

            //site url rules
            ['site_url', 'url', 'defaultScheme' => 'http'],

            //admin email rules
            ['admin_email', 'email'],

            //date_format rules
            ['date_format', 'in', 'range' => $this->default_date_format],

            //time format rules
            ['time_format', 'in', 'range' => $this->default_time_format],
        ];
    }

    /**
     * @return bool
     */
    public function save()
    {
        $attr = $this->safeAttributes();

        if ($attr) {
            foreach ($attr as $name) {

                $this->_setting->setAttributes(['name' => $name, 'value' => $this->{$name},]);
                $this->_setting->save();
            }
            return true;
        }

        return false;
    }
}