<?php

/**
 * Created by PhpStorm.
 * User: Vlad
 * Date: 11.04.2017
 * Time: 16:10
 */

namespace backend\models;

use Yii;
use yii\helpers\ArrayHelper;
use common\models\User;
use common\models\UserRole;
use common\models\UserProfile;
use common\models\MultipleAreaForm;
use common\models\MultipleLanguageForm;

class RegistrationForm extends User
{
    /** @var UserProfile */
    public $_profile;

    /** @var string */
    public $password_hash_repeat;

    public function __construct(UserProfile $profile, array $config = [])
    {
        $this->_profile = $profile;

        parent::__construct($config);
    }

    /**
     * @inheritdoc
     * @return array
     */
    public function rules()
    {
        return ArrayHelper::merge([
            //password rules
            ['password_hash', 'compare'],
            //password repeat rules
            ['password_hash_repeat', 'safe'],
        ], parent::rules());
    }

    /**
     * @inheritdoc
     * @return array
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge([
            //password rules
            'password_hash_repeat'   => 'Password repeat',
        ], parent::attributeLabels());
    }

    /**
     * Register user
     * @return bool
     * @throws \yii\db\Exception
     */
    public function register($admin = false)
    {
        $this->populateRelation('profile', $this->_profile);

        return $this->save();
    }
}