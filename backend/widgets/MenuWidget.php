<?php 
	namespace backend\widgets;

	//use yii\base\Widget;
	use Closure;
	use Yii;
	use yii\base\Widget;
	use yii\helpers\ArrayHelper;
	use yii\helpers\Url;
	use yii\helpers\Html;
	use yii\widgets\Menu;

	class MenuWidget extends Menu
	{
	    protected function renderItem($item)
	    {
	        if (isset($item['url'])) {
	            $template = ArrayHelper::getValue($item, 'template', $this->linkTemplate);

	            return strtr($template, [
	                '{url}' => Html::encode(Url::to($item['url'])),
	                '{label}' => "<span>".$item['label']."</span>",
	                '{favicon}' => '<i class="'.$item['favicon'].'"></i>',
					'{badge}' => (isset($item['badge']) ? '<span class="label label-'.$item['badge']['color'].' pull-right hidden-collapsed">'.$item['badge']['text'].'</span>' : ''),
	            ]);
	        } else {
	            $template = ArrayHelper::getValue($item, 'template', $this->labelTemplate);

	            return strtr($template, [
	                '{label}' => $item['label'],
	                '{favicon}' => '<i class="'.$item['favicon'].'"></i>',
					'{badge}' => (isset($item['badge']) ? '<span class="label label-'.$item['badge']['color'].' pull-right hidden-collapsed">'.$item['badge']['text'].'</span>' : ''),
	            ]);
	        }
	    }
	    protected function normalizeItems($items, &$active)
	    {
	       
	        foreach ($items as $i => $item) {
	            if (isset($item['visible']) && !$item['visible']) {
	                unset($items[$i]);
	                continue;
	            }
	            if (!isset($item['label'])) {
	                $item['label'] = '';
	            }
	            if (!isset($item['favicon'])) {                
	                $items[$i]['favicon'] = '';
	            }
	            
	            $encodeLabel = isset($item['encode']) ? $item['encode'] : $this->encodeLabels;
	            $items[$i]['label'] = $encodeLabel ? Html::encode($item['label']) : $item['label'];
	            $hasActiveChild = false;
	            if (isset($item['items'])) {
	                $items[$i]['items'] = $this->normalizeItems($item['items'], $hasActiveChild);
	                if (empty($items[$i]['items']) && $this->hideEmptyItems) {
	                    unset($items[$i]['items']);
	                    if (!isset($item['url'])) {
	                        unset($items[$i]);
	                        continue;
	                    }
	                }
	            }
	            if (!isset($item['active'])) {
	                if ($this->activateParents && $hasActiveChild || $this->activateItems && $this->isItemActive($item)) {
	                    $active = $items[$i]['active'] = true;
	                } else {
	                    $items[$i]['active'] = false;
	                }
	            } elseif ($item['active'] instanceof Closure) {
	                $active = $items[$i]['active'] = call_user_func($item['active'], $item, $hasActiveChild, $this->isItemActive($item), $this);
	            } elseif ($item['active']) {
	                $active = true;
	            }
	        }
	        return array_values($items);
	    }
	}
?>