<?php
namespace backend\widgets;

use Yii;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\base\Widget;
use yii\widgets\LinkPager;
use yii\widgets\LinkSorter;


class PropertyWidget extends Widget
{
    public $pageSize = 2;

    public $roles = [];

	public $pager = [];

    public $sorter = [];

    public $dataProvider;

	public function init()
    {
        parent::init();
    }

    public function run()
    {
    	parent::run();

        $this->dataProvider->pagination->pageSize = $this->pageSize;
        $models = $this->dataProvider->getModels();
        $content = $this->renderList($models);
        echo $content;
    	
    }

    public function getSortAttribute(){
        
        if( isset(\Yii::$app->request->queryParams['sort']) )
            return $this->dataProvider->getSort()->attributes[str_replace('-', '', \Yii::$app->request->queryParams['sort'])]['label'];
        return '---';
    }

    public function renderList($models)
    {
    	$listItems = [];
    	foreach ($models as $key => $model) {
    		$listItems[] = $this->renderListItem($model);
    	}
    	if (empty($listItems)) {
    		$listItems[] = $this->renderEmptyItem();
    	}

    	$pager = $this->renderPager();

        $sorter = $this->renderSorter();

    	return "<div class='filters col-xs-6'>
                    <div class=\"btn-group dropdown-btn-group pull-right\">
                        <button class=\"btn btn-gray dropdown-toggle\" data-toggle=\"dropdown\" aria-expanded=\"false\">
                            Sort By ".$this->getSortAttribute()."<span class=\"caret\"></span>
                        </button>"
                        .$sorter.
                    "</div>
                </div>\n
                <ul class=\"list flexbox col-md-12\">\n"
                    .implode("\n", $listItems)."\n
                </ul>\n
                <div class=\"col-xs-12\">".$pager."</div>";
    }

    public function renderListItem($model)
    {
    	$propertyInfo = $this->renderPropertyInfo($model);

        $transactionInfo = $this->renderTransactionInfo($model);

    	return Html::decode('
	    	<li class="">
	    		'.$propertyInfo.'<hr>
                '.$transactionInfo.'
	    	</li>');    	
    }

    public function renderPropertyInfo($model)
    {
        $property_model = $model->property;

        $t_info = $model->getTransactionInfo()->one();

        $img = '<div class="img">'.Html::img( isset($property_model->location->photo) ? $property_model->location->photo : Url::to(['@uri_img/no_thumb.png'], true) ).'</div>';

        $title = '<div class="title">'.$property_model->title.'</div>';

        $address = '<address>
                        <i class="icon-location-pin2"></i>
                        <span>
                            '.sprintf('%s, %s, %s', $property_model->location->address_1, $property_model->location->city, $property_model->location->short_name).'                                             
                        </span>
                    </address>';

        $details = '<ul class="transaction_details flexbox">
                        <li>
                            <i class="icon-bed2"></i>'.Html::encode($property_model->beds).' Bedrooms
                        </li>
                        <li>
                            <i class="icon-man-woman"></i>'.Html::encode($property_model->baths).' Bathrooms
                        </li>
                        <li>
                            <i class="icon-directions_car"></i>'.Html::encode($property_model->garage).' Garage
                        </li>
                    </ul>';

        $price = '<div class="listed_on">
                    Listed on '.\Yii::$app->formatter->asDate($t_info->listing_date, 'medium').' for $'.Html::encode( number_format($t_info->listing_price, 0, ',', '\'') ).'
                </div>';

        $mls = '<div class="mls flexbox">
                    <p>MLS Name: '.$t_info->mls.'</p>
                    <p>MLS Number: '.$t_info->mls_number.'</p>
                </div>';

        $result = '
            <div class="property_info">'.
                $img.
                '<div class="content">'.
                    $title.
                    $address.
                    $details.
                    $price.
                '</div>'.
                $mls.
            '</div>
        ';

        return $result;

    }   

    public function renderTransactionInfo($model)
    {
        $t_info = $model->getTransactionInfo()->one();

        $actions = '<div class="actions row">
                        <div class="col-xs-6">
                            <div class="checkbox">
                                <label class="control-label col-sm-5">Approved</label>
                                <div class="col-sm-7">
                                    <input type="checkbox" class="iswitch iswitch-secondary"'.( $model->status ? 'checked="checked"' : '' ).'
                                        onchange="transactionStatus(event, "approved")" />
                                </div>
                            </div> 
                        </div>
                        <div class="col-xs-6">
                            <div class="checkbox">
                                <label class="control-label col-sm-5">Featured</label>
                                <div class="col-sm-7">
                                    <input type="checkbox" class="iswitch iswitch-secondary"'.( $model->featured ? 'checked="checked"' : '' ).' 
                                        onchange="transactionStatus(event, "featured")" />
                                </div>
                            </div> 
                        </div>
                    </div>';

        if($t_info->selling_date){
            $sold = '<div class="sold">
                    Sold on '.\Yii::$app->formatter->asDate($t_info->selling_date, 'medium').' for $'.Html::encode( number_format($t_info->listing_price, 0, ',', '\'') ).'
                </div>';
        } else {
            $sold = '<div class="sold">Isn\'t sold yet!</div>';
        }     
              
        $client = '<div class="client_info">'
                        .$this->renderClientInfo($model->client, $sold).'
                    </div>';       
        
        $result = '<div class="transaction_info">'.
                        $actions.
                        $client.
                    '</div>';
        
        return $result;
    }

    public function renderClientInfo($client_model, $sold){
        if($client_model){
            $client_img = '<div class="img">'.
                            Html::img($client_model->profile->getPhotoUrl()).
                        '</div>';
            $client_info = '<div class="content">
                                <div class="title">'.Html::encode($this->roles[$client_model->id_role]).'</div>'
                                .Html::encode($client_model->profile->getFullName()).
                                $sold.
                            '</div>';
            return $client_img.$client_info;
        }
        return '<h3>No client added!</h3>';
    }

    public function renderEmptyItem()
    {
    	return '<li class="empty"><h3 class="text-center">No Properties Yet!</h3></li>';
    }



    public function renderPager()
    {
        $pagination = $this->dataProvider->getPagination();
        if ($pagination === false || $this->dataProvider->getCount() <= 0) {
            return '';
        }
        /* @var $class LinkPager */
        $pager = $this->pager;
        $class = ArrayHelper::remove($pager, 'class', LinkPager::className());
        $pager['pagination'] = $pagination;
        $pager['options'] = ['class' => 'pagination pull-right'];
        $pager['view'] = $this->getView();

        return $class::widget($pager);
    }

    public function renderSorter()
    {
        $sort = $this->dataProvider->getSort();
        if ($sort === false || empty($sort->attributes) || $this->dataProvider->getCount() <= 0) {
            return '';
        }
        /* @var $class LinkSorter */
        $sorter = $this->sorter;
        $class = ArrayHelper::remove($sorter, 'class', LinkSorter::className());
        $sorter['sort'] = $sort;
        $sorter['options'] = ['class' => 'dropdown-menu'];
        $sorter['linkOptions'] = ['class' => 'outline-btn', 'data-pjax' => 'true'];
        //$sorter['attributes'] = ['transaction_quantity', ];
        $sorter['view'] = $this->getView();

        return $class::widget($sorter);
    }

    public function renderPrice($price){
        if( $price ){
            $num = 0;
            $affix = '';
            if( $price / 1000 > 1 ){
                if( $price / 1000000 > 1 ){                    
                    $num = round($price, -5) / 1000000;
                    $affix = 'm';
                }
                else{
                    $num = round($price, -2) / 1000;
                    $affix = 'k';
                }               
            } else {
                $num = $price;
            }
            return number_format( $num, 1, '.', '' ).$affix;
        }
        return 0;
    }
}