<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/linecons.css',   
        'css/elusive.css', 
        'css/icomoon.css', 
        'css/xenon.css',
        'css/xenon-components.css',
        'js/select2/select2.css',
        'js/cropper/cropper.min.css',
        'js/uikit/uikit.css',
        'css/global.css',  
        'css/main.css'      
        //'css/xenon-core.css',
        //'css/xenon-forms.css',
        //'css/xenon-skins.css'
    ];
    public $js = [
        'js/TweenMax.min.js',
        'js/resizeable.js',
        'js/joinable.js',        
        'js/ckeditor/ckeditor.js',
        'js/cropper/cropper.min.js',
        'js/bootstrap-datepicker.js',
        'js/dropzone/dropzone.min.js',
        'js/select2/select2.min.js',
        'js/toastr/toastr.min.js',
        'js/colorpicker/bootstrap-colorpicker.min.js',
        'js/uikit/js/uikit.min.js',
        'js/uikit/js/addons/nestable.min.js',
        'js/xenon-api.js',
        'js/xenon-toggles.js',
        'js/xenon-custom.js',
        'js/autoComplete.js',
        'js/main.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
        '\rmrevin\yii\fontawesome\AssetBundle',
    ];
}
