<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use rmrevin\yii\fontawesome\FA;
use kartik\rating\StarRating;
use yii\widgets\MaskedInput;

/* @var $this yii\web\View */
/* @var $review_model common\models\Review */

$this->title = 'Add new review';
$this->params['breadcrumbs'][] = ['label' => 'Reviews', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="panel panel-default">
    <?php $form = ActiveForm::begin([
        'layout' => 'horizontal',
        'options' => [

        ],
        'fieldConfig' => [
            'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
            'horizontalCssClasses' => [
                'label' => 'col-sm-4',
                'offset' => 'col-sm-offset-4',
                'wrapper' => 'col-sm-8',
            ],
        ]
    ]) ?>
        <div class="row">
            <div class="col-sm-6">
                <?= $form->field($review_model, 'star')->widget(StarRating::className(), [
                    'class' => 'inline_rating',
                    'pluginOptions' => [
                        'step' => 0.1,
                        'showClear' => false,
                        'filledStar' => Html::decode( FA::icon('star') ),
                        'emptyStar' => Html::decode( FA::icon('star-o') ),
                        'size' => '',
                        //'starCaptions' => $starCaptions
                    ],
                ])->label('Agent mark') ?>
            </div>
            <div class="col-sm-6">
                <?= $form->field($review_model, 'title')->textInput() ?>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6">
                <?= $form->field($review_model, 'first_name')->textInput()->label('Customer First Name') ?>
            </div>
            <div class="col-sm-6">
                <?= $form->field($review_model, 'last_name')->textInput()->label('Customer Last Name') ?>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6">
                <?= $form->field($review_model, 'email')->widget(MaskedInput::className(), [
                    'options' => [
                        'class' => 'form-control'
                    ],
                    'clientOptions' => [
                        'alias' => 'email'
                    ]
                ]) ?>
            </div>
            <div class="col-sm-6">
                <?= $form->field($review_model, 'id_user')->hiddenInput([
                    'class' => 'select2-remote',
                    'data-type' => json_encode(['agent', 'buyer', 'seller']),
                    'data-chosen-name' => (isset(\Yii::$app->request->bodyParams['Review']['chosen_user']) ?\Yii::$app->request->bodyParams['Review']['chosen_user'] : '')
                ])->label('Choose user') ?>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12">
                <?= $form->field($review_model, 'body', [
                    'horizontalCssClasses' => [
                        'label' => 'col-sm-12',
                        'offset' => 'col-sm-offset-0',
                        'wrapper' => 'col-sm-12',
                    ],
                ])->textarea(['rows' => 7])->label('Message', ['style' => ['text-align' => 'left']]) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-2 col-sm-4 pull-right-sm">

                <div class="action-buttons">
                    <?= Html::submitButton('Save', ['class' => 'btn btn-block btn-secondary']) ?>
                    <button type="reset" class="btn btn-block btn-gray">Reset form</button>
                </div>

            </div>
        </div>
    <?php ActiveForm::end() ?>
</div>
