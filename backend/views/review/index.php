<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\Pjax;
use rmrevin\yii\fontawesome\FA;
use kartik\date\DatePicker;
use kartik\rating\StarRating;

use common\models\UserRole;

/* @var $this yii\web\View */
/* @var $review_search common\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Reviews';
$this->params['breadcrumbs'][] = $this->title;

$roles = UserRole::find()->agent()->select(['name'])->indexBy('id')->all();

$tableActions = "<div class='row'>\n
                    <div class='col-sm-6'>\n
                        <div class='members-table-actions'>\n
                            <div class='selected-items'>\n
                                <span>0</span>\n
                                members selected\n
                            </div>\n

                            <div class='selected-actions'>\n
                                <a href='#' class='delete' data-action='delete-reviews'><i class='linecons-trash'></i>Delete</a>\n
                                or\n
                                <a href='#' class='edit' data-action='activate-reviews'><i class='fa-check'></i>Confirm</a>\n
                                selected.
                            </div>\n
                        </div>\n
                    </div>\n                    
                </div>";

?>

<?php Pjax::begin([
    'options' => ['id' => 'agent-list-container'],
    'timeout' => 5000,
]) ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $review_search,
        'layout'=>"{summary}\n".$tableActions."\n
                    <div class=\"table-responsive\" data-pattern=\"priority-columns\">\n{items}\n</div>".$tableActions."\n<div class='text-right text-center-sm'>{pager}</div>",

        'tableOptions' => [
            'class' => 'table table-small-font table-bordered table-striped members-table middle-align',
            'cellspacing' => 0,
        ],

        'options' => [
            'class' => 'table-responsive grid-view',
            'id' => 'agent-list',
        ],

        'pager' => [
            'options' => [
                'class' => 'pagination pagination-sm no-margin',
                'style' => 'float: right',
            ],
            'nextPageLabel' => FA::icon('angle-right'),
            'prevPageLabel' => FA::icon('angle-left'),
        ],

        'summary' => 'Showing <strong>{begin}</strong> - <strong>{end}</strong> of <strong>{totalCount}</strong> items.',
        'columns' => [
            // checkbox
            [
                'checkboxOptions' => ['class' => 'cbr'],
                'class' => 'yii\grid\CheckboxColumn',
                //'header' => '<input type="checkbox" class="select-on-check-all cbr" name="selection_all" value="1">',
            ],
            // title
            [
                'label' => 'Title',
                'attribute' => 'title',
                'format' => 'raw',
                'value' => function($review){
                    $delete = Html::a(
                        '<i class="linecons-trash"></i> Delete',
                        'javascript:void(0)',
                        [
                            'onclick' => 'ajaxDelete(event, '.$review->id.', \'review\')',
                            'class' => 'delete'
                        ]
                    );

                    $edit = Html::a('<i class="linecons-pencil"></i> Edit Review',
                        Url::toRoute(['review/update', 'id' => $review->id]),
                        [
                            'class' => 'edit',
                            'data-pjax' => 'false'
                        ]
                    );
                    $title = Html::tag('div', $review->title);
                    return $title.
                            '<div class="action-links">'.
                                $edit.
                                $delete.
                            '</div>';
                }
            ],
            // sender
            [
                //'label' => 'Commented by',
                'attribute' => 'sender_info',
                'format' => 'raw',
                'value' => function($review){
                    $fullName =  Html::tag('span',$review->first_name." ".$review->last_name, ['class' => 'name']);
                    $email = Html::a($review->email, 'mailto:'.$review->email);
                    return $fullName."\n".$email;
                }
            ],
            //agent
            [
                //'label' => 'Agent Info',
                'attribute' => 'owner_info',
                'format' => 'raw',
                'contentOptions' => ['class' => 'user-name'],
                'value' => function($review){

                    $img = Html::a(
                        Html::img($review->user->profile->getPhotoUrl(), [
                            'width'     => '32px',
                            'height'    => '32px',
                            'alt'       => $review->user->profile->getFullName(),
                            'class'     => 'img-circle',
                        ]),
                        Url::toRoute(['update', 'id' => $review->user->id], true),['data-pjax' => 'false']);

                    $fullName =  Html::a($review->user->profile->getFullName(), Url::toRoute(['update', 'id' => $review->user->id], true), ['class' => 'name', 'data-pjax' => 'false']);

                    $delete = Html::a(
                        '<i class="linecons-trash"></i> Delete',
                        'javascript:void(0)',
                        [
                            'onclick' => 'ajaxDelete(event, '.$review->user->id.')',
                            'class' => 'delete',
                        ]
                    );

                    $edit = Html::a('<i class="linecons-pencil"></i> Edit Profile',
                        Url::toRoute(['agent/update', 'id' => $review->user->id]),
                        [
                            'class' => 'edit',
                            'data-pjax' => 'false'
                        ]
                    );

                    $view = Html::a('<i class="fa-eye"></i> View Profile',
                        \Yii::$app->urlFrontEndManager->createAbsoluteUrl(['agent/profile', 'id' => $review->user->id], true),
                        [
                            'class' => 'view',
                            'data-pjax' => 'false'
                        ]
                    );

                    return '<div>'.
                                $img.
                                $fullName.
                                '<div class="action-links">'.
                                    $edit.
                                    $delete.
                                    $view.
                                '</div>
                            </div>';
                }
            ],
            //rating
            [
                'label' => 'Rating',
                'attribute' => 'star',
                'format' => 'raw',
                'value' => function($review){
                    return

                        StarRating::widget([
                            'name' => 'reviews_rating',
                            'value' => $review->star,
                            'pluginOptions' => [
                                'showClear' => false,
                                'filledStar' => Html::decode( FA::icon('star') ),
                                'emptyStar' => Html::decode( FA::icon('star-o') ),
                                'size' => 'sm',
                                'displayOnly' => true
                            ]
                        ]);
                },
                /*'filter' => Html::activeDropDownList($review_search, 'star', [1,2,3,4,5] ,[
                    'prompt' => 'All ratings',
                    'class'=>'select2-offscreen'
                ])*/
            ],
            //message
            [
                'contentOptions' => ['class' => 'message'],
                'label' => 'Message',
                'attribute' => 'body',
                'value' => 'body'
            ],

            //approved
            [
                'label' => 'Approved',
                'format' => 'raw',
                'value' => function($review){
                    return '<input type="checkbox" class="iswitch iswitch-secondary" value="'.$review->id.'" '.($review->user->status ? 'checked="checked"' : '').' 
                                        data-action="change-review-status" onchange="changeStatus(event)" />';
                }
            ],
            //creation date
            [
                'label' => 'Creation Date',
                'attribute' => 'created_at',
                'value' => function($review){
                    return \Yii::$app->formatter->asDate($review->created_at, 'medium');
                },
                'filter' => DatePicker::widget([
                    'model' => $review_search,
                    'attribute' => 'created_at',
                    'options' => ['placeholder' => 'Select creation date ...'],
                    'readonly' => true,
                    'pluginOptions' => [
                        'autoclose'=>true,
                        'format' => 'dd-mm-yyyy',
                        //'todayHighlight' => true
                    ]
                ])
            ],
            //modify date
            [
                'label' => 'Last Modify',
                'attribute' => 'updated_at',
                'value' => function($review){
                    return \Yii::$app->formatter->asDate($review->updated_at, 'medium');
                },
                'filter' => DatePicker::widget([
                    'model' => $review_search,
                    'attribute' => 'updated_at',
                    'options' => ['placeholder' => 'Select update date ...'],
                    'readonly' => true,
                    'pluginOptions' => [
                        'autoclose'=>true,
                        'format' => 'dd-mm-yyyy',
                        //'todayHighlight' => true
                    ]
                ])
            ]
        ],
    ]) ?>
<?php Pjax::end() ?>
