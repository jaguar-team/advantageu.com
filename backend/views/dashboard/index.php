<div class="xe-widget xe-counter" data-count=".num" data-from="0" data-to="99.9" data-suffix="%" data-duration="2">
    <div class="xe-icon"> <i class="linecons-cloud"></i> </div>
    <div class="xe-label"> <strong class="num">99.9%</strong> <span>Server uptime</span> </div>
</div>