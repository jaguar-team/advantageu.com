<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\widgets\MaskedInput;
use common\models\UserRole;


/* @var $this yii\web\View */
/* @var $user_role_model common\models\UserRole */
/* @var $register_form common\models\User */

$roles = UserRole::find()->admin()->indexBy('id')->select(['name'])->column();

$user_profile_model = $register_form->_profile;

$this->title = 'Create Administrator';
$this->params['breadcrumbs'][] = ['label' => 'Administrators', 'url' => ['administrator/index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="user-create">    

    <div class="panel panel-headerless">
        <div class="panel-body">
            <?php $form = ActiveForm::begin([
                'layout' => 'horizontal',
                'options' => [
                    'class' => ''
                ],
                'fieldConfig' => [
                    'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
                    'horizontalCssClasses' => [
                        'label' => 'col-md-2 col-sm-3',
                        'offset' => 'col-md-offset-2 col-sm-offset-3',
                        'wrapper' => 'col-md-10 col-sm-9',
                    ],
                ]
            ]) ?>

            <h3 class="panel-title">Required fields</h3>

            <?= $form->field($register_form, 'id_role')->dropDownList($roles, ['class' => 'select2-offscreen']) ?>

            <?= $form->field($register_form, 'email')->widget(MaskedInput::className(), [
                'options' => [
                    'class' => 'form-control'
                ],
                'clientOptions' => [
                    'alias' => 'email'
                ]
            ]) ?>

            <?= $form->field($register_form, 'password_hash')->passwordInput() ?>

            <?= $form->field($register_form, 'password_hash_repeat')->passwordInput() ?>

            <div class="row">
                <div class="col-md-2 col-sm-4 pull-right-sm">
                    <div class="action-buttons">
                        <?= Html::submitButton('Create Administrator', ['class' => 'btn btn-block btn-secondary']) ?>
                    </div>

                </div>
            </div>

            <div class="form-group-separator"></div>

            <h3 class="panel-title">Additional info</h3>

            <?= $form->field($register_form, 'status', [
                'checkboxTemplate' => "{beginWrapper}\n<div class=\"checkbox\">\n{beginLabel}\n{input}\n{labelTitle}\n{endLabel}\n</div>\n{error}\n{endWrapper}\n{hint}",
            ])->checkbox([
                'class'=>'iswitch iswitch-secondary'
            ], false)->label('Active') ?>

            <?= $form->field($user_profile_model, 'first_name')->textInput() ?>

            <?= $form->field($user_profile_model, 'last_name')->textInput() ?>

            <?= $form->field($user_profile_model, 'website_url')->textInput() ?>

            <?= $form->field($user_profile_model, 'company')->textInput() ?>

            <?= $form->field($user_profile_model, 'education')->textInput() ?>

            <?= $form->field($user_profile_model, 'phone_number', [
                'template' => "{label}\n{beginWrapper}\n<div class='input-group'>\n<div class='input-group-addon'>\n<i class='fa-mobile'></i>\n</div>\n{input}\n</div>\n{hint}\n{error}\n{endWrapper}"
            ])->widget(MaskedInput::className(), [
                'mask' => '(999)999-9999',
                'options' => [
                    'placeholder' => '(___)___-____',
                    'class' => 'form-control'
                ]
            ])->label('Phone number') ?>

            <div class="row">
                <div class="col-md-2 col-sm-4 pull-right-sm">
                    <div class="action-buttons">
                        <?= Html::submitButton('Create Administrator', ['class' => 'btn btn-block btn-secondary']) ?>
                    </div>

                </div>
            </div>

            <?php ActiveForm::end() ?>
        </div>

    </div>

</div>

