<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\Pjax;
use rmrevin\yii\fontawesome\FA;

use common\models\UserRole;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Administrators list';
$this->params['breadcrumbs'][] = $this->title;

$roles = UserRole::find()->admin()->select(['name'])->indexBy('id')->all();

$tableActions = "<div class='row'>\n
                    <div class='col-sm-6'>\n
                        <div class='members-table-actions'>\n
                            <div class='selected-items'>\n
                                <span>0</span>\n
                                members selected\n
                            </div>\n

                            <div class='selected-actions'>\n
                                <a href='#' class='delete' data-action='delete-users'><i class='linecons-trash'></i>Delete</a>\n
                                or\n
                                <a href='#' class='edit' data-action='activate-users'><i class='fa-check'></i>Confirm</a>\n
                                or\n
                                <a href='#' class='' data-action='certify-users'><i class='fa-certificate'></i>Certify</a>\n
                                selected.\n
                            </div>\n
                        </div>\n
                    </div>\n                    
                </div>";

?>

<div class="row">
    <div class="col-md-12">
        <?php Pjax::begin(['id' => 'admin-list-container']); ?>

            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                //'filterModel' => $searchModel,
                'layout'=>"{summary}\n".$tableActions."\n
                    <div class=\"table-responsive\" data-pattern=\"priority-columns\">\n{items}\n</div>".$tableActions."\n<div class='text-right text-center-sm'>{pager}</div>",

                'tableOptions' => [
                    'class' => 'table table-small-font table-bordered table-striped members-table middle-align',
                    'cellspacing' => 0,
                ],

                'options' => [
                    'class' => 'table-responsive grid-view',
                    'id' => 'admin-list',
                ],

                'pager' => [
                    'options' => [
                        'class' => 'pagination pagination-sm no-margin',
                        'style' => 'float: right',
                    ],
                    'nextPageLabel' => FA::icon('angle-right'),
                    'prevPageLabel' => FA::icon('angle-left'),
                ],

                'summary' => 'Showing <strong>{begin}</strong> - <strong>{end}</strong> of <strong>{totalCount}</strong> items.',

                'columns' => [
                    // checkbox
                    [
                        'cssClass' => 'cbr',
                        'class' => 'yii\grid\CheckboxColumn',
                        'header' => ''
                    ],
                    //name
                    [
                        'label' => 'Name',
                        'attribute' => 'full_name',
                        'format' => 'raw',
                        'contentOptions' => ['class' => 'user-name'],
                        'value' => function($user){

                            $img = Html::a(
                                Html::img($user->profile->getPhotoUrl(), [
                                    'width'     => '32px',
                                    'height'    => '32px',
                                    'alt'       => $user->profile->getFullName(),
                                    'class'     => 'img-circle',
                                ]),
                                Url::toRoute(['update', 'id' => $user->id], true),['data-pjax' => 'false']);

                            $fullName =  Html::a($user->profile->getFullName(), Url::toRoute(['update', 'id' => $user->id], true), ['class' => 'name', 'data-pjax' => 'false']);

                            $delete = Html::a(
                                '<i class="linecons-trash"></i> Delete',
                                'javascript:void(0)',
                                [
                                    'onclick' => 'ajaxDelete(event, '.$user->id.', \'user\')',
                                    'class' => 'delete'
                                ]
                            );

                            $edit = Html::a('<i class="linecons-pencil"></i> Edit Profile',
                                Url::toRoute(['agent/update', 'id' => $user->id]),
                                [
                                    'class' => 'edit',
                                    'data-pjax' => 'false'
                                ]
                            );

                            $view = Html::a('<i class="fa-eye"></i> View Profile',
                                \Yii::$app->urlFrontEndManager->createAbsoluteUrl(['agent/profile', 'id' => $user->id], true),
                                [
                                    'class' => 'view',
                                    'data-pjax' => 'false'
                                ]
                            );

                            return '<div>'.
                            $img.
                            $fullName.
                            '<div class="action-links">'.
                            $edit.
                            $delete.
                            $view.
                            '</div>
                        </div>';
                        }
                    ],
                    //approved
                    [
                        'label' => 'Approved',
                        'format' => 'raw',
                        'value' => function($user){
                            return '<input type="checkbox" class="iswitch iswitch-secondary" value="'.$user->id.'" '.($user->status ? 'checked="checked"' : '').' 
                                    data-action="change-user-status" onchange="changeStatus(event)" />';
                        }
                    ],
                    //role
                    [
                        'label' => 'Role',
                        'attribute' => 'id_role',
                        'format' => 'raw',
                        'value' => function($user){
                            $role = Html::dropDownList('user_role', $user->id_role, UserRole::find()->agent()->indexBy('id')->select(['name'])->column(), [
                                'class' => 'select2-offscreen',
                                'onchange' => 'changeRole(event, "change-user-role", '.$user->id.')'
                            ]);
                            return $role;
                        },
                        /*'filter' => Html::activeDropDownList($user_search, 'id_role', UserRole::find()->agent()->indexBy('id')->select(['name'])->column() ,[
                            'prompt' => 'All roles',
                            'class'=>'select2-offscreen'
                        ])*/
                    ],
                    //email
                    [
                        'label' => 'Email',
                        'attribute' => 'email',
                        'value' => 'email'
                    ],
                ],
                
            ]); ?>
        <?php Pjax::end() ?>
    </div>
</div>
