<?php


use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $user_model common\models\User */
/* @var $user_profile_model common\models\UserProfile */

$this->title = 'Update User: ' . $user_model->id;
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $user_model->id, 'url' => ['view', 'id' => $user_model->id]];
$this->params['breadcrumbs'][] = 'Update';
$roles = [null => ''];
foreach($user_model->role->findRoles(true) as $role){
    $roles[$role['id']] = $role['name'];
}
$gender_labels = [null => ''];
foreach ($user_profile_model->genderLabels() as $key => $gender_label) {
    $gender_labels[$key] = $gender_label;
}
?>
<div class="user-update">

    <h1><?= Html::encode($this->title) ?></h1>
    
    <div class="user-form">

        <div class="panel panel-headerless">
            <div class="panel-body">
                <h1><?= Html::encode($this->title) ?></h1>
                <?php Pjax::begin();?>

                    <div class="member-form-add-header">
                        <div class="row">
                            <div class="col-md-10 col-sm-8">
                                
                                <div id="advancedDropzone" class="droppable-area overflow-hidden" data-toggle="popover" data-trigger="hover" title="Notice" data-content="Click to change photo">
                                        <?= Html::img($user_profile_model->getPhotoUrl(), ['alt' => 'User avatar', 'width' => '100%', 'height' => '100%', 'id' => 'avatar-preview']); ?>
                                    </div>
                                <div class="user-name">
                                    <a href="#"><?= $user_profile_model->getFullName() ?></a>
                                    <span><?= $user_model->role->name ?></span>
                                </div>

                            </div>

                            <div class="col-md-2 col-sm-4 pull-right-sm">
                                
                            </div>
                        </div>
                    </div>

                    <?php $form = ActiveForm::begin([
                        'id' => 'photo-update',
                        'options' => ['enctype' => 'multipart/form-data']
                    ]); ?>

                        <?= $form->field($upload_avatar_form, 'user_avatar', ['template' => '{input}'])->fileInput(['class' => 'hidden', 'id' => 'user-avatar-file'])->label(false); ?>

                        <?= $form->field($upload_avatar_form, 'crop[width]', ['template' => '{input}'])->hiddenInput(['id' => 'user-avatar-width'])->label(false); ?>

                        <?= $form->field($upload_avatar_form, 'crop[height]', ['template' => '{input}'])->hiddenInput(['id' => 'user-avatar-height'])->label(false); ?>

                        <?= $form->field($upload_avatar_form, 'crop[start][x]', ['template' => '{input}'])->hiddenInput(['id' => 'user-avatar-offset-x'])->label(false); ?>

                        <?= $form->field($upload_avatar_form, 'crop[start][y]', ['template' => '{input}'])->hiddenInput(['id' => 'user-avatar-offset-y'])->label(false); ?>

                    <?php ActiveForm::end(); ?>

                    <?php $form = ActiveForm::begin([
                        'options' => [
                            'enctype' => 'multipart/form-data', 
                            'class' => 'member-form-inputs'
                        ],
                        'fieldConfig' => [
                            'template' => '<div class="row">
                                <div class="col-sm-3">{label}</div>
                                <div class="col-sm-9">{input}{error}{hint}</div>
                            </div>'
                        ]
                    ]); ?>

                        <?= $form->field($user_profile_model, 'first_name')->textInput(); ?>

                        <?= $form->field($user_profile_model, 'last_name')->textInput(); ?>

                        <?= $form->field($user_profile_model, 'biography')->textarea(['class' => 'form-control ckeditor', 'rows' => '5']); ?>

                        <?= $form->field($user_profile_model, 'blog_url')->textInput(['class' => 'form-control']); ?>

                        <?=$form->field($user_profile_model, 'gender')->dropDownList($gender_labels, ['class' => 'select2-offscreen']); ?>

                        <?= $form->field($user_profile_model, 'birth_year', [
                            'template' => '<div class="row">
                                    <div class="col-sm-3">{label}</div>
                                    <div class="col-sm-9">
                                        <div class="input-group">
                                            {input}
                                            <div class="input-group-addon">
                                                <a href="#"><i class="linecons-calendar"></i></a>
                                            </div>
                                            {error}{hint}    
                                        </div>
                                     </div>
                                </div>'
                            ])->textInput(['class' => 'form-control datepicker', 'data-format' => 'dd-mm-yyyy',
                        ]); ?>

                        <?= $form->field($user_profile_model, 'email_alternative')->textInput(['class' => 'form-control']); ?>

                        <?= $form->field($user_profile_model, 'education')->textInput(['class' => 'form-control']); ?>

                        <?= $form->field($user_profile_model, 'phone_number')->textInput(['class' => 'form-control']); ?>

                        <?= $form->field($user_profile_model, 'office_number')->textInput(['class' => 'form-control']); ?>

                        <div class="row">
                            <div class="col-md-2 col-sm-4 pull-right-sm">

                                <div class="action-buttons">
                                    <?= Html::submitButton('Save', ['class' => 'btn btn-block btn-secondary']) ?>
                                    <button type="reset" class="btn btn-block btn-gray">Reset form</button>
                                </div>

                            </div>
                        </div>

                    <?php ActiveForm::end(); ?>
                <?php Pjax::end();?>
            </div>
        </div>

    </div>

</div>
