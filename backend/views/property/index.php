<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\Pjax;
use rmrevin\yii\fontawesome\FA;
use common\models\UserRole;
use common\models\PropertyType;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $searchModel common\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Properties';
$this->params['breadcrumbs'][] = $this->title;

$roles = UserRole::find()->agent()->seller(true)->buyer(true)->select(['name'])->indexBy('id')->all();

$tableActions = "<div class='row'>\n
                    <div class='col-sm-6'>\n
                        <div class='members-table-actions'>\n
                            <div class='selected-items'>\n
                                <span>0</span>\n
                                members selected\n
                            </div>\n

                            <div class='selected-actions'>\n
                                <a href='#' class='delete' data-action='delete-users'><i class='linecons-trash'></i>Delete</a>\n
                                or\n
                                <a href='#' class='edit' data-action='activate-transactions'><i class='fa-check'></i>Approve</a>\n
                                or\n
                                <a href='#' class='' data-action='make-transactions-as-featured'><i class='fa-certificate'></i>Make Featured</a>\n
                                selected.\n
                            </div>\n
                        </div>\n
                    </div>\n                    
                </div>";

?>
<div class="user-index">

    <?php Pjax::begin([
        'options' => ['id' => 'property-list-container'],
        'timeout' => 5000,
    ]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $property_search,
        'layout'=>"{summary}\n".$tableActions."\n
                <div class=\"table-responsive\" data-pattern=\"priority-columns\">\n{items}\n</div>".$tableActions."\n<div class='text-right text-center-sm'>{pager}</div>",

        'tableOptions' => [
            'class' => 'table table-small-font table-bordered table-striped members-table middle-align',
            'cellspacing' => 0,
        ],

        'options' => [
            'class' => 'table-responsive grid-view',
            'id' => 'agent-list',
        ],

        'pager' => [
            'options' => [
                'class' => 'pagination pagination-sm no-margin',
                'style' => 'float: right',
            ],
            'nextPageLabel' => FA::icon('angle-right'),
            'prevPageLabel' => FA::icon('angle-left'),
        ],

        'summary' => 'Showing <strong>{begin}</strong> - <strong>{end}</strong> of <strong>{totalCount}</strong> items.',
        'columns' => [
            // checkbox
            [
                'checkboxOptions' => ['class' => 'cbr'],
                'class' => 'yii\grid\CheckboxColumn',
                //'header' => '<input type="checkbox" class="select-on-check-all cbr" name="selection_all" value="1">',
            ],
            //title
            [
                'label' => 'Title',
                'attribute' => 'name',
                'format' => 'raw',
                'value' => function($transaction){

                    $img = Html::a(
                        Html::img($transaction->property->location->photo, [
                            'width'     => '32px',
                            'height'    => '32px',
                            'alt'       => $transaction->property->title,
                            'class'     => 'img-circle',
                        ]),
                        Url::toRoute(['update', 'id' => $transaction->id], true),['data-pjax' => 'false']);

                    $title =  Html::a($transaction->property->title, Url::toRoute(['property/update', 'id' => $transaction->id], true), ['class' => 'name', 'data-pjax' => 'false']);
                    $delete = Html::a(
                        '<i class="linecons-trash"></i> Delete',
                        'javascript:void(0)',
                        [
                            'onclick' => 'ajaxDelete(event, '.$transaction->id.', \'transaction\')',
                            'class' => 'delete',
                        ]
                    );
                    $edit = Html::a('<i class="linecons-pencil"></i> Edit',
                        Url::toRoute(['property/update', 'id' => $transaction->id]),
                        [
                            'class' => 'edit',
                            'data-pjax' => 'false'
                        ]
                    );
                    return '<div>'.
                                $img.
                                $title.
                                '<div class="action-links">'.
                                    $edit.
                                    $delete.
                                '</div>
                            </div>';
                }
            ],
            //transaction agent
            [
                'label' => 'Agent Name',
                'attribute' => 'agent_full_name',
                'format' => 'raw',
                'contentOptions' => ['class' => 'user-name'],
                'value' => function($transaction){
                    if(!$transaction->agent)
                        return "No agent yet";

                    $img = Html::a(
                        Html::img($transaction->agent->profile->getPhotoUrl(), [
                            'width'     => '32px',
                            'height'    => '32px',
                            'alt'       => $transaction->agent->profile->getFullName(),
                            'class'     => 'img-circle',
                        ]),
                        Url::toRoute(['agent/edit', 'id' => $transaction->agent->id], true),['data-pjax' => 'false']);

                    $fullName =  Html::a($transaction->agent->profile->getFullName(true), Url::toRoute(['agent/edit', 'id' => $transaction->agent->id], true), ['class' => 'name', 'data-pjax' => 'false']);

                    $edit = Html::a('<i class="linecons-pencil"></i> Edit',
                        Url::toRoute(['agent/update', 'id' => $transaction->agent->id]),
                        [
                            'class' => 'edit',
                            'data-pjax' => 'false'
                        ]
                    );

                    $view = Html::a(
                        '<i class="fa-eye"></i> View',
                        \Yii::$app->urlFrontEndManager->createAbsoluteUrl(['agent/profile', 'id' => $transaction->agent->id], true),
                        [
                            'class' => 'view',
                            'data-pjax' => 'false'
                        ]
                    );

                    return '<div>'.
                                $img.
                                $fullName.
                                '<div class="action-links">'.
                                    $edit.
                                    $view.
                                '</div>
                            </div>';
                }
            ],
            //transaction client
            [
                'label' => 'Client Name',
                'attribute' => 'client_full_name',
                'format' => 'raw',
                'contentOptions' => ['class' => 'user-name'],
                'value' => function($transaction){
                    if(!$transaction->client){
                        return "No client yet";
                    }

                    $img = Html::a(
                        Html::img($transaction->client->profile->getPhotoUrl(), [
                            'width'     => '32px',
                            'height'    => '32px',
                            'alt'       => $transaction->client->profile->getFullName(),
                            'class'     => 'img-circle',
                        ]),
                        Url::toRoute(['agent/edit', 'id' => $transaction->client->id], true),['data-pjax' => 'false']);

                    $fullName =  Html::a($transaction->client->profile->getFullName(true), Url::toRoute(['agent/edit', 'id' => $transaction->client->id], true), ['class' => 'name', 'data-pjax' => 'false']);

                    $edit = Html::a('<i class="linecons-pencil"></i> Edit',
                        Url::toRoute(['agent/update', 'id' => $transaction->client->id]),
                        [
                            'class' => 'edit',
                            'data-pjax' => 'false'
                        ]
                    );

                    $view = Html::a(
                        '<i class="fa-eye"></i> View',
                        \Yii::$app->urlFrontEndManager->createAbsoluteUrl(['agent/profile', 'id' => $transaction->client->id], true),
                        [
                            'class' => 'view',
                            'data-pjax' => 'false'
                        ]
                    );

                    return '<div>'.
                                $img.
                                $fullName.
                                '<div class="action-links">'.
                                    $edit.
                                    $view.
                                '</div>
                            </div>';
                }
            ],
            //location
            [
                'label' => 'Location',
                'attribute' => 'location',
                'format' => 'raw',
                'value' => function($transaction){
                    $city = "<p>City: ".$transaction->property->location['city'].", ".$transaction->property->location['short_name']."</p>";
                    $address = "<p>Address: ".$transaction->property->location['address_1']."</p>";
                    $zipcode = "<p>ZipCode: ".$transaction->property->location['zipcode']."</p>";
                    return $address."\n".$city."\n".$zipcode;
                }
            ],
            //type
            [
                'label' => 'Type',
                'attribute' => 'type',
                'format' => 'raw',
                'value' => function($transaction){
                    $property_labels = PropertyType::find()->select(['name'])->indexBy('id')->column();
                    return $property_labels[$transaction->property->id_type];
                },
                'filter' => Html::activeDropDownList($property_search, 'type', PropertyType::find()->select(['name'])->indexBy('id')->column() ,[
                    'prompt' => 'All types',
                    'class'=>'select2-offscreen'
                ])
            ],
            //listing date
            [
                'label' => 'Listing Date',
                'attribute' => 'listing_date',
                'value' => function($transaction){
                    return \Yii::$app->formatter->asDate($transaction->transactionInfo->listing_date, 'medium');
                },
                'filter' => DatePicker::widget([
                    'model' => $property_search,
                    'attribute' => 'listing_date',
                    'options' => ['placeholder' => 'Select listing date ...'],
                    'readonly' => true,
                    'pluginOptions' => [
                        'autoclose'=>true,
                        'format' => 'dd-mm-yyyy',
                        //'todayHighlight' => true
                    ]
                ])
            ],
            //listing price
            [
                'label' => 'Listing Price',
                'attribute' => 'listing_price',
                'format' => 'raw',
                'value' => function($transaction){
                    return //\Yii::$app->formatter->asCurrency($transaction->transactionInfo->listing_price, "$");
                        "$".number_format($transaction->transactionInfo->listing_price, 0, '.', "'");
                }
            ],
            //selling date
            [
                'label' => 'Selling Date',
                'attribute' => 'selling_date',
                'value' => function($transaction){
                    return \Yii::$app->formatter->asDate($transaction->transactionInfo->selling_date, 'medium');
                },
                'filter' => DatePicker::widget([
                    'model' => $property_search,
                    'attribute' => 'selling_date',
                    'options' => ['placeholder' => 'Select selling date ...'],
                    'readonly' => true,
                    'pluginOptions' => [
                        'autoclose'=>true,
                        'format' => 'dd-mm-yyyy',
                        //'todayHighlight' => true
                    ]
                ])
            ],
            //selling price
            [
                'label' => 'Selling Price',
                'attribute' => 'selling_price',
                'format' => 'raw',
                'value' => function($transaction){
                    return //\Yii::$app->formatter->asCurrency($transaction->transactionInfo->selling_price, "en-US");
                        "$".number_format($transaction->transactionInfo->listing_price, 0, '.', "'");
                }
            ],
            //is approved
            [
                'label' => 'Approved',
                'attribute' => 'status',
                'format' => 'raw',
                'value' => function($transaction){
                    return '<input type="checkbox" class="iswitch iswitch-secondary" value="'.$transaction->id.'" '.($transaction->status ? 'checked="checked"' : '').' 
                                    data-action="change-transaction-status" onchange="changeStatus(event)" />';
                }
            ],
            //is featured
            [
                'label' => 'Featured',
                'attribute' => 'featured',
                'format' => 'raw',
                'value' => function($transaction){
                    return '<input type="checkbox" class="iswitch iswitch-secondary" value="'.$transaction->id.'" '.($transaction->featured ? 'checked="checked"' : '').' 
                                    data-action="change-transaction-feature-status" onchange="changeStatus(event)" />';
                }
            ],
            //creation date
            [
                'label' => 'Creation Date',
                'attribute' => 'created_at',
                'value' => function($transaction){
                    return \Yii::$app->formatter->asDate($transaction->created_at, 'medium');
                },
                'filter' => DatePicker::widget([
                    'model' => $property_search,
                    'attribute' => 'created_at',
                    'options' => ['placeholder' => 'Select creation date ...'],
                    'readonly' => true,
                    'pluginOptions' => [
                        'autoclose'=>true,
                        'format' => 'dd-mm-yyyy',
                        //'todayHighlight' => true
                    ]
                ])
            ],
            //modify date
            [
                'label' => 'Last Modify',
                'attribute' => 'updated_at',
                'value' => function($transaction){
                    return \Yii::$app->formatter->asDate($transaction->updated_at, "medium");
                },
                'filter' => DatePicker::widget([
                    'model' => $property_search,
                    'attribute' => 'updated_at',
                    'options' => ['placeholder' => 'Select update date ...'],
                    'readonly' => true,
                    'pluginOptions' => [
                        'autoclose'=>true,
                        'format' => 'dd-mm-yyyy',
                        //'todayHighlight' => true
                    ]
                ])
            ]
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
