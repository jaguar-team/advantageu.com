<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\widgets\MaskedInput;
use rmrevin\yii\fontawesome\FA;

use common\models\User;
use common\models\PropertyType;


$this->title = 'Add New Property';
$this->params['breadcrumbs'][] = ['label' => 'Properties', 'url' => ['property/index']];
$this->params['breadcrumbs'][] = $this->title;

$property_types = [];
foreach (PropertyType::find()->asArray()->all() as $key => $type) {
    $property_types[$type['id']] = $type['name'];
}


$clients = [];
foreach (User::find()->buyer()->seller(true)->all() as $key => $user) {
    $clients[$user->getRole()->one()->name][$user->id] = $user->email.'  ('.$user->profile->getFullName().')';
}

$agents = [];
foreach (User::find()->agent()->all() as $key => $user) {
    $agents[$user->getRole()->one()->name][$user->id] = $user->email.'  ('.$user->profile->getFullName().')';
}

?>

<div class="panel panel-default">

    <?php $form = ActiveForm::begin([
        'layout' => 'horizontal',
        'options' => [

        ],
        'fieldConfig' => [
            'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
            'horizontalCssClasses' => [
                'label' => 'col-sm-4',
                'offset' => 'col-sm-offset-4',
                'wrapper' => 'col-sm-8',
            ],
        ]
    ]) ?>

    <div class="row">

        <div class="col-sm-6">

                <?= $form->field($transaction_model, 'id_client')->hiddenInput(['class' => 'select2-remote', 'data-type' => json_encode(['buyer', 'seller'])])->label('Choose client') ?>

            </div>

            <div class="col-sm-6">
                <?= $form->field($transaction_model, 'id_agent')->hiddenInput([
                    'class' => 'select2-remote',
                    'data-type' => "agent",
                    'data-chosen-name' => (isset(\Yii::$app->request->bodyParams['Transaction']) ?\Yii::$app->request->bodyParams['Transaction']['chosen_agent'] : '')
                ])->label('Choose agent') ?>
            </div>

    </div>

    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
                <label class="control-label col-sm-4">Address</label>
                <div class="col-sm-8 search_areas loading async_inline">

                    <input class="form-control" type="text"
                           data-type="address"
                           data-city-id="<?= Html::getInputId($property_location_model, 'city') ?>"
                           data-state-id="<?= Html::getInputId($property_location_model, 'state') ?>"
                           data-zipcode-id="<?= Html::getInputId($property_location_model, 'zipcode') ?>"
                           data-address-id="<?= Html::getInputId($property_location_model, 'address_1') ?>"
                           data-lat-id="<?= Html::getInputId($property_location_model, 'lat') ?>"
                           data-lng-id="<?= Html::getInputId($property_location_model, 'lng') ?>"
                           data-place_id-id="<?= Html::getInputId($property_location_model, 'place_id') ?>"
                           data-short-id="<?= Html::getInputId($property_location_model, 'short_name') ?>"
                           data-photo-id="<?= Html::getInputId($property_location_model, 'photo') ?>" />

                    <div class="hidden">
                        <?= $form->field($property_location_model, 'city')->hiddenInput()->label(false) ?>
                        <?= $form->field($property_location_model, 'state')->hiddenInput()->label(false) ?>
                        <?= $form->field($property_location_model, 'zipcode')->hiddenInput()->label(false) ?>
                        <?= $form->field($property_location_model, 'address_1')->hiddenInput()->label(false) ?>
                        <?= $form->field($property_location_model, 'lat')->hiddenInput()->label(false) ?>
                        <?= $form->field($property_location_model, 'lng')->hiddenInput()->label(false) ?>
                        <?= $form->field($property_location_model, 'place_id')->hiddenInput()->label(false) ?>
                        <?= $form->field($property_location_model, 'short_name')->hiddenInput()->label(false) ?>
                        <?= $form->field($property_location_model, 'photo')->hiddenInput()->label(false) ?>
                    </div>

                </div>
            </div>

        </div>

        <div class="col-md-6">
            <?= $form->field($property_model, 'id_type')->dropDownList($property_types, ['class'=>'select2-offscreen'])->label('Property type') ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-6">
            <?= $form->field($property_model, 'beds', [
                'template' =>  '{label}
					    		{beginWrapper}
					    			<div class="input-group">						    				
					    				<div class="input-group-addon"><i class="icon-bed2"></i></div>
					    				{input}
					    				<div class="input-group-addon" onclick="decrease(event, `'.Html::getInputId($property_model, 'beds').'`)"><i class="icon-minus"></i></div>
					    				<div class="input-group-addon" onclick="increase(event, `'.Html::getInputId($property_model, 'beds').'`)"><i class="icon-plus"></i></div>
					    			</div>
					    			{error}{hint}
					    		{endWrapper}
					    		<div class="clearfix"></div>',
            ])->widget(MaskedInput::className(), [
                'mask' => '9{1,}',
                'options' => [
                    'placeholder' => '',
                    'class' => 'form-control'
                ],
                'clientOptions' => [
                    'autoUnmask' => true,
                    'allowMinus' => false,
                ]
            ]) ?>
        </div>
        <div class="col-sm-6">
            <?= $form->field($property_model, 'baths', [
                'template' =>  '{label}
					    		{beginWrapper}
					    			<div class="input-group">						    				
					    				<div class="input-group-addon"><i class="icon-man-woman"></i></div>
					    				{input}
					    				<div class="input-group-addon" onclick="decrease(event, `'.Html::getInputId($property_model, 'baths').'`)"><i class="icon-minus"></i></div>
					    				<div class="input-group-addon" onclick="increase(event, `'.Html::getInputId($property_model, 'baths').'`)"><i class="icon-plus"></i></div>
					    			</div>
					    			{error}{hint}
					    		{endWrapper}
					    		<div class="clearfix"></div>',
            ])->widget(MaskedInput::className(), [
                'mask' => '9{1,}',
                'options' => [
                    'placeholder' => '',
                    'class' => 'form-control'
                ],
                'clientOptions' => [
                    'autoUnmask' => true,
                    'allowMinus' => false,
                ]
            ]) ?>
        </div>
    </div>

    <div class="row">

        <div class="col-sm-6">
            <?= $form->field($property_model, 'garage', [
                'template' =>  '{label}
					    		{beginWrapper}
					    			<div class="input-group">						    				
					    				<div class="input-group-addon"><i class="icon-directions_car"></i></div>
					    				{input}
					    				<div class="input-group-addon" onclick="decrease(event, `'.Html::getInputId($property_model, 'garage').'`)"><i class="icon-minus"></i></div>
					    				<div class="input-group-addon" onclick="increase(event, `'.Html::getInputId($property_model, 'garage').'`)"><i class="icon-plus"></i></div>
					    			</div>
					    			{error}{hint}
					    		{endWrapper}
					    		<div class="clearfix"></div>',
            ])->widget(MaskedInput::className(), [
                'mask' => '9{1,}',

                'options' => [
                    'placeholder' => '',
                    'class' => 'form-control'
                ],
                'clientOptions' => [
                    'autoUnmask' => true,
                    'allowMinus' => false,
                ]
            ]) ?>
        </div>

        <div class="col-sm-6">
            <?= $form->field($property_model, 'square_feet', [
                'template' =>  '{label}
					    		{beginWrapper}
					    			<div class="input-group">						    				
					    				<div class="input-group-addon"><i class="icon-transform"></i></div>
					    				{input}
					    			</div>
					    			{error}{hint}
					    		{endWrapper}
					    		<div class="clearfix"></div>',
            ])->widget(MaskedInput::className(), [
                'mask' => '9{1,}',

                'options' => [
                    'placeholder' => '',
                    'class' => 'form-control'
                ],
                'clientOptions' => [
                    'autoUnmask' => true,
                    'allowMinus' => false,
                ]
            ]) ?>
        </div>

    </div>

    <div class="row">
        <div class="col-sm-6">
            <?= $form->field($transaction_info_model, 'listing_price', [
                'template' =>  '{label}
					    		{beginWrapper}
					    			<div class="input-group">						    				
					    				<div class="input-group-addon">$</div>
					    				{input}
					    			</div>
					    			{error}{hint}
					    		{endWrapper}
					    		<div class="clearfix"></div>',
            ])->widget(MaskedInput::className(), [
                'mask' => '9{1,}',
                'options' => [
                    'placeholder' => '',
                    'class' => 'form-control'
                ],
                'clientOptions' => [
                    'autoUnmask' => true,
                ]
            ]) ?>
        </div>

        <div class="col-sm-6">
            <?= $form->field($transaction_info_model, 'listing_date', [
                'template' => '{label}
								{beginWrapper}
								<div class="input-group date">
									<div class="input-group-addon">
										<i class="icon-calendar3"></i>
									</div>
									{input}									
								</div>
								{error}{hint}
								{endWrapper}
								<div class="clearfix"></div>'
            ])->textInput([
                'class' => 'form-control datepicker',
                'readonly' => 'readonly',
                'value' => ($transaction_info_model->listing_date) ? \Yii::$app->formatter->asDate($transaction_info_model->listing_date, 'php:m/d/Y') : null
            ]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-6">
            <?= $form->field($transaction_info_model, 'selling_price', [
                'template' =>  '{label}
					    		{beginWrapper}
					    			<div class="input-group">						    				
					    				<div class="input-group-addon">$</div>
					    				{input}
					    			</div>
					    			{error}{hint}
					    		{endWrapper}
					    		<div class="clearfix"></div>',
            ])->widget(MaskedInput::className(), [
                'mask' => '9{1,}',
                'options' => [
                    'placeholder' => '',
                    'class' => 'form-control'
                ],
                'clientOptions' => [
                    'autoUnmask' => true,
                ]
            ]) ?>
        </div>

        <div class="col-sm-6">
            <?= $form->field($transaction_info_model, 'selling_date', [
                'template' => '{label}
								{beginWrapper}
								<div class="input-group date">
									<div class="input-group-addon">
										<i class="icon-calendar3"></i>
									</div>
									{input}									
								</div>
								{error}{hint}
								{endWrapper}
								<div class="clearfix"></div>'
            ])->textInput([
                'class' => 'form-control datepicker',
                'readonly' => 'readonly',
                'value' => ($transaction_info_model->selling_date) ? \Yii::$app->formatter->asDate($transaction_info_model->selling_date, 'php:m/d/Y') : null
            ]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-6">
            <?= $form->field($transaction_info_model, 'mls')->textInput() ?>
        </div>

        <div class="col-sm-6">
            <?= $form->field($transaction_info_model, 'mls_number')->textInput() ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-6">
            <?= $form->field($property_model, 'title')->textInput() ?>
        </div>
        <div class="col-sm-6">
            <div class="row">
                <div class="col-xs-6">
                    <?= $form->field($transaction_model, 'status', [
                        'checkboxTemplate' => "{beginWrapper}\n<div class=\"checkbox\">\n{beginLabel}\n{input}\n{labelTitle}\n{endLabel}\n</div>\n{error}\n{endWrapper}\n{hint}",
                        'horizontalCssClasses' => [
                            'label' => 'col-sm-8',
                            'offset' => 'col-sm-offset-8',
                            'wrapper' => 'col-sm-4',
                        ],
                    ])->checkbox([
                        'class'=>'iswitch iswitch-secondary'
                    ], false)->label('Active') ?>

                </div>
                <div class="col-xs-6">
                    <?= $form->field($transaction_model, 'featured', [
                        'checkboxTemplate' => "{beginWrapper}\n<div class=\"checkbox\">\n{beginLabel}\n{input}\n{labelTitle}\n{endLabel}\n</div>\n{error}\n{endWrapper}\n{hint}",
                    ])->checkbox([
                        'class'=>'iswitch iswitch-secondary',
                    ], false) ?>
                </div>
            </div>

        </div>
    </div>

    <div class="row">
        <div class="col-md-2 col-sm-4 pull-right-sm">

            <div class="action-buttons">
                <button type="submit" class="btn btn-block btn-secondary">Save Changes</button>
            </div>

        </div>
    </div>

    <?php ActiveForm::end() ?>
</div>
