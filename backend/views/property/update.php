<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 002 02.08.17
 * Time: 14:01
 */

/**
 * @var $transaction_model
 * @var $transaction_model->transactionInfo
 * @var $transaction_model->property
 * @var $transaction_model->property->location
 */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\widgets\Pjax;
use yii\widgets\MaskedInput;

use common\models\User;
use common\models\PropertyType;

$property_types = [];
foreach (PropertyType::find()->asArray()->all() as $key => $type) {
    $property_types[$type['id']] = $type['name'];
}

$this->title = 'Update Property: '. $transaction_model->id;
$this->params['breadcrumbs'][] = ['label' => 'Properties', 'url' => ['property/index']];
$this->params['breadcrumbs'][] = 'Update: ' . $transaction_model->id;

?>

<div class="panel panel-default">
    <div class="row">

        <div class="col-sm-6">
            <?php Pjax::begin(['options' => ['class' => 'async_inline']]) ?>
                <?php $form = ActiveForm::begin([
                    'layout' => 'horizontal',
                    'fieldConfig' => [
                        'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
                        'horizontalCssClasses' => [
                            'label' => 'col-sm-4',
                            'offset' => 'col-sm-offset-4',
                            'wrapper' => 'col-sm-8',
                        ],
                    ],
                    'options' => [
                        'data-pjax' => 'true',
                    ]
                ]) ?>
                    <?= $form->field($transaction_model, 'id_client')->hiddenInput([
                        'class' => 'select2-remote',
                        'data-type' => json_encode(['buyer', 'seller']),
                        'data-chosen-name' => (!is_null(User::findOne($transaction_model->id_client)) ? User::findOne($transaction_model->id_client)->getProfile()->one()->getFullName() : null ),
                        'onblur' => 'submitForm(event)'
                    ])->label('Choose client') ?>
                <?php ActiveForm::end() ?>
            <?php Pjax::end() ?>
        </div>

        <div class="col-sm-6">
            <?php Pjax::begin(['options' => ['class' => 'async_inline']]) ?>
                <?php $form = ActiveForm::begin([
                    'layout' => 'horizontal',
                    'fieldConfig' => [
                        'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
                        'horizontalCssClasses' => [
                            'label' => 'col-sm-4',
                            'offset' => 'col-sm-offset-4',
                            'wrapper' => 'col-sm-8',
                        ],
                    ],
                    'options' => [
                        'data-pjax' => 'true',
                    ]
                ]) ?>
                    <?= $form->field($transaction_model, 'id_agent')->hiddenInput([
                        'class' => 'select2-remote',
                        'data-type' => "agent",
                        'data-chosen-name' => (!is_null(User::findOne($transaction_model->id_agent)) ? User::findOne($transaction_model->id_agent)->getProfile()->one()->getFullName() : null ),
                        'onblur' => 'submitForm(event)'
                    ])->label('Choose agent') ?>
                <?php ActiveForm::end() ?>
            <?php Pjax::end(); ?>
        </div>

    </div>

    <div class="row">
        <div class="col-sm-6">
            <?php Pjax::begin([
                'options' => ['class' => 'async_inline'],
                'enablePushState' => 0
            ]) ?>
                <?php $form = ActiveForm::begin([
                    'layout' => 'horizontal',
                    'fieldConfig' => [
                        'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
                        'horizontalCssClasses' => [
                            'label' => 'col-sm-4',
                            'offset' => 'col-sm-offset-4',
                            'wrapper' => 'col-sm-8',
                        ],
                    ],
                    'options' => [
                        'class' => 'area_validation',
                        'data-pjax' => 'true',
                        'validateOnBlur' => false
                    ]
                ]) ?>
                    <div class="form-group">
                        <label class="control-label col-sm-4" for="a1">Address</label>
                        <div class="col-sm-8 search_areas loading async_inline">

                            <input class="form-control" type="text" id="a1"
                                   data-type="address" value="<?= sprintf('%s %s, %s',
                                        ($transaction_model->property->location->address_1 ? $transaction_model->property->location->address_1.',' : ''),
                                        $transaction_model->property->location->city,
                                        $transaction_model->property->location->short_name
                                    ) ?>"
                                   data-city-id="<?= Html::getInputId($transaction_model->property->location, 'city') ?>"
                                   data-state-id="<?= Html::getInputId($transaction_model->property->location, 'state') ?>"
                                   data-zipcode-id="<?= Html::getInputId($transaction_model->property->location, 'zipcode') ?>"
                                   data-address-id="<?= Html::getInputId($transaction_model->property->location, 'address_1') ?>"
                                   data-lat-id="<?= Html::getInputId($transaction_model->property->location, 'lat') ?>"
                                   data-lng-id="<?= Html::getInputId($transaction_model->property->location, 'lng') ?>"
                                   data-place_id-id="<?= Html::getInputId($transaction_model->property->location, 'place_id') ?>"
                                   data-short-id="<?= Html::getInputId($transaction_model->property->location, 'short_name') ?>"
                                   data-photo-id="<?= Html::getInputId($transaction_model->property->location, 'photo') ?>" />

                            <div class="hidden">
                                <?= $form->field($transaction_model->property->location, 'city')->hiddenInput()->label(false) ?>
                                <?= $form->field($transaction_model->property->location, 'state')->hiddenInput()->label(false) ?>
                                <?= $form->field($transaction_model->property->location, 'zipcode')->hiddenInput()->label(false) ?>
                                <?= $form->field($transaction_model->property->location, 'address_1')->hiddenInput()->label(false) ?>
                                <?= $form->field($transaction_model->property->location, 'lat')->hiddenInput()->label(false) ?>
                                <?= $form->field($transaction_model->property->location, 'lng')->hiddenInput()->label(false) ?>
                                <?= $form->field($transaction_model->property->location, 'place_id')->hiddenInput()->label(false) ?>
                                <?= $form->field($transaction_model->property->location, 'short_name')->hiddenInput()->label(false) ?>
                                <?= $form->field($transaction_model->property->location, 'photo')->hiddenInput()->label(false) ?>
                            </div>

                        </div>
                    </div>
                <?php ActiveForm::end() ?>
            <?php Pjax::end(); ?>
        </div>

        <div class="col-md-6">
            <?php Pjax::begin(['options' => ['class' => 'async_inline']]) ?>
                <?php $form = ActiveForm::begin([
                    'layout' => 'horizontal',
                    'fieldConfig' => [
                        'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
                        'horizontalCssClasses' => [
                            'label' => 'col-sm-4',
                            'offset' => 'col-sm-offset-4',
                            'wrapper' => 'col-sm-8',
                        ],
                    ],
                    'options' => [
                        'data-pjax' => 'true',
                    ]
                ]) ?>
                    <?= $form->field($transaction_model->property, 'id_type')->dropDownList($property_types, [
                        'class'=> 'select2-offscreen',
                        'onblur' => 'submitForm(event)'
                    ])->label('Property type') ?>
                <?php ActiveForm::end() ?>
            <?php Pjax::end() ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-6">
            <?php Pjax::begin(['options' => ['class' => 'async_inline']]) ?>
                <?php $form = ActiveForm::begin([
                    'layout' => 'horizontal',
                    'fieldConfig' => [
                        'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
                        'horizontalCssClasses' => [
                            'label' => 'col-sm-4',
                            'offset' => 'col-sm-offset-4',
                            'wrapper' => 'col-sm-8',
                        ],
                    ],
                    'options' => ['data-pjax' => 'true']
                ]) ?>
                    <?= $form->field($transaction_model->property, 'beds', [
                        'template' =>  '{label}
                                        {beginWrapper}
                                            <div class="input-group">						    				
                                                <div class="input-group-addon"><i class="icon-bed2"></i></div>
                                                {input}
                                                <div class="input-group-addon" onclick="decrease(event, `'.Html::getInputId($transaction_model->property, 'beds').'`)"><i class="icon-minus"></i></div>
                                                <div class="input-group-addon" onclick="increase(event, `'.Html::getInputId($transaction_model->property, 'beds').'`)"><i class="icon-plus"></i></div>
                                            </div>
                                            {error}{hint}
                                        {endWrapper}
                                        <div class="clearfix"></div>',
                    ])->widget(MaskedInput::className(), [
                        'mask' => '9{1,}',
                        'options' => [
                            'placeholder' => '',
                            'class' => 'form-control',
                            //'onblur' => 'submitForm(event)'
                        ],
                        'clientOptions' => [
                            'autoUnmask' => true,
                            'allowMinus' => false,
                        ]
                    ]) ?>
                <?php ActiveForm::end() ?>
            <?php Pjax::end() ?>
        </div>
        <div class="col-sm-6">
            <?php Pjax::begin(['options' => ['class' => 'async_inline']]) ?>
                <?php $form = ActiveForm::begin([
                    'layout' => 'horizontal',
                    'fieldConfig' => [
                        'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
                        'horizontalCssClasses' => [
                            'label' => 'col-sm-4',
                            'offset' => 'col-sm-offset-4',
                            'wrapper' => 'col-sm-8',
                        ],
                    ],
                    'options' => ['data-pjax' => 'true']
                ]) ?>
                    <?= $form->field($transaction_model->property, 'baths', [
                        'template' =>  '{label}
                                        {beginWrapper}
                                            <div class="input-group">						    				
                                                <div class="input-group-addon"><i class="icon-man-woman"></i></div>
                                                {input}
                                                <div class="input-group-addon" onclick="decrease(event, `'.Html::getInputId($transaction_model->property, 'baths').'`)"><i class="icon-minus"></i></div>
                                                <div class="input-group-addon" onclick="increase(event, `'.Html::getInputId($transaction_model->property, 'baths').'`)"><i class="icon-plus"></i></div>
                                            </div>
                                            {error}{hint}
                                        {endWrapper}
                                        <div class="clearfix"></div>',
                    ])->widget(MaskedInput::className(), [
                        'mask' => '9{1,}',
                        'options' => [
                            'placeholder' => '',
                            'class' => 'form-control',
                            //'onblur' => 'submitForm(event)'
                        ],
                        'clientOptions' => [
                            'autoUnmask' => true,
                            'allowMinus' => false,
                        ]
                    ]) ?>
                <?php ActiveForm::end() ?>
            <?php Pjax::end() ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-6">
            <?php Pjax::begin(['options' => ['class' => 'async_inline']]) ?>
                <?php $form = ActiveForm::begin([
                    'layout' => 'horizontal',
                    'fieldConfig' => [
                        'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
                        'horizontalCssClasses' => [
                            'label' => 'col-sm-4',
                            'offset' => 'col-sm-offset-4',
                            'wrapper' => 'col-sm-8',
                        ],
                    ],
                    'options' => ['data-pjax' => 'true']
                ]) ?>
                    <?= $form->field($transaction_model->property, 'garage', [
                        'template' =>  '{label}
                                        {beginWrapper}
                                            <div class="input-group">						    				
                                                <div class="input-group-addon"><i class="icon-directions_car"></i></div>
                                                {input}
                                                <div class="input-group-addon" onclick="decrease(event, `'.Html::getInputId($transaction_model->property, 'garage').'`)"><i class="icon-minus"></i></div>
                                                <div class="input-group-addon" onclick="increase(event, `'.Html::getInputId($transaction_model->property, 'garage').'`)"><i class="icon-plus"></i></div>
                                            </div>
                                            {error}{hint}
                                        {endWrapper}
                                        <div class="clearfix"></div>',
                    ])->widget(MaskedInput::className(), [
                        'mask' => '9{1,}',
                        'options' => [
                            'placeholder' => '',
                            'class' => 'form-control',
                            //'onblur' => 'submitForm(event)'
                        ],
                        'clientOptions' => [
                            'autoUnmask' => true,
                            'allowMinus' => false,
                        ]
                    ]) ?>
                <?php ActiveForm::end() ?>
            <?php Pjax::end() ?>
        </div>

        <div class="col-sm-6">
            <?php Pjax::begin(['options' => ['class' => 'async_inline']]) ?>
                <?php $form = ActiveForm::begin([
                    'layout' => 'horizontal',
                    'fieldConfig' => [
                        'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
                        'horizontalCssClasses' => [
                            'label' => 'col-sm-4',
                            'offset' => 'col-sm-offset-4',
                            'wrapper' => 'col-sm-8',
                        ],
                    ],
                    'options' => ['data-pjax' => 'true']
                ]) ?>
                    <?= $form->field($transaction_model->property, 'square_feet', [
                        'template' =>  '{label}
                                        {beginWrapper}
                                            <div class="input-group">						    				
                                                <div class="input-group-addon"><i class="icon-transform"></i></div>
                                                {input}
                                            </div>
                                            {error}{hint}
                                        {endWrapper}
                                        <div class="clearfix"></div>',
                    ])->widget(MaskedInput::className(), [
                        'mask' => '9{1,}',
                        'options' => [
                            'placeholder' => '',
                            'class' => 'form-control',
                            'onblur' => 'submitForm(event)'
                        ],
                        'clientOptions' => [
                            'autoUnmask' => true,
                            'allowMinus' => false,
                        ]
                    ]) ?>
                <?php ActiveForm::end() ?>
            <?php Pjax::end() ?>
        </div>

    </div>

    <div class="row">
        <div class="col-sm-6">
            <?php Pjax::begin(['options' => ['class' => 'async_inline']]) ?>
                <?php $form = ActiveForm::begin([
                    'layout' => 'horizontal',
                    'fieldConfig' => [
                        'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
                        'horizontalCssClasses' => [
                            'label' => 'col-sm-4',
                            'offset' => 'col-sm-offset-4',
                            'wrapper' => 'col-sm-8',
                        ],
                    ],
                    'options' => ['data-pjax' => 'true']
                ]) ?>
                    <?= $form->field($transaction_model->transactionInfo, 'listing_price', [
                        'template' =>  '{label}
                                        {beginWrapper}
                                            <div class="input-group">						    				
                                                <div class="input-group-addon">$</div>
                                                {input}
                                            </div>
                                            {error}{hint}
                                        {endWrapper}
                                        <div class="clearfix"></div>',
                    ])->widget(MaskedInput::className(), [
                        'mask' => '9{1,}',
                        'options' => [
                            'placeholder' => '',
                            'class' => 'form-control',
                            'onblur' => 'submitForm(event)',
                            'value' => number_format($transaction_model->transactionInfo->listing_price, 0, '.', "")
                        ],
                        'clientOptions' => [
                            'autoUnmask' => true,
                        ],
                    ]) ?>
                <?php ActiveForm::end() ?>
            <?php Pjax::end() ?>
        </div>

        <div class="col-sm-6">
            <?php Pjax::begin(['options' => ['class' => 'async_inline']]) ?>
                <?php $form = ActiveForm::begin([
                    'layout' => 'horizontal',
                    'fieldConfig' => [
                        'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
                        'horizontalCssClasses' => [
                            'label' => 'col-sm-4',
                            'offset' => 'col-sm-offset-4',
                            'wrapper' => 'col-sm-8',
                        ],
                    ],
                    'options' => ['data-pjax' => 'true']
                ]) ?>
                    <?= $form->field($transaction_model->transactionInfo, 'listing_date', [
                        'template' => '{label}
                                        {beginWrapper}
                                        <div class="input-group date">
                                            <div class="input-group-addon">
                                                <i class="icon-calendar3"></i>
                                            </div>
                                            {input}									
                                        </div>
                                        {error}{hint}
                                        {endWrapper}
                                        <div class="clearfix"></div>'
                    ])->textInput([
                        'class' => 'form-control datepicker',
                        'readonly' => 'readonly',
                        'value' => ($transaction_model->transactionInfo->listing_date) ? \Yii::$app->formatter->asDate($transaction_model->transactionInfo->listing_date, 'php:m/d/Y') : null,
                        'onblur' => 'submitForm(event)'
                    ]) ?>
                <?php ActiveForm::end() ?>
            <?php Pjax::end() ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-6">
            <?php Pjax::begin(['options' => ['class' => 'async_inline']]) ?>
                <?php $form = ActiveForm::begin([
                    'layout' => 'horizontal',
                    'fieldConfig' => [
                        'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
                        'horizontalCssClasses' => [
                            'label' => 'col-sm-4',
                            'offset' => 'col-sm-offset-4',
                            'wrapper' => 'col-sm-8',
                        ],
                    ],
                    'options' => ['data-pjax' => 'true']
                ]) ?>
                    <?= $form->field($transaction_model->transactionInfo, 'selling_price', [
                        'template' =>  '{label}
                                        {beginWrapper}
                                            <div class="input-group">						    				
                                                <div class="input-group-addon">$</div>
                                                {input}
                                            </div>
                                            {error}{hint}
                                        {endWrapper}
                                        <div class="clearfix"></div>',
                    ])->widget(MaskedInput::className(), [
                        'mask' => '9{1,}',
                        'options' => [
                            'placeholder' => '',
                            'class' => 'form-control',
                            'onblur' => 'submitForm(event)',
                            'value' => number_format($transaction_model->transactionInfo->selling_price, 0, '.', "")
                        ],
                        'clientOptions' => [
                            'autoUnmask' => true,
                        ]
                    ]) ?>
                <?php ActiveForm::end() ?>
            <?php Pjax::end() ?>
        </div>

        <div class="col-sm-6">
            <?php Pjax::begin(['options' => ['class' => 'async_inline']]) ?>
                <?php $form = ActiveForm::begin([
                    'layout' => 'horizontal',
                    'fieldConfig' => [
                        'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
                        'horizontalCssClasses' => [
                            'label' => 'col-sm-4',
                            'offset' => 'col-sm-offset-4',
                            'wrapper' => 'col-sm-8',
                        ],
                    ],
                    'options' => ['data-pjax' => 'true']
                ]) ?>
                    <?= $form->field($transaction_model->transactionInfo, 'selling_date', [
                        'template' => '{label}
                                        {beginWrapper}
                                        <div class="input-group date">
                                            <div class="input-group-addon">
                                                <i class="icon-calendar3"></i>
                                            </div>
                                            {input}									
                                        </div>
                                        {error}{hint}
                                        {endWrapper}
                                        <div class="clearfix"></div>'
                    ])->textInput([
                        'class' => 'form-control datepicker',
                        'readonly' => 'readonly',
                        'value' => ($transaction_model->transactionInfo->selling_date) ? \Yii::$app->formatter->asDate($transaction_model->transactionInfo->selling_date, 'php:m/d/Y') : null,
                        'onblur' => 'submitForm(event)'
                    ]) ?>
                <?php ActiveForm::end() ?>
            <?php Pjax::end() ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-6">
            <?php Pjax::begin(['options' => ['class' => 'async_inline']]) ?>
                <?php $form = ActiveForm::begin([
                    'layout' => 'horizontal',
                    'fieldConfig' => [
                        'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
                        'horizontalCssClasses' => [
                            'label' => 'col-sm-4',
                            'offset' => 'col-sm-offset-4',
                            'wrapper' => 'col-sm-8',
                        ],
                    ],
                    'options' => ['data-pjax' => 'true']
                ]) ?>
                    <?= $form->field($transaction_model->transactionInfo, 'mls')->textInput(['onblur' => 'submitForm(event)']) ?>
                <?php ActiveForm::end() ?>
            <?php Pjax::end() ?>
        </div>

        <div class="col-sm-6">
            <?php Pjax::begin(['options' => ['class' => 'async_inline']]) ?>
                <?php $form = ActiveForm::begin([
                    'layout' => 'horizontal',
                    'fieldConfig' => [
                        'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
                        'horizontalCssClasses' => [
                            'label' => 'col-sm-4',
                            'offset' => 'col-sm-offset-4',
                            'wrapper' => 'col-sm-8',
                        ],
                    ],
                    'options' => ['data-pjax' => 'true']
                ]) ?>
                    <?= $form->field($transaction_model->transactionInfo, 'mls_number')->textInput(['onblur' => 'submitForm(event)']) ?>
                <?php ActiveForm::end() ?>
            <?php Pjax::end() ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-6">
            <?php Pjax::begin(['options' => ['class' => 'async_inline']]) ?>
                <?php $form = ActiveForm::begin([
                    'layout' => 'horizontal',
                    'fieldConfig' => [
                        'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
                        'horizontalCssClasses' => [
                            'label' => 'col-sm-4',
                            'offset' => 'col-sm-offset-4',
                            'wrapper' => 'col-sm-8',
                        ],
                    ],
                    'options' => ['data-pjax' => 'true']
                ]) ?>
                    <?= $form->field($transaction_model->property, 'title')->textInput(['onblur' => 'submitForm(event)']) ?>
                <?php ActiveForm::end() ?>
            <?php Pjax::end() ?>
        </div>
        <div class="col-sm-6">
            <div class="row">
                <div class="col-xs-6">
                    <?php Pjax::begin(['options' => ['class' => 'async_inline']]) ?>
                        <?php $form = ActiveForm::begin([
                            'layout' => 'horizontal',
                            'options' => ['data-pjax' => 'true']
                        ]) ?>
                            <?= $form->field($transaction_model, 'status', [
                                'checkboxTemplate' => "{beginWrapper}\n<div class=\"checkbox\">\n{beginLabel}\n{input}\n{labelTitle}\n{endLabel}\n</div>\n{error}\n{endWrapper}\n{hint}",
                                'horizontalCssClasses' => [
                                    'label' => 'col-sm-8',
                                    'offset' => 'col-sm-offset-8',
                                    'wrapper' => 'col-sm-4',
                                ],
                            ])->checkbox([
                                'class'=>'iswitch iswitch-secondary',
                                'onchange' => 'submitForm(event)'
                            ], false)->label('Active') ?>
                        <?php ActiveForm::end() ?>
                    <?php Pjax::end() ?>
                </div>
                <div class="col-xs-6">
                    <?php Pjax::begin(['options' => ['class' => 'async_inline']]) ?>
                        <?php $form = ActiveForm::begin([
                        'layout' => 'horizontal',
                        'fieldConfig' => [
                            'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
                            'horizontalCssClasses' => [
                                'label' => 'col-sm-4',
                                'offset' => 'col-sm-offset-4',
                                'wrapper' => 'col-sm-8',
                            ],
                        ],
                            'options' => ['data-pjax' => 'true']
                        ]) ?>
                            <?= $form->field($transaction_model, 'featured', [
                                'checkboxTemplate' => "{beginWrapper}\n<div class=\"checkbox\">\n{beginLabel}\n{input}\n{labelTitle}\n{endLabel}\n</div>\n{error}\n{endWrapper}\n{hint}",
                            ])->checkbox([
                                'class'=>'iswitch iswitch-secondary',
                                'onchange' => 'submitForm(event)'
                            ], false) ?>
                        <?php ActiveForm::end() ?>
                    <?php Pjax::end() ?>
                </div>
            </div>

        </div>
    </div>
</div>

