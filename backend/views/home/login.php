<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use rmrevin\yii\fontawesome\FA;

?>

<div class="login-container">

    <div class="row">

        <div class="col-sm-6">


            <!-- Errors container -->
            <div class="errors-container">


            </div>

            <!-- Add class "fade-in-effect" for login form effect -->

                <?php $form = ActiveForm::begin(['id' => 'login', 'options' => ['class' => 'login-form fade-in-effect in']]); ?>

                    <div class="login-header">
                        <a href="#" class="logo">
                            <?= Html::img(Url::to('@web/img/logo.png', true), ['alt' => 'Our logo', 'width' => 200]) ?>
                            <span>log in</span>
                        </a>
                        <p>Dear user, log in to access the admin area!</p>
                    </div>

                    <?= $form->field($model, 'email')->textInput(['autofocus' => true, 'class' => 'form-control', 'placeholder' => 'Email'])->label(false); ?>

                    <?= $form->field($model, 'password')->passwordInput(['class' => 'form-control', 'placeholder' => 'Password'])->label(false); ?>

                    <?= $form->field($model, 'rememberMe')->checkbox(['class' => 'cbr cbr-primary']); ?>

                    <div class="form-group">
                        <?= Html::submitButton(FA::icon('lock').'Log in', ['class' => 'btn btn-primary  btn-block text-left']) ?>
                    </div>

                <?php ActiveForm::end(); ?>

        </div>

    </div>


</div>