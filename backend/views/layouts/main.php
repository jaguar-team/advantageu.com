<?php

/* @var $this \yii\web\View */
/* @var $content string */
use yii\widgets\Pjax;
use backend\assets\AppAsset;
use yii\base\view;
use yii\helpers\Html;
use yii\helpers\Url;
use common\widgets\Alert;
use backend\widgets\ToastAlert;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= \Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>

        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDbgI-qDa-U_JWtQbuqj7kOdBK4vYBCJQ8&libraries=places&language=en" async></script>
    </head>
    <body class="page-body">

        <?php $this->registerJs('_base_url = "'.Url::to(\Yii::$app->homeUrl, true).'";', yii\web\View::POS_HEAD); ?>

        <?php $this->beginBody() ?>

        <?php echo $this->render('/elements/_settings_pane', array()); ?>

        <div class="page-container">

            <?php echo $this->render('/elements/_sidebar_menu', array()); ?>

            <div class="main-content">

                <?php echo $this->render('/elements/_header', array()); ?>

                <?php echo $this->render('/elements/_breadcrumbs', array()); ?>

                <?= Alert::widget() ?>

                <?= $content ?>

            </div>
        </div>

        <footer class="footer">
            
        </footer>

        <?php echo $this->render('/elements/_modals', array()); ?>

        <?php $this->endBody() ?>
    </body>
</html>

<?php 
    //Pjax::begin();
        //echo ToastAlert::widget();
    //Pjax::end() 
?>
<?php $this->endPage() ?>
