<?php 
  use yii\helpers\Html;
  use yii\helpers\Url;
?>
<!-- Modal -->
<div class="modal fade" id="crop" tabindex="-1" role="dialog" aria-labelledby="cropLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content panel panel-default">
      <div class="modal-header panel-heading">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="cropLabel">Image Cropping Options</h4>
      </div>
      <div class="modal-body panel-body">
        <div class="row">
            <div class="col-md-8">
                
                <strong class="text-primary">Original Image</strong>
                <br />
                <br />
                
                <div class="img-container">
                    
                </div>
                
            </div>
            <div class="col-md-4">
                
                <strong class="text-primary">Preview Image</strong>
                <br />
                <br />
                <div class="img-shade">
                    <div id="img-preview" class="img-preview"></div>
                </div>
                
                <table class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th width="25%">X</th>
                            <th width="25%">Y</th>
                            <th width="25%">W</th>
                            <th width="25%">H</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td id="img-1-x">0px</td>
                            <td id="img-1-y">0px</td>
                            <td id="img-1-w">0px</td>
                            <td id="img-1-h">0px</td>
                        </tr>
                    </tbody>
                </table>
                
                <a id="crop-img" href="" class="btn btn-secondary">Crop Image</a>
                
            </div>
        </div>
      </div>
    </div>
  </div>
</div>