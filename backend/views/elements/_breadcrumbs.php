<?php 
	use yii\helpers\Html;
	use yii\widgets\Breadcrumbs;
	use rmrevin\yii\fontawesome\FA;
?>

<div class="page-title">
			
	<div class="title-env">
		<h1 class="title"><?= Html::encode($this->title) ?></h1>
		<!--<p class="description">Members management page separated with tabs.</p>-->
	</div>

		<div class="breadcrumb-env">
			<?= Breadcrumbs::widget([
	            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
	            'options' => [
	            	'class' => 'breadcrumb bc-1',
	            ],
	            'homeLink' => [
	            	'url'=> Yii::$app->homeUrl,
	            	'label'=> 'Home',
	            	'template' => '<li>'.Html::decode(FA::icon('home')).'{link}</li>'
	            ],
	        ]) ?>				
			
	</div>
	
</div>