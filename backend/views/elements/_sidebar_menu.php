<?php
    use yii\helpers\Html;
    use yii\helpers\Url;
    use backend\widgets\MenuWidget;
    use backend\components\DisplayItems;

    use common\models\User;
 ?>

<div class="sidebar-menu toggle-others fixed <?= (DisplayItems::getParam(DisplayItems::SIDEBAR_ITEM) ? '' : 'collapsed') ?>">
        
    <div class="sidebar-menu-inner">
        
        <header class="logo-env">

            <!-- logo -->
            <div class="logo">
                <?= Html::a(Html::img( Url::to('@web/img/logo.png', true ), ['alt' => '', 'width' => 80]), ['administrator/index'], ['class' => 'logo-expanded']) ?>
                <?= Html::a(Html::img( Url::to('@web/img/logo.png', true ), ['alt' => '', 'width' => 40]), ['administrator/index'], ['class' => 'logo-collapsed']) ?>
            </div>

            <!-- This will toggle the mobile menu and will be visible only on mobile devices -->
            <div class="mobile-menu-toggle visible-xs">
                <a href="#" data-toggle="user-info-menu">
                    <i class="fa-bell-o"></i>
                    <span class="badge badge-success">7</span>
                </a>

                <a href="#" data-toggle="mobile-menu">
                    <i class="fa-bars"></i>
                </a>
            </div>

            <!-- This will open the popup with user profile settings, you can use for any purpose, just be creative -->
            <div class="settings-icon">
                <a href="#" data-toggle="settings-pane" data-animate="true">
                    <i class="linecons-cog"></i>
                </a>
            </div>

        </header>

        <section class="sidebar-user-info">
            <div class="sidebar-user-info-inner">
                <a href="#/app/extra-profile" class="user-profile">
                    <img src="<?= \Yii::$app->user->identity->profile->getPhotoUrl(); ?>" width="60" height="60" class="img-circle img-corona" alt="user-pic">
                    <span>
                        <strong><?= Html::encode(\Yii::$app->user->identity->profile->getFullName()); ?></strong>
                        Page admin
                    </span>
                </a>

                <ul class="user-links list-unstyled">
                    <li>
                        <?= Html::a(Html::decode('<i class="linecons-user"></i>').' Edit profile', Url::toRoute(['update', 'id' => \Yii::$app->user->id], true)); ?>
                        
                    </li>
                    
                    <li class="logout-link">
                        <?= Html::a(Html::decode('<i class="fa-power-off"></i>'), Url::toRoute(['home/logout']), ['data-method' => 'post']) ?>
                    </li>
                </ul>
            </div>
        </section>
        <!-- add class "multiple-expanded" to allow multiple submenus to open -->
            <!-- class "auto-inherit-active-class" will automatically add "active" class for parent elements who are marked already with class "active" -->
        
        <?php 
            echo MenuWidget::widget([
                'items' => [
                    // Important: you need to specify url as 'controller/action',
                    // not just as 'controller' even if default action is used.
                    // 'Products' menu item will be selected as long as the route is 'product/index'
                    ['label' => 'Dashboard', 'url' => ['#'], 'favicon' => 'fa-dashboard', 'badge' =>['text' => 'Coming soon', 'color' => 'purple']],
                    ['label' => 'Admins', 'url' => ['#'], 'favicon' => 'linecons-user', 'badge' =>['text' => 'New', 'color' => 'warning'], 'items' => [
                        ['label' => 'All administrators', 'url' => ['administrator/index'], 'badge' => ['text' => User::find()->admin()->count(), 'color' => 'secondary']],
                        ['label' => 'Add administrator', 'url' => ['administrator/create'], 'badge' =>['text' => 'New', 'color' => 'warning']],
                        ['label' => 'Add role', 'url' => ['#'], 'badge' =>['text' => 'Coming soon', 'color' => 'purple']],
                    ]],
                    ['label' => 'Agents', 'url' => ['#'], 'favicon' => 'el-adult', 'items' => [
                        ['label' => 'All agents', 'url' => ['agent/index'], 'badge' => ['text' => User::find()->agent()->count(), 'color' => 'secondary']],
                        ['label' => 'Add agent', 'url' => ['agent/create'], 'badge' => ['text' => 'New', 'color' => 'warning']],
                        ['label' => 'Add role', 'url' => ['#'], 'badge' =>['text' => 'Coming soon', 'color' => 'purple']],
                    ]],
                    ['label' => 'Clients', 'url' => ['#'], 'favicon' => 'el-child', 'badge' =>['text' => 'Coming soon', 'color' => 'purple'], 'items' => [
                        ['label' => 'All clients', 'url' => ['#'], 'badge' => ['text' => User::find()->buyer()->seller(true)->count(), 'color' => 'secondary']],
                        ['label' => 'Add client', 'url' => ['#']],
                        ['label' => 'Add role', 'url' => ['#'], 'badge' =>['text' => 'Coming soon', 'color' => 'purple']],
                    ]],
                    ['label' => 'Properties', 'url' => ['#'], 'favicon' => 'fa-building-o', 'items' => [
                        ['label' => 'All properties', 'url' => ['property/index']],
                        ['label' => 'Add property', 'url' => ['property/create']],
                    ]],
                    ['label' => 'Reviews', 'url' => ['#'], 'favicon' => 'linecons-star', 'badge' =>['text' => 'New', 'color' => 'warning'], 'items' => [
                        ['label' => 'All reviews', 'url' => ['review/index']],
                        ['label' => 'Add review', 'url' => ['review/create'], 'badge' =>['text' => 'New', 'color' => 'warning']],
                    ]],
                    ['label' => 'Settings', 'url' => ['#'], 'favicon' => 'linecons-params', 'items' => [
                        ['label' => 'General', 'url' => ['setting/general']],
                        //['label' => 'Custom fields', 'url' => ['setting/custom-fields']],
                    ]],
                    ['label' => 'Notifications', 'url' => ['#'], 'favicon' => 'el-bell', 'badge' => ['text' => 'Coming soon', 'color' => 'purple']],
                ],
                'activeCssClass'=>'active',
                'linkTemplate' => '<a href="{url}">{favicon}{label}{badge}</a>',
                'labelTemplate' => '<span class="title">{label}</span>',
                'activateParents' => true,
                'options' => [
                    'id'=>'main-menu',
                    'class' => 'main-menu',
                ],                
            ]);
        ?>
    </div>
</div>