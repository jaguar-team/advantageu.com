<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\widgets\MaskedInput;
use common\models\UserRole;

/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title = 'Add new agent';
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$roles = UserRole::find()->agent()->select(['name'])->indexBy('id')->column();
?>
<div class="user-create">
    <div class="panel panel-default">

        <?php $form = ActiveForm::begin([
            'layout' => 'horizontal',            
            'options' => [
                
            ],
            'fieldConfig' => [
                'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
                'horizontalCssClasses' => [
                    'label' => 'col-sm-4',
                    'offset' => 'col-sm-offset-4',
                    'wrapper' => 'col-sm-8',
                ],
            ]
        ]) ?>

            <h3>Required Info</h3>

            <div class="row">

                <div class="col-sm-6">                    

                    <?= $form->field($user_model, 'email')->widget(MaskedInput::className(), [
                        'options' => [
                            'class' => 'form-control'
                        ],
                        'clientOptions' => [
                            'alias' => 'email'
                        ]                                  
                    ]) ?>                       

                </div>   

                <div class="col-sm-6">

                    <?= $form->field($user_profile_model, 'first_name')->textInput(['class' => 'form-control']) ?>

                </div>                 

            </div>                    

            <div class="row">

                <div class="col-sm-6">
       
                    <?= $form->field($user_model, 'password_hash')->passwordInput() ?>
             
                </div>

                <div class="col-md-6">

                    <?= $form->field($user_profile_model, 'last_name')->textInput(['class' => 'form-control']) ?>

                </div>

            </div>

            <div class="row">

                <div class="col-md-6">

                    <?= $form->field($user_model, 'id_role')->dropDownList($roles, ['class' => 'select2-offscreen']); ?>

                </div>
                
            </div>

            <h3>Additional Info</h3>

            <div class="row">
                <div class="col-sm-4">

                    <?= $form->field($user_model, 'private_account', [
                            'checkboxTemplate' => "{beginWrapper}\n<div class=\"checkbox\">\n{beginLabel}\n{input}\n{labelTitle}\n{endLabel}\n</div>\n{error}\n{endWrapper}\n{hint}",
                            'horizontalCssClasses' => [
                                'label' => 'col-sm-8',
                                'offset' => 'col-sm-offset-8',
                                'wrapper' => 'col-sm-4',
                            ],
                        ])->checkbox([
                            'class'=>'iswitch iswitch-secondary'
                        ], false) ?>

                </div>

                <div class="col-sm-4">                       

                    <?= $form->field($user_model, 'private_property', [
                            'checkboxTemplate' => "{beginWrapper}\n<div class=\"checkbox\">\n{beginLabel}\n{input}\n{labelTitle}\n{endLabel}\n</div>\n{error}\n{endWrapper}\n{hint}",
                            'horizontalCssClasses' => [
                                'label' => 'col-sm-8',
                                'offset' => 'col-sm-offset-8',
                                'wrapper' => 'col-sm-4',
                            ],
                        ])->checkbox([
                            'class'=>'iswitch iswitch-secondary'
                        ], false) ?>                        

                </div>

                <div class="col-sm-4">                    

                    <?= $form->field($user_profile_model, 'sms_send', [
                            'checkboxTemplate' => "{beginWrapper}\n<div class=\"checkbox\">\n{beginLabel}\n{input}\n{labelTitle}\n{endLabel}\n</div>\n{error}\n{endWrapper}\n{hint}",
                            'horizontalCssClasses' => [
                                'label' => 'col-sm-8',
                                'offset' => 'col-sm-offset-8',
                                'wrapper' => 'col-sm-4',
                            ],
                        ])->checkbox([
                            'class'=>'iswitch iswitch-secondary'
                        ], false) ?>                       

                </div>
            </div>

            <div class="row">
                <div class="col-sm-4">                      

                    <?= $form->field($user_model, 'status', [
                            'checkboxTemplate' => "{beginWrapper}\n<div class=\"checkbox\">\n{beginLabel}\n{input}\n{labelTitle}\n{endLabel}\n</div>\n{error}\n{endWrapper}\n{hint}",
                            'horizontalCssClasses' => [
                                'label' => 'col-sm-8',
                                'offset' => 'col-sm-offset-8',
                                'wrapper' => 'col-sm-4',
                            ],
                        ])->checkbox([
                            'class'=>'iswitch iswitch-secondary'
                        ], false) ?>                        

                </div>

                <div class="col-sm-4">

                    <?= $form->field($user_model, 'certificate', [
                            'checkboxTemplate' => "{beginWrapper}\n<div class=\"checkbox\">\n{beginLabel}\n{input}\n{labelTitle}\n{endLabel}\n</div>\n{error}\n{endWrapper}\n{hint}",
                            'horizontalCssClasses' => [
                                'label' => 'col-sm-8',
                                'offset' => 'col-sm-offset-8',
                                'wrapper' => 'col-sm-4',
                            ],
                        ])->checkbox([
                            'class'=>'iswitch iswitch-secondary'
                        ], false) ?>
                </div>

                <div class="col-sm-4">

                    <?= $form->field($user_profile_model, 'email_send', [
                            'checkboxTemplate' => "{beginWrapper}\n<div class=\"checkbox\">\n{beginLabel}\n{input}\n{labelTitle}\n{endLabel}\n</div>\n{error}\n{endWrapper}\n{hint}",
                            'horizontalCssClasses' => [
                                'label' => 'col-sm-8',
                                'offset' => 'col-sm-offset-8',
                                'wrapper' => 'col-sm-4',
                            ],
                        ])->checkbox([
                            'class'=>'iswitch iswitch-secondary'
                        ], false) ?>

                </div>
            </div>

            <div class="row">

                <div class="col-sm-6">
                     
                    <?= $form->field($user_profile_model, 'license_number', [
                        'template' => "{label}\n{beginWrapper}\n<div class='input-group'>\n<div class='input-group-addon'>\n<i class='linecons-note'></i>\n</div>\n{input}\n</div>\n{hint}\n{error}\n{endWrapper}"
                    ])->textInput() ?>


                </div>

                <div class="col-sm-6">
                    
                    <?= $form->field($user_profile_model, 'company', [
                        'template' => "{label}\n{beginWrapper}\n<div class='input-group'>\n<div class='input-group-addon'>\n<i class='fa-building-o'></i>\n</div>\n{input}\n</div>\n{hint}\n{error}\n{endWrapper}"
                    ])->textInput() ?>

                </div>

            </div>

            <div class="row">

                <div class="col-sm-6">

                    <?= $form->field($user_profile_model, 'phone_number', [
                            'template' => "{label}\n{beginWrapper}\n<div class='input-group'>\n<div class='input-group-addon'>\n<i class='fa-mobile'></i>\n</div>\n{input}\n</div>\n{hint}\n{error}\n{endWrapper}"
                        ])->widget(MaskedInput::className(), [
                        'mask' => '(999)999-9999',
                        'options' => [
                            'placeholder' => '(___)___-____',
                            'class' => 'form-control'
                        ]                                   
                    ])->label('Phone number') ?>
      
                </div>

                <div class="col-sm-6">

                    <?= $form->field($user_profile_model, 'website_url', [
                        'template' => "{label}\n{beginWrapper}\n<div class='input-group'>\n<div class='input-group-addon'>\n<i class='fa-globe'></i>\n</div>\n{input}\n</div>\n{hint}\n{error}\n{endWrapper}"
                    ])->textInput() ?>

                </div>                     
                
            </div>

            <div class="row">

                <div class="col-sm-6">                    
                       
                    <?= $form->field($user_profile_model, 'education', [
                        'template' => "{label}\n{beginWrapper}\n<div class='input-group'>\n<div class='input-group-addon'>\n<i class='fa-graduation-cap'></i>\n</div>\n{input}\n</div>\n{hint}\n{error}\n{endWrapper}"
                    ])->textInput() ?>                       
     
                </div>    

                <div class="col-sm-6">         
                       
                    <?= $form->field($user_model, 'id_zoho', [
                        'template' => "{label}\n{beginWrapper}\n<div class='input-group'>\n<div class='input-group-addon'>\n<i class='fa-users'></i>\n</div>\n{input}\n</div>\n{hint}\n{error}\n{endWrapper}"
                    ])->textInput() ?>   
          
                </div>                
                
            </div>

            <div class="row">                    
                <div class="col-xs-12">                    

                    <?= $form->field($user_profile_model, 'biography', [
                        'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}", 
                        'horizontalCssClasses' => [
                            'label' => 'col-sm-2',
                            'offset' => 'col-sm-offset-2',
                            'wrapper' => 'col-sm-10',
                        ]
                    ])->textarea(['class' => 'form-control ckeditor', 'rows' => '5']) ?>
     
                </div>
            </div>                

            <div class="row">
                <div class="col-md-2 col-sm-4 pull-right-sm">

                    <div class="action-buttons">
                        <?= Html::submitButton('Save', ['class' => 'btn btn-block btn-secondary']) ?>
                        <button type="reset" class="btn btn-block btn-gray">Reset form</button>
                    </div>

                </div>                    
            </div>
        <?php ActiveForm::end() ?>
    </div>
</div>
