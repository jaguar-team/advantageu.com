<?php

use common\models\UserRole;
use common\models\UserAddress;
use common\models\Language;
use backend\components\DisplayItems;

$this->title = 'Update User: ' . $user_model->id;
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Update: ' . $user_model->id;

$user_address = \Yii::createObject(['class' => UserAddress::className()]);
$language_model = \Yii::createObject(['class' => Language::className()]);

$roles = UserRole::find()->agent()->select(['name'])->indexBy('id')->column();
$areas_served = $user_profile_model->getAreas()->select(['zip_code'])->indexBy('id')->column();
$languages = $user_profile_model->getLanguages()->select(['name'])->indexBy('id')->column();

$states = [
    null => "", 
    "AL" => "Alabama",
    "AK" => "Alaska",
    "AS" => "American Samoa",
    "AZ" => "Arizona",
    "AR" => "Arkansas",
    "CA" => "California",
    "CO" => "Colorado",
    "CT" => "Connecticut",
    "DE" => "Delaware",
    "DC" => "District Of Columbia",
    "FM" => "Federated States Of Micronesia",
    "FL" => "Florida",
    "GA" => "Georgia",
    "GU" => "Guam",
    "HI" => "Hawaii",
    "ID" => "Idaho",
    "IL" => "Illinois",
    "IN" => "Indiana",
    "IA" => "Iowa",
    "KS" => "Kansas",
    "KY" => "Kentucky",
    "LA" => "Louisiana",
    "ME" => "Maine",
    "MH" => "Marshall Islands",
    "MD" => "Maryland",
    "MA" => "Massachusetts",
    "MI" => "Michigan",
    "MN" => "Minnesota",
    "MS" => "Mississippi",
    "MO" => "Missouri",
    "MT" => "Montana",
    "NE" => "Nebraska",
    "NV" => "Nevada",
    "NH" => "New Hampshire",
    "NJ" => "New Jersey",
    "NM" => "New Mexico",
    "NY" => "New York",
    "NC" => "North Carolina",
    "ND" => "North Dakota",
    "MP" => "Northern Mariana Islands",
    "OH" => "Ohio",
    "OK" => "Oklahoma",
    "OR" => "Oregon",
    "PW" => "Palau",
    "PA" => "Pennsylvania",
    "PR" => "Puerto Rico",
    "RI" => "Rhode Island",
    "SC" => "South Carolina",
    "SD" => "South Dakota",
    "TN" => "Tennessee",
    "TX" => "Texas",
    "UT" => "Utah",
    "VT" => "Vermont",
    "VI" => "Virgin Islands",
    "VA" => "Virginia",
    "WA" => "Washington",
    "WV" => "West Virginia",
    "WI" => "Wisconsin",
    "WY" => "Wyoming"
];
$active_tab = (DisplayItems::getParam(DisplayItems::AGENT_EDIT_TAB_ITEM) ? DisplayItems::getParam(DisplayItems::AGENT_EDIT_TAB_ITEM) : "#update_1");
?>

<div class="user-update">
    <ul class="nav nav-tabs nav-tabs-justified">
        <li class="<?= ($active_tab == "#update_1" ? "active" : "") ?>">
            <a href="#update_1" data-toggle="tab">
                <span class="visible-xs"><i class="fa-user"></i></span>
                <span class="hidden-xs">Account Info</span>
            </a>
        </li>
        <li class="<?= ($active_tab == "#update_5" ? "active" : "") ?>">
            <a href="#update_5" data-toggle="tab">
                <span class="visible-xs"><i class="fa-trophy"></i></span>
                <span class="hidden-xs">Bio</span>
            </a>
        </li>
        <li class="<?= ($active_tab == "#update_2" ? "active" : "") ?>">
            <a href="#update_2" data-toggle="tab">
                <span class="visible-xs"><i class="fa-home"></i></span>
                <span class="hidden-xs">Properties</span>
            </a>
        </li>
        <li class="<?= ($active_tab == "#update_3" ? "active" : "") ?>">
            <a href="#update_3" data-toggle="tab">
                <span class="visible-xs"><i class="fa-comments"></i></span>
                <span class="hidden-xs">Reviews</span>
            </a>
        </li>
        <li class="<?= ($active_tab == "#update_4" ? "active" : "") ?>">
            <a href="#update_4" data-toggle="tab">
                <span class="visible-xs"><i class="fa-trophy"></i></span>
                <span class="hidden-xs">Awards, etc..</span>
            </a>
        </li>
    </ul>

    <div class="tab-content no-margin">

        <!-- Tabs Content -->
        <div class="tab-pane with-bg <?= ($active_tab == "#update_1" ? "active" : "") ?>" id="update_1">
            <?= $this->render('update-tabs/update_1', [
                'upload_avatar_form'    => $upload_avatar_form,
                'user_model'            => $user_model,
                'user_profile_model'    => $user_profile_model,
                'area_model'            => $area_model,
                'user_address'          => $user_address,
                'language_model'        => $language_model,
                'roles'                 => $roles,
                'areas_served'          => $areas_served,
                'languages'             => $languages

            ]) ?>
        </div>

        <div class="tab-pane with-bg <?= ($active_tab == "#update_2" ? "active" : "") ?>" id="update_2">
            <?= $this->render('update-tabs/update_2', [
                'user_model'            => $user_model,
                'user_profile_model'    => $user_profile_model
            ]) ?>
        </div>

        <div class="tab-pane with-bg <?= ($active_tab == "#update_3" ? "active" : "") ?>" id="update_3">
            <?= $this->render('update-tabs/update_3', [
                'user_model'            => $user_model
            ]) ?>
        </div>

        <div class="tab-pane with-bg <?= ($active_tab == "#update_4" ? "active" : "") ?>" id="update_4">
            <?= $this->render('update-tabs/update_4', [
                'user_model'            => $user_model
            ]) ?>
        </div>

        <div class="tab-pane with-bg <?= ($active_tab == "#update_5" ? "active" : "") ?>" id="update_5">
            <?= $this->render('update-tabs/update_5', [
                'user_model'            => $user_model,
                'user_profile_model'    => $user_profile_model,
                'user_address_model'    => $user_address,
            ]) ?>
        </div>

    </div>
</div>
