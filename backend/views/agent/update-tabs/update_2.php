<?php

use yii\helpers\Html;
use yii\helpers\Url;
use common\models\UserRole;

use yii\bootstrap\ActiveForm;
use yii\widgets\Pjax;
use yii\widgets\MaskedInput;

use yii\data\ActiveDataProvider;
use backend\widgets\PropertyWidget;

$propertyProvider = new ActiveDataProvider([
    'query' => $user_model->getTransactions(),
    'pagination' => [
        'pageSize' => 20,
    ],
]);

$roles = UserRole::find()->select(['name'])->indexBy('id')->column();
?>

<?php Pjax::begin([
	'options' => ['id' => 'agent-list-container'],
	'timeout' => 5000,
]) ?>
	<div class="row">
		<div class="col-xs-6">
			<?php $form = ActiveForm::begin([
				'action' => Url::toRoute(['property/create'], true)
			]) ?>
				<input type="hidden" name="Transaction[id_agent]" value="<?= $user_model->id ?>" />
				<input type="hidden" name="Transaction[chosen_agent]" value="<?= $user_model->profile->getFullName() ?>" />
				<?= Html::submitButton('Add property to '.$user_profile_model->getFullName(), ['class' => 'btn btn-secondary btn-sm']) ?>
			<?php ActiveForm::end() ?>

		</div>
		<div class="property_list">
			<?= PropertyWidget::widget([
				'dataProvider' => $propertyProvider,
				'roles' => $roles
			]) ?>
		</div>


	</div>
<?php Pjax::end() ?>