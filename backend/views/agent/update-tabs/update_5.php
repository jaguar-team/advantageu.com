<?php

use yii\helpers\Html;
use yii\helpers\Url;

use yii\bootstrap\ActiveForm;
use yii\widgets\Pjax;
use yii\widgets\MaskedInput;

/* @var $this yii\web\View */
/* @var $user_model common\models\User */
/* @var $user_profile_model common\models\UserProfile */
/* @var $user_address_model common\models\UserAddress */

$carriers = $user_profile_model->carrier_list;
$timezones = $user_profile_model->timezone_list;
$variants = ['0' => 'No', '1' => 'Yes'];
$designations = $user_profile_model->designation_list;
$statements = $user_profile_model->statements_list;
$advantages = $user_profile_model->advantages_list;
$package_levels = $user_profile_model->package_level_list;
$property_avoid_list = $user_profile_model->property_type_list;

?>


<div class="row">
    <div class="col-sm-6">
        <?php Pjax::begin(['options' => ['class' => 'async_inline']]) ?>
            <?php $form = ActiveForm::begin([
                'action' => Url::toRoute(['agent/update', 'id' => $user_model->id], true),
                'options' => [
                    'data-pjax' => 'true',
                ],
            ]) ?>
                <?= $form->field($user_profile_model, 'account_number')->textInput(['onchange' => 'submitForm(event)'])->label('What is your Account Number (Can be found in your "Mega Agent Pro Welcome Package")') ?>
            <?php ActiveForm::end() ?>
        <?php Pjax::end() ?>
    </div>
</div>

<div class="row">
    <div class="col-sm-6">
        <?php Pjax::begin(['options' => ['class' => 'async_inline']]) ?>
            <?php $form = ActiveForm::begin([
                'action' => Url::toRoute(['agent/update', 'id' => $user_model->id], true),
                'options' => [
                    'data-pjax' => 'true',
                ],
            ]) ?>
                <?= $form->field($user_profile_model, 'first_name')->textInput(['onchange' => 'submitForm(event)'])->label('First Name') ?>
            <?php ActiveForm::end() ?>
        <?php Pjax::end() ?>
    </div>
    <div class="col-sm-6">
        <?php Pjax::begin(['options' => ['class' => 'async_inline']]) ?>
            <?php $form = ActiveForm::begin([
                'action' => Url::toRoute(['agent/update', 'id' => $user_model->id], true),
                'options' => [
                    'data-pjax' => 'true',
                ],
            ]) ?>
                <?= $form->field($user_profile_model, 'last_name')->textInput(['onchange' => 'submitForm(event)'])->label('Last Name') ?>
            <?php ActiveForm::end() ?>
        <?php Pjax::end() ?>
    </div>
</div>

<div class="row">
    <div class="col-sm-6">
        <?php Pjax::begin(['options' => ['class' => 'async_inline']]) ?>
            <?php $form = ActiveForm::begin([
                'action' => Url::toRoute(['agent/update', 'id' => $user_model->id], true),
                'options' => [
                    'data-pjax' => 'true',
                ],
            ]) ?>
                <?= $form->field($user_profile_model, 'company')->textInput(['onchange' => 'submitForm(event)'])->label('Company Name') ?>
            <?php ActiveForm::end() ?>
        <?php Pjax::end() ?>
    </div>

    <div class="col-sm-6">
        <?php Pjax::begin(['options' => ['class' => 'async_inline']]) ?>

            <?php $form = ActiveForm::begin([
                'action' => Url::toRoute(['agent/update', 'id' => $user_model->id], true),
                'options' => [
                    'class' => 'area_validation',
                    'data-pjax' => 'true'
                ]
            ]) ?>
                <div class="form-group area_field">
                    <label class="label-control">Office Addresses</label>

                    <div class="group">
                        <div class="search_areas field loading async_inline">
                            <input class="controls form-control" type="text" placeholder="Add one area at a time"
                                   data-type="address"
                                   data-city-id="bio_city"
                                   data-state-id="bio_state"
                                   data-zipcode-id="bio_zip_code"
                                   data-address-id="bio_address"
                                   data-lat-id="bio_lat"
                                   data-lng-id="bio_lng"
                                   data-place_id-id="bio_place_id"
                                   data-short-id="bio_short_name" />
                        </div>
                        <?= Html::submitButton('Add', ['class' => 'btn btn-blue']) ?>
                    </div>

                    <div class="hidden">
                        <?= $form->field($user_address_model, 'city')->hiddenInput(['id' => 'bio_city'])->label(false) ?>
                        <?= $form->field($user_address_model, 'state')->hiddenInput(['id' => 'bio_state'])->label(false) ?>
                        <?= $form->field($user_address_model, 'zip_code')->hiddenInput(['id' => 'bio_zip_code'])->label(false) ?>
                        <?= $form->field($user_address_model, 'address')->hiddenInput(['id' => 'bio_address'])->label(false) ?>
                        <?= $form->field($user_address_model, 'lat')->hiddenInput(['id' => 'bio_lat'])->label(false) ?>
                        <?= $form->field($user_address_model, 'lng')->hiddenInput(['id' => 'bio_lng'])->label(false) ?>
                        <?= $form->field($user_address_model, 'place_id')->hiddenInput(['id' => 'bio_place_id'])->label(false) ?>
                        <?= $form->field($user_address_model, 'short_name')->hiddenInput(['id' => 'bio_short_name'])->label(false) ?>
                    </div>
                </div>


            <?php ActiveForm::end() ?>

            <ul class="select2-choices">
                <?php foreach ($user_model->addresses as $key => $address) : ?>
                    <li class="select2-search-choice">
                        <?= $address['address'].', '.$address['city'].', '.$address['short_name'] ?>
                        <?= Html::a('×', Url::toRoute(['agent/delete-address', 'id' => $address['id']], true), ['class' => 'select2-search-choice-close', 'data-pjax' => 'true', 'data-method' => 'POST', 'tabindex' => '-1']) ?>
                    </li>
                <?php endforeach ?>
            </ul>

        <?php Pjax::end() ?>
    </div>
</div>

<div class="row">
    <div class="col-sm-6">
        <?php Pjax::begin(['options' => ['class' => 'async_inline']]) ?>
            <?php $form = ActiveForm::begin([
                'action' => Url::toRoute(['agent/update', 'id' => $user_model->id], true),
                'options' => [
                    'data-pjax' => 'true',
                ],
            ]) ?>
                <?= $form->field($user_profile_model, 'phone_number')->widget(MaskedInput::className(), [
                    'mask' => '(999)999-9999',
                    'options' => [
                        'id' => 'phone_repeat',
                        'placeholder' => '(___)___-____',
                        'class' => 'form-control',
                        'onchange' => 'submitForm(event)'
                    ]
                ])->label('Phone') ?>
            <?php ActiveForm::end() ?>
        <?php Pjax::end() ?>
    </div>

</div>

<div class="row">
    <div class="col-sm-6">
        <?php Pjax::begin(['options' => ['class' => 'async_inline']]) ?>
            <?php $form = ActiveForm::begin([
                'action' => Url::toRoute(['agent/update', 'id' => $user_model->id], true),
                'options' => [
                    'data-pjax' => 'true',
                ],
            ]) ?>
                <?= $form->field($user_profile_model, 'website_url')->textInput(['onchange' => 'submitForm(event)'])->label('Company Website') ?>
            <?php ActiveForm::end() ?>
        <?php Pjax::end() ?>
    </div>
    <div class="col-sm-6">
        <?php Pjax::begin(['options' => ['class' => 'async_inline']]) ?>
            <?php $form = ActiveForm::begin([
                'action' => Url::toRoute(['agent/update', 'id' => $user_model->id], true),
                'options' => [
                    'data-pjax' => 'true',
                ],
            ]) ?>
                <?= $form->field($user_profile_model, 'experience')->textInput(['onchange' => 'submitForm(event)'])->label('How many years of real estate related experience do you have (or team)') ?>
            <?php ActiveForm::end() ?>
        <?php Pjax::end() ?>
    </div>
</div>

<div class="row">
    <div class="col-sm-6">
        <?php Pjax::begin(['options' => ['class' => 'async_inline']]) ?>
            <?php $form = ActiveForm::begin([
                'action' => Url::toRoute(['agent/update', 'id' => $user_model->id], true),
                'options' => [
                    'data-pjax' => 'true',
                ],
            ]) ?>
                <?= $form->field($user_profile_model, 'google_calendar')->radioList($variants, ['onchange' => 'submitForm(event)'])->label('Confirm that you shared your Google Calendar with us?') ?>
            <?php ActiveForm::end() ?>
        <?php Pjax::end() ?>
    </div>
    <div class="col-sm-6">
        <?php Pjax::begin(['options' => ['class' => 'async_inline']]) ?>
            <?php $form = ActiveForm::begin([
                'action' => Url::toRoute(['agent/update', 'id' => $user_model->id], true),
                'options' => [
                    'data-pjax' => 'true',
                ],
            ]) ?>
                <?= $form->field($user_profile_model, 'timezone')->dropDownList($timezones, ['prompt' => '', 'class' => 'select2-offscreen', 'onchange' => 'submitForm(event)'])->label('Confirm your timezone') ?>
            <?php ActiveForm::end() ?>
        <?php Pjax::end() ?>
    </div>
</div>

<div class="row">
    <div class="col-sm-6">
        <?php Pjax::begin(['options' => ['class' => 'async_inline']]) ?>
            <?php $form = ActiveForm::begin([
                'action' => Url::toRoute(['agent/update', 'id' => $user_model->id], true),
                'options' => [
                    'data-pjax' => 'true',
                ],
            ]) ?>
                <?= $form->field($user_profile_model, 'phonetic_pronunciation')->textarea(['onchange' => 'submitForm(event)'])->label('What is the phonetic pronunciation of your name to ensure prospectors say your name properly?') ?>
            <?php ActiveForm::end() ?>
        <?php Pjax::end() ?>
    </div>
    <div class="col-sm-6">
        <?php Pjax::begin(['options' => ['class' => 'async_inline']]) ?>
            <?php $form = ActiveForm::begin([
                'action' => Url::toRoute(['agent/update', 'id' => $user_model->id], true),
                'options' => [
                    'data-pjax' => 'true',
                ],
            ]) ?>
                <?= $form->field($user_profile_model, 'team_name')->textInput(['onchange' => 'submitForm(event)'])->label('Team Name') ?>
            <?php ActiveForm::end() ?>
        <?php Pjax::end() ?>
    </div>
</div>

<div class="row">
    <div class="col-sm-6">
        <?php Pjax::begin(['options' => ['class' => 'async_inline']]) ?>
            <?php $form = ActiveForm::begin([
                'action' => Url::toRoute(['agent/update', 'id' => $user_model->id], true),
                'options' => [
                    'data-pjax' => 'true',
                ],
            ]) ?>
                <?= $form->field($user_profile_model, 'team_info')->textarea(['onchange' => 'submitForm(event)'])->label('Provide name, email and phone number for anyone on your team who will be attending the appointments') ?>
            <?php ActiveForm::end() ?>
        <?php Pjax::end() ?>
    </div>
    <div class="col-sm-6">
        <?php Pjax::begin(['options' => ['class' => 'async_inline']]) ?>
            <?php $form = ActiveForm::begin([
                'action' => Url::toRoute(['agent/update', 'id' => $user_model->id], true),
                'options' => [
                    'data-pjax' => 'true',
                ],
            ]) ?>
                <?= $form->field($user_profile_model, 'listing_agent_phone_number')->widget(MaskedInput::className(), [
                    'mask' => '(999)999-9999',
                    'options' => [
                        'placeholder' => '(___)___-____',
                        'class' => 'form-control',
                        'onchange' => 'submitForm(event)'
                    ]
                ])->label('Listing Agent Cell Phone Number') ?>
            <?php ActiveForm::end() ?>
        <?php Pjax::end() ?>
    </div>
</div>

<div class="row">
    <div class="col-sm-6">
        <?php Pjax::begin(['options' => ['class' => 'async_inline']]) ?>
            <?php $form = ActiveForm::begin([
                'action' => Url::toRoute(['agent/update', 'id' => $user_model->id], true),
                'options' => [
                    'data-pjax' => 'true',
                ],
            ]) ?>
                <?= $form->field($user_profile_model, 'listing_agent_first_name')->textInput(['onchange' => 'submitForm(event)'])->label('Listing Agent First Name') ?>
            <?php ActiveForm::end() ?>
        <?php Pjax::end() ?>
    </div>
    <div class="col-sm-6">
        <?php Pjax::begin(['options' => ['class' => 'async_inline']]) ?>
            <?php $form = ActiveForm::begin([
                'action' => Url::toRoute(['agent/update', 'id' => $user_model->id], true),
                'options' => [
                    'data-pjax' => 'true',
                ],
            ]) ?>
                <?= $form->field($user_profile_model, 'listing_agent_last_name')->textInput(['onchange' => 'submitForm(event)'])->label('Listing Agent Last Name') ?>
            <?php ActiveForm::end() ?>
        <?php Pjax::end() ?>
    </div>
</div>

<div class="row">
    <div class="col-sm-6">
        <?php Pjax::begin(['options' => ['class' => 'async_inline']]) ?>
            <?php $form = ActiveForm::begin([
                'action' => Url::toRoute(['agent/update', 'id' => $user_model->id], true),
                'options' => [
                    'data-pjax' => 'true',
                ],
            ]) ?>
                <?= $form->field($user_profile_model, 'listing_agent_mobile_number')->widget(MaskedInput::className(), [
                    'mask' => '(999)999-9999',
                    'options' => [
                        'placeholder' => '(___)___-____',
                        'class' => 'form-control',
                        'onchange' => 'submitForm(event)'
                    ]
                ])->label('Listing Agent Mobile Phone') ?>
            <?php ActiveForm::end() ?>
        <?php Pjax::end() ?>
    </div>
    <div class="col-sm-6">
        <?php Pjax::begin(['options' => ['class' => 'async_inline']]) ?>
            <?php $form = ActiveForm::begin([
                'action' => Url::toRoute(['agent/update', 'id' => $user_model->id], true),
                'options' => [
                    'data-pjax' => 'true',
                ],
            ]) ?>
                <?= $form->field($user_profile_model, 'listing_mobile_carrier')->dropDownList($carriers, ['prompt' => '', 'onchange' => 'submitForm(event)', 'class' => 'select2-offscreen'])->label('Select Mobile Carrier') ?>
            <?php ActiveForm::end() ?>
        <?php Pjax::end() ?>
    </div>
</div>

<div class="row">
    <div class="col-sm-6">
        <?php Pjax::begin(['options' => ['class' => 'async_inline']]) ?>
            <?php $form = ActiveForm::begin([
                'action' => Url::toRoute(['agent/update', 'id' => $user_model->id], true),
                'options' => [
                    'data-pjax' => 'true',
                ],
            ]) ?>
                <?= $form->field($user_profile_model, 'admin_first_name')->textInput(['onchange' => 'submitForm(event)'])->label('Administrator First Name') ?>
            <?php ActiveForm::end() ?>
        <?php Pjax::end() ?>
    </div>
    <div class="col-sm-6">
        <?php Pjax::begin(['options' => ['class' => 'async_inline']]) ?>
            <?php $form = ActiveForm::begin([
                'action' => Url::toRoute(['agent/update', 'id' => $user_model->id], true),
                'options' => [
                    'data-pjax' => 'true',
                ],
            ]) ?>
                <?= $form->field($user_profile_model, 'admin_last_name')->textInput(['onchange' => 'submitForm(event)'])->label('Administrator Last Name') ?>
            <?php ActiveForm::end() ?>
        <?php Pjax::end() ?>
    </div>
</div>

<div class="row">
    <div class="col-sm-6">
        <?php Pjax::begin(['options' => ['class' => 'async_inline']]) ?>
            <?php $form = ActiveForm::begin([
                'action' => Url::toRoute(['agent/update', 'id' => $user_model->id], true),
                'options' => [
                    'data-pjax' => 'true',
                ],
            ]) ?>
                <?= $form->field($user_profile_model, 'admin_email')->widget(MaskedInput::className(), [
                    'options' => [
                        'class' => 'form-control',
                        'onchange' => 'submitForm(event)'
                    ],
                    'clientOptions' => [
                        'alias' => 'email'
                    ]
                ])->label('Administrator\'s email') ?>
            <?php ActiveForm::end() ?>
        <?php Pjax::end() ?>
    </div>
    <div class="col-sm-6">
        <?php Pjax::begin(['options' => ['class' => 'async_inline']]) ?>
            <?php $form = ActiveForm::begin([
                'action' => Url::toRoute(['agent/update', 'id' => $user_model->id], true),
                'options' => [
                    'data-pjax' => 'true',
                ],
            ]) ?>
                <?= $form->field($user_profile_model, 'admin_phone_number')->widget(MaskedInput::className(), [
                    'mask' => '(999)999-9999',
                    'options' => [
                        'placeholder' => '(___)___-____',
                        'class' => 'form-control',
                        'onchange' => 'submitForm(event)'
                    ]
                ])->label('Administrator\'s phone number') ?>
            <?php ActiveForm::end() ?>
        <?php Pjax::end() ?>
    </div>
</div>

<div class="row">
    <div class="col-sm-6">
        <?php Pjax::begin(['options' => ['class' => 'async_inline']]) ?>
            <?php $form = ActiveForm::begin([
                'action' => Url::toRoute(['agent/update', 'id' => $user_model->id], true),
                'options' => [
                    'data-pjax' => 'true',
                ],
            ]) ?>
                <?= $form->field($user_profile_model, 'admin_mobile_carrier')->dropDownList($carriers,['prompt' => '', 'onchange' => 'submitForm(event)', 'class' => 'select2-offscreen'])->label('Select Mobile Carrier') ?>
            <?php ActiveForm::end() ?>
        <?php Pjax::end() ?>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <?php Pjax::begin(['options' => ['class' => 'async_inline']]) ?>
            <?php $form = ActiveForm::begin([
                'action' => Url::toRoute(['agent/update', 'id' => $user_model->id], true),
                'options' => [
                    'data-pjax' => 'true',
                ],
            ]) ?>
                <?= $form->field($user_profile_model, 'special_designations')->checkboxList($designations, ['onchange' => 'submitForm(event)'])->label('What are some special designations you have and/or real estate related awards received?') ?>
            <?php ActiveForm::end() ?>
        <?php Pjax::end() ?>
    </div>
</div>

<div class="row">
    <div class="col-sm-6">
        <?php Pjax::begin(['options' => ['class' => 'async_inline']]) ?>
            <?php $form = ActiveForm::begin([
                'action' => Url::toRoute(['agent/update', 'id' => $user_model->id], true),
                'options' => [
                    'data-pjax' => 'true',
                ],
            ]) ?>
                <?= $form->field($user_profile_model, 'average_days_on_market')->textInput(['onchange' => 'submitForm(event)'])->label('What\'s your average days on market') ?>
            <?php ActiveForm::end() ?>
        <?php Pjax::end() ?>
    </div>
    <div class="col-sm-6">
        <?php Pjax::begin(['options' => ['class' => 'async_inline']]) ?>
            <?php $form = ActiveForm::begin([
                'action' => Url::toRoute(['agent/update', 'id' => $user_model->id], true),
                'options' => [
                    'data-pjax' => 'true',
                ],
            ]) ?>
                <?= $form->field($user_profile_model, 'number_home_listed')->textInput(['onchange' => 'submitForm(event)'])->label('Number of homes Listed') ?>
            <?php ActiveForm::end() ?>
        <?php Pjax::end() ?>
    </div>
</div>

<div class="row">
    <div class="col-sm-6">
        <?php Pjax::begin(['options' => ['class' => 'async_inline']]) ?>
            <?php $form = ActiveForm::begin([
                'action' => Url::toRoute(['agent/update', 'id' => $user_model->id], true),
                'options' => [
                    'data-pjax' => 'true',
                ],
            ]) ?>
                <?= $form->field($user_profile_model, 'number_home_sold')->textInput(['onchange' => 'submitForm(event)'])->label('Number of homes sold') ?>
            <?php ActiveForm::end() ?>
        <?php Pjax::end() ?>
    </div>
    <div class="col-sm-6">
        <?php Pjax::begin(['options' => ['class' => 'async_inline']]) ?>
            <?php $form = ActiveForm::begin([
                'action' => Url::toRoute(['agent/update', 'id' => $user_model->id], true),
                'options' => [
                    'data-pjax' => 'true',
                ],
            ]) ?>
                <?= $form->field($user_profile_model, 'average_home_sale_price')->textInput(['onchange' => 'submitForm(event)'])->label('Average Home Sale Price') ?>
            <?php ActiveForm::end() ?>
        <?php Pjax::end() ?>
    </div>
</div>

<div class="row">
    <div class="col-sm-6">
        <?php Pjax::begin(['options' => ['class' => 'async_inline']]) ?>
            <?php $form = ActiveForm::begin([
                'action' => Url::toRoute(['agent/update', 'id' => $user_model->id], true),
                'options' => [
                    'data-pjax' => 'true',
                ],
            ]) ?>
                <?= $form->field($user_profile_model, 'total_sale_price')->textInput(['onchange' => 'submitForm(event)'])->label('Total Sales Volume') ?>
            <?php ActiveForm::end() ?>
        <?php Pjax::end() ?>
    </div>
    <div class="col-sm-6">
        <?php Pjax::begin(['options' => ['class' => 'async_inline']]) ?>
            <?php $form = ActiveForm::begin([
                'action' => Url::toRoute(['agent/update', 'id' => $user_model->id], true),
                'options' => [
                    'data-pjax' => 'true',
                ],
            ]) ?>
                <?= $form->field($user_profile_model, 'listing_sale_price_ratio')->textInput(['onchange' => 'submitForm(event)'])->label('Listing to Sales Price Ratio') ?>
            <?php ActiveForm::end() ?>
        <?php Pjax::end() ?>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <?php Pjax::begin(['options' => ['class' => 'async_inline']]) ?>
            <?php $form = ActiveForm::begin([
                'action' => Url::toRoute(['agent/update', 'id' => $user_model->id], true),
                'options' => [
                    'data-pjax' => 'true',
                ],
            ]) ?>
                <?= $form->field($user_profile_model, 'statements')->checkboxList($statements, ['onchange' => 'submitForm(event)'])->label('Select 2-3 of the statements below that you can support') ?>
            <?php ActiveForm::end() ?>
        <?php Pjax::end() ?>
    </div>
    <div class="col-sm-12">
        <?php Pjax::begin(['options' => ['class' => 'async_inline']]) ?>
            <?php $form = ActiveForm::begin([
                'action' => Url::toRoute(['agent/update', 'id' => $user_model->id], true),
                'options' => [
                    'data-pjax' => 'true',
                ],
            ]) ?>
                <?= $form->field($user_profile_model, 'uvps')->textarea(['onchange' => 'submitForm(event)'])->label('If you have a unique talking point(s) in addition to the options above, please provide below. Or if you want to edit the pre-written UVPs enter here.') ?>
            <?php ActiveForm::end() ?>
        <?php Pjax::end() ?>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <?php Pjax::begin(['options' => ['class' => 'async_inline']]) ?>
            <?php $form = ActiveForm::begin([
                'action' => Url::toRoute(['agent/update', 'id' => $user_model->id], true),
                'options' => [
                    'data-pjax' => 'true',
                ],
            ]) ?>
                <?= $form->field($user_profile_model, 'advantages')->checkboxList($advantages, ['onchange' => 'submitForm(event)'])->label('Select 3-5 advantages of your marketing plan') ?>
            <?php ActiveForm::end() ?>
        <?php Pjax::end() ?>
    </div>
</div>

<div class="row">
    <div class="col-sm-6">
        <?php Pjax::begin(['options' => ['class' => 'async_inline']]) ?>
            <?php $form = ActiveForm::begin([
                'action' => Url::toRoute(['agent/update', 'id' => $user_model->id], true),
                'options' => [
                    'data-pjax' => 'true',
                ],
            ]) ?>
                <?= $form->field($user_profile_model, 'package_level')->dropDownList($package_levels, ['prompt' => '', 'onchange' => 'submitForm(event)', 'class' => 'select2-offscreen'])->label('Please select your package level') ?>
            <?php ActiveForm::end() ?>
        <?php Pjax::end() ?>
    </div>
</div>

<div class="row">
    <div class="col-sm-6">
        <?php Pjax::begin(['options' => ['class' => 'async_inline']]) ?>
            <?php $form = ActiveForm::begin([
                'action' => Url::toRoute(['agent/update', 'id' => $user_model->id], true),
                'options' => [
                    'data-pjax' => 'true',
                ],
            ]) ?>
                <?= $form->field($user_profile_model, 'facebook_login')->widget(MaskedInput::className(), [
                    'options' => [
                        'class' => 'form-control',
                        'onchange' => 'submitForm(event)'
                    ],
                    'clientOptions' => [
                        'alias' => 'email'
                    ]
                ])->label('Facebook Email / Mobile Number') ?>
            <?php ActiveForm::end() ?>
        <?php Pjax::end() ?>
    </div>
    <div class="col-sm-6">
        <?php Pjax::begin(['options' => ['class' => 'async_inline']]) ?>
            <?php $form = ActiveForm::begin([
                'action' => Url::toRoute(['agent/update', 'id' => $user_model->id], true),
                'options' => [
                    'data-pjax' => 'true',
                ],
            ]) ?>
                <?= $form->field($user_profile_model, 'facebook_password')->passwordInput(['onchange' => 'submitForm(event)'])->label('Facebook Password') ?>
            <?php ActiveForm::end() ?>
        <?php Pjax::end() ?>
    </div>
</div>

<div class="row">
    <div class="col-sm-6">
        <?php Pjax::begin(['options' => ['class' => 'async_inline']]) ?>
            <?php $form = ActiveForm::begin([
                'action' => Url::toRoute(['agent/update', 'id' => $user_model->id], true),
                'options' => [
                    'data-pjax' => 'true',
                ],
            ]) ?>
                <?= $form->field($user_profile_model, 'twitter_login')->textInput(['onchange' => 'submitForm(event)'])->label('Twitter Username') ?>
            <?php ActiveForm::end() ?>
        <?php Pjax::end() ?>
    </div>
    <div class="col-sm-6">
        <?php Pjax::begin(['options' => ['class' => 'async_inline']]) ?>
            <?php $form = ActiveForm::begin([
                'action' => Url::toRoute(['agent/update', 'id' => $user_model->id], true),
                'options' => [
                    'data-pjax' => 'true',
                ],
            ]) ?>
                <?= $form->field($user_profile_model, 'twitter_password')->passwordInput(['onchange' => 'submitForm(event)'])->label('Twitter Password') ?>
            <?php ActiveForm::end() ?>
        <?php Pjax::end() ?>
    </div>
</div>

<div class="row">
    <div class="col-sm-6">
        <?php Pjax::begin(['options' => ['class' => 'async_inline']]) ?>
            <?php $form = ActiveForm::begin([
                'action' => Url::toRoute(['agent/update', 'id' => $user_model->id], true),
                'options' => [
                    'data-pjax' => 'true',
                ],
            ]) ?>
                <?= $form->field($user_profile_model, 'youtube_login')->widget(MaskedInput::className(), [
                    'options' => [
                        'class' => 'form-control',
                        'onchange' => 'submitForm(event)'
                    ],
                    'clientOptions' => [
                        'alias' => 'email'
                    ]
                ])->label('YouTube Email Login') ?>
            <?php ActiveForm::end() ?>
        <?php Pjax::end() ?>
    </div>
    <div class="col-sm-6">
        <?php Pjax::begin(['options' => ['class' => 'async_inline']]) ?>
            <?php $form = ActiveForm::begin([
                'action' => Url::toRoute(['agent/update', 'id' => $user_model->id], true),
                'options' => [
                    'data-pjax' => 'true',
                ],
            ]) ?>
                <?= $form->field($user_profile_model, 'youtube_password')->passwordInput(['onchange' => 'submitForm(event)'])->label('YouTube Password') ?>
            <?php ActiveForm::end() ?>
        <?php Pjax::end() ?>
    </div>
</div>

<div class="row">
    <div class="col-sm-6">
        <?php Pjax::begin(['options' => ['class' => 'async_inline']]) ?>
            <?php $form = ActiveForm::begin([
                'action' => Url::toRoute(['agent/update', 'id' => $user_model->id], true),
                'options' => [
                    'data-pjax' => 'true',
                ],
            ]) ?>
                <?= $form->field($user_profile_model, 'schedule_question_1')->textarea(['onchange' => 'submitForm(event)'])->label('When are you available for Appointments during weekdays M-F?') ?>
            <?php ActiveForm::end() ?>
        <?php Pjax::end() ?>
    </div>
    <div class="col-sm-6">
        <?php Pjax::begin(['options' => ['class' => 'async_inline']]) ?>
            <?php $form = ActiveForm::begin([
                'action' => Url::toRoute(['agent/update', 'id' => $user_model->id], true),
                'options' => [
                    'data-pjax' => 'true',
                ],
            ]) ?>
                <?= $form->field($user_profile_model, 'schedule_question_2')->textInput(['onchange' => 'submitForm(event)'])->label('When are you available for appointments during Weekends Sat-Sun?') ?>
            <?php ActiveForm::end() ?>
        <?php Pjax::end() ?>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <?php Pjax::begin(['options' => ['class' => 'async_inline']]) ?>
            <?php $form = ActiveForm::begin([
                'action' => Url::toRoute(['agent/update', 'id' => $user_model->id], true),
                'options' => [
                    'data-pjax' => 'true',
                ],
            ]) ?>
                <?= $form->field($user_profile_model, 'schedule_question_3')->textarea(['onchange' => 'submitForm(event)'])->label('Do you need 24 hours before an appointment?') ?>
            <?php ActiveForm::end() ?>
        <?php Pjax::end() ?>
    </div>
    <div class="col-sm-12">
        <?php Pjax::begin(['options' => ['class' => 'async_inline']]) ?>
            <?php $form = ActiveForm::begin([
                'action' => Url::toRoute(['agent/update', 'id' => $user_model->id], true),
                'options' => [
                    'data-pjax' => 'true',
                ],
            ]) ?>
                <?= $form->field($user_profile_model, 'schedule_question_4')->textInput(['onchange' => 'submitForm(event)'])->label('Any vacation days planned? (Give specific dates)') ?>
            <?php ActiveForm::end() ?>
        <?php Pjax::end() ?>
    </div>
</div>

<div class="row">
    <div class="col-sm-6">
        <?php Pjax::begin(['options' => ['class' => 'async_inline']]) ?>
            <?php $form = ActiveForm::begin([
                'action' => Url::toRoute(['agent/update', 'id' => $user_model->id], true),
                'options' => [
                    'data-pjax' => 'true',
                ],
            ]) ?>
                <?= $form->field($user_profile_model, 'mls_name')->textInput(['onchange' => 'submitForm(event)'])->label('What is the name of your associated MLS board?') ?>
            <?php ActiveForm::end() ?>
        <?php Pjax::end() ?>
    </div>
    <div class="col-sm-6">
        <?php Pjax::begin(['options' => ['class' => 'async_inline']]) ?>
            <?php $form = ActiveForm::begin([
                'action' => Url::toRoute(['agent/update', 'id' => $user_model->id], true),
                'options' => [
                    'data-pjax' => 'true',
                ],
            ]) ?>
                <?= $form->field($user_profile_model, 'mls_website')->textInput(['onchange' => 'submitForm(event)'])->label('MLS Website Link') ?>
            <?php ActiveForm::end() ?>
        <?php Pjax::end() ?>
    </div>
</div>

<div class="row">
    <div class="col-sm-6">
        <?php Pjax::begin(['options' => ['class' => 'async_inline']]) ?>
            <?php $form = ActiveForm::begin([
                'action' => Url::toRoute(['agent/update', 'id' => $user_model->id], true),
                'options' => [
                    'data-pjax' => 'true',
                ],
            ]) ?>
                <?= $form->field($user_profile_model, 'mls_username')->textInput(['onchange' => 'submitForm(event)'])->label('Username for your local MLS') ?>
            <?php ActiveForm::end() ?>
        <?php Pjax::end() ?>
    </div>
    <div class="col-sm-6">
        <?php Pjax::begin(['options' => ['class' => 'async_inline']]) ?>
            <?php $form = ActiveForm::begin([
                'action' => Url::toRoute(['agent/update', 'id' => $user_model->id], true),
                'options' => [
                    'data-pjax' => 'true',
                ],
            ]) ?>
                <?= $form->field($user_profile_model, 'mls_password')->passwordInput(['onchange' => 'submitForm(event)'])->label('Password for your local MLS') ?>
            <?php ActiveForm::end() ?>
        <?php Pjax::end() ?>
    </div>
</div>

<div class="row">
    <div class="col-sm-6">
        <?php Pjax::begin(['options' => ['class' => 'async_inline']]) ?>
            <?php $form = ActiveForm::begin([
                'action' => Url::toRoute(['agent/update', 'id' => $user_model->id], true),
                'options' => [
                    'data-pjax' => 'true',
                ],
            ]) ?>
                <?= $form->field($user_profile_model, 'redx_username')->textInput(['onchange' => 'submitForm(event)'])->label('REDX Username') ?>
            <?php ActiveForm::end() ?>
        <?php Pjax::end() ?>
    </div>
    <div class="col-sm-6">
        <?php Pjax::begin(['options' => ['class' => 'async_inline']]) ?>
            <?php $form = ActiveForm::begin([
                'action' => Url::toRoute(['agent/update', 'id' => $user_model->id], true),
                'options' => [
                    'data-pjax' => 'true',
                ],
            ]) ?>
                <?= $form->field($user_profile_model, 'redx_password')->passwordInput(['onchange' => 'submitForm(event)'])->label('REDX Password') ?>
            <?php ActiveForm::end() ?>
        <?php Pjax::end() ?>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <?php Pjax::begin(['options' => ['class' => 'async_inline']]) ?>
            <?php $form = ActiveForm::begin([
                'action' => Url::toRoute(['agent/update', 'id' => $user_model->id], true),
                'options' => [
                    'data-pjax' => 'true',
                ],
            ]) ?>
                <?= $form->field($user_profile_model, 'zip_codes_daily')->textarea(['onchange' => 'submitForm(event)'])->label('Provide your select zip codes for daily calling based on at least a 15 mile radius around your office/home?') ?>
            <?php ActiveForm::end() ?>
        <?php Pjax::end() ?>
    </div>
    <div class="col-sm-12">
        <?php Pjax::begin(['options' => ['class' => 'async_inline']]) ?>
            <?php $form = ActiveForm::begin([
                'action' => Url::toRoute(['agent/update', 'id' => $user_model->id], true),
                'options' => [
                    'data-pjax' => 'true',
                ],
            ]) ?>
                <?= $form->field($user_profile_model, 'zip_codes_just')->textarea(['onchange' => 'submitForm(event)'])->label('Provide your zip codes, areas, or towns for the “Just Listed Just Sold & Circle Prospecting Campaigns"') ?>
            <?php ActiveForm::end() ?>
        <?php Pjax::end() ?>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <?php Pjax::begin(['options' => ['class' => 'async_inline']]) ?>
            <?php $form = ActiveForm::begin([
                'action' => Url::toRoute(['agent/update', 'id' => $user_model->id], true),
                'options' => [
                    'data-pjax' => 'true',
                ],
            ]) ?>
                <?= $form->field($user_profile_model, 'zip_codes_high_turnover')->textarea(['onchange' => 'submitForm(event)'])->label('Provide at least 3 zip codes for high turnover or sales volume in your area?') ?>
            <?php ActiveForm::end() ?>
        <?php Pjax::end() ?>
    </div>
    <div class="col-sm-12">
        <?php Pjax::begin(['options' => ['class' => 'async_inline']]) ?>
            <?php $form = ActiveForm::begin([
                'action' => Url::toRoute(['agent/update', 'id' => $user_model->id], true),
                'options' => [
                    'data-pjax' => 'true',
                ],
            ]) ?>
                <?= $form->field($user_profile_model, 'property_avoid')->checkboxList($property_avoid_list, ['onchange' => 'submitForm(event)'])->label('What property types should we avoid') ?>
            <?php ActiveForm::end() ?>
        <?php Pjax::end() ?>
    </div>
    <div class="col-sm-12">
        <?php Pjax::begin(['options' => ['class' => 'async_inline']]) ?>
            <?php $form = ActiveForm::begin([
                'action' => Url::toRoute(['agent/update', 'id' => $user_model->id], true),
                'options' => [
                    'data-pjax' => 'true',
                ],
            ]) ?>
                <?= $form->field($user_profile_model, 'property_list')->textarea(['onchange' => 'submitForm(event)'])->label('You may list your property address, year sold and price here.') ?>
            <?php ActiveForm::end() ?>
        <?php Pjax::end() ?>
    </div>
</div>

