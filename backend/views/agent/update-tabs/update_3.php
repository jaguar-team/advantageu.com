<?php

use yii\helpers\Html;
use yii\helpers\Url;
use common\models\UserRole;

use yii\bootstrap\ActiveForm;
use yii\widgets\Pjax;
use kartik\rating\StarRating;
use rmrevin\yii\fontawesome\FA;

$roles = UserRole::find()->select(['name'])->indexBy('id')->column();

$starCaptions = [];
for ($i = 0; $i <= 50; $i++){
    $starCaptions[(string)($i/10)] = ((double)$i/10).' out of 5 stars';
}
$reviews = $user_model->getReviews()->select('*')->asArray()->all();
?>

<div class="row">

    <div class="col-sm-6">
        <h4>Client Reviews</h4>

        <div class="agent-reviews">
            <div class="rating">
                <?=  StarRating::widget([
                    'name' => 'reviews_rating',
                    'value' => $user_model->averageRatingReviews,
                    'pluginOptions' => [
                        'displayOnly' => true,
                        'showClear' => false,
                        'filledStar' => Html::decode( FA::icon('star') ),
                        'emptyStar' => Html::decode( FA::icon('star-o') ),
                        'size' => 'sm',
                        'starCaptions' => $starCaptions
                    ],
                ]) ?>
                <div class="row">
                    <div class="col-md-10 col-xs-12">
                        <?php for ($i=5; $i>=1; $i--) : ?>
                            <div class="progress-group flexbox">
                                <label><?= $i ?> star</label>
                                <div class="progress">
                                    <div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="<?= isset($user_model->performanceReviews[$i]) ? $user_model->performanceReviews[$i]['percent'] : 0 ?>"
                                         aria-valuemin="0" aria-valuemax="100" style="width:<?= isset($user_model->performanceReviews[$i]) ? $user_model->performanceReviews[$i]['percent'] : 0 ?>%">
                                        <span class="sr-only"><?= isset($user_model->performanceReviews[$i]) ? $user_model->performanceReviews[$i]['percent'] : 0 ?>% Complete</span>
                                    </div>
                                </div>
                                <div class="value">
                                    <?= isset($user_model->performanceReviews[$i]) ? $user_model->performanceReviews[$i]['percent'] : 0 ?>%
                                </div>
                            </div>
                        <?php endfor ?>
                        <div class="clearfix"></div>
                    </div>

                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-2 col-sm-4">

                <?php $form = ActiveForm::begin([
                    'action' => Url::toRoute(['review/create'], true)
                ]) ?>
                    <input type="hidden" name="Review[id_user]" value="<?= $user_model->id ?>" />
                    <input type="hidden" name="Review[chosen_user]" value="<?= $user_model->profile->getFullName() ?>" />
                    <div class="action-buttons">
                        <?= Html::submitButton('Add review', ['class' => 'btn btn-block btn-secondary']) ?>
                    </div>
                <?php ActiveForm::end() ?>

            </div>
        </div>
    </div>

    <div class="col-sm-6">
        <h4>Top Customer Reviews</h4>
        <div class="reviews_container">
            <?php if(!empty($reviews)) : ?>
                <?php foreach ($reviews as $key => $review) : ?>
                    <div class="single">
                        <div class="rating">
                            <?= StarRating::widget([
                                'name' => 'reviews_rating',
                                'value' => $review['star'],
                                'pluginOptions' => [
                                    'displayOnly' => true,
                                    'showClear' => false,
                                    'showCaption' => false,
                                    'filledStar' => Html::decode( FA::icon('star') ),
                                    'emptyStar' => Html::decode( FA::icon('star-o') ),
                                    'size' => 'sm',
                                ],
                            ]); ?>
                        </div>
                        <p class="written_by">
                            By <?= Html::a($review['first_name'].' '.$review['last_name'], Url::toRoute(['agent/profile', 'id' => $review['id_user']])) ?> - <?= Html::encode(\Yii::$app->formatter->asRelativeTime($review['updated_at'])) ?>
                        </p>
                        <h5>
                            <?= Html::encode($review['title']) ?>
                        </h5>
                        <div class="review_body">
                            <?= Html::encode($review['body']) ?>
                        </div>
                    </div>
                <?php endforeach ?>
            <?php else : ?>
                <h5>No reviews yet!</h5>
            <?php endif ?>
        </div>

    </div>

</div>
