<?php

use yii\helpers\Html;
use yii\helpers\Url;

use yii\bootstrap\ActiveForm;
use yii\widgets\Pjax;
use yii\widgets\MaskedInput;

/* @var $this yii\web\View */
/* @var $user_profile_model common\models\UserProfile */
/* @var $upload_avatar_form common\models\UploadFileForm */
/* @var $area_model common\models\Area */
/* @var $user_address common\models\UserAddress */
/* @var $language_model common\models\Language */

/* @var $roles array */
/* @var $areas_served array */
/* @var $languages array */

?>

<div class="row">

    <div class="col-md-3">

        <div id="advancedDropzone" class="droppable-area overflow-hidden" data-toggle="popover" data-trigger="hover" title="Notice" data-content="Click to change photo">
            <?php Pjax::begin([
                    'options' => ['class' => ''],
                    'enablePushState' => 0
                ]) ?>
                <?php $form = ActiveForm::begin([
                    'action' => Url::toRoute(['agent/update', 'id' => $user_model->id], true),
                    'options' => [
                        'data-pjax' => 'true',
                        'id' => 'photo-update'
                    ],
                ]) ?>
                    <?= Html::img($user_profile_model->getPhotoUrl(), ['alt' => 'User avatar', 'width' => '100%', 'height' => '100%', 'id' => 'avatar-preview']) ?>

                     <div class="hidden">

                        <?= $form->field($upload_avatar_form, 'user_avatar', ['template' => '{input}'])->fileInput(['class' => 'hidden', 'id' => 'user-avatar-file'])->label(false); ?>

                        <?= $form->field($upload_avatar_form, 'crop[width]', ['template' => '{input}'])->hiddenInput(['id' => 'user-avatar-width'])->label(false); ?>

                        <?= $form->field($upload_avatar_form, 'crop[height]', ['template' => '{input}'])->hiddenInput(['id' => 'user-avatar-height'])->label(false); ?>

                        <?= $form->field($upload_avatar_form, 'crop[start][x]', ['template' => '{input}'])->hiddenInput(['id' => 'user-avatar-offset-x'])->label(false); ?>

                        <?= $form->field($upload_avatar_form, 'crop[start][y]', ['template' => '{input}'])->hiddenInput(['id' => 'user-avatar-offset-y'])->label(false); ?>

                    </div>
                <?php ActiveForm::end() ?>
            <?php Pjax::end() ?>
        </div>
    </div>

    <div class="col-md-9">
        <div class="row">

            <div class="col-md-4">
                <?php Pjax::begin(['options' => ['class' => 'async_inline']]) ?>
                    <?php $form = ActiveForm::begin([
                    'action' => Url::toRoute(['agent/update', 'id' => $user_model->id], true),
                        'options' => [
                            'data-pjax' => 'true',
                        ],
                    ]) ?>

                        <?= $form->field($user_model, 'id_role')->dropDownList($roles, ['class' => 'select2-offscreen', 'onchange' => 'submitForm(event)']) ?>

                    <?php ActiveForm::end() ?>
                <?php Pjax::end() ?>
            </div>

            <div class="col-md-4">
                <?php Pjax::begin(['options' => ['class' => 'async_inline']]) ?>
                    <?php $form = ActiveForm::begin([
                    'action' => Url::toRoute(['agent/update', 'id' => $user_model->id], true),
                        'options' => [
                            'data-pjax' => 'true',
                        ],
                    ]) ?>

                        <?= $form->field($user_model, 'id_zoho')->textInput(['onchange' => 'submitForm(event, true)']) ?>

                    <?php ActiveForm::end() ?>
                <?php Pjax::end() ?>
            </div>

            <div class="col-md-4">
                <?php Pjax::begin(['options' => ['class' => 'async_inline']]) ?>
                    <?php $form = ActiveForm::begin([
                    'action' => Url::toRoute(['agent/update', 'id' => $user_model->id], true),
                        'options' => [
                            'data-pjax' => 'true',
                        ],
                    ]) ?>

                        <?= $form->field($user_profile_model, 'license_number')->textInput(['onchange' => 'submitForm(event)']) ?>

                    <?php ActiveForm::end() ?>
                <?php Pjax::end() ?>
            </div>

        </div>

        <div class="row">
            <div class="col-md-8">
                <div class="row">
                    <div class="col-md-4">
                        <?php Pjax::begin(['options' => ['class' => 'async_inline']]) ?>
                            <?php $form = ActiveForm::begin([
                                'action' => Url::toRoute(['agent/update', 'id' => $user_model->id], true),
                                'layout' => 'horizontal',
                                'options' => [
                                    'data-pjax' => 'true',
                                ],
                                'fieldConfig' => [
                                    'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
                                    'checkboxTemplate' => "{beginWrapper}\n<div class=\"checkbox\">\n{beginLabel}\n{input}\n{labelTitle}\n{endLabel}\n</div>\n{error}\n{endWrapper}\n{hint}",
                                    'horizontalCssClasses' => [
                                        'label' => 'col-sm-5',
                                        'offset' => 'col-sm-offset-5',
                                        'wrapper' => 'col-sm-7',
                                    ]
                                ]
                            ]) ?>

                                <?= $form->field($user_model, 'private_account')->checkbox([
                                    'class'=>'iswitch iswitch-secondary',
                                    'onchange' => 'submitForm(event)'
                                ], false) ?>
        
                            <?php ActiveForm::end() ?>
                        <?php Pjax::end() ?>
                    </div>

                    <div class="col-md-4">
                        <?php Pjax::begin(['options' => ['class' => 'async_inline']]) ?>
                            <?php $form = ActiveForm::begin([
                                'action' => Url::toRoute(['agent/update', 'id' => $user_model->id], true),
                                'layout' => 'horizontal',
                                'options' => [
                                    'data-pjax' => 'true',
                                ],
                                'fieldConfig' => [
                                    'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
                                    'checkboxTemplate' => "{beginWrapper}\n<div class=\"checkbox\">\n{beginLabel}\n{input}\n{labelTitle}\n{endLabel}\n</div>\n{error}\n{endWrapper}\n{hint}",
                                    'horizontalCssClasses' => [
                                        'label' => 'col-sm-5',
                                        'offset' => 'col-sm-offset-5',
                                        'wrapper' => 'col-sm-7',
                                    ]
                                ]
                            ]) ?>

                                <?= $form->field($user_model, 'private_property')->checkbox([
                                    'class'=>'iswitch iswitch-secondary',
                                    'onchange' => 'submitForm(event)'
                                ], false) ?>

                            <?php ActiveForm::end() ?>
                        <?php Pjax::end() ?>
                    </div>

                    <div class="col-md-4">
                        <?php Pjax::begin(['options' => ['class' => 'async_inline']]) ?>
                            <?php $form = ActiveForm::begin([
                                'action' => Url::toRoute(['agent/update', 'id' => $user_model->id], true),
                                'layout' => 'horizontal',
                                'options' => [
                                    'data-pjax' => 'true',
                                ],
                                'fieldConfig' => [
                                    'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
                                    'checkboxTemplate' => "{beginWrapper}\n<div class=\"checkbox\">\n{beginLabel}\n{input}\n{labelTitle}\n{endLabel}\n</div>\n{error}\n{endWrapper}\n{hint}",
                                    'horizontalCssClasses' => [
                                        'label' => 'col-sm-5',
                                        'offset' => 'col-sm-offset-5',
                                        'wrapper' => 'col-sm-7',
                                    ]
                                ]
                            ]) ?>

                                <?= $form->field($user_model, 'status')->checkbox([
                                    'class'=>'iswitch iswitch-secondary',
                                    'onchange' => 'submitForm(event)'
                                ], false) ?>

                            <?php ActiveForm::end() ?>
                        <?php Pjax::end() ?>
                    </div>

                </div>
            </div>


            <div class="col-md-4">
                <?php Pjax::begin(['options' => ['class' => 'async_inline']]) ?>
                    <?php $form = ActiveForm::begin([
                        'action' => Url::toRoute(['agent/update', 'id' => $user_model->id], true),
                        'options' => [
                            'data-pjax' => 'true',
                        ],
                    ]) ?>

                        <?= $form->field($user_profile_model, 'phone_number')->widget(MaskedInput::className(), [
                            'mask' => '(999)999-9999',
                            'options' => [
                                'placeholder' => '(___)___-____',
                                'class' => 'form-control',
                                'onchange' => 'submitForm(event)'
                            ]
                        ]) ?>

                    <?php ActiveForm::end() ?>
                <?php Pjax::end() ?>
            </div>

        </div>

        <div class="row">

            <div class="col-md-8">
                <div class="row">
                    <div class="col-md-4">
                        <?php Pjax::begin(['options' => ['class' => 'async_inline']]) ?>
                            <?php $form = ActiveForm::begin([
                                'action' => Url::toRoute(['agent/update', 'id' => $user_model->id], true),
                                'layout' => 'horizontal',
                                'options' => [
                                    'data-pjax' => 'true',
                                ],
                                'fieldConfig' => [
                                    'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
                                    'checkboxTemplate' => "{beginWrapper}\n<div class=\"checkbox\">\n{beginLabel}\n{input}\n{labelTitle}\n{endLabel}\n</div>\n{error}\n{endWrapper}\n{hint}",
                                    'horizontalCssClasses' => [
                                        'label' => 'col-sm-5',
                                        'offset' => 'col-sm-offset-5',
                                        'wrapper' => 'col-sm-7',
                                    ]
                                ]
                            ]) ?>

                                <?= $form->field($user_model, 'certificate')->checkbox([
                                    'class'=>'iswitch iswitch-secondary',
                                    'onchange' => 'submitForm(event)'
                                ], false) ?>

                            <?php ActiveForm::end() ?>
                        <?php Pjax::end() ?>
                    </div>

                    <div class="col-md-4">
                        <?php Pjax::begin(['options' => ['class' => 'async_inline']]) ?>
                            <?php $form = ActiveForm::begin([
                                'action' => Url::toRoute(['agent/update', 'id' => $user_model->id], true),
                                'layout' => 'horizontal',
                                'options' => [
                                    'data-pjax' => 'true',
                                ],
                                'fieldConfig' => [
                                    'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
                                    'checkboxTemplate' => "{beginWrapper}\n<div class=\"checkbox\">\n{beginLabel}\n{input}\n{labelTitle}\n{endLabel}\n</div>\n{error}\n{endWrapper}\n{hint}",
                                    'horizontalCssClasses' => [
                                        'label' => 'col-sm-5',
                                        'offset' => 'col-sm-offset-5',
                                        'wrapper' => 'col-sm-7',
                                    ]
                                ]
                            ]) ?>

                                <?= $form->field($user_profile_model, 'sms_send')->checkbox([
                                    'class'=>'iswitch iswitch-secondary',
                                    'onchange' => 'submitForm(event)'
                                ], false) ?>

                            <?php ActiveForm::end() ?>
                        <?php Pjax::end() ?>
                    </div>

                    <div class="col-md-4">
                        <?php Pjax::begin(['options' => ['class' => 'async_inline']]) ?>
                            <?php $form = ActiveForm::begin([
                                'action' => Url::toRoute(['agent/update', 'id' => $user_model->id], true),
                                'layout' => 'horizontal',
                                'options' => [
                                    'data-pjax' => 'true',
                                ],
                                'fieldConfig' => [
                                    'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
                                    'checkboxTemplate' => "{beginWrapper}\n<div class=\"checkbox\">\n{beginLabel}\n{input}\n{labelTitle}\n{endLabel}\n</div>\n{error}\n{endWrapper}\n{hint}",
                                    'horizontalCssClasses' => [
                                        'label' => 'col-sm-5',
                                        'offset' => 'col-sm-offset-5',
                                        'wrapper' => 'col-sm-7',
                                    ]
                                ]
                            ]) ?>

                                <?= $form->field($user_profile_model, 'email_send')->checkbox([
                                    'class'=>'iswitch iswitch-secondary',
                                    'onchange' => 'submitForm(event)'
                                ], false) ?>

                            <?php ActiveForm::end() ?>
                        <?php Pjax::end() ?>
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <?php Pjax::begin(['options' => ['class' => 'async_inline']]) ?>
                    <?php $form = ActiveForm::begin([
                        'action' => Url::toRoute(['agent/update', 'id' => $user_model->id], true),
                        'options' => [
                            'data-pjax' => 'true',
                        ],
                    ]) ?>

                        <?= $form->field($user_profile_model, 'website_url')->textInput(['onchange' => 'submitForm(event)']) ?>

                    <?php ActiveForm::end() ?>
                <?php Pjax::end() ?>
            </div>

        </div>

        <div class="row">
            <div class="col-md-8">
                <div class="row">
                    <div class="col-md-4">
                        <?php Pjax::begin(['options' => ['class' => 'async_inline']]) ?>
                            <?php $form = ActiveForm::begin([
                                'action' => Url::toRoute(['agent/update', 'id' => $user_model->id], true),
                                'layout' => 'horizontal',
                                'options' => [
                                    'data-pjax' => 'true',
                                ],
                                'fieldConfig' => [
                                    'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
                                    'checkboxTemplate' => "{beginWrapper}\n<div class=\"checkbox\">\n{beginLabel}\n{input}\n{labelTitle}\n{endLabel}\n</div>\n{error}\n{endWrapper}\n{hint}",
                                    'horizontalCssClasses' => [
                                        'label' => 'col-sm-5',
                                        'offset' => 'col-sm-offset-5',
                                        'wrapper' => 'col-sm-7',
                                    ]
                                ]
                            ]) ?>

                                <?= $form->field($user_model, 'top')->checkbox([
                                    'class'=>'iswitch iswitch-secondary',
                                    'onchange' => 'submitForm(event)'
                                ], false) ?>

                            <?php ActiveForm::end() ?>
                        <?php Pjax::end() ?>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <?php Pjax::begin(['options' => ['class' => 'async_inline']]) ?>
                    <?php $form = ActiveForm::begin([
                    'action' => Url::toRoute(['agent/update', 'id' => $user_model->id], true),
                        'options' => [
                            'data-pjax' => 'true',
                        ],
                    ]) ?>

                        <?= $form->field($user_profile_model, 'company')->textInput(['onchange' => 'submitForm(event)']) ?>

                    <?php ActiveForm::end() ?>
                <?php Pjax::end() ?>
            </div>

        </div>

    </div>

</div>

<div class="row">

    <div class="col-md-4">
        <?php Pjax::begin(['options' => ['class' => 'async_inline']]) ?>
            <?php $form = ActiveForm::begin([
                    'action' => Url::toRoute(['agent/update', 'id' => $user_model->id], true),
                    'options' => [
                        'data-pjax' => 'true',
                    ]
                ])
            ?>

                <?= $form->field($user_profile_model, 'first_name')->textInput(['onchange' => 'submitForm(event)']) ?>

            <?php ActiveForm::end() ?>
        <?php Pjax::end() ?>
    </div>

    <div class="col-md-4">
        
            <?php $form = ActiveForm::begin([
                    'action' => Url::toRoute(['agent/update', 'id' => $user_model->id], true),
                    'options' => [
                        'data-pjax' => 'true',
                    ]
                ])
            ?>

                <?= $form->field($user_profile_model, 'last_name')->textInput(['onchange' => 'submitForm(event)']) ?>

            <?php ActiveForm::end() ?>

    </div>

    <div class="col-md-4">
        <?php Pjax::begin(['options' => ['class' => 'async_inline']]) ?>
            <?php $form = ActiveForm::begin([
                    'action' => Url::toRoute(['agent/update', 'id' => $user_model->id], true),
                    'options' => [
                        'data-pjax' => 'true',
                    ]
                ])
            ?>

                <?= $form->field($user_profile_model, 'education')->textInput(['onchange' => 'submitForm(event)']) ?>

            <?php ActiveForm::end() ?>
        <?php Pjax::end() ?>
    </div>

</div>

<div class="row">

    <div class="col-md-12">
        <?php Pjax::begin([
            'options' => ['class' => 'async_inline row'],
            'enablePushState' => 0
            ]) ?>
            <?php $form = ActiveForm::begin([
                    'action' => Url::toRoute(['agent/update', 'id' => $user_model->id], true),
                    'options' => [
                        'class' => 'area_validation col-md-4',
                        'data-pjax' => 'true',
                        'validateOnBlur' => false
                    ]
                ])
            ?>
                <div class="form-group area_field">

                        <label class="label-control">Real Estate Market</label>

                        <div class="group">
                            
                            <div class="field search_areas loading async_inline">
                                <input class="form-control area_field" type="text" placeholder="Add one area at a time"
                                    data-type="(regions)"
                                    data-state-id="<?= Html::getInputId($area_model, 'state') ?>"
                                    data-short-id="<?= Html::getInputId($area_model, 'short_name') ?>"
                                    data-zipcode-id="<?= Html::getInputId($area_model, 'zip_code') ?>"
                                    data-city-id="<?= Html::getInputId($area_model, 'city') ?>"
                                    data-region-id="<?= Html::getInputId($area_model, 'region') ?>" />
                            </div>

                            <?= Html::submitButton('Add', ['class' => 'btn btn-blue']) ?>

                        </div>

                    <div class="hidden">

                        <?= $form->field($area_model, 'state', ['template' => '{input}'])->hiddenInput()->label(false) ?>

                        <?= $form->field($area_model, 'short_name', ['template' => '{input}'])->hiddenInput()->label(false) ?>

                        <?= $form->field($area_model, 'zip_code', ['template' => '{input}'])->hiddenInput()->label(false) ?>

                        <?= $form->field($area_model, 'city', ['template' => '{input}'])->hiddenInput()->label(false) ?>

                        <?= $form->field($area_model, 'region', ['template' => '{input}'])->hiddenInput()->label(false) ?>

                    </div>
                </div>
            <?php ActiveForm::end() ?>
                <ul class="select2-choices">
                    <?php foreach ($areas_served as $key => $area) : ?>
                        <li class="select2-search-choice">
                            <?= $area ?>
                            <?= Html::a('×', Url::toRoute(['agent/delete-area', 'id' => $key], true), ['class' => 'select2-search-choice-close', 'data-pjax' => 'true', 'data-method' => 'POST', 'tabindex' => '-1']) ?>
                        </li>
                    <?php endforeach ?>
                </ul>
        <?php Pjax::end() ?>
    </div>

</div>

<div class="row">

    <div class="col-md-12">
        <?php Pjax::begin([
            'options' => ['class' => 'async_inline row'],
            'enablePushState' => 0
            ]) ?>
            <?php $form = ActiveForm::begin([
                    'action' => Url::toRoute(['agent/update', 'id' => $user_model->id], true),
                    'options' => [
                        'class' => 'area_validation col-md-4',
                        'data-pjax' => 'true',
                        'validateOnBlur' => false
                    ]
                ])
            ?>
                <div class="form-group area_field">

                    <label class="label-control">Addresses</label>

                    <div class="group">
                        <div class="search_areas field loading async_inline">
                            <input class="controls form-control area_field" type="text" placeholder="Add one area at a time"
                                data-type="address"
                                data-city-id="<?= Html::getInputId($user_address, 'city') ?>"
                                data-state-id="<?= Html::getInputId($user_address, 'state') ?>"
                                data-zipcode-id="<?= Html::getInputId($user_address, 'zip_code') ?>"
                                data-address-id="<?= Html::getInputId($user_address, 'address') ?>"
                                data-lat-id="<?= Html::getInputId($user_address, 'lat') ?>"
                                data-lng-id="<?= Html::getInputId($user_address, 'lng') ?>"
                                data-place_id-id="<?= Html::getInputId($user_address, 'place_id') ?>"
                                data-short-id="<?= Html::getInputId($user_address, 'short_name') ?>" />
                        </div>
                        <?= Html::submitButton('Add', ['class' => 'btn btn-blue']) ?>
                    </div>

                    <div class="hidden">
                        <?= $form->field($user_address, 'city')->hiddenInput()->label(false) ?>
                        <?= $form->field($user_address, 'state')->hiddenInput()->label(false) ?>
                        <?= $form->field($user_address, 'zip_code')->hiddenInput()->label(false) ?>
                        <?= $form->field($user_address, 'address')->hiddenInput()->label(false) ?>
                        <?= $form->field($user_address, 'lat')->hiddenInput()->label(false) ?>
                        <?= $form->field($user_address, 'lng')->hiddenInput()->label(false) ?>
                        <?= $form->field($user_address, 'place_id')->hiddenInput()->label(false) ?>
                        <?= $form->field($user_address, 'short_name')->hiddenInput()->label(false) ?>
                    </div>
                </div>
            <?php ActiveForm::end() ?>
                <ul class="select2-choices">
                    <?php foreach ($user_model->addresses as $key => $address) : ?>
                        <li class="select2-search-choice">
                            <?= $address['address'].', '.$address['city'].', '.$address['short_name'] ?>
                            <?= Html::a('×', Url::toRoute(['agent/delete-address', 'id' => $address['id']], true), ['class' => 'select2-search-choice-close', 'data-pjax' => 'true', 'data-method' => 'POST', 'tabindex' => '-1']) ?>
                        </li>
                    <?php endforeach ?>
                </ul>
        <?php Pjax::end() ?>
    </div>

</div>

<div class="row">

    <div class="col-md-12">
        <?php Pjax::begin([
            'options' => ['class' => 'async_inline row'],
            'enablePushState' => 0
        ]) ?>
            <?php $form = ActiveForm::begin([
                    'action' => Url::toRoute(['agent/update', 'id' => $user_model->id], true),
                    'options' => [
                        'class' => 'area_validation col-md-4',
                        'data-area-model' => 'area',
                        'data-pjax' => 'true',
                        'validateOnBlur' => false
                    ],
                    'fieldConfig' => [
                        'template' => "{label}\n<div class='group'>\n<div class='field'>\n{input}\n</div>\n".Html::submitButton('Add', ['class' => 'btn btn-blue'])."\n</div>\n{hint}\n{error}\n",
                    ]
                ])
            ?>

                <?= $form->field($language_model, 'name')->textInput()->label('Language') ?>

            <?php ActiveForm::end() ?>
                <ul class="select2-choices">
                    <?php foreach ($languages as $key => $language) : ?>
                        <li class="select2-search-choice">
                            <?= $language ?>
                            <?= Html::a('×', Url::toRoute(['agent/delete-language', 'id' => $key], true), ['class' => 'select2-search-choice-close', 'data-pjax' => 'true', 'data-method' => 'POST', 'tabindex' => '-1']) ?>
                        </li>
                    <?php endforeach ?>
                </ul>
        <?php Pjax::end() ?>
    </div>

</div>

<div class="row">
    <div class="col-md-12">

        <?php Pjax::begin(['options' => ['class' => 'async_inline']]) ?>
            <?php $form = ActiveForm::begin([
                    'action' => Url::toRoute(['agent/update', 'id' => $user_model->id], true),
                    'options' => [
                        'data-pjax' => 'true',
                    ],
                    'fieldConfig' => [
                        'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}"
                    ]
                ])
            ?>

                <?= $form->field($user_profile_model, 'biography')->textarea(['class' => 'form-control', 'rows' => '5', 'onchange' => 'submitForm(event)']) ?>

            <?php ActiveForm::end() ?>
        <?php Pjax::end() ?>
    </div>
</div>
