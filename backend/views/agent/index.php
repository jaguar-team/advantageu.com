<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\Pjax;
use rmrevin\yii\fontawesome\FA;
use common\models\UserRole;
use common\helpers\Converter;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $user_search common\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Agents';
$this->params['breadcrumbs'][] = $this->title;

$roles = UserRole::find()->agent()->select(['name'])->indexBy('id')->all();

$tableActions = "<div class='row'>\n
                    <div class='col-sm-6'>\n
                        <div class='members-table-actions'>\n
                            <div class='selected-items'>\n
                                <span>0</span>\n
                                members selected\n
                            </div>\n

                            <div class='selected-actions'>\n
                                <a href='#' class='delete' data-action='delete-users'><i class='linecons-trash'></i>Delete</a>\n
                                or\n
                                <a href='#' class='edit' data-action='activate-users'><i class='fa-check'></i>Confirm</a>\n
                                or\n
                                <a href='#' class='' data-action='certify-users'><i class='fa-certificate'></i>Certify</a>\n
                                selected.\n
                            </div>\n
                        </div>\n
                    </div>\n                    
                </div>";


?>
<div class="user-index">

    <?php Pjax::begin([
        'options' => ['id' => 'agent-list-container'],
        'timeout' => 5000,
        ]) ?>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $user_search,
            'layout'=>"{summary}\n".$tableActions."\n
                <div class=\"table-responsive\" data-pattern=\"priority-columns\">\n{items}\n</div>".$tableActions."\n<div class='text-right text-center-sm'>{pager}</div>",

            'tableOptions' => [
                'class' => 'table table-small-font table-bordered table-striped members-table middle-align',
                'cellspacing' => 0,
            ],

            'options' => [
                'class' => 'table-responsive grid-view',
                'id' => 'agent-list',
            ],

            'pager' => [
                'options' => [
                    'class' => 'pagination pagination-sm no-margin',
                    'style' => 'float: right',
                ],
                'nextPageLabel' => FA::icon('angle-right'),
                'prevPageLabel' => FA::icon('angle-left'),
            ],

            'summary' => 'Showing <strong>{begin}</strong> - <strong>{end}</strong> of <strong>{totalCount}</strong> items.',
            'columns' => [
                // checkbox
                [
                    'checkboxOptions' => ['class' => 'cbr'],
                    'class' => 'yii\grid\CheckboxColumn',
                    //'header' => '<input type="checkbox" class="select-on-check-all cbr" name="selection_all" value="1">',
                ],
                //name
                [
                    'label' => 'Name',
                    'attribute' => 'full_name',
                    'format' => 'raw',
                    'contentOptions' => ['class' => 'user-name'],
                    'value' => function($user){

                        $img = Html::a(
                            Html::img($user->profile->getPhotoUrl(), [
                                'width'     => '32px',
                                'height'    => '32px',
                                'alt'       => $user->profile->getFullName(),
                                'class'     => 'img-circle',
                            ]),
                        Url::toRoute(['update', 'id' => $user->id], true),['data-pjax' => 'false']);

                        $fullName =  Html::a($user->profile->getFullName(), Url::toRoute(['update', 'id' => $user->id], true), ['class' => 'name', 'data-pjax' => 'false']);

                        $delete = Html::a(
                            '<i class="linecons-trash"></i> Delete',
                            'javascript:void(0)',
                            [
                                'onclick' => 'ajaxDelete(event, '.$user->id.', \'user\')',
                                'class' => 'delete'
                            ]
                        );

                        $edit = Html::a('<i class="linecons-pencil"></i> Edit Profile', 
                            Url::toRoute(['agent/update', 'id' => $user->id]),
                            [
                                'class' => 'edit',
                                'data-pjax' => 'false'
                            ]
                        );

                        $view = Html::a('<i class="fa-eye"></i> View Profile',
                            \Yii::$app->urlFrontEndManager->createAbsoluteUrl(['agent/profile', 'id' => $user->id], true),
                            [
                                'class' => 'view',
                                'data-pjax' => 'false'
                            ]
                        );

                        return '<div>'.
                            $img.
                            $fullName.
                            '<div class="action-links">'.
                                $edit.
                                $delete.
                                $view.
                            '</div>
                        </div>';
                    }
                ],
                //role
                [
                    'label' => 'Role',
                    'attribute' => 'id_role',
                    'format' => 'raw',
                    'value' => function($user){
                        $role = Html::dropDownList('user_role', $user->id_role, UserRole::find()->agent()->indexBy('id')->select(['name'])->column(), [
                                'class' => 'select2-offscreen',
                                'onchange' => 'changeRole(event, "change-user-role", '.$user->id.')'
                            ]);
                        return $role;
                    },
                    'filter' => Html::activeDropDownList($user_search, 'id_role', UserRole::find()->agent()->indexBy('id')->select(['name'])->column() ,[
                        'prompt' => 'All roles',
                        'class'=>'select2-offscreen'
                    ])
                ],
                //zip codes
                [
                    'label' => 'Zip codes',
                    'attribute' => 'zip_codes',
                    'contentOptions' => ['class' => 'zip_codes'],
                    'format' => 'raw',
                    'value' => function($user){
                        $first = (!empty(Converter::stringToZipCodesArray($user->profile->zip_codes_daily, true)) ?
                            '<a href="javascript:void(0)" data-container="body" data-toggle="popover" data-placement="bottom" data-content="'.Converter::stringToZipCodesArray($user->profile->zip_codes_daily, true).'">
                                <strong data-toggle="tooltip" data-placement="top" title="Click to view all">Zip codes in radius 15 miles</strong>
                            </a><br>'.Converter::stringToZipCodesArray($user->profile->zip_codes_daily, true).'<br>' : '');
                        $second = (!empty(Converter::stringToZipCodesArray($user->profile->zip_codes_just, true)) ?
                            '<a href="javascript:void(0)" data-container="body" data-toggle="popover" data-placement="bottom" data-content="'.Converter::stringToZipCodesArray($user->profile->zip_codes_just, true).'">
                                <strong data-toggle="tooltip" data-placement="top" title="Click to view all">Just Listed Just Sold</strong>
                            </a><br>'.Converter::stringToZipCodesArray($user->profile->zip_codes_just, true).'<br>' : '');
                        $third = (!empty(Converter::stringToZipCodesArray($user->profile->zip_codes_high_turnover, true)) ?
                            '<a href="javascript:void(0)" data-container="body" data-toggle="popover" data-placement="bottom" data-content="'.Converter::stringToZipCodesArray($user->profile->zip_codes_high_turnover, true).'">
                                <strong data-toggle="tooltip" data-placement="top" title="Click to view all">High turnover</strong>
                            </a><br>'.Converter::stringToZipCodesArray($user->profile->zip_codes_high_turnover, true).'<br>' : '');

                        return $first.$second.$third;
                    },
                ],
                //email
                [
                    'label' => 'Email',
                    'attribute' => 'email',
                    'value' => 'email'
                ],
                //properties
                [
                    'label' => 'Properties',
                    'attribute' => 'transaction_quantity',
                    'value' => function($user){
                        return $user->transaction_quantity;
                    }
                ],
                //active
                [
                    'label' => 'Active',
                    'format' => 'raw',
                    'value' => function($user){
                        return '<input type="checkbox" class="iswitch iswitch-secondary" value="'.$user->id.'" '.($user->status ? 'checked="checked"' : '').' 
                                    data-action="change-user-status" onchange="changeStatus(event)" />';
                    }
                ],
                //certified
                [
                    'label' => 'Certified',
                    'format' => 'raw',
                    'value' => function($user){
                        return '<input type="checkbox" class="iswitch iswitch-secondary" value="'.$user->id.'" '.($user->certificate ? 'checked="checked"' : '').' 
                                    data-action="change-user-certified-status" onchange="changeStatus(event)" />';
                    }
                ],
                //top
                [
                    'label' => 'Top',
                    'format' => 'raw',
                    'value' => function($user){
                        return '<input type="checkbox" class="iswitch iswitch-secondary" value="'.$user->id.'" '.($user->top ? 'checked="checked"' : '').' 
                                    data-action="change-user-top-status" onchange="changeStatus(event)" />';
                    }
                ],
                //creation date
                [
                    'label' => 'Creation Date',
                    'attribute' => 'created_at',
                    'value' => function($user){
                        return \Yii::$app->formatter->asDate($user->created_at, 'medium');
                    },
                    'filter' => DatePicker::widget([
                        'model' => $user_search,
                        'attribute' => 'created_at',
                        'options' => ['placeholder' => 'Select creation date ...'],
                        'readonly' => true,
                        'pluginOptions' => [
                            'autoclose'=>true,
                            'format' => 'dd-mm-yyyy',
                            //'todayHighlight' => true
                        ]
                    ])
                ],
                //modify date
                [
                    'label' => 'Last Modify',
                    'attribute' => 'updated_at',
                    'value' => function($user){
                        return \Yii::$app->formatter->asDate($user->updated_at, 'medium');
                    },
                    'filter' => DatePicker::widget([
                        'model' => $user_search,
                        'attribute' => 'updated_at',
                        'options' => ['placeholder' => 'Select update date ...'],
                        'readonly' => true,
                        'pluginOptions' => [
                            'autoclose'=>true,
                            'format' => 'dd-mm-yyyy',
                            //'todayHighlight' => true
                        ]
                    ])
                ]
            ],
        ]) ?>
    <?php Pjax::end() ?>
</div>
