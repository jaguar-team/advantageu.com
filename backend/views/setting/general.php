<?php
/**
 * Created by PhpStorm.
 * User: Vlad
 * Date: 18.04.2017
 * Time: 14:22
 */

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$date_format = array();
foreach ($general_setting_form->default_date_format as $key => $value) {        
    $date_format[$value] = $value;
}

$time_format = array();
foreach ($general_setting_form->default_time_format as $key => $value) {        
    $time_format[$value] = $value;
}

$this->title = 'General Settings';
$this->params['breadcrumbs'][] = ['label' => 'Settings', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'General';
?>

<div class="row">
    <div class="col-sm-12">

        <div class="panel panel-default">
            <!--<div class="panel-heading">
                <h3 class="panel-title">Default form inputs</h3>
            </div>-->
            <div class="panel-body">

                <?php $form = ActiveForm::begin([
                    'options' => [
                        'class' => 'form-horizontal',
                        'data-pjax' => true,
                        'enctype' => 'multipart/form-data'
                    ],
                    'fieldConfig' => [
                        'template' => '                            
                            {label}
                            <div class="col-sm-10">{input}{error}{hint}</div>',  
                        'labelOptions' => [
                            'class' => 'col-sm-2 control-label'
                        ]  
                    ],
                         
                ]); ?>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Site logo</label>
                        <div class="col-sm-10">

                            <div class="hidden">
                                <?= $form->field($upload_site_logo, 'img')->fileInput(['id' => 'user-avatar-file', 'accept' => 'image/*']) ?>
                            </div>

                            <div class="col-md-2 col-sm-4">
                                <div class="action-buttons">
                                    <button id="photo-upload" data-input="user-avatar-file" data-img="logo_preview" class="btn btn-block btn-secondary">Upload logo</button>
                                </div>
                            </div>
                            
                            <div class="col-sm-8">
                                <div class="img" id="logo_preview">
                                    <?php echo (Yii::$app->setting->getValueByName('site_logo') ? Html::img('@uri_img_upload/'.Yii::$app->setting->getValueByName('site_logo')) : '') ?>
                                </div>
                            </div>
                            
                        </div>
                    </div>

                    <div class="form-group-separator"></div>


                    <?= $form->field($general_setting_form, 'site_name')->textInput(); ?>

                    <div class="form-group-separator"></div>

                    <?= $form->field($general_setting_form, 'site_description')->textInput(); ?>

                    <div class="form-group-separator"></div>

                    <?= $form->field($general_setting_form, 'site_url')->textInput(); ?>

                    <div class="form-group-separator"></div>

                    <!--<?//= $form->field($general_setting_form, 'site_logo')->fileInput(['value' => \Yii::$app->setting->getValueByName('site_logo')]); ?>-->

                    <?= $form->field($general_setting_form, 'admin_email')->textInput(); ?>

                    <div class="form-group-separator"></div>
                    <?= $form->field($general_setting_form, 'date_format')->radioList($date_format,
                        [
                            'item' => function($index, $label, $name, $checked, $value) {
                                return '<div class="radio">
                                    <label>
                                        <input type="radio" name="' . $name . '" value="' . $value . '" '.($checked ? 'checked' : '').' />
                                        <div class="radio-label">'.date( $label ).'</div><code>'.$label.'</code>
                                    </label>
                                </div>';
                            }
                        ]
                    ); ?>

                    <div class="form-group-separator"></div>
                    <?= $form->field($general_setting_form, 'time_format')->radioList($time_format,
                        [
                            'item' => function($index, $label, $name, $checked, $value) {
                                return '<div class="radio">
                                    <label>
                                        <input type="radio" name="' . $name . '" value="' . $value . '" '.($checked ? 'checked' : '').'>
                                        <div class="radio-label">'.date( $label ).'</div><code>'.$label.'</code>
                                    </label>
                                </div>';
                            }
                        ]
                    ); ?>

                    <div class="form-group-separator"></div>                    

                    <div class="row">
                        <div class="col-md-2 col-sm-4">

                            <div class="action-buttons">
                                <?= Html::submitButton('Save', ['class' => 'btn btn-block btn-secondary']) ?>
                            </div>

                        </div>
                    </div>

                <?php ActiveForm::end(); ?>

            </div>
        </div>

    </div>
</div>