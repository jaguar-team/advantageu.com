<?php
/**
 * Created by PhpStorm.
 * User: Vlad
 * Date: 18.04.2017
 * Time: 14:22
 */

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\helpers\Url;

$this->title = 'Custom Fields';
$this->params['breadcrumbs'][] = ['label' => 'Settings', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$roles = $user_role_model->getRolesDropdown();
$advantages_default = $advantage_model->getDropdownByUserRole(array_keys($roles)[0]);

$know_from_list = $know_from_model->getDropdown();
$know_from_default = $know_from_model->getDropdownByUserRole(array_keys($roles)[0]);

$section_list = $section_model->getByIdRole(2);

$property_type_list = $property_type_model->find()->select(['name'])->indexBy('id')->column();
$property_status_list = $property_status_model->find()->select(['name'])->indexBy('id')->column();
$property_owner_type_list = $property_owner_type_model->find()->select(['name'])->indexBy('id')->column();
$transaction_type_list = $transaction_type_model->find()->select(['name'])->indexBy('id')->column();

?>



<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title col-md-12">
                	Reviews
                	<p class="description">Advantages</p>
                </h3>
            </div>
            <div class="panel-body">
        		<div class="col-sm-6 form-group">
        			<?= Html::label('Role', '', ['class' => 'control-label']) ?>
        			<?= Html::dropDownList('role', '', $roles, ['onchange' => 'setRole(event, "'.Html::getInputId($advantage_model, 'id_role').'", "advantages_list", "advantage")', 'class' => 'select2-offscreen']) ?>
        		</div>

	            <?php Pjax::begin([
	            	'enablePushState' => false,
	            	'enableReplaceState' => false,
	            ]) ?>

		            <div class="col-sm-6 visible-xs">
	                    <ul class="advantages advantages_list">
	                    	<?php if(!empty($advantages_default)) {
	                    		foreach ($advantages_default as $key => $value) : ?>
	                        		<li>
	                        			<div><?= $value ?></div>
		            					<?= Html::a( '', Url::toRoute(['setting/delete-advantage', 'id' => $key], true), ['class' => 'select2-search-choice-close', 'tabindex' => -1, 'data-method' => 'post', 'data-pjax' => 'true'] ) ?>

	                        		</li>
	                    		<?php 
	                    		endforeach;
	                		} else echo '
	                			<li>
	                				<div>No options yet</div>
	                			</li>'; ?>
	                    </ul>
	                </div>

	                <?php $form = ActiveForm::begin([
		            		'options' => [
		            			'class' => 'col-sm-6',
		            			'data-pjax' => true, 
		            		],
		            		'validateOnBlur' => false
		            	]); ?>    
		            	<div class="row">
			                <div class="col-md-12">           	

			                    <?= $form->field($advantage_model, 'name')->textInput(); ?>

			            		<?= $form->field($advantage_model, 'id_role',['template' => '{input}'])->hiddenInput(['value' => array_keys($roles)[0]])->label(false) ?>

			                </div>

	                    
		                    <div class="col-md-2 col-sm-4 pull-right-sm">

		                        <div class="action-buttons">
		                            <?= Html::submitButton('Add', ['class' => 'btn btn-block btn-secondary']) ?>
		                            
		                        </div>

		                    </div>
		                </div>

	            	<?php ActiveForm::end(); ?>
    		
	    			<div class="col-sm-6 hidden-xs">
                        <ul class="advantages advantages_list">
                        	<?php if(!empty($advantages_default)) {
                        		foreach ($advantages_default as $key => $value) : ?>
	                        		<li>
	                        			<div><?= $value ?></div>
		            					<?= Html::a( '', Url::toRoute(['setting/delete-advantage', 'id' => $key], true), ['class' => 'select2-search-choice-close', 'tabindex' => -1, 'data-method' => 'post', 'data-pjax' => 'true'] ) ?>

	                        		</li>
                        		<?php 
                        		endforeach;
                    		} else echo '
                    			<li>
                    				<div>No options yet</div>
                    			</li>'; ?>
                        </ul>
	                </div>
	                <div class="clearfix"></div>
	            <?php Pjax::end() ?>
            </div>

            <div class="panel-heading">
                <h3 class="panel-title col-md-12">

                	<p class="description">Know from options</p>
                </h3>
            </div>

            <div class="panel-body">
        		<div class="col-md-6 form-group">
        			<?= Html::label('Role', '', ['class' => 'control-label']) ?>
        			<?= Html::dropDownList('role', '', $roles, ['onchange' => 'setRole(event, "'.Html::getInputId($know_from_model, 'id_role').'", "know_from_list", "know-from")', 'class' => 'select2-offscreen']) ?>
        		</div>



	            <?php Pjax::begin([
	            	'enablePushState' => false,
	            	'enableReplaceState' => false,
	            ]) ?>

	            	<div class="col-sm-6 visible-xs">
	                    <ul class="advantages know_from_list">
	                    	<?php if(!empty($know_from_default)) {
	                    		foreach ($know_from_default as $key => $value) : ?>
	                        		<li>
	                        			<div><?= $value ?></div>
		            					<?= Html::a( '', Url::toRoute(['setting/delete-know-from', 'id' => $key], true), ['class' => 'select2-search-choice-close', 'tabindex' => -1, 'data-method' => 'post', 'data-pjax' => 'true'] ) ?>

	                        		</li>
	                    		<?php 
	                    		endforeach;
	                		} else echo '
	                			<li>
	                				<div>No options yet</div>
	                			</li>'; ?>
	                    </ul>
	                </div>

	                <?php $form = ActiveForm::begin([
		            		'options' => [
		            			'class' => 'col-sm-6',
		            			'data-pjax' => true, 
		            		],
		            		'validateOnBlur' => false
		            	]); ?>    
		            	<div class="row">

			                <div class="col-sm-12">           	

			                    <?= $form->field($know_from_model, 'name')->textInput(); ?>

			            		<?= $form->field($know_from_model, 'id_role', ['template' => '{input}'])->hiddenInput(['value' => array_keys($roles)[0]])->label(false) ?>

			                </div>
		                    <div class="col-md-2 col-sm-4 pull-right-sm">

		                        <div class="action-buttons">
		                            <?= Html::submitButton('Add', ['class' => 'btn btn-block btn-secondary']) ?>
		                            
		                        </div>

		                    </div>
		                </div>

	            	<?php ActiveForm::end(); ?>

	            	<div class="col-sm-6 hidden-xs">
	                    <ul class="advantages know_from_list">
	                    	<?php if(!empty($know_from_default)) {
	                    		foreach ($know_from_default as $key => $value) : ?>
	                        		<li>
	                        			<div><?= $value ?></div>
		            					<?= Html::a( '', Url::toRoute(['setting/delete-know-from', 'id' => $key], true), ['class' => 'select2-search-choice-close', 'tabindex' => -1, 'data-method' => 'post', 'data-pjax' => 'true'] ) ?>

	                        		</li>
	                    		<?php 
	                    		endforeach;
	                		} else echo '
	                			<li>
	                				<div>No options yet</div>
	                			</li>'; ?>
	                    </ul>
	                </div>
	                <div class="clearfix"></div>
	            <?php Pjax::end() ?>
            </div>

        </div>

        <div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title col-md-12">
					Agent Profile
	            	<p class="description">Aside Sections</p>
	            </h3>
            </div>
			<div class="panel-body">

				<div class="col-sm-6 form-group">
        			<?= Html::label('Role', '', ['class' => 'control-label']) ?>
        			<?= Html::dropDownList('role', '', $roles, [
        				'onchange' => 'setRole(event, "'.Html::getInputId($section_model, 'id_role').'", "uk-nestable", "section")', 'class' => 'select2-offscreen'
        			]) ?>
        		</div>

				<?php Pjax::begin([
	            	'enablePushState' => false,
	            	'enableReplaceState' => false,
	            	'options' => [
	            		'id' => 'colorpicker_form'
	            	]
	            	
	            ]) ?>
	            	<div class="col-md-12 visible-xs">
	            		<ul class="uk-nestable grouped-nestables" data-uk-nestable="{group:'sections', maxDepth:1}">
		            		<?php foreach ($section_list as $key => $section) : ?>
		            			<li>
									<div class="uk-nestable-item">
									    <div class="uk-nestable-handle" style="background-color: <?= isset($section['color']) ? $section['color'] : ''; ?>"></div>
									    <div data-nestable-action="toggle"></div>

							    		<div class="list-label"><?= $section['name'] ?></div>
							    		
									</div>
									<?= Html::a( '', Url::toRoute(['setting/delete-section', 'id' => $key], true), ['class' => 'select2-search-choice-close', 'tabindex' => -1, 'data-method' => 'post', 'data-pjax' => 'true'] ) ?>
								</li>
		            		<?php endforeach; ?>							
						</ul>
	            	</div>

	            	<?php $form = ActiveForm::begin([
		            		'options' => [
		            			'class' => 'col-sm-6',
		            			'data-pjax' => true, 
		            		],
		            		'validateOnBlur' => false
		            	]) ?>    

		            	<div class="hidden">
		            		<?= $form->field($section_model, 'id_role', ['template' => '{input}'])->hiddenInput(['value' => array_keys($roles)[0]])->label(false) ?>	
		    			</div>

		    			<div class="row">
		    				<div class="col-md-12">
		    					<?= $form->field($section_model, 'name')->textInput() ?>
		    				</div>

		    				<div class="col-md-12">
			    				<?= $form->field($section_model, 'color', ['template' => '
			    					{label}
			    					<div class="input-group">
			    						{input}{hint}{error}
			    						<div class="input-group-addon">
											<i class="color-preview"></i>
										</div>
			    					</div>
			    				'])->textInput(['class' => 'form-control colorpicker', 'data-format' => 'hex']) ?>	
			    			</div>

		    				<div class="col-md-2 col-sm-4 pull-right-sm">
		                        <div class="action-buttons">
		                            <?= Html::submitButton('Add', ['class' => 'btn btn-block btn-secondary']) ?>                           
		                        </div>

		                    </div>
						</div>
					<?php ActiveForm::end() ?>

					<div class="col-sm-6 hidden-xs">
	            		<ul class="uk-nestable grouped-nestables" data-uk-nestable="{group:'sections', maxDepth:1}">
		            		<?php foreach ($section_list as $key => $section) : ?>
		            			<li class="uk-nestable-list-item">
									<div class="uk-nestable-item">
									    <div class="uk-nestable-handle" style="background-color: <?= isset($section['color']) ? $section['color'] : ''; ?>"></div>
									    <div data-nestable-action="toggle"></div>

							    		<div class="list-label"><?= $section['name'] ?></div>
							    		
									</div>
									<?= Html::a( '', Url::toRoute(['setting/delete-section', 'id' => $key], true), ['class' => 'select2-search-choice-close', 'tabindex' => -1, 'data-method' => 'post', 'data-pjax' => 'true'] ) ?>
								</li>
		            		<?php endforeach; ?>							
						</ul>
	            	</div>
	            	<div class="clearfix"></div>
				<?php Pjax::end() ?>	
			</div>
		</div>

		<div class="panel panel-default">
			<div class="panel-heading">
                <h3 class="panel-title col-md-12">
                	Propety Settings
                	<p class="description">Property types</p>
                </h3>
            </div>

            <div class="panel-body">
        		
	            <?php Pjax::begin([
	            	'enablePushState' => false,
	            	'enableReplaceState' => false,
	            ]) ?>

	            	<div class="col-sm-6">
	                    <ul class="advantages property_list">
	                    	<?php if(!empty($property_type_list)) {
	                    		foreach ($property_type_list as $key => $value) : ?>
	                        		<li>
	                        			<div><?= $value ?></div>
		            					<?= Html::a( '', Url::toRoute(['setting/delete-property-type', 'id' => $key], true), ['class' => 'select2-search-choice-close', 'tabindex' => -1, 'data-method' => 'post', 'data-pjax' => 'true'] ) ?>
	                        		</li>
	                    		<?php 
	                    		endforeach;
	                		} else echo '
	                			<li>
	                				<div>No options yet</div>
	                			</li>'; ?>
	                    </ul>
	                </div>

	            	<?php $form = ActiveForm::begin([
		            		'options' => [
		            			'class' => 'col-sm-6',
		            			'data-pjax' => true, 
		            		],
		            		'validateOnBlur' => false
		            	]); ?>    
		            	<div class="row">

			                <div class="col-sm-12">       	

			            		<?= $form->field($property_type_model, 'name', ['template' => '{input}'])->textInput() ?>

			                </div>
		                    <div class="col-md-2 col-sm-4 pull-right-sm">

		                        <div class="action-buttons">
		                            <?= Html::submitButton('Add', ['class' => 'btn btn-block btn-secondary']) ?>
		                            
		                        </div>

		                    </div>
		                </div>

	            	<?php ActiveForm::end(); ?>	            	
	            	<div class="clearfix"></div>
	            <?php Pjax::end() ?>
            </div>

            <div class="panel-heading">
                <h3 class="panel-title col-md-12">
                	<br><p class="description">Property status</p>
                </h3>
            </div>

            <div class="panel-body">
        		
	            <?php Pjax::begin([
	            	'enablePushState' => false,
	            	'enableReplaceState' => false,
	            ]) ?>

	            	<div class="col-sm-6">
	                    <ul class="advantages property_list">
	                    	<?php if(!empty($property_status_list)) {
	                    		foreach ($property_status_list as $key => $value) : ?>
	                        		<li>
	                        			<div><?= $value ?></div>
		            					<?= Html::a( '', Url::toRoute(['setting/delete-property-status', 'id' => $key], true), ['class' => 'select2-search-choice-close', 'tabindex' => -1, 'data-method' => 'post', 'data-pjax' => 'true'] ) ?>

	                        		</li>
	                    		<?php 
	                    		endforeach;
	                		} else echo '
	                			<li>
	                				<div>No options yet</div>
	                			</li>'; ?>
	                    </ul>
	                </div>

	            	<?php $form = ActiveForm::begin([
		            		'options' => [
		            			'class' => 'col-sm-6',
		            			'data-pjax' => true, 
		            		],
		            		'validateOnBlur' => false
		            	]); ?>    
		            	<div class="row">

			                <div class="col-sm-12">       	

			            		<?= $form->field($property_status_model, 'name', ['template' => '{input}'])->textInput() ?>

			                </div>
		                    <div class="col-md-2 col-sm-4 pull-right-sm">

		                        <div class="action-buttons">
		                            <?= Html::submitButton('Add', ['class' => 'btn btn-block btn-secondary']) ?>
		                            
		                        </div>

		                    </div>
		                </div>

	            	<?php ActiveForm::end(); ?>	            	
	            	<div class="clearfix"></div>
	            <?php Pjax::end() ?>
            </div>

            <div class="panel-heading">
                <h3 class="panel-title col-md-12">
                	<br><p class="description">Property owner types</p>
                </h3>
            </div>

            <div class="panel-body">
        		
	            <?php Pjax::begin([
	            	'enablePushState' => false,
	            	'enableReplaceState' => false,
	            	'scrollTo' => false
	            ]) ?>

	            	<div class="col-sm-6">
	                    <ul class="advantages property_list">
	                    	<?php if(!empty($property_owner_type_list)) {
	                    		foreach ($property_owner_type_list as $key => $value) : ?>
	                        		<li>
	                        			<div><?= $value ?></div>
		            					<?= Html::a( '', Url::toRoute(['setting/delete-property-owner-type', 'id' => $key], true), ['class' => 'select2-search-choice-close', 'tabindex' => -1, 'data-method' => 'post', 'data-pjax' => 'true'] ) ?>

	                        		</li>
	                    		<?php 
	                    		endforeach;
	                		} else echo '
	                			<li>
	                				<div>No options yet</div>
	                			</li>'; ?>
	                    </ul>
	                </div>

	            	<?php $form = ActiveForm::begin([
		            		'options' => [
		            			'class' => 'col-sm-6',
		            			'data-pjax' => true, 
		            		],
		            		'validateOnBlur' => false
		            	]); ?>    
		            	<div class="row">

			                <div class="col-sm-12">       	

			            		<?= $form->field($property_owner_type_model, 'name', ['template' => '{input}'])->textInput() ?>

			                </div>
		                    <div class="col-md-2 col-sm-4 pull-right-sm">

		                        <div class="action-buttons">
		                            <?= Html::submitButton('Add', ['class' => 'btn btn-block btn-secondary']) ?>
		                            
		                        </div>

		                    </div>
		                </div>

	            	<?php ActiveForm::end(); ?>	            	
	            	<div class="clearfix"></div>
	            <?php Pjax::end() ?>
            </div>

            <div class="panel-heading">
                <h3 class="panel-title col-md-12">
                	<br><p class="description">Property owner types</p>
                </h3>
            </div>

            <div class="panel-body">
        		
	            <?php Pjax::begin([
	            	'enablePushState' => false,
	            	'enableReplaceState' => false,
	            	'scrollTo' => false
	            ]) ?>

	            	<div class="col-sm-6">
	                    <ul class="advantages property_list">
	                    	<?php if(!empty($transaction_type_list)) {
	                    		foreach ($transaction_type_list as $key => $value) : ?>
	                        		<li>
	                        			<div><?= $value ?></div>
		            					<?= Html::a( '', Url::toRoute(['setting/delete-transaction-type', 'id' => $key], true), ['class' => 'select2-search-choice-close', 'tabindex' => -1, 'data-method' => 'post', 'data-pjax' => 'true'] ) ?>

	                        		</li>
	                    		<?php 
	                    		endforeach;
	                		} else echo '
	                			<li>
	                				<div>No options yet</div>
	                			</li>'; ?>
	                    </ul>
	                </div>

	            	<?php $form = ActiveForm::begin([
		            		'options' => [
		            			'class' => 'col-sm-6',
		            			'data-pjax' => true, 
		            		],
		            		'validateOnBlur' => false
		            	]); ?>    
		            	<div class="row">

			                <div class="col-sm-12">       	

			            		<?= $form->field($transaction_type_model, 'name', ['template' => '{input}'])->textInput() ?>

			                </div>
		                    <div class="col-md-2 col-sm-4 pull-right-sm">

		                        <div class="action-buttons">
		                            <?= Html::submitButton('Add', ['class' => 'btn btn-block btn-secondary']) ?>
		                            
		                        </div>

		                    </div>
		                </div>

	            	<?php ActiveForm::end(); ?>	            	
	            	<div class="clearfix"></div>
	            <?php Pjax::end() ?>
            </div>

		</div>

    </div>
</div>
