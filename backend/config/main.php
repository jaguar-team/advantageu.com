<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);
# CONSTRUCT REQUEST COMPONENTS
switch ($_SERVER['SERVER_NAME']) {
    case 'admin.advantageu.jaguar-team.com':
        $baseUrl = '';
        break;
    default:
        $baseUrl = '/admin';
        break;
}
$request = [
    'baseUrl'   => $baseUrl,
    'csrfParam' => '_csrf-backend',
];
# CONSTRUCT URL MANAGER FRONT END COMPONENTS
switch ($_SERVER['SERVER_NAME']) {
    case 'admin.advantageu.jaguar-team.com':
        $baseUrlForFromManager = 'http://advantageu.jaguar-team.com';
        break;
    default:
        $baseUrlForFromManager = '';
        break;
}
$urlFrontEndManager = [
    'class'             => 'yii\web\urlManager',
    'baseUrl'           => $baseUrlForFromManager,
    'enablePrettyUrl'   => true,
    'showScriptName'    => false,
    'suffix'            => '',
];

return [
    'id' => 'app-backend',
    'homeUrl' => $baseUrl,
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'defaultRoute' => 'home',
    'bootstrap' => ['log', 'jsUrlManager'],
    'modules' => [],
    'components' => [
        'request' => $request,
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-backend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the backend
            'name' => 'advanced-backend',
        ],
        'formatter' => [
            'class' => 'yii\i18n\Formatter',
            'dateFormat' => 'php:d.m.Y',
            'datetimeFormat' => 'php:d.m.Y H:i:s',
            'timeFormat' => 'php:H:i:s',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'suffix' => '',
            'rules' => [
                // Home controller
                'login'     => 'home/login',
                'logout'    => 'home/logout',
                // Administrator controller
                'user'                          => 'administrator/index',
                'user/create'                   => 'administrator/create',
                'user/update/<id:\d+>'          => 'administrator/update',
                'user/delete/<id:\d+>'          => 'administrator/delete',
                'user/confirm/<id:\d+>'         => 'administrator/confirm',
            ],
        ],
        'urlFrontEndManager' => $urlFrontEndManager,
    ],
    'params' => $params,
];
