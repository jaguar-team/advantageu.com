<?php

namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\Response;
use common\models\User;
use common\models\search\UserSearch;
use common\models\UserRole;
use common\models\UploadFileForm;
use backend\models\RegistrationForm;

/**
 * AdministratorController implements the CRUD actions for User model.
 */
class AdministratorController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'create', 'update', 'delete', 'confirm', 'delete-multiple'],
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete'             => ['POST'],
                    'delete-multiple'    => ['POST'],
                    'confirm'            => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Administrators.
     * @return mixed
     */
    public function actionIndex()
    {
        $user_search = \Yii::createObject(['class' => UserSearch::className()], [['admin']]);
        $user_role_model = \Yii::createObject(['class' => UserRole::className()]);

        $dataProvider = $user_search->search();
        
        return $this->render('index', [
            'dataProvider'      => $dataProvider,
            'user_role_model'   => $user_role_model
        ]);
    }

    /**
     * Create administrator
     * If creation is successful, the browser will be redirected to the 'index' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $register_form = \Yii::createObject(['class' => RegistrationForm::className()]);
        $upload_file_form = \Yii::createObject(['class' => UploadFileForm::className()]);

        if ($register_form->load(\Yii::$app->request->post()) &&
            $register_form->_profile->load(\Yii::$app->request->post()) &&
            $register_form->validate() &&
            $register_form->_profile->validate()) {

            /** registration */
            if ($register_form->register(true)) {
                \Yii::$app->session->setFlash('toast-success', 'User has been created.');
            } else {
                \Yii::$app->session->setFlash('toast-success', 'An error was occurred trying to create user.');
            }

            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'upload_avatar_form'    => $upload_file_form,
                'register_form'         => $register_form,
                //'user_role_model'       => $register_form->_role,
                //'user_profile_model'    => $register_form->_profile,
            ]);
        }
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $user_model = User::findOne($id);

        if (!$user_model) {
            \Yii::$app->session->setFlash('toast-warning', 'User with id '.$id.' does not exists.');
            $this->redirect(['index']);
        }

        $user_profile_model = $user_model->profile;
        $upload_avatar_form = \Yii::createObject(['class' => UploadFileForm::className()],[$user_profile_model]);

        /** update user profile */
        if ($user_profile_model->load(\Yii::$app->request->post())) {
            if ($user_profile_model->save()) {
                \Yii::$app->session->setFlash('toast-success', 'User with id '.$id.' has been updated.');
                return $this->redirect(['index']);
            }
        }
        /** update user avatar */
        if ($upload_avatar_form->load(\Yii::$app->request->post()) && $upload_avatar_form->validate()) {
            if ($upload_avatar_form->upload()) {
                \Yii::$app->session->setFlash('toast-success', 'User\'s photo has been updated.');
                $this->refresh();
            } else {
                \Yii::$app->session->setFlash('toast-error', 'An error was occurred trying to update user\' photo.');
            }
        }

        return $this->render('update', [
            'user_model'            => $user_model,
            'user_profile_model'    => $user_profile_model,
            'upload_avatar_form'    => $upload_avatar_form,
        ]);
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $user_model = User::findOne($id);

        if (!$user_model) {
            \Yii::$app->session->setFlash('toast-warning', 'User with id '.$id.' does not exists');
        } else {
            if ($user_model->delete()) {
                \Yii::$app->session->setFlash('toast-success', 'User with id '.$id.' has been deleted.');
            } else {
                \Yii::$app->session->setFlash('toast-error', 'An error has occurred trying to delete user.');
            }
        }

        return $this->redirect(['index']);
    }

    /**
     * @param \yii\base\Action $action
     * @return bool|\yii\web\Response
     * @throws \yii\web\BadRequestHttpException
     */
    public function beforeAction($action)
    {
        if (\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        return parent::beforeAction($action);
    }
}