<?php

namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use common\models\User;
use common\models\search\UserSearch;
use common\models\Area;
use common\models\Review;
use common\models\UploadFileForm;
use common\models\Language;
use common\models\UserAddress;
use common\models\UserProfile;

/**
 * AdministratorController implements the CRUD actions for User model.
 */
class AgentController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => [
                            'index', 'create', 'update', 'delete', 'confirm', 'delete-multiple', 'confirm-multiple', 'delete-review', 'delete-area',
                            'delete-address'
                        ],
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'index'              => ['GET'],
                    'create'             => ['GET', 'POST'],
                    'update'             => ['GET', 'POST', 'UPDATE'],
                    'delete'             => ['POST', 'DELETE'],
                    'delete-review'      => ['POST', 'DELETE'],
                    'delete-area'        => ['POST'],
                    'delete-address'     => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Administrators.
     * @return mixed
     */
    public function actionIndex()
    {
        $user_search = \Yii::createObject(['class' => UserSearch::className()], [['agent']]);

        $dataProvider = $user_search->search();

        return $this->render('index', [
            'dataProvider'      => $dataProvider,
            'user_search'       => $user_search
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $user_model = \Yii::createObject(['class' => User::className()]);
        $user_profile_model = \Yii::createObject(['class' => UserProfile::className(), 'scenario' => UserProfile::SCENARIO_CREATE_ACCOUNT]);
        $upload_file_form = \Yii::createObject(['class' => UploadFileForm::className(), 'scenario' => UploadFileForm::SCENARIO_USER_AVATAR]);

        if ($user_model->load(\Yii::$app->request->post()) && $user_model->validate() &&
            $user_profile_model->load(\Yii::$app->request->post()) && $user_profile_model->validate()) {

            /** upload avatar */
            if ($upload_file_form->load(\Yii::$app->request->post()) && $upload_file_form->validate()) {
                $user_profile_model->photo = $upload_file_form->upload();
            }
            /** registration */
            $user_model->populateRelation('profile', $user_profile_model);
            if ($user_model->save()) {
                \Yii::$app->session->setFlash('toast-success', 'Agent has been created.');
                return $this->redirect(['update', 'id' => $user_model->id]);
            } else {
                \Yii::$app->session->setFlash('toast-success', 'An error was occurred trying to create agent.');
            }
        }

        return $this->render('create', [
            'upload_avatar_form'    => $upload_file_form,
            'user_model'            => $user_model,
            'user_profile_model'    => $user_profile_model,
        ]);
    }

    /**
     * If update is successful, the browser will be redirected to the 'index' page.
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     * @throws \yii\base\InvalidConfigException
     */
    public function actionUpdate($id)
    {
        $user_model = User::findOne($id);

        if (!$user_model) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        $user_profile_model = $user_model->profile;
        $area_model = \Yii::createObject(['class' => Area::className(), 'scenario' => Area::SCENARIO_SKIP_ID_PROFILE]);
        $user_address_model = \Yii::createObject(['class' => UserAddress::className()]);
        $language_model = \Yii::createObject(['class' => Language::className(), 'scenario' => Language::SCENARIO_SKIP_ID_PROFILE]);
        //$review_model = \Yii::createObject(['class' => Review::className(), 'scenario' => Review::SCENARIO_SKIP_ID_USER]);
        $upload_file_form = \Yii::createObject(['class' => UploadFileForm::className(), 'scenario' => UploadFileForm::SCENARIO_USER_AVATAR]);

        /** update user */
        if ($user_model->load(\Yii::$app->request->post()) && $user_model->validate()) {
            if ($user_model->save()) {
                \Yii::$app->session->setFlash('toast-success', 'User with id '.$id.' has been updated.');
            }
        }

        /** update user profile */
        if ($user_profile_model->load(\Yii::$app->request->post()) && $user_profile_model->validate()) {
            if ($user_profile_model->save()) {
                \Yii::$app->session->setFlash('toast-success', 'User with id '.$id.' has been updated.');
            }
        }

        /** upload avatar */
        if ($upload_file_form->load(\Yii::$app->request->post()) && $upload_file_form->validate()) {
            $user_profile_model->photo = $upload_file_form->upload();
            if ($user_profile_model->save()) {
                \Yii::$app->session->setFlash('toast-success', 'User avatar has been updated.');
            }
        }

        /** save user address */
        if ($user_address_model->load(\Yii::$app->request->post()) && $user_address_model->validate()) {
            $user_model->link('addresses', $user_address_model);
            \Yii::$app->session->setFlash('toast-success', 'Addresses has been added .');
        }

        /** add new area */
        if ($area_model->load(\Yii::$app->request->post()) && $area_model->validate()) {
            $user_profile_model->link('areas', $area_model);
            \Yii::$app->session->setFlash('toast-success', 'Area has been added.');
        }

        /** add language */
        if ($language_model->load(\Yii::$app->request->post()) && $language_model->validate()) {
            $user_profile_model->link('languages', $language_model);
            \Yii::$app->session->setFlash('toast-success', 'Language has been added.');
        }

        /** add new review */
        /**if ($review_model->load(\Yii::$app->request->post()) && $review_model->validate()) {
            $user_profile_model->link('reviews', $review_model);
            \Yii::$app->session->setFlash('toast-success', 'Review has been added.');
        }**/

        return $this->render('update', [
            'user_model'            => $user_model,
            'user_profile_model'    => $user_profile_model,
            'area_model'            => $area_model,
            //'review_model'          => $review_model,
            'upload_avatar_form'    => $upload_file_form,
        ]);
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $user_model = User::findOne($id);

        if (!$user_model) {
            \Yii::$app->session->setFlash('toast-warning', 'User with id '.$id.' does not exists');
        } else {
            if ($user_model->delete()) {
                \Yii::$app->session->setFlash('toast-success', 'User with id '.$id.' has been deleted.');
            } else {
                \Yii::$app->session->setFlash('toast-error', 'An error has occurred trying to delete user.');
            }
        }

        return $this->redirect(['index']);
    }

    /**
     * Delete user address
     * @param $id
     * @return mixed
     */
    public function actionDeleteAddress($id)
    {
        $user_address_model = UserAddress::findOne($id);

        if (!$user_address_model) {
            \Yii::$app->session->setFlash('toast-warning', 'Address with id '.$id.' does not exists');
        } else {
            if ($user_address_model->delete()) {
                \Yii::$app->session->setFlash('toast-success', 'Address with id '.$id.' has been deleted.');
            } else {
                \Yii::$app->session->setFlash('toast-error', 'An error has occurred trying to delete address.');
            }
        }

        return $this->run('update', ['id' => $user_address_model->user->id]);
    }

    /**
     * Delete area
     * @param $id
     * @return mixed
     */
    public function actionDeleteArea($id)
    {
        $area_model = Area::findOne($id);
        $user_id = $area_model->userProfile->user->id;

        if (!$area_model) {
            \Yii::$app->session->setFlash('toast-warning', 'Area with id '.$id.' does not exists');
        } else {
            if ($area_model->delete()) {
                \Yii::$app->session->setFlash('toast-success', 'Area with id '.$id.' has been deleted.');
            } else {
                \Yii::$app->session->setFlash('toast-error', 'An error has occurred trying to delete area.');
            }
        }

        return $this->run('update', ['id' => $user_id]);
    }

    /**
     * Delete language
     * @param $id
     * @return mixed
     */
    public function actionDeleteLanguage($id)
    {
        $language_model = Language::findOne($id);
        $user_id = $language_model->userProfile->user->id;

        if (!$language_model) {
            \Yii::$app->session->setFlash('toast-warning', 'Language with id '.$id.' does not exists');
        } else {
            if ($language_model->delete()) {
                \Yii::$app->session->setFlash('toast-success', 'Language with id '.$id.' has been deleted.');
            } else {
                \Yii::$app->session->setFlash('toast-error', 'An error has occurred trying to delete language.');
            }
        }

        return $this->run('update', ['id' => $user_id]);
    }

    /**
     * Delete agent review
     * @param $id
     * @return mixed
     */
    public function actionDeleteReview($id)
    {
        $review_model = Review::findOne($id);

        if (!$review_model) {
            \Yii::$app->session->setFlash('toast-warning', 'Review with id '.$id.' does not exists');
        } else {
            if ($review_model->delete()) {
                \Yii::$app->session->setFlash('toast-success', 'Review with id '.$id.' has been deleted.');
            } else {
                \Yii::$app->session->setFlash('toast-error', 'An error has occurred trying to delete review.');
            }
        }

        return $this->run(['update']);
    }
}