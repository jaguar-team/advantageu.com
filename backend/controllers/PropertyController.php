<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 027 27.07.17
 * Time: 16:31
 */

namespace backend\controllers;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use common\models\Property;
use common\models\PropertyLocation;
use common\models\TransactionInfo;
use common\models\Transaction;
use common\models\search\PropertySearch;

class PropertyController extends Controller
{
    /**
     * List of Properties.
     * @return mixed
     */
    public function actionIndex()
    {
        $property_search = \Yii::createObject(['class' => PropertySearch::className()]);

        $dataProvider = $property_search->search();

        return $this->render('index', [
            'dataProvider'      => $dataProvider,
            'property_search'   => $property_search
        ]);
    }

    /**
     * Create property
     * @return string
     * @throws \yii\base\InvalidConfigException
     */
    public function actionCreate()
    {
        $transaction_model = \Yii::createObject(['class' => Transaction::className()]);
        $transaction_info_model = \Yii::createObject(['class' => TransactionInfo::className()]);
        $property_model = \Yii::createObject(['class' => Property::className()]);
        $property_location_model = \Yii::createObject(['class' => PropertyLocation::className()]);

        //load pre params
        $transaction_model->load(\Yii::$app->request->get());

        if ($transaction_model->load(\Yii::$app->request->post()) && $transaction_model->validate() &&
            $transaction_info_model->load(\Yii::$app->request->post()) && $transaction_info_model->validate() &&
            $property_model->load(\Yii::$app->request->post()) && $property_model->validate() &&
            $property_location_model->load(\Yii::$app->request->post()) && $property_location_model->validate()) {

            $transaction_model->populateRelation('transactionInfo', $transaction_info_model);
            $transaction_model->populateRelation('property', $property_model);
            $property_model->populateRelation('location', $property_location_model);

            /** save transaction */
            if ($transaction_model->save()) {
                $this->redirect('index');
            }

        }

        return $this->render('create', [
            'transaction_model'         => $transaction_model,
            'transaction_info_model'    => $transaction_info_model,
            'property_model'            => $property_model,
            'property_location_model'   => $property_location_model,
        ]);
    }

    /**
     * Update property
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        if ($transaction_model = Transaction::findOne($id)) {

            //transaction
            if ($transaction_model->load(\Yii::$app->request->post()) && $transaction_model->validate()) {
                if ($transaction_model->save()) {
                    \Yii::$app->session->setFlash('toast-success', 'Property has been updated.');
                }

            }
            //transaction info
            if ($transaction_model->transactionInfo->load(\Yii::$app->request->post()) &&
                $transaction_model->transactionInfo->validate()) {
                if ($transaction_model->transactionInfo->save()) {
                    \Yii::$app->session->setFlash('toast-success', 'Property has been updated.');
                }
            }
            //property
            if ($transaction_model->property->load(\Yii::$app->request->post()) &&
                $transaction_model->property->validate()) {
                if ($transaction_model->property->save()) {
                    \Yii::$app->session->setFlash('toast-success', 'Property has been updated.');
                }
            }
            //property location
            if ($transaction_model->property->location->load(\Yii::$app->request->post()) &&
                $transaction_model->property->location->validate()) {
                if ($transaction_model->property->location->save()) {
                    \Yii::$app->session->setFlash('toast-success', 'Property has been updated.');
                }
            }

            return $this->render('update', [
                'transaction_model' => $transaction_model,
            ]);
        } else {
            throw new NotFoundHttpException('Property is not found.');
        }
    }
}