<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 002 02.08.17
 * Time: 12:45
 */

namespace backend\controllers;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use common\models\Review;
use common\models\search\ReviewSearch;

class ReviewController extends Controller
{
    /**
     * List of reviews.
     * @return mixed
     */
    public function actionIndex()
    {
        $review_search = \Yii::createObject(['class' => ReviewSearch::className()]);

        $dataProvider = $review_search->search();

        return $this->render('index', [
            'dataProvider'      => $dataProvider,
            'review_search'     => $review_search
        ]);
    }

    /**
     * Create review
     * @return string
     * @throws \yii\base\InvalidConfigException
     */
    public function actionCreate()
    {
        $review_model = \Yii::createObject(['class' => Review::className()]);

        //load pre params
        $review_model->load(\Yii::$app->request->get());

        if ($review_model->load(\Yii::$app->request->post()) && $review_model->validate()) {
            if ($review_model->save()) {
                \Yii::$app->session->setFlash('toast-success', 'Review has been created.');
                $this->redirect('index');
            }
        }

        return $this->render('create', [
            'review_model'  => $review_model
        ]);
    }

    /**
     * Update review
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        if ($review_model = Review::findOne($id)) {

            if ($review_model->load(\Yii::$app->request->post()) && $review_model->validate()) {
                if ($review_model->save()) {
                    \Yii::$app->session->setFlash('toast-success', 'Review has been updated.');
                    $this->redirect('index');
                }
            }

            return $this->render('update', [
                'review_model'  => $review_model
            ]);
        } else {
            throw new NotFoundHttpException('Property is not found.');
        }
    }
}