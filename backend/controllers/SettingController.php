<?php
/**
 * Created by PhpStorm.
 * User: Vlad
 * Date: 18.04.2017
 * Time: 14:21
 */

namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use common\models\UploadFileForm;
use common\models\UserRole;
use common\models\Advantage;
use common\models\KnowFrom;
use common\models\Section;
use common\models\TransactionType;
use common\models\PropertyType;
use common\models\PropertyOwnerType;
use common\models\PropertyStatus;
use backend\models\GeneralSettingForm;

class SettingController extends Controller
{
    /** @var string  */
    public $defaultAction = 'general';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'general'                       => ['GET', 'POST'],
                    'custom-fields'                 => ['GET', 'POST'],
                    'delete-advantage'              => ['POST'],
                    'delete-know-from'              => ['POST'],
                    'delete-section'                => ['POST'],
                    'delete-transaction-type'       => ['POST'],
                    'delete-property-owner-type'    => ['POST'],
                    'delete-property-type'          => ['POST'],
                    'get-section-by-id-role'        => ['POST'],
                    'get-know-from-by-id-role'      => ['POST'],
                    'get-advantages-by-id-role'     => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Default route
     * General setting
     * @return string
     */
    public function actionGeneral()
    {
        $general_setting_form = \Yii::createObject(['class' => GeneralSettingForm::className()]);
        $upload_file_form = \Yii::createObject(['class' => UploadFileForm::className(), 'scenario' => UploadFileForm::SCENARIO_IMG]);

        if ($general_setting_form->load(Yii::$app->request->post()) && $general_setting_form->validate()) {

            // upload site logo
            if ($upload_file_form->load(\Yii::$app->request->post()) && $upload_file_form->validate()) {

                $general_setting_form->site_logo = $upload_file_form->upload();
            }

            if ($general_setting_form->save()) {
                \Yii::$app->session->setFlash('success', 'Setting successfully updated.');
            } else {
                \Yii::$app->session->setFlash('error', 'An error was occurred trying to update setting.');
            }
        }

        return $this->render('general', [
            'general_setting_form' => $general_setting_form,
            'upload_site_logo' => $upload_file_form,
        ]);
    }

    /**
     * Custom fields setting
     * Review: Advantage, Know from
     * @return string
     */
    public function actionCustomFields()
    {
        $user_role_model = \Yii::createObject(['class' => UserRole::className()]);
        $advantage_model = \Yii::createObject(['class' => Advantage::className()]);
        $know_from_model = \Yii::createObject(['class' => KnowFrom::className()]);
        $section_model = \Yii::createObject(['class' => Section::className()]);
        $transaction_type_model = \Yii::createObject(['class' => TransactionType::className()]);
        $property_type_model = \Yii::createObject(['class' => PropertyType::className()]);
        $property_owner_type_model = \Yii::createObject(['class' => PropertyOwnerType::className()]);
        $property_status_model = \Yii::createObject(['class' => PropertyStatus::className()]);

        /** Save advantage */
        if ($advantage_model->load(Yii::$app->request->post()) && $advantage_model->validate()) {
            if ($advantage_model->save()) {
                \Yii::$app->session->setFlash('success', 'Setting successfully updated.');
            } else {
                \Yii::$app->session->setFlash('error', 'An error was occurred trying to update setting.');
            }
        }

        /** Save know from */
        if ($know_from_model->load(Yii::$app->request->post()) && $know_from_model->validate()) {
            if ($know_from_model->save()) {
                \Yii::$app->session->setFlash('success', 'Setting successfully updated.');
            } else {
                \Yii::$app->session->setFlash('error', 'An error was occurred trying to update setting.');
            }
        }

        /** Save section */
        if ($section_model->load(Yii::$app->request->post()) && $section_model->validate()) {
            if ($section_model->save()) {
                \Yii::$app->session->setFlash('success', 'Setting successfully updated.');
            } else {
                \Yii::$app->session->setFlash('error', 'An error was occurred trying to update setting.');
            }
        }

        /** Save transaction type */
        if ($transaction_type_model->load(\Yii::$app->request->post()) && $transaction_type_model->validate()) {
            if ($transaction_type_model->save()) {
                \Yii::$app->session->setFlash('success', 'Transaction type successfully updated.');
            } else {
                \Yii::$app->session->setFlash('error', 'An error was occurred trying to update setting.');
            }
        }

        /** Save property type */
        if ($property_type_model->load(\Yii::$app->request->post()) && $property_type_model->validate()) {
            if ($property_type_model->save()) {
                \Yii::$app->session->setFlash('success', 'Property type successfully updated.');
            } else {
                \Yii::$app->session->setFlash('error', 'An error was occurred trying to update setting.');
            }
        }

        /** Save property owner type */
        if ($property_owner_type_model->load(\Yii::$app->request->post()) && $property_owner_type_model->validate()) {
            if ($property_owner_type_model->save()) {
                \Yii::$app->session->setFlash('success', 'Property owner type successfully updated.');
            } else {
                \Yii::$app->session->setFlash('error', 'An error was occurred trying to update setting.');
            }
        }

        /** Save property status */
        if ($property_status_model->load(\Yii::$app->request->post()) && $property_status_model->validate()) {
            if ($property_status_model->save()) {
                \Yii::$app->session->setFlash('success', 'Property status successfully updated.');
            } else {
                \Yii::$app->session->setFlash('error', 'An error was occurred trying to update setting.');
            }
        }

        return $this->render('custom-fields', [
            'user_role_model'               => $user_role_model,
            'advantage_model'               => $advantage_model,
            'know_from_model'               => $know_from_model,
            'section_model'                 => $section_model,
            'transaction_type_model'        => $transaction_type_model,
            'property_type_model'           => $property_type_model,
            'property_status_model'         => $property_status_model,
            'property_owner_type_model'     => $property_owner_type_model,
        ]);
    }

    /**
     * Delete property owner type
     * @param $id
     * @return mixed
     */
    public function actionDeletePropertyOwnerType($id)
    {
        $property_owner_type_model = PropertyOwnerType::findOne($id);

        if (!$property_owner_type_model) {
            \Yii::$app->session->setFlash('toast-warning', 'Property owner type with id '.$id.' does not exists');
        } else {
            if ($property_owner_type_model->delete()) {
                \Yii::$app->session->setFlash('toast-success', 'Property owner type with id '.$id.' has been deleted.');
            } else {
                \Yii::$app->session->setFlash('toast-error', 'An error has occurred trying to delete property owner type.');
            }
        }

        return $this->run('custom-fields');
    }

    /**
     * Delete transaction type
     * @param $id
     * @return mixed
     */
    public function actionDeleteTransactionType($id)
    {
        $transaction_type_model = TransactionType::findOne($id);

        if (!$transaction_type_model) {
            \Yii::$app->session->setFlash('toast-warning', 'Transaction type with id '.$id.' does not exists');
        } else {
            if ($transaction_type_model->delete()) {
                \Yii::$app->session->setFlash('toast-success', 'Transaction type with id '.$id.' has been deleted.');
            } else {
                \Yii::$app->session->setFlash('toast-error', 'An error has occurred trying to delete transaction type.');
            }
        }

        return $this->run('custom-fields');
    }

    /**
     * Delete advantage
     * @param $id
     * @return mixed
     */
    public function actionDeleteAdvantage($id)
    {
        $advantage_model = Advantage::findOne($id);

        if (!$advantage_model) {
            \Yii::$app->session->setFlash('toast-warning', 'Advantage with id '.$id.' does not exists');
        } else {
            if ($advantage_model->delete()) {
                \Yii::$app->session->setFlash('toast-success', 'Advantage with id '.$id.' has been deleted.');
            } else {
                \Yii::$app->session->setFlash('toast-error', 'An error has occurred trying to delete advantage.');
            }
        }

        return $this->run('custom-fields');
    }

    /**
     * Delete know from
     * @param $id
     * @return mixed
     */
    public function actionDeleteKnowFrom($id)
    {
        $know_from_model = KnowFrom::findOne($id);

        if (!$know_from_model) {
            \Yii::$app->session->setFlash('toast-warning', 'Know from with id '.$id.' does not exists');
        } else {
            if ($know_from_model->delete()) {
                \Yii::$app->session->setFlash('toast-success', 'Know from with id '.$id.' has been deleted.');
            } else {
                \Yii::$app->session->setFlash('toast-error', 'An error has occurred trying to delete know from.');
            }
        }

        return $this->run('custom-fields');
    }

    /**
     * Delete section
     * @param $id
     * @return mixed
     */
    public function actionDeleteSection($id)
    {
        $section_model = Section::findOne($id);

        if (!$section_model) {
            \Yii::$app->session->setFlash('toast-warning', 'Section with id '.$id.' does not exists');
        } else {
            if ($section_model->delete()) {
                \Yii::$app->session->setFlash('toast-success', 'Section with id '.$id.' has been deleted.');
            } else {
                \Yii::$app->session->setFlash('toast-error', 'An error has occurred trying to delete section.');
            }
        }

        return $this->run('custom-fields');
    }

    /**
     * Delete property type
     * @param $id
     * @return mixed
     */
    public function actionDeletePropertyType($id)
    {
        $property_type_model = PropertyType::findOne($id);

        if (!$property_type_model) {
            \Yii::$app->session->setFlash('toast-warning', 'Property type with id '.$id.' does not exists');
        } else {
            if ($property_type_model->delete()) {
                \Yii::$app->session->setFlash('toast-success', 'Property type with id '.$id.' has been deleted.');
            } else {
                \Yii::$app->session->setFlash('toast-error', 'An error has occurred trying to delete property type.');
            }
        }

        return $this->run('custom-fields');
    }

    /**
     * Delete property status
     * @param $id
     * @return mixed
     */
    public function actionDeletePropertyStatus($id)
    {
        $property_status_model = PropertyStatus::findOne($id);

        if (!$property_status_model) {
            \Yii::$app->session->setFlash('toast-warning', 'Property status with id '.$id.' does not exists');
        } else {
            if ($property_status_model->delete()) {
                \Yii::$app->session->setFlash('toast-success', 'Property status with id '.$id.' has been deleted.');
            } else {
                \Yii::$app->session->setFlash('toast-error', 'An error has occurred trying to delete property status.');
            }
        }

        return $this->run('custom-fields');
    }

    /**
     * Get section list by id role
     * Ajax
     * @return array
     */
    public function actionGetSectionByIdRole()
    {
        $response = ['status' => false];

        if (\Yii::$app->request->isAjax) {
            $section_model = \Yii::createObject(['class' => Section::className()]);
            $id_role = \Yii::$app->request->post('id_role');

            $response['data'] = $section_model->getByIdRole($id_role);
            $response['status'] = true;
        }

        \Yii::$app->response->format = Response::FORMAT_JSON;

        return $response;
    }

    /**
     * Get know from list by id role
     * Ajax
     * @return array
     */
    public function actionGetKnowFromByIdRole()
    {
        $response = ['status' => false];

        if (\Yii::$app->request->isAjax) {
            $know_from_model = \Yii::createObject(['class' => KnowFrom::className()]);
            $id_role = \Yii::$app->request->post('id_role');

            $response['data'] = $know_from_model->getDropdownByUserRole($id_role);
            $response['status'] = true;
        }

        \Yii::$app->response->format = Response::FORMAT_JSON;

        return $response;
    }

    /**
     * Get advantages by id role
     * Ajax
     * @return array
     */
    public function actionGetAdvantagesByIdRole()
    {
        $response = ['status' => false];

        if (\Yii::$app->request->isAjax) {
            $advantage_model = \Yii::createObject(['class' => Advantage::className()]);
            $id_role = \Yii::$app->request->post('id_role');

            $response['data'] = $advantage_model->getDropdownByUserRole($id_role);
            $response['status'] = true;
        }

        \Yii::$app->response->format = Response::FORMAT_JSON;

        return $response;
    }

    /**
     * @param \yii\base\Action $action
     * @return bool
     */
    public function beforeAction($action)
    {
        if (\Yii::$app->user->isGuest) {
            $this->goHome();
            return false;
        }

        return true;
    }
}