<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 026 26.07.17
 * Time: 12:07
 */

namespace backend\controllers;

use yii\web\Response;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use backend\components\DisplayItems;
use common\models\User;
use common\models\Transaction;
use common\models\Review;

class AjaxController extends Controller
{
    public $enableCsrfValidation = false;

    /**
     *******************************************************************************************************************
     * ALL REQUESTS SHOULD BE ONLY AJAX ( NOT PJAX )
     * ALL REQUEST CAN BE SENT ONLY REGISTERED USER ( ONLY ADMIN )
     * ALL REQUESTS WILL HAVE FOLLOWING STRUCTURE:
     * {'status': false|true, 'errors': {'Error 1', 'Error 2'}, 'data': {'Data 1', 'Data 2'}}
     *
     * IF YOU SEND NOT EXISTING DATA ( EXAMPLE: user_id or etc. ), YOU WILL GET CORRESPONDING ERROR
     *******************************************************************************************************************
     */

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => [
                            //USER
                            'change-user-status', 'change-user-certified-status', 'change-user-role',
                            'certify-users', 'activate-users', 'delete-users', 'delete-user', 'change-user-top-status',
                            'get-users-by-matches',
                            //TRANSACTION
                            'change-transaction-status', 'change-transaction-feature-status', 'activate-transactions',
                            'make-transactions-as-featured', 'delete-transaction',
                            //REVIEW
                            'delete-review', 'change-review-status', 'delete-reviews', 'delete-transactions',
                            'activate-reviews',
                            //DISPLAY ITEMS
                            'change-sidebar-position', 'get-sidebar-position', 'set-edit-agent-tab',
                        ],
                        'roles' => ['@', '?'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    //USER
                    'get-users-by-matches'                  => ['GET'],
                    'change-user-role'                      => ['POST'],
                    'change-user-status'                    => ['POST'],
                    'change-user-certified-status'          => ['POST'],
                    'change-user-top-status'                => ['POST'],
                    'activate-users'                        => ['POST'],
                    'certify-users'                         => ['POST'],
                    'delete-user'                           => ['POST', 'DELETE'],
                    'delete-users'                          => ['POST', 'DELETE'],
                    //TRANSACTION
                    'delete-transaction'                    => ['POST', 'DELETE'],
                    'delete-transactions'                   => ['POST', 'DELETE'],
                    'change-transaction-status'             => ['POST'],
                    'change-transaction-feature-status'     => ['POST'],
                    'activate-transactions'                 => ['POST'],
                    'make-transactions-as-featured'         => ['POST'],
                    //REVIEW
                    'delete-review'                         => ['POST', 'DELETE'],
                    'delete-reviews'                        => ['POST', 'DELETE'],
                    'change-review-status'                  => ['POST'],
                    'activate-reviews'                      => ['POST'],
                    //DISPLAY ITEMS
                    'set-edit-agent-tab'                    => ['POST'],
                    'change-sidebar-position'               => ['GET'],
                    'get-sidebar-position'                  => ['GET'],
                ],
            ],
        ];
    }

    /**
     *******************************************************************************************************************
     * USER MODEL
     * USER PROFILE MODEL
     *******************************************************************************************************************
     */


    /**
     * Delete user
     * Method: POST
     * Params: $id - integer (id_user)
     * @return array
     * @throws NotFoundHttpException
     */
    public function actionDeleteUser()
    {
        $response = ['status' => false];
        $id_user = \Yii::$app->request->post('id');

        if ($id_user) {
            if ($user_model = User::findOne($id_user)) {
                if ($user_model->delete()) {
                    $response['status'] = true;
                }
            } else {
                throw new NotFoundHttpException('User with id ' . $id_user . ' is not found.');
            }
        } else {
            throw new NotFoundHttpException('Variable "id" is empty.');
        }

        return $response;
    }

    /**
     * @param $string
     * @param array $role
     * @param int $limit
     * @return array
     */
    public function actionGetUsersByMatches($string, array $role = [], $limit = 10)
    {
        $query = User::find()->byMatches($string)->limit($limit)->asArray();

        //add role
        foreach ($role as $key => $type) {
            if ($key == 0) {
                $query = $query->{$type}();
            } else {
                $query = $query->{$type}(true);
            }
        }

        return [
            'status' => true,
            'data' => $query->all(),
        ];
    }

    /**
     * Change user role
     * Method: POST
     * Params: $id - integer (id_user), $role - integer (id_role)
     * @return array
     * @throws NotFoundHttpException
     */
    public function actionChangeUserRole()
    {
        $response = ['status' => false];

        $id_user = \Yii::$app->request->post('id');

        if ($user_model = User::findOne($id_user)) {

            $user_model->id_role = \Yii::$app->request->post('role');

            if ($user_model->validate() && $user_model->save()) {
                $response['status'] = true;
                $response['data'] = $user_model->getAttributes();
            } else {
                $response['errors'] = $user_model->errors;
            }

        } else {
            throw new NotFoundHttpException('\'User with id ' . $id_user . ' is not found.');
        }

        return $response;
    }

    /**
     * Change user status
     * Method: POST
     * Params: $id - integer (id_user)
     * @return array
     * @throws NotFoundHttpException
     */
    public function actionChangeUserStatus()
    {
        $response = ['status' => false];

        if ($id_user = \Yii::$app->request->post('id')) {
            if ($user_model = User::findOne($id_user)) {
                if ($user_model->status == User::STATUS_NOT_ACTIVE) {
                    $user_model->status = User::STATUS_ACTIVE;
                } else {
                    $user_model->status = User::STATUS_NOT_ACTIVE;
                }

                if ($user_model->save()) {
                    $response['data']['id_user'] = $user_model->id;
                    $response['data']['status'] = $user_model->status;
                    $response['status'] = true;
                }
            } else {
                throw new NotFoundHttpException('User with id ' . $id_user . ' is not found.');
            }
        } else {
            throw new NotFoundHttpException('Variable "id_user" is empty.');
        }

        return $response;
    }

    /**
     * Change user certified status
     * Method: POST
     * Params: $id - integer (id_user)
     * @return array
     * @throws NotFoundHttpException
     */
    public function actionChangeUserCertifiedStatus()
    {
        $response = ['status' => false];

        if ($id_user = \Yii::$app->request->post('id')) {
            if ($user_model = User::findOne($id_user)) {
                if ($user_model->certificate == User::CERTIFICATE) {
                    $user_model->certificate = User::NOT_CERTIFICATE;
                } else {
                    $user_model->certificate = User::CERTIFICATE;
                }

                if ($user_model->save()) {
                    $response['data']['id_user'] = $user_model->id;
                    $response['data']['certificate'] = $user_model->certificate;
                    $response['status'] = true;
                }
            } else {
                throw new NotFoundHttpException('User with id ' . $id_user . ' is not found.');
            }
        } else {
            throw new NotFoundHttpException('Variable "id_user" is empty.');
        }

        return $response;
    }

    /**
     * Change user top status
     * Method: POST
     * Params: $id - integer (id_user)
     * @return array
     * @throws NotFoundHttpException
     */
    public function actionChangeUserTopStatus()
    {
        $response = ['status' => false];

        if ($id_user = \Yii::$app->request->post('id')) {
            if ($user_model = User::findOne($id_user)) {
                if ($user_model->top == User::TOP) {
                    $user_model->top = User::NOT_TOP;
                } else {
                    $user_model->top = User::TOP;
                }

                if ($user_model->save()) {
                    $response['data'] = $user_model->getAttributes();;
                    $response['status'] = true;
                }
            } else {
                throw new NotFoundHttpException('User with id ' . $id_user . ' is not found.');
            }
        } else {
            throw new NotFoundHttpException('Variable "id" is empty.');
        }

        return $response;
    }

    /**
     * Delete users
     * Method: POST
     * Params: $ids - array (list of user ids)
     * @return array
     * @throws NotFoundHttpException
     */
    public function actionDeleteUsers()
    {
        $response = ['status' => false];

        $ids = \Yii::$app->request->post('ids');

        if ($ids) {
            $response['rows'] = User::deleteAll(['id' => $ids]);
            $response = ['status' => true];
        }

        return $response;
    }

    /**
     * Activate users
     * Method: POST
     * Params: $ids - array (list of user ids)
     * @return array
     * @throws NotFoundHttpException
     */
    public function actionActivateUsers()
    {
        $response = ['status' => false];

        $ids = \Yii::$app->request->post('ids');

        if ($ids) {
            $response['rows'] = User::updateAll(['status' => User::STATUS_ACTIVE], ['id' => $ids]);
            $response['status'] = true;
        } else {
            throw new NotFoundHttpException('Variable "user_ids" is empty.');
        }

        return $response;
    }

    /**
     * Certify users
     * Method: POST
     * Params: $ids - array (list of user ids)
     * @return array
     * @throws NotFoundHttpException
     */
    public function actionCertifyUsers()
    {
        $response = ['status' => false];

        $ids = \Yii::$app->request->post('ids');

        if ($ids) {
            $response['rows'] = User::updateAll(['certificate' => User::CERTIFICATE], ['id' => $ids]);
            $response['status'] = true;
        } else {
            throw new NotFoundHttpException('Variable "ids" is empty.');
        }

        return $response;
    }

    /**
     *******************************************************************************************************************
     * TRANSACTION MODEL
     * TRANSACTION INFO MODEL
     * PROPERTY MODEL
     * PROPERTY LOCATION MODEL
     * PROPERTY TYPE MODEL
     *******************************************************************************************************************
     */

    /**
     * Delete transaction
     * Method: POST
     * Params: $id - integer (id_transaction)
     * @return array
     * @throws NotFoundHttpException
     */
    public function actionDeleteTransaction()
    {
        $response = ['status' => false];
        $id_transaction = \Yii::$app->request->post('id');

        if ($id_transaction) {
            if ($transaction_model = Transaction::findOne($id_transaction)) {
                if ($transaction_model->delete()) {
                    $response['status'] = true;
                }
            } else {
                throw new NotFoundHttpException('Transaction with id ' . $id_transaction . ' is not found.');
            }
        } else {
            throw new NotFoundHttpException('Variable "id" is empty.');
        }

        return $response;
    }

    /**
     * Change transaction status
     * Method: POST
     * Params: $id - integer (id_user)
     * @return array
     * @throws NotFoundHttpException
     */
    public function actionChangeTransactionStatus()
    {
        $response = ['status' => false];

        if ($id_user = \Yii::$app->request->post('id')) {
            if ($transaction_model = Transaction::findOne($id_user)) {
                if ($transaction_model->status == Transaction::STATUS_ACTIVE) {
                    $transaction_model->status = Transaction::STATUS_NOT_ACTIVE;
                } else {
                    $transaction_model->status = Transaction::STATUS_ACTIVE;
                }

                if ($transaction_model->save()) {
                    $response['data'] = $transaction_model->getAttributes();
                    $response['status'] = true;
                }
            } else {
                throw new NotFoundHttpException('Transaction with id ' . $id_user . ' is not found.');
            }
        } else {
            throw new NotFoundHttpException('POST Variable "id" is empty.');
        }

        return $response;
    }

    /**
     * Change transaction feature status
     * Method: POST
     * Params: $id - integer (id_user)
     * @return array
     * @throws NotFoundHttpException
     */
    public function actionChangeTransactionFeatureStatus()
    {
        $response = ['status' => false];

        if ($id_user = \Yii::$app->request->post('id')) {
            if ($transaction_model = Transaction::findOne($id_user)) {
                if ($transaction_model->featured == Transaction::FEATURED_ACTIVE) {
                    $transaction_model->featured = Transaction::FEATURED_NOT_ACTIVE;
                } else {
                    $transaction_model->featured = Transaction::FEATURED_ACTIVE;
                }

                if ($transaction_model->save()) {
                    $response['data'] = $transaction_model->getAttributes();
                    $response['status'] = true;
                }
            } else {
                throw new NotFoundHttpException('Transaction with id ' . $id_user . ' is not found.');
            }
        } else {
            throw new NotFoundHttpException('POST Variable "id" is empty.');
        }

        return $response;
    }

    /**
     * Delete transactions
     * Method: POST, DELETE
     * Params: $ids - array (list of transaction ids)
     * @return array
     * @throws NotFoundHttpException
     */
    public function actionDeleteTransactions()
    {
        $response = ['status' => false];

        $ids = \Yii::$app->request->post('ids');

        if ($ids) {
            $response['rows'] = Transaction::deleteAll(['id' => $ids]);
            $response = ['status' => true];
        }

        return $response;
    }

    /**
     * Activate transaction ( Approve )
     * Method: POST
     * Params: $ids - array (list of user ids)
     * @return array
     * @throws NotFoundHttpException
     */
    public function actionActivateTransactions()
    {
        $response = ['status' => false];

        $ids = \Yii::$app->request->post('ids');

        if ($ids) {
            $response['rows'] = Transaction::updateAll(['status' => Transaction::STATUS_ACTIVE], ['id' => $ids]);
            $response['status'] = true;
        } else {
            throw new NotFoundHttpException('Variable "ids" is empty.');
        }

        return $response;
    }

    /**
     * Make transactions as featured
     * Method: POST
     * Params: $ids - array (list of user ids)
     * @return array
     * @throws NotFoundHttpException
     */
    public function actionMakeTransactionsAsFeatured()
    {
        $response = ['status' => false];

        $ids = \Yii::$app->request->post('ids');

        if ($ids) {
            $response['rows'] = Transaction::updateAll(['featured' => Transaction::FEATURED_ACTIVE], ['id' => $ids]);
            $response['status'] = true;
        } else {
            throw new NotFoundHttpException('Variable "ids" is empty.');
        }

        return $response;
    }

    /**
     *******************************************************************************************************************
     * REVIEW MODEL
     *******************************************************************************************************************
     */

    /**
     * Delete review
     * Method: POST
     * Params: $id - integer (id_review)
     * @return array
     * @throws NotFoundHttpException
     */
    public function actionDeleteReview()
    {
        $response = ['status' => false];
        $id_review = \Yii::$app->request->post('id');

        if ($id_review) {
            if ($review_model = Review::findOne($id_review)) {
                if ($review_model->delete()) {
                    $response['status'] = true;
                }
            } else {
                throw new NotFoundHttpException('Review with id ' . $id_review . ' is not found.');
            }
        } else {
            throw new NotFoundHttpException('Variable "id" is empty.');
        }

        return $response;
    }

    /**
     * Change review status
     * Method: POST
     * Params: $id - integer (id_review)
     * @return array
     * @throws NotFoundHttpException
     */
    public function actionChangeReviewStatus()
    {
        $response = ['status' => false];

        if ($id_review = \Yii::$app->request->post('id')) {
            if ($review_model = User::findOne($id_review)) {
                if ($review_model->status == Review::STATUS_NOT_ACTIVE) {
                    $review_model->status = Review::STATUS_ACTIVE;
                } else {
                    $review_model->status = Review::STATUS_NOT_ACTIVE;
                }

                if ($review_model->save()) {
                    $response['data'] = $review_model->getAttributes();
                    $response['status'] = true;
                }
            } else {
                throw new NotFoundHttpException('Review with id ' . $id_review . ' is not found.');
            }
        } else {
            throw new NotFoundHttpException('Variable "id" is empty.');
        }

        return $response;
    }

    /**
     * Delete reviews
     * Method: POST
     * Params: $ids - array (list of user ids)
     * @return array
     * @throws NotFoundHttpException
     */
    public function actionDeleteReviews()
    {
        $response = ['status' => false];

        $ids = \Yii::$app->request->post('ids');

        if ($ids) {
            $response['rows'] = Review::deleteAll(['id' => $ids]);
            $response = ['status' => true];
        }

        return $response;
    }

    /**
     * Activate reviews ( Approve )
     * Method: POST
     * Params: $ids - array (list of review ids)
     * @return array
     * @throws NotFoundHttpException
     */
    public function actionActivateReviews()
    {
        $response = ['status' => false];

        $ids = \Yii::$app->request->post('ids');

        if ($ids) {
            $response['rows'] = Review::updateAll(['status' => Review::STATUS_ACTIVE], ['id' => $ids]);
            $response['status'] = true;
        } else {
            throw new NotFoundHttpException('Variable "ids" is empty.');
        }

        return $response;
    }

    /**
     *******************************************************************************************************************
     * DISPLAY ITEMS
     *******************************************************************************************************************
     */

    /**
     * Set edit agent tab
     * Method: POST
     * Params: $number - integer (tab number)
     * @throws NotFoundHttpException
     * @return array
     */
    public function actionSetEditAgentTab()
    {
        $response = ['status' => false];
        
        if ($number = \Yii::$app->request->post('number')) {
            $response['status'] = true;
            $response['response'] = DisplayItems::setParam(DisplayItems::AGENT_EDIT_TAB_ITEM, $number);
        } else {
            throw new NotFoundHttpException('Variable "number" is empty.');
        }

        return $response;
    }

    /**
     * Get sidebar position
     * Method: GET
     * @return array
     */
    public function actionGetSidebarPosition()
    {
        return [
            'status'    => true,
            'position'  => DisplayItems::getParam(DisplayItems::SIDEBAR_ITEM),
        ];
    }

    /**
     * Change sidebar position
     * Method: GET
     * @return array
     */
    public function actionChangeSidebarPosition()
    {
        $display_items = \Yii::createObject(['class' => DisplayItems::className()]);

        return [
            'status'    => true,
            'position'  => $display_items->changeSidebarPosition(),
        ];
    }

    /**
     * @param \yii\base\Action $action
     * @return bool
     */
    public function beforeAction($action)
    {
        //if (!\Yii::$app->request->isAjax) {
            //return false;
        //}

        \Yii::$app->response->format = Response::FORMAT_JSON;

        return parent::beforeAction($action); // TODO: Change the autogenerated stub
    }
}