<?php
/**
 * Created by PhpStorm.
 * User: Vlad
 * Date: 10.04.2017
 * Time: 18:48
 */

namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

class DashboardController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index'],
                        'roles' => ['admin'],
                    ],
                ],
            ]
        ];
    }

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function beforeAction($action)
    {
        if (Yii::$app->user->isGuest) {
            $this->goHome();
            return false;
        }

        return true;
    }
}