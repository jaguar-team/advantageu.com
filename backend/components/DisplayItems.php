<?php

/**
 * Created by PhpStorm.
 * User: User
 * Date: 004 04.08.17
 * Time: 11:52
 */

namespace backend\components;

use Yii;
use yii\base\Object;

class DisplayItems extends Object
{
    CONST SIDEBAR_OPEN = 1;
    CONST SIDEBAR_CLOSE = 0;
    CONST SESSION_NAME = 'display';
    /** ITEM */
    CONST SIDEBAR_ITEM = 'sidebar';
    CONST AGENT_EDIT_TAB_ITEM = 'agent-edit-tab';

    /**
     * Change Sidebar position
     * @return bool|null|void
     */
    public function changeSidebarPosition()
    {
        $sidebar_position = self::getParam(DisplayItems::SIDEBAR_ITEM);

        if ($sidebar_position == DisplayItems::SIDEBAR_OPEN) {
            self::setParam(DisplayItems::SIDEBAR_ITEM, DisplayItems::SIDEBAR_CLOSE);
        }

        if ($sidebar_position == DisplayItems::SIDEBAR_CLOSE) {
            self::setParam(DisplayItems::SIDEBAR_ITEM, DisplayItems::SIDEBAR_OPEN);
        }

        return self::getParam(DisplayItems::SIDEBAR_ITEM);
    }

    /**
     * Close Sidebar
     * @return bool|void
     */
    public function closeSidebar()
    {
        return self::setParam(DisplayItems::SIDEBAR_ITEM, DisplayItems::SIDEBAR_CLOSE);
    }

    /**
     * Open Sidebar
     * @return bool|void
     */
    public function openSidebar()
    {
        return $this->setParam(DisplayItems::SIDEBAR_ITEM, DisplayItems::SIDEBAR_OPEN);
    }

    /**
     * Set edit agent's number tab
     * @param $number
     * @return bool|void
     */
    public function setEditAgentTab($number)
    {
        return self::setParam(DisplayItems::AGENT_EDIT_TAB_ITEM, $number);
    }

    /**
     * Get edit agent's number tab
     * @return null|integer
     */
    public function getEditAgentTab()
    {
        return $this->getParam(DisplayItems::AGENT_EDIT_TAB_ITEM);
    }

    /**
     * @param $name
     * @param $value
     * @return bool|void
     */
    public static function setParam($name, $value)
    {
        $items = self::get();
        $items[$name] = $value;

        return self::set($items);
    }

    /**
     * @param $name
     * @return null
     */
    public static function getParam($name)
    {
        $items = self::get();

        return isset($items[$name]) ? $items[$name] : NULL;
    }

    /**
     * @param array $value
     * @return bool|void
     */
    public static function set(array $value)
    {
        if (!is_array($value)) {
            return false;
        }

        return \Yii::$app->session->set(DisplayItems::SESSION_NAME, $value);
    }

    /**
     * @return array
     */
    public static function get()
    {
        return \Yii::$app->session->get(DisplayItems::SESSION_NAME, []);
    }
}