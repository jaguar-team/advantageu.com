<?php

/**
 * Created by PhpStorm.
 * User: Vlad
 * Date: 12.04.2017
 * Time: 9:34
 */
namespace backend\tests\unit\models;

use Yii;
use backend\models\RegistrationForm;
use common\fixtures\UserFixture as UserFixture;
use common\fixtures\UserRoleFixture as UserRoleFixture;

class RegistrationFormTest extends \Codeception\Test\Unit
{
    /**
     * @var \common\tests\UnitTester
     */
    protected $tester;

    public function _before()
    {
        $this->tester->haveFixtures([
            'user_role' => [
                'class' => UserRoleFixture::className(),
                'dataFile' => codecept_data_dir() . 'user_role.php'
            ],
            'user' => [
                'class' => UserFixture::className(),
                'dataFile' => codecept_data_dir() . 'user.php'
            ],
        ]);
    }

    public function testRegisterSuccess()
    {
        $model = new RegistrationForm([
            'id_role'           => 1,
            'email'             => 'andreyd37@gmail.com',
            'password_hash'          => 'qwerty',
            'password_hash_repeat'   => 'qwerty',
        ]);

        expect('should be true', $model->register())->true();
        expect('error message should be empty', $model->errors)->isEmpty();
    }

    public function testRegisterWithWrongEmail()
    {
        $model = new RegistrationForm([
            'id_role'           => 1,
            'email'             => 'wrong_email',
            'password_hash'          => 'qwerty',
            'password_hash_repeat'   => 'qwerty',
        ]);

        expect('should be false', $model->register())->false();
        expect('error message should be set', $model->errors)->hasKey('email');
    }

    public function testRegisterWithEmptyEmail()
    {
        $model = new RegistrationForm([
            'id_role'           => 1,
            'email'             => 'andreyd37@gmailcom',
            'password_hash'          => 'qwerty',
            'password_hash_repeat'   => 'qwerty',
        ]);

        expect('should be false', $model->register())->false();
        expect('error message should be set', $model->errors)->hasKey('email');
    }

    public function testRegisterWithExistsEmail()
    {
        $model = new RegistrationForm([
            'id_role'           => 1,
            'email'             => 'isset_email@isset.com',
            'password_hash'          => 'qwerty',
            'password_hash_repeat'   => 'qwerty',
        ]);

        expect('should be false', $model->register())->false();
        expect('error message should be set', $model->errors)->hasKey('email');
    }

    public function testRegisterWithEmptyPassword()
    {
        $model = new RegistrationForm([
            'id_role'                => 1,
            'email'                  => 'andreyd37@gmail.com',
            'password_hash'          => '',
            'password_hash_repeat'   => '',
        ]);
        
        expect('should be false', $model->register())->false();
        expect('error message should be set', $model->errors)->hasKey('password_hash');
    }

    public function testRegisterWithNotEqualPassword()
    {
        $model = new RegistrationForm([
            'id_role'           => 1,
            'email'             => 'andreyd37@gmail.com',
            'password_hash'          => 'qwerty',
            'password_hash_repeat'   => 'qwerty-qwerty',
        ]);

        expect('should be false', $model->register())->false();
        expect('error message should be set', $model->errors)->hasKey('password_hash');
    }

    public function testRegisterWithWrongRole()
    {
        $model = new RegistrationForm([
            'id_role'           => 9999999,
            'email'             => 'andreyd37@gmail.com',
            'password_hash'          => 'qwerty',
            'password_hash_repeat'   => 'qwerty-qwerty',
        ]);

        expect('should be false', $model->register())->false();
        expect('error message should be set', $model->errors)->hasKey('id_role');
    }

    public function testRegisterWithEmptyRole()
    {
        $model = new RegistrationForm([
            'email'             => 'andreyd37@gmail.com',
            'password_hash'          => 'qwerty',
            'password_hash_repeat'   => 'qwerty-qwerty',
        ]);

        expect('should be false', $model->register())->false();
        expect('error message should be set', $model->errors)->hasKey('id_role');
    }

    public function testRegisterWithWrongStatus()
    {
        $model = new RegistrationForm([
            'id_role'           => 1,
            'email'             => 'andreyd37@gmail.com',
            'password_hash'          => 'qwerty',
            'password_hash_repeat'   => 'qwerty',
            'status'            => 3,
        ]);

        expect('should be false', $model->register())->false();
        expect('error message should be set', $model->errors)->hasKey('status');
    }
}