(function ( $ ) {

	let searchBox = function (element) {
	    var self = this;
	    self.$container = $(element);
	    self.input = self.$container.find('input');
		self.dropdown = $('<ul class="areas-dropdown"></ul>');
		self.$container.append(self.dropdown);		
		self.init();
		self.addEventListener(self.input);
	};

	$.fn.searchTips = function () {
	    this.each(function () {
	        var self = $(this), 
	        	data;	        
	        data = new searchBox(this);	        
	    });
	    return this;
	};

	searchBox.prototype = {
	    constructor: searchBox,
		KEY: {
	        TAB: 9,
	        ENTER: 13,
	        ESC: 27,
	        SPACE: 32,
	        LEFT: 37,
	        UP: 38,
	        RIGHT: 39,
	        DOWN: 40,
	        SHIFT: 16,
	        CTRL: 17,
	        ALT: 18,
	        PAGE_UP: 33,
	        PAGE_DOWN: 34,
	        HOME: 36,
	        END: 35,
	        BACKSPACE: 8,
	        DELETE: 46,
	    },

	    init: function(){
	    	let self = this;
	    	if(typeof(google) != 'undefined'){
	    		self.$container.removeClass('loading');

	    	}
	    	else{
	    		$.ajax({
					url: 'https://maps.googleapis.com/maps/api/js?key=AIzaSyDbgI-qDa-U_JWtQbuqj7kOdBK4vYBCJQ8&libraries=places&language=en',
					method: 'GET',
					crossDomain: true,
					success: self.init()
				});
	    	}
	    },

	    addEventListener($element){
	    	let self = this;
	    	$element.on({
				'keyup paste': function(e){	
					switch (e.which) {
			            case self.KEY.ENTER:
			                e.preventDefault();
			                e.stopPropagation();
			                return;
			            case self.KEY.DOWN:
			            	e.preventDefault();
			            	return;
			            case self.KEY.UP:
			            	e.preventDefault();
			            	return;	
			        }
					self.autoComplete($(e.target).val());
				},
				'keydown': function(e){
					switch (e.which) {
			            case self.KEY.ENTER:
			                e.preventDefault();
			                e.stopPropagation();
			                self.selectHighlighted();
			                return;
			            case self.KEY.DOWN:
			            	e.preventDefault();
			            	self.moveHighLighted();
			            	return;
			            case self.KEY.UP:
			            	e.preventDefault();
			            	self.moveHighLighted(true);
			            	return;
			            case self.KEY.ESC:
			            	self.dropdown.html('').hide(200);
			            	return;
			        }
				}
			})
	    },

		highlight: function(el){
			this.dropdown.find('li').removeClass('select2-highlighted');
			el.addClass('select2-highlighted');
			this.input.val(el.text());
		},

		selectHighlighted: function(){
			this.dropdown.find('.select2-highlighted').trigger('click');
			this.dropdown.hide();
		},

		moveHighLighted: function(direction){
			let self = this,
				current = this.dropdown.find('.select2-highlighted');

			if( direction ) {
				if ( current.prev().length )
					self.highlight(current.prev());
				else return;
			}
			else {
				if ( current.next().length )
					self.highlight(current.next());
				else return;
			}
		},

		selectValue: function(place_id){
			let self = this,		
				area = {},
				geocoder = new google.maps.Geocoder;

			self.dropdown.hide(200);
			geocoder.geocode({'placeId': place_id}, function(results, status) {

				if (status === 'OK') {
		            if (results[0]) {

		            	area = self.parseArea(results[0]);

		            	self.setData(area);

						//self.$container.closest('form').submit();

					} else {
		          		window.alert('No results found');
		            }
		      	} else {
		        	window.alert('Geocoder failed due to: ' + status);
		      	}
			});	
		},

		autoComplete: function(string){
			let autoComplete = new google.maps.places.AutocompleteService(),
				self = this;
				$(window).on('click', function(e){
					self.dropdown.hide();	      			
				});
			if(string){
				let restrictions = {
					componentRestrictions: {'country':'us'}, 
					input: string
				};
				( self.input.data('type') ? restrictions['types'] = [self.input.data('type')] : false );
				autoComplete.getPlacePredictions( restrictions, 
			  		function(result, status){
			  			let listItems = [];
			      		for (let i in result){
			      			listItems.push(
			      				$("<li></li>").text(result[i].description).on({
				      				click: function(e){
					      				self.selectValue(result[i].place_id);
				      				},
				      				mouseover: function(e){
				      					self.highlight($(e.target));
				      				}
				      			}).addClass( ( i == 0 ) ? 'select2-highlighted' : '')
			      			); 
			      		}
			      		self.dropdown.html(listItems).css({
							'top': self.input.position().top + self.input.outerHeight(),
							'left': self.input.position().left,
							'width': self.input.outerWidth()
						}).show(200);
			      		self.dropdown.on('click', function(e){
			      			e.stopPropagation();
			      		});
			      	}
			    );
			} else {
				self.dropdown.html('').hide(200);;
			}  	
		},

		parseArea: function(place){
			
			let area = {};
			for(i in place.address_components){
				
				if ( place.address_components[i].types[0] == 'administrative_area_level_1' ) {
					area['short_name'] = place.address_components[i].short_name;
					area['state'] = place.address_components[i].long_name;
				}
				if ( place.address_components[i].types[0] == 'administrative_area_level_2' )
					area['region'] = place.address_components[i].long_name;
				if ( place.address_components[i].types[0] == 'locality' || place.address_components[i].types[0] == 'sublocality_level_1' || place.address_components[i].types[0] == 'sublocality' )
					area['city'] = place.address_components[i].long_name;
				if ( place.address_components[i].types[0] == 'route' )
					area['route'] = place.address_components[i].long_name;
				if ( place.address_components[i].types[0] == 'street_number' )
					area['street_number'] = place.address_components[i].long_name;
				if ( place.address_components[i].types[0] == 'postal_code' )
					area['zipcode'] = place.address_components[i].long_name;
			}

			if ( place.place_id )
				area['place_id'] = place.place_id;

			if (area['route']){
				area['address'] = area['route'];
				if (area['street_number']){
					area['address'] = area['street_number'] + ', ' + area['route'];
				}
			}

			if (!area.region)
				area.region = area.city;

			area.lat = place['geometry']['location'].lat();
			area.lng = place['geometry']['location'].lng();
			
			return area;
		},

		setData: function(data){

			let self = this;

			( self.input.data('city-id') && data.city ) 			? $('#' + self.input.data('city-id')).val(data.city) 				: false;
		    ( self.input.data('state-id') && data.state ) 			? $('#' + self.input.data('state-id')).val(data.state) 				: false;
		    ( self.input.data('region-id') && data.region ) 		? $('#' + self.input.data('region-id')).val(data.region) 			: false;
		    ( self.input.data('short-id') && data.short_name ) 		? $('#' + self.input.data('short-id')).val(data.short_name) 		: false;
		    ( self.input.data('address-id') && data.address ) 		? $('#' + self.input.data('address-id')).val(data.address) 			: false;
		    ( self.input.data('zipcode-id') && data.zipcode ) 		? $('#' + self.input.data('zipcode-id')).val(data.zipcode) 			: false;
		    ( self.input.data('photo-id') && data.photo ) 			? $('#' + self.input.data('photo-id')).val(data.photo) 				: false;
		    ( self.input.data('lat-id') && data.lat ) 				? $('#' + self.input.data('lat-id')).val(data.lat) 					: false;
		    ( self.input.data('lng-id') && data.lng ) 				? $('#' + self.input.data('lng-id')).val(data.lng) 					: false;
		    ( self.input.data('place_id-id') && data.place_id ) 	? $('#' + self.input.data('place_id-id')).val(data.place_id) 		: false;

		    if( self.input.data('zipcode-id') && !data.zipcode ){
		    	let geocoder = new google.maps.Geocoder;    
			    
			    let latlng = new google.maps.LatLng(data.lat, data.lng);
				geocoder.geocode({'latLng': latlng}, function(results) {
					
					for(let i = 0; i < results.length; i++){
			      		for(let j = 0; j < results[i].address_components.length; j++){
			          		for(let k = 0; k < results[i].address_components[j].types.length; k++){
			              		if(results[i].address_components[j].types[k] == "postal_code"){

			                  		data.zipcode = results[i].address_components[j].short_name;	 
			                  		( self.input.data('zipcode-id')  && data.zipcode ) 		? $('#' + self.input.data('zipcode-id')).val(data.zipcode) 		: false;
			                  		( self.input.data('place_id-id') && data.place_id ) 	? $('#' + self.input.data('place_id-id')).val(data.place_id) 	: false;		                  		
			                  	}
			                }
			            }
			        }

			    });			    
			}

			if( self.input.data('photo-id') && !data.photo ){
				if(data.city && data.short_name){
					if(data.route){	
						$('#' + self.input.data('photo-id')).val('https://maps.googleapis.com/maps/api/streetview?size=260x190&location=' + data.route + ',' + data.city + ',' + data.short_name + '&fov=100&key=AIzaSyDbgI-qDa-U_JWtQbuqj7kOdBK4vYBCJQ8');
					} 
					else
						$('#' + self.input.data('photo-id')).val('https://maps.googleapis.com/maps/api/streetview?size=260x190&location=' + data.city + ',' + data.short_name + '&fov=100&key=AIzaSyDbgI-qDa-U_JWtQbuqj7kOdBK4vYBCJQ8');
				}				
			}

			return;
		}
	}
}( jQuery ));