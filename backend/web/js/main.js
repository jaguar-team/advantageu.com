let _URL = window.URL || window.webkitURL,
	x_input = $("#user-avatar-offset-x"),
    y_input = $("#user-avatar-offset-y"),
    w_input = $("#user-avatar-width"),
    h_input = $("#user-avatar-height");

function GetURLParameter(sParam){
	var sPageURL = window.location.search.substring(1);
	var sURLVariables = sPageURL.split('&');
	for (var i = 0; i < sURLVariables.length; i++){
		var sParameterName = sURLVariables[i].split('=');
		if (sParameterName[0] == sParam)
			return sParameterName[1];
	}
}

function multipleAgentAjax(action, grid_el, callback){
	let data = {
		ids: grid_el.yiiGridView('getSelectedRows')
	};
	data[yii.getCsrfParam()] = yii.getCsrfToken();
	$.ajax({
		url: UrlManager.createUrl('ajax/' + action),
		data: data,
		method: 'POST',
		success: callback
	});
}

function changeRole(event, action, user_id){
	event.preventDefault();
	$el = $(event.target);
	let data = {
		id: user_id,
		role: $el.val()
	};
	data[yii.getCsrfParam()] = yii.getCsrfToken();
	$.ajax({
		url: UrlManager.createUrl('ajax/' + action),
		data: data,
		method: 'POST',
		success: function(resp){
			let pjax = $el.closest('[data-pjax-container]');
			$.pjax.reload({container: '#'+pjax.attr('id')});
		}
	});
}

function changeStatus(event){
	event.preventDefault();
	let $el = $(event.target),
		data = {
			id: $el.val(),
		};
	data[yii.getCsrfParam()] = yii.getCsrfToken();
	$.ajax({
		url: UrlManager.createUrl('ajax/' + $el.data('action')),
		data: data,
		method: 'POST',
		success: function(resp){
			let pjax = $el.closest('[data-pjax-container]');
			$.pjax.reload({container: '#'+pjax.attr('id')});
		}
	});
}

function ajaxDelete(event, id, entity){

	//event.preventDefault();
	if(!window.confirm("Are you sure you want to delete this " + entity + "?"))
		return;
	let $el = $(event.target),
		data = {
			id: id,
		};
	data[yii.getCsrfParam()] = yii.getCsrfToken();
	$.ajax({

		url: UrlManager.createUrl('ajax/delete-' + entity),
		data: data,
		method: 'POST',
		success: function(resp){
			let pjax = $el.closest('[data-pjax-container]');
			$.pjax.reload({container: '#'+pjax.attr('id')});
		}
	});
}

function submitForm(event, confirm=false){
	event.preventDefault();
	if(confirm){
		if(window.confirm('Do you really want to change it?')){
			$(event.target).closest('form').trigger('submit');
		}
		else{
			$(event.target).closest('form').trigger('reset');
		}
	}
	else
		$(event.target).closest('form').trigger('submit');
}

function dropArea(){
	let example_dropzone = $("#advancedDropzone").dropzone({
		url: UrlManager.createUrl('/'),
		acceptedFiles: 'image/*',
		maxFiles: 1,
		autoProcessQueue: false,
		el: 'user-avatar-file',
		//modelName: 'UserProfile[photo]',

		// Events
		init: function(){
			$('#crop').on({
				'shown.bs.modal': function(e){
					$image = $(".img-container img");
					let preview_size = [250, 250],
					aspect_ratio = preview_size[0] / preview_size[1],
				    $x = $("#img-1-x"),
				    $y = $("#img-1-y"),
				    $w = $("#img-1-w"),
				    $h = $("#img-1-h");

				    // Plugin Initialization
				    $image.cropper({
				        aspectRatio: aspect_ratio,
				        preview: '#img-preview',
				        restore: true,
				        done: function(data)
				        {
				            $x.text( data.x );
				            $y.text( data.y );
				            $w.text( data.width );
				            $h.text( data.height );
				            x_input.val( data.x );
				            y_input.val( data.y );
				            w_input.val( data.width );
				            h_input.val( data.height );
				        }
				    });

				    // Preview Image Setup (based on defined crop width and height)
				    $("#img-preview").css({
				        width: preview_size[0],
				        height: preview_size[1]
				    });

				    $(document).on('click', '#crop-img', function(e){
				    	e.preventDefault();
				    	let data = $image.cropper('getData');
				    	x_input.val( data.x );
			            y_input.val( data.y );
			            w_input.val( data.width );
			            h_input.val( data.height );
			            $('#crop').modal('hide');
				    })
				},
				'hidden.bs.modal': function(){
					($('#photo-update')[0]) ? $('#photo-update').yiiActiveForm('submitForm') : false;
				}
			})
		},

		addedfile: function(file)
		{
			$('#crop .img-container').html('<img src="' + _URL.createObjectURL(file) + '" />');
			$('#crop').modal('show');
		},

		sending: function(file, xhr, formData){
			formData.append(yii.getCsrfParam(), yii.getCsrfToken());
		},

		success: function(file, resp)
		{
			console.log(resp);
		},

		error: function(file, resp)
		{
			console.log(resp);
		}
	});

}

function setRole(event, input_id, list_class, type){
	let value  = $(event.target).val(),
		data = {
			id_role: value
		};
	$('#' + input_id).val($(event.target).val());

	data[yii.getCsrfParam()] = yii.getCsrfToken();

	$.ajax({
		type: "POST",
		url: UrlManager.createUrl('setting/get-' + (type == 'advantage' ? 'advantages' : type) + '-by-id-role'),
		data: data,
		success: function(res){
			let list = [];
			if(type == 'section'){
				for (let i in res.data){
					list.push('<li class="uk-nestable-list-item">' +
									'<div class="uk-nestable-item">' +
										'<div class="uk-nestable-handle" style="background-color: ' + (res.data[i].color ? res.data[i].color : '') + '"></div>' +
										'<div data-nestable-action="toggle"></div>' +
										'<div class="list-label">' + res.data[i].name + '</div>' +
									'</div>' +
									'<a href="' + UrlManager.createUrl('setting/delete-' + type, { id: i}) + '" data-pjax="true" data-method="POST" class="select2-search-choice-close" tabindex="-1"></a>' +
								'</li>');
				}
			} else {
				for(let i in res.data){
					list.push('<li>' +
							'<div>' + res.data[i] + '</div>' +
							'<a href="' + UrlManager.createUrl('setting/delete-' + type, { id: i}) + '" data-pjax="true" data-method="POST" class="select2-search-choice-close" tabindex="-1"></a>' +
						'</li>');

				}
			}
			$('.' + list_class).html(list.length ? list : '<li><div>No options yet</div></li>');
		}
	});
}

function increase(event, input_id){
	event.preventDefault();
	$('#' + input_id).focus().val( function(i, oldval) {
		return ++oldval;
	});
}
function decrease(event, input_id){
	event.preventDefault();
	$('#' + input_id).focus().val( function(i, oldval) {
		return (oldval != 0) ? --oldval : 0;
	});
}

function validateArea(event, attribute, messages){
	let area_field = $(event.target).find('.area_field'),
		invalid_attributes = [];
	for (let i in messages){
		if( $(messages[i].input).parents('.area_field').length )
			invalid_attributes.push(messages[i].name);
	}
	if(invalid_attributes.length){
		if(area_field.find('.help-block.help-block-error').length){
			area_field.removeClass('has-success').find('.help-block.help-block-error').html('<p class="help-block help-block-error">'+invalid_attributes.join(', ')+(invalid_attributes.length > 1 ? ' are': ' is')+' invalid.</p>');
		} else{
			area_field.removeClass('has-success').addClass('has-error').append('<p class="help-block help-block-error">'+invalid_attributes.join(', ')+(invalid_attributes.length > 1 ? ' are': ' is')+' invalid.</p>');
		}
	}
}

$(document).on({
	'pjax:error': function(event, textStatus, error, options){
		//event.preventDefault();
		console.log(error);

		if($(event.target).hasClass('loading'))
			$(event.target).removeClass('loading');
		console.log('Error occured trying to save data. Please try again');
	},
	'pjax:send': function(event){
		if(!$(event.target).hasClass('loading'))
			$(event.target).addClass('loading')
	},
	'pjax:complete': function(event){
		cbr_replace();

		if($.isFunction($.fn.dropzone))
			try{
				dropArea()
			}
			catch(error){
				//console.log(error)
			}
		if($.isFunction($.fn.searchTips))
			$('.search_areas').searchTips()

		if($.isFunction($.fn.colorpicker))
			$('.colorpicker').colorpicker()

		if($.isFunction($.fn.ckeditor))
			$('.ckeditor').ckeditor()

		if($.isFunction($.fn.select2))
			$(event.target).find(".select2-offscreen").select2({
				minimumResultsForSearch: 10,
			})
			.on("select2-open", function() {
				if($.isFunction($.fn.perfectScrollbar))
					$(this).data("select2").results.addClass("overflow-hidden").perfectScrollbar();
			});

		if($(event.target).hasClass('loading'))
			$(event.target).removeClass('loading')
	},
	'pjax:timeout': function(event){
		event.preventDefault();
		console.log(event);
	},
	'pjax:end': function (event) {
		console.log(event.target);
		$('.area_validation input:not([name="' + yii.getCsrfParam() + '"])').val('');
	},
	'click.yiiGridView': function(e){
		$(e.target).closest('.grid-view').find('.cbr').each(function(){
			$(this).trigger('change')
		});
	}
});


jQuery(document).ready(function($){
	dropArea();
	$('.search_areas').searchTips();

	$('[data-toggle="tooltip"]').tooltip()

	$(document).on('afterValidate', 'form.area_validation', validateArea);

	$(document).on('shown.bs.tab', '.user-update', function (event) {
		let data = {
			number: event.target.hash
		};
		data[yii.getCsrfParam()] = yii.getCsrfToken();
		$.ajax({
			method: "POST",
			url: UrlManager.createUrl('ajax/set-edit-agent-tab'),
			data: data,
			success: function (resp) {}

		})
	});

	$(document).on('submit.yiiGridView', '.grid-view', function (event) {
		let data = $(event.target).serializeArray();
		for(let i in data){
			if(data[i].name == '_pjax')
				continue;

			//console.log(data[i].name+": " + GetURLParameter(encodeURIComponent(data[i].name)));
		}
	});

	$(document).on('click', '#photo-upload', function(e){
		e.preventDefault();
		let $this = $(this),
			input = $('#' + $this.data('input')),
			image = $('#' + $this.data('img'));

		input.trigger('click');
		input.on('change', function(){
			let file, img;
		    if ((file = this.files[0])) {
		        //image.attr('src', _URL.createObjectURL(file));
		        $('#logo_preview').html('<img src="' + _URL.createObjectURL(file) + '" />');
		    }
		});
	});

	$(document).on('change', 'input.cbr', function(){
	    $('.selected-items span').html($(this).closest('.grid-view').yiiGridView('getSelectedRows').length);
	});

	$(document).on('click', '.selected-actions a', function(e){
		e.preventDefault();
		let $this = $(this);
		multipleAgentAjax($this.data('action'), $('.grid-view'), function(resp){
			//console.log(resp);
			let pjax = $this.closest('[data-pjax-container]');
			$.pjax.reload({container: '#'+pjax.attr('id')});
		});
	});

	$(".select2-offscreen").select2({
        minimumResultsForSearch: 10,
    })
    .on("select2-open", function() {
	    $(this).data("select2").results.addClass("overflow-hidden").perfectScrollbar();
	});

	$(".select2-remote").select2({
		minimumInputLength: 1,
		quietMillis: 50,
		minimumResultsForSearch: 10,
		ajax: {
			url: UrlManager.createUrl('ajax/get-users-by-matches'),
			dataType: 'json',
			data: function (term) {
				var request = {
					string: term,
					role: $(this).data('type')
				};
				request[yii.getCsrfParam()] = yii.getCsrfToken();
				return request;
			},
			results: function (resp) {
				return {
						results: $.map(resp.data, function (person) {
							if(!person)
								return;

							return {
								id: person.id,
								role: (person.role ? person.role.name : ''),
								email: person.email,
								name: (person.profile ? person.profile.first_name + " " + person.profile.last_name : '')
							}
					})
				}
			},
			cache: true
		},
		id: function(person) {
			if(!person)
				return;
			return person.id;
		},
		initSelection: function (element, callback) {
			var id = $(element).val();
			if (id !== "") {
				callback({
					id: id,
					name: $(element).data('chosenName')
				});
			}
		},
		formatResult: function (person) {
			if(!person)
				return;
			var markup =
				'<div class="select2-user-result">' +
				'<span>' + person.email + '</span>' +
				'<span style="margin-left: 20px;">' + person.name + '</span>' +
				'<span>(' + person.role + ')</span>' +
				'</div>';
			return markup;
		},
		formatSelection: function (person) {
			if(!person)
				return;
			return person.name;
		},
	})
	.on("select2-open", function() {
		$(this).data("select2").results.addClass("overflow-hidden").perfectScrollbar();
	});

    $('#registrationform-id_role').select2({
    	placeholder: "Select user role..."
    });

    $('.member-form-add-header button[type="reset"]').on('click', function(){
    	$(".select2-offscreen").val(null).trigger('change');
    	$('#avatar-preview').replaceWith('Drop Photo Here');
    });

	$(document).on('click', '#avatar-preview', function(e){
		$('#advancedDropzone').trigger(e.type);
	});

	$(document).on('change', 'input.live', function(){
		$($(this).data('live')).text($(this).val())
	});

	$(document).on('click', 'select2-search-choice-close', function(){
		$(this).remove();
	});
});